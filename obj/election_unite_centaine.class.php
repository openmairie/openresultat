<?php
//$Id$
//gen openMairie le 10/08/2020 10:06

require_once "../obj/election_unite.class.php";

class election_unite_centaine extends election_unite {
    // L'ajout de cette classe sert à éviter que le nom du sous-formulaire
    // et celui de l'objet soient identique. Ainsi dans le lien de retour
    // les deux éléments ne sont plus confondus et il est donc possible
    // d'accéder au formulaire de l'unité lié à la centaine pas à l'élection
    protected $_absolute_class_name = "election_unite_centaine";

    /**
     * SETTER FORM - setType.
     *
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        // L'unité et l'élection ne sont pas modifiable
        $form->setType('emargement', "hidden");
        $form->setType('procuration', "hidden");
        $form->setType('inscrit', "hiddenstatic");
    }

    /**
     * SETTER FORM - set_form_default_values
     *
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     * @param integer $validation Marqueur de validation du formulaire.
     *
     * @return void
     */
    function set_form_default_values(&$form, $maj, $validation) {
        parent::set_form_default_values($form, $maj, $validation);
        // Remplis le nombre d'inscrit en récupérant celui de l'élection de référence
        if ($validation==0 && $maj==1) {
            $centaine = $this->f->get_element_by_id('election', $this->getVal('election'));
            $uniteRef = $this->get_election_unite(
                $centaine->getVal('election_reference'),
                $this->getVal('unite')
            );
            $form->setVal('inscrit', $uniteRef->getVal('inscrit'), $validation);
        }
    }
}
