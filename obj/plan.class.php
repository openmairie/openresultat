<?php
//$Id$
//gen openMairie le 16/12/2020 17:02

require_once "../gen/obj/plan.class.php";

class plan extends plan_gen {

    /**
     * SETTER FORM - setLib
     *
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        $form->setLib('image_plan', 'image plan');
        $form->setLib('largeur_icone', 'largeur des icones');
    }

    /**
     * Récupére chaque lien entre le plan et les unités
     *
     * @return array liste d'instance des plan_unite lié au plan
     */
    public function get_liste_plan_unite() {
        // Requete permettant de récupérer les id de tous les plans de l'élection
        $plansUnite = $this->f->simple_query('plan_unite', 'plan_unite', 'plan', $this->getVal('plan'));
        // Stockage dans un tableau des instances de chaque lien plan - election à partir
        // des id récupérés
        $listePlansUnite = array();
        foreach ($plansUnite as $plansUniteId) {
            $planUnite = $this->f->get_element_by_id('plan_unite', $plansUniteId);
            $listePlansUnite[] = $planUnite;
        }
        return $listePlansUnite;
    }
}
