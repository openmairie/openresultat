<?php
//$Id$ 
//gen openMairie le 17/09/2020 14:29

require_once "../gen/obj/type_election.class.php";

class type_election extends type_election_gen {

    /**
     * SETTER FORM - setLib
     * 
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     * 
     * @return void
     */
    function setLib(&$form, $maj) {
        //libelle des champs pour éviter les termes techniques
        parent::setLib($form, $maj);
        $form->setLib('type_election', 'id');
    }
}
