<?php
//$Id$ 
//gen openMairie le 06/01/2021 11:14

require_once "../gen/obj/web.class.php";

class web extends web_gen {

    /**
     * Definition des actions disponibles sur la classe
     *
     * @return void
     */
    function init_class_actions() {
        parent::init_class_actions();
        // ACTION - 006 - activer
        $this->class_actions[6] = array(
            "identifier" => "activer",
            "portlet" => array(
                "type" => "action-direct-with-confirmation",
                "libelle" => _("activer"),
                "order" => 40,
                "class" => "lu-16",
            ),
            "view" => "formulaire",
            "method" => "activer",
            "button" => "activer",
            "permission_suffix" => "activer",
            "condition" => "is_not_actif"
        );

        // ACTION - 007 - desactiver
        $this->class_actions[7] = array(
            "identifier" => "desactiver",
            "portlet" => array(
                "type" => "action-direct-with-confirmation",
                "libelle" => _("désactiver"),
                "order" => 41,
                "class" => "lu-16",
            ),
            "view" => "formulaire",
            "method" => "desactiver",
            "button" => "desactiver",
            "permission_suffix" => "activer",
            "condition" => "is_actif"
        );
    }

    /**
     * SETTER FORM - setType.
     *
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        // Pour les actions appelée en POST en Ajax, il est nécessaire de
        // qualifier le type de chaque champs (Si le champ n'est pas défini un
        // PHP Notice:  Undefined index dans core/om_formulaire.class.php est
        // levé). On sélectionne donc les actions de portlet de type
        // action-direct ou assimilé et les actions spécifiques avec le même
        // comportement.
        if ($this->get_action_param($maj, "portlet_type") == "action-direct"
            || $this->get_action_param($maj, "portlet_type") == "action-direct-with-confirmation") {
            //
            foreach ($this->champs as $value) {
                $form->setType($value, 'hidden');
            }
            $form->setType($this->clePrimaire, "hiddenstatic");
        }

        // Il n'est possible d'activer un modèle web qu'en utilisant l'action d'activation
        if ($maj != 3) {
            $form->setType('actif', 'hidden');
        }
    }

    /**
     * SETTER FORM - setLib
     *
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        //libelle des champs
        parent::setLib($form, $maj);
        $form->setLib('url_collectivite', 'url de la collectivite');
        $form->setLib('libelle_url', 'nom du lien');
        $form->setLib('feuille_style', 'feuille de style');
        $form->setLib('display_simulation', 'afficher les centaines');
        $form->setLib('jscript_stats', 'script');
    }

    /**
     * SETTER FORM - set_form_default_values
     *
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     * @param integer $validation Marqueur de validation du formulaire.
     *
     * @return void
     */
    function set_form_default_values(&$form, $maj, $validation) {
        if ($maj == 0 && $validation == 0) {
            $form->setVal('entete', 'Résultats électoraux');
        }
    }

    /**
     * TRIGGER - triggermodifier.
     *
     * @param string $id
     * @param null &$dnu1 @deprecated  Ne pas utiliser.
     * @param array $val Tableau des valeurs brutes.
     * @param null $dnu2 @deprecated  Ne pas utiliser.
     *
     * @return boolean
     */
    function triggermodifier($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Mise à jour du paramétrage dans le répertoire web si il s'agit du modèle actif
        if ($this->f->boolean_string_to_boolean($this->getVal('actif'))) {
            $succes = $this->envoi_parametrage($val, true);
            return $succes;
        }
    }

    /**
     * Récupère les informations issues du formulaire, les mets en forme
     * (format fichier .ini) et écris un fichier .ini contenant ces informations
     * dans le repertoire web de l'élection.
     *
     * @param array $electionId identifiant de l'élection
     * @return boolean indique si le traitement a réussi ou pas
     */
    protected function envoi_parametrage($val = array(), $trigger = false) {
        // Récupération du paramétrage
        // Si la méthode est appellé via un trigger les valeurs sont issu du formulaire
        // Si la méthode est appellé via une action les valeurs sont issu de la BD
        $parametres = array();
        foreach ($this->champs as $champ) {
            $parametres[$champ] = $this->getVal($champ);
            if ($trigger === true) {
                $parametres[$champ] = $val[$champ];
            }
        }
        
        // Mise en forme des valeurs issues du formulaire, sous cette forme :
        // [parametrage]
        // champs1="val_champs1"
        // ...
        // champsN="val_champsN"
        // et creation de la feuille de style du portail web
        $parametrage = array_map(function ($key, $value) {
            return $key.'="'.$value.'"';
        }, array_keys($parametres), $parametres);

        $contenu = sprintf(
            "[parametrage]\n%s",
            implode("\n", $parametrage)
        );

        // Ecriture du fichier contenant le paramétrage de la page web
        $envoiParam = $this->f->write_file("../web/dyn/param.ini", $contenu);
        if (! $envoiParam) {
            $this->addToMessage('Erreur lors de l\'envoi du paramétrage au portail web');
        }

        // Envoi du logo. Si le dossier logo de l'élection existe, c'est qu'un
        // logo existe déjà. IL faut donc le supprimer et recreer un nouveau dossier
        // avec le logo choisi en paramétre
        $pathLogo = '../web/img/';
        $envoiLogo = true;
        // Suppression du logo si un autre logo existe déjà
        array_map('unlink', glob($pathLogo.'/logo.*'));
        if (! empty($parametres['logo'])) {
            $logo = $this->f->get_element_by_id('om_logo', $parametres['logo']);
            $envoiLogo = $this->f->copier_fichier_du_filestorage(
                $logo->getVal('fichier'),
                $pathLogo,
                'logo'
            );
            if (! $envoiLogo) {
                $this->addToMessage('Erreur lors de l\'envoi du logo au portail web');
            }
        }

        return $envoiParam && $envoiLogo;
    }

    /**
     * Supprime le répertoire de paramétrage ainsi que le logo.
     *
     * @return boolean indique si le traitement a réussi ou pas
     */
    protected function supprime_parametrage() {
        // Suppression du fichier contenant le paramétrage de la page web
        $supprimeParametrage = true;
        if (file_exists("../web/dyn/param.ini")) {
            $supprimeParametrage = unlink("../web/dyn/param.ini");
        }
        if (! $supprimeParametrage) {
            $this->addToMessage('Erreur lors de la suppression du paramétrage au portail web');
        }

        // Suppression du logo en supprimant le dossier contenant le logo.
        $supprimeLogo = true;
        foreach (glob("../web/img/logo.*") as $logo) {
            $supprimeLogo = $supprimeLogo && unlink($logo);
        }
        if (! $supprimeLogo) {
            $this->addToMessage('Erreur lors de la suppression du paramétrage au portail web');
        }
        
        return $supprimeParametrage && $supprimeLogo;
    }

    /**
     * Indique si l'animation n'est pas active.
     * @return boolean -> true : pas actif, false : actif
     */
    protected function is_not_actif() {
        return ! $this->f->boolean_string_to_boolean($this->getVal('actif'));
    }

    /**
     * Indique si l'animation est active ou pas.
     * @return boolean -> true : actif, false : pas actif
     */
    protected function is_actif() {
        return $this->f->boolean_string_to_boolean($this->getVal('actif'));
    }

    /**
     * activer.
     * Met à 'true' l'attribut actif du modèle web dans la base de
     * donnée et à 'false' celui de tous les autres. Envoi également au
     * repertoire web le paramétrage du modèle.
     *
     * @return boolean indique si le traitement a fonctionné
     */
    protected function activer() {
        $this->begin_treatment(__METHOD__);

        // Tous les modèles web sont désactivés
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.'web',
            array(
                'actif' => false,
            ),
            DB_AUTOQUERY_UPDATE,
            ''
        );
        if ($this->f->isDatabaseError($res, true)) {
            return $this->end_treatment(__METHOD__, false);
        }

        // Le modèle web choisi est activé
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.'web',
            array(
                'actif' => true,
            ),
            DB_AUTOQUERY_UPDATE,
            'web = '.$this->f->db->escapeSimple($this->getVal('web'))
        );
        if ($this->f->isDatabaseError($res, true)) {
            return $this->end_treatment(__METHOD__, false);
        }
        $this->addToMessage('Le modèle a été correctement activé.');

        // Le paramétrage et le logo sont transmis au répertoire web
        
        if (! $this->envoi_parametrage()) {
            return $this->end_treatment(__METHOD__, false);
        }
        $this->addToMessage('Le paramétrage a été envoyé au portail web.');

        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * desactiver.
     * Met à 'false' l'attribut actif du modèle web dans la base de
     * donnée. Supprime également du repertoire web le paramétrage du modèle
     * et le logo.
     *
     * @return boolean indique si le traitement a fonctionné
     */
    protected function desactiver() {
        $this->begin_treatment(__METHOD__);

        // Le modèle web est désactivé
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.'web',
            array(
                'actif' => false,
            ),
            DB_AUTOQUERY_UPDATE,
            'web = '.$this->getVal('web')
        );
        if ($this->f->isDatabaseError($res, true)) {
            return $this->end_treatment(__METHOD__, false);
        }

        if (! $this->supprime_parametrage()) {
            return $this->end_treatment(__METHOD__, false);
        }
        $this->addToMessage('Le paramétrage a été supprimé du portail web.');

        return $this->end_treatment(__METHOD__, true);
    }
}
