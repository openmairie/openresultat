<?php
//$Id$
//gen openMairie le 04/06/2019 19:52

require_once "../gen/obj/tranche.class.php";

class tranche extends tranche_gen {

    /**
     * SETTER FORM - setOnchange.
     * 
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     * 
     * @return void
     */
    function setOnchange(&$form, $maj) {
        parent::setOnchange($form, $maj);
        // format du type : 00:00:00
        $form->setOnchange('libelle', 'ftime(this)');
    }

    /**
     * Vérifie la validité des valeurs en mode CREATE & UPDATE.
     *
     * - Vérifie que le libellé a bien le format d'un date
     * 
     * @param array $val Tableau des valeurs brutes.
     * @param null &$dnu1 @deprecated  Ne pas utiliser.
     * @param null $dnu2 @deprecated  Ne pas utiliser.
     *
     * @return void
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        parent::verifier($val);
        if (! is_string($val['libelle']) ||
            (preg_match('{^[0-9]{1,2}:[0-9]{2}\z}', $val['libelle']) == false &&
            preg_match('{^[0-9]{1,2}:[0-9]{2}:[0-9]{2}\z}', $val['libelle']) == false)
        ) {
            $this->addToMessage('Le format du libellé est incorrect');
            $this->correct = false;
        }
    }
}
