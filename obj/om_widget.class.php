<?php
/**
 * Ce script contient la définition de la classe *om_widget*.
 *
 * @package openresultat
 * @version SVN : $Id$
 */

require_once PATH_OPENMAIRIE."obj/om_widget.class.php";

/**
 * Définition de la classe *om_widget*.
 */
class om_widget extends om_widget_core {
    public function view_widget__election($content = null) {
        $elections = array();
        $query = sprintf(
            'SELECT to_char(election.date ,\'DD/MM/YYYY\') as election_date, * FROM %1$selection WHERE is_centaine IS NOT TRUE AND workflow NOT LIKE \'Archivage\'',
            DB_PREFIXE
        );
        $res = $this->f->db->query($query);
        $this->f->isDatabaseError($res);
        while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $elections[] = $row;
        }
        if (count($elections) == 0) {
            echo __("Aucune élection en cours.");
            return false;
        }
        $content = "";
        $line_template = '
        <a href="%1$s" class="list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">%2$s</h5>
                <small>%3$s</small>
            </div>
            <p class="mb-1"><span class="badge badge-warning">%4$s</span></p>
        </a>
        ';
        $out = '<div class="list-group">';
        foreach ($elections as $key => $value) {
            $out .= sprintf(
                $line_template,
                OM_ROUTE_FORM."&obj=election&action=3&idx=".$value["election"],
                $value["libelle"],
                $value["election_date"],
                $value["workflow"]
            );
        }
        $out .= '</div>';
        echo $out;
        return false;

    }

    public function view_widget__web($content = null) {
        $contenuWidget = 'Le portail web n\'est pas actif.';
        if ($this->portail_actif()) {
            $contenuWidget = sprintf(
                '<div class="link-web"><a href="%1$s" target="blank" title="%3$s"><img src="%2$s" /></a></div>',
                "../web/",
                "../app/img/openresultat-screenshot-web.png",
                __("Accéder au portail web de publication des résultats électoraux")
            );
        }
        echo $contenuWidget;
        return false;
    }

    /**
     * WIDGET_DASHBOARD - view_widget__animation.
     *
     * @return boolean
     */
    public function view_widget__animation($content = null) {
        $animations = array();
        $query = sprintf(
            'SELECT e1.election as election_id, e1.libelle as election_libelle, animation.libelle as animation_libelle, * FROM %1$sanimation INNER JOIN %1$selection as e1 on animation.election=e1.election LEFT JOIN %1$selection as e2 on e1.election_reference=e2.election WHERE ((e2.workflow IS NULL OR e2.workflow NOT LIKE \'Archivage\') AND e1.workflow NOT LIKE \'Archivage\') ORDER BY e1.date DESC, e1.election, animation.libelle',
            DB_PREFIXE
        );
        $res = $this->f->db->query($query);
        $this->f->isDatabaseError($res);
        while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $animations[] = $row;
        }
        if (count($animations) == 0) {
            echo __("Aucune animation en cours.");
            return false;
        }
        $out = "<ul>";
        foreach ($animations as $key => $value) {
            $out .= sprintf(
                '<li><a target="blank" href="%1$s">%2$s</a></li>',
                '../aff/animation.php?idx='.$value["election_id"].'&identifier='.$value["animation"],
                $value["election_libelle"]." - ".$value["animation_libelle"]
            );
        }
        $out .= "</ul>";
        print($out);
        return false;
    }

    /**
     * Vérifie si le portail web est actif.
     * La vérification se fait via une requête sql qui récupère le
     * modèle web actif. Si aucun modèle web n'est récupéré c'est que
     * le portail web n'est pas actif.
     *
     * /!\ cette méthode renvoie false en cas d'erreur de base de données
     *
     * @return boolean
     */
    protected function portail_actif() {
        $sql = sprintf(
            'SELECT
                web
            FROM
                %1$sweb
            WHERE
                actif IS TRUE',
            DB_PREFIXE
        );
        $result = $this->f->get_one_result_from_db_query($sql);
        if ($result['code'] == 'KO') {
            return false;
        }
        // Si le portail web est actif on a un résultat si ce n'est pas le cas
        // la requête ne renverra rien
        return ! empty($result['result']);
    }
}
