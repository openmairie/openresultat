<?php
//$Id$
//gen openMairie le 10/08/2020 10:07

require_once "../gen/obj/participation_unite.class.php";

class participation_unite extends participation_unite_gen {
    /**
     * Definition des actions disponibles sur la classe
     *
     * @return void
     */
    function init_class_actions() {
        parent::init_class_actions();
        // Aucune action disponible
        $this->class_actions[0] = null;
        $this->class_actions[1] = null;
        $this->class_actions[2] = null;
    }

    /**
     * SETTER FORM - setLib
     * 
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     * 
     * @return void
     */
    function setLib(&$form, $maj) {
        //libelle des champs pour éviter les termes techniques
        parent::setLib($form, $maj);
        $form->setLib('participation_unite', 'id');
        $form->setLib('election_unite', 'unite de l\'élection');
        $form->setLib('participation_election', 'tranche de l\'élection');
    }
    
    /**
     *
     * @return string Valeur de la variable récupérée ou chaîne vide
     */
    function get_var_sql_forminc__sql_election_unite() {
        return "SELECT
                    election_unite.election_unite,
                    CONCAT_WS(' - ', election.libelle, unite.code_unite, unite.libelle) as libelle
                FROM
                    ".DB_PREFIXE."election_unite
                    INNER JOIN ".DB_PREFIXE."election
                        ON election.election=election_unite.election
                    INNER JOIN ".DB_PREFIXE."unite
                        ON unite.unite=election_unite.unite
                ORDER BY
                    election.election,unite.ordre ASC";
    }

    /**
     *
     * @return string Valeur de la variable récupérée ou chaîne vide
     */
    function get_var_sql_forminc__sql_election_unite_by_id() {
        return "SELECT 
                    election_unite.election_unite,
                    CONCAT_WS(' - ', election.libelle, unite.code_unite, unite.libelle) as libelle
                FROM
                    ".DB_PREFIXE."election_unite
                    INNER JOIN ".DB_PREFIXE."election
                        ON election.election=election_unite.election
                    INNER JOIN ".DB_PREFIXE."unite
                        ON unite.unite=election_unite.unite
                WHERE election_unite = <idx>";
    }

    /**
     *
     * @return string Valeur de la variable récupérée ou chaîne vide
     */
    function get_var_sql_forminc__sql_participation_election() {
        return "SELECT
                    participation_election.participation_election,
                    concat(election.libelle,' - ',tranche.libelle) as lib
                FROM
                    ".DB_PREFIXE."participation_election
                    INNER JOIN ".DB_PREFIXE."election
                        ON election.election = participation_election.election
                    INNER JOIN ".DB_PREFIXE."tranche
                        ON tranche.tranche = participation_election.tranche
                    ORDER BY
                        tranche.ordre ASC";
    }

    /**
     *
     * @return string Valeur de la variable récupérée ou chaîne vide
     */
    function get_var_sql_forminc__sql_participation_election_by_id() {
        return "SELECT
                    participation_election.participation_election,
                    concat(election.libelle,' - ',tranche.libelle) as lib
                FROM
                    ".DB_PREFIXE."participation_election
                    INNER JOIN ".DB_PREFIXE."election
                        ON election.election = participation_election.election
                    INNER JOIN ".DB_PREFIXE."tranche
                        ON tranche.tranche = participation_election.tranche
                WHERE
                    participation_election = <idx>";
    }


    /**
     *
     * @return string Valeur de la variable récupérée ou chaîne vide
     */
    protected function get_var_sql_forminc__sql_tranche() {
        return "SELECT
                    distinct tranche.tranche,
                    tranche.libelle
                FROM
                    ".DB_PREFIXE."tranche
                    INNER JOIN ".DB_PREFIXE."participation_election
                        ON participation_election.tranche = tranche.tranche
                    INNER JOIN ".DB_PREFIXE."election
                        ON participation_election.election = election.election
                ORDER BY
                    tranche.libelle ASC ";
    }

    /**
     *
     * @return string Valeur de la variable récupérée ou chaîne vide
     */
    protected function get_var_sql_forminc__sql_tranche_by_id() {
        return "SELECT
                    tranche.tranche,
                    tranche.libelle
                FROM 
                    ".DB_PREFIXE."tranche
                WHERE
                    tranche = <idx>";
    }

    /**
     *
     * @return string Valeur de la variable récupérée ou chaîne vide
     */
    protected function get_var_sql_forminc__sql_election() {
        return "SELECT
                    election.election,
                    election.libelle
                FROM
                    ".DB_PREFIXE."election
                ORDER BY
                    election.libelle ASC";
    }

    /**
     *
     * @return string Valeur de la variable récupérée ou chaîne vide
     */
    protected function get_var_sql_forminc__sql_election_by_id() {
        return "SELECT
                    election.election,
                    election.libelle
                FROM
                    ".DB_PREFIXE."election
                WHERE
                    election = <idx>";
    }

    /**
     *
     * @return string Valeur de la variable récupérée ou chaîne vide
     */
    protected function get_var_sql_forminc__sql_unite() {
        return " SELECT
                    distinct unite.unite,
                    concat (unite.ordre,' - ',unite.libelle) as lib,
                    unite.ordre
                FROM
                    ".DB_PREFIXE."unite
                    INNER JOIN ".DB_PREFIXE."election_unite
                        ON election_unite.unite = unite.unite
                    INNER JOIN ".DB_PREFIXE."election
                        ON election_unite.election = election.election
                ORDER BY
                    unite.ordre ASC ";
    }

    /**
     *
     * @return string Valeur de la variable récupérée ou chaîne vide
     */
    protected function get_var_sql_forminc__sql_unite_by_id() {
        return "SELECT
                    unite.unite,
                    unite.libelle
                FROM
                    ".DB_PREFIXE."unite
                WHERE
                    unite = <idx>";
    }

    /**
     * SETTER FORM - setSelect.
     * 
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     * @param null &$dnu1 @deprecated  Ne pas utiliser.
     * @param null $dnu2 @deprecated  Ne pas utiliser.
     *
     * @return void
     */    
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj, $dnu1, $dnu2);
        // candidat
        $this->init_select(
            $form,
            $this->f->db,
            $maj,
            null,
            "tranche",
            $this->get_var_sql_forminc__sql("tranche"),
            $this->get_var_sql_forminc__sql("tranche_by_id"),
            false
        );
        // election
        $this->init_select(
            $form,
            $this->f->db,
            $maj,
            null,
            "election",
            $this->get_var_sql_forminc__sql("election"),
            $this->get_var_sql_forminc__sql("election_by_id"),
            false
        );
        // unite
        $this->init_select(
            $form,
            $this->f->db,
            $maj,
            null,
            "unite",
            $this->get_var_sql_forminc__sql("unite"),
            $this->get_var_sql_forminc__sql("unite_by_id"),
            false
        );
    }
}
