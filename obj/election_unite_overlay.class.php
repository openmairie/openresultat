<?php
//$Id$
//gen openMairie le 10/08/2020 10:06

require_once "../obj/election_unite.class.php";

class election_unite_overlay extends election_unite {
    // L'ajout de cette classe sert à éviter que le nom du sous-formulaire
    // et celui de l'objet soient identique. Ainsi dans le lien de retour
    // les deux éléments ne sont plus confondus et il est donc possible
    // d'accéder au formulaire de l'unité lié à la centaine pas à l'élection
    protected $_absolute_class_name = "election_unite_overlay";
}