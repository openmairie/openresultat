/**
 * Script JS spécifique à l'applicatif, ce script est destiné à être
 * appelé en dernier dans la pile des fichiers JS.
 *
 * @package openresultat
 * @version SVN : $Id: script.js 2202 2013-03-28 17:30:48Z fmichon $
 */

function handle__widget_form__select_multiple_permissions() {
    //
    $('.field-type-select_multiple select#permissions"').DualListBox({
        'json': false,
        'title': "éléments",
        'warning': "Cette action va déplacer beaucoup d'éléments et peut ralentir votre navigateur."
    });
}

/**
 * Cette fonction permet de charger dans un dialog jqueryui un formulaire tel
 * qu'il aurait été chargé avec ajaxIt
 * 
 * @param objsf string : objet de sousformulaire
 * @param link string : lien vers un sousformulaire
 * @param width integer: width en px
 * @param height integer: height en px
 * @param callback function (optionel) : nom de la méthode à appeler
 *                                       à la fermeture du dialog
 * @param callbackParams mixed (optionel) : paramètre à traiter dans la function
 *                                          callback 
 *
 **/
function popupIt(objsf, link, width, height, callback, callbackParams, position) {
    // Insertion du conteneur du dialog
    var dialog = $('<div id=\"sousform-'+objsf+'\"></div>').insertAfter('#tabs-1 .formControls-bottom');
    if( typeof(position) == 'undefined' ){
        position = 'left top';
    }
    // execution de la requete passee en parametre
    // (idem ajaxIt + callback)
    $.ajax({
        type: "GET",
        url: link,
        cache: false,
        success: function(html){
            //Suppression d'un precedent dialog
            dialog.empty();
            //Ajout du contenu recupere
            dialog.append(html);
            //Initialisation du theme OM
            om_initialize_content();
            //Creation du dialog
            $(dialog).dialog({
                //OnClose suppression du contenu
                close: function(ev, ui) {
                    // Si le formulaire est submit et valide on execute la méthode
                    // passée en paramètre
                    if (typeof(callback) === "function") {
                        callback(callbackParams);
                    }
                    $(this).remove();
                },
                resizable: true,
                modal: true,
                width: width,
                height: height,
                position: position,
            });
        },
        async : false
    });

    //Fermeture du dialog lors d'un clic sur le bouton retour
    $('#sousform-'+objsf).off("click").on("click",'a.retour',function() {
        $(dialog).dialog('close').remove();
        return false;
    });
}


/**
 *
 */
function form_confirmation_action(callback, elem, action, prefix) {
    //
    var dialogbloc = $("<div id=\"dialog-action-confirmation\">"+msg_form_action_confirmation+"</div>").insertAfter('#footer');
    //
    $(dialogbloc).dialog( "destroy" );
    $(dialogbloc).dialog({
        resizable: false,
        height:160,
        width:350,
        modal: true,
        buttons: [
            {
                text: msg_form_action_confirmation_button_confirm,
                    click: function() {
                        $(this).dialog("close");
                        callback(elem, action, prefix);
                    }
            }, {
                text: msg_form_action_confirmation_button_cancel,
                    click: function() {
                        $(this).dialog("close");
                    }
            }
        ]
    });
}

/**
 * Cette fonction permet de gérer les actions de formulaire et de sousformulaire :
 *
 * - action-direct en FORM
 * - action-direct en SOUSFORM
 * - action-self en SOUSFORM
 * - action-back en SOUSFORM
 * 
 * Cette fonction est une surcharge de la fonction form_bind_actions définie dans
 * le core.
 * Cette surcharge permet pour les action-direct de récupérer le préfixe du
 * sous-formulaire et de l'utiliser dans les fonctions qui déclenche l'action.
 */
function form_bind_actions() {
    //
    compose_data_href_on_link('#form-container a.action-direct');
    $("#form-container a.action-direct").off('click').on('click', function() {
        // On récupère l'objet du sous-formulaire en parsant l'attribut
        // class de l'action qui commence par sousform-
        sousform_obj = $(this).attr('id').split('-')[2];
        prefix = sousform_obj;
        //
        if ($(this).attr('class').indexOf("action-with-confirmation") >= 0) {
            form_confirmation_action(form_execute_action_direct, "form", this, prefix);
        } else {
            form_execute_action_direct("form", this, prefix);
        }
        //
        return false;
    });
    //
    compose_data_href_on_link('#sousform-container a.action-direct');
    $("#sousform-container a.action-direct").off('click').on('click', function(event) {
        // On récupère l'objet du sous-formulaire en parsant l'attribut
        // class de l'action qui commence par sousform-
        sousform_obj = $(this).attr('id').split('-')[2];
        prefix = sousform_obj;
        //
        if ($(this).attr('class').indexOf("action-with-confirmation") >= 0) {
            form_confirmation_action(form_execute_action_direct, "sousform", this, prefix);
        } else {
            form_execute_action_direct("sousform", this, prefix);
        }
        //
        return false;
    });
    //
    compose_data_href_on_link('#sousform-container a.action-self');
    $("#sousform-container a.action-self").off('click').on('click', function(event) {
        // On récupère l'objet du sous-formulaire en parsant l'attribut
        // class de l'action qui commence par sousform-
        sousform_obj = $(this).attr('id').split('-')[2];
        prefix = sousform_obj;
        ajaxIt(sousform_obj, $(this).attr('data-href'));
        //
        return false;
    });
    //
    compose_data_href_on_link('#sousform-container a.retour');
    $("#sousform-container a.retour").off('click').on('click', function(event) {
        // On récupère l'objet du sous-formulaire en parsant l'attribut
        // class de l'action qui commence par sousform-
        sousform_obj = $(this).attr('id').split('-')[2];
        prefix = sousform_obj;
        ajaxIt(sousform_obj, $(this).attr('data-href'));
        //
        return false;
    });
}

/**
 * Surcharge de la fonction form_execute_action_direct() du corps.
 *
 * Cette surcharge permet de passer en paramétre un préfixe. Ce préfixe
 * est ensuite utilisé pour sélectionné correctement la zone ou afficher
 * les messages liés à l'action et pour raffraichir le conatiner voulu.
 *
 * Sans cette surcharge si il y a plusieurs container dans la
 * page c'est toujours le premier qui servira à afficher le message et dont le
 * contenu sera mis à jour. Avec cette évolution on peut cibler le container
 * de l'objet dont le prefix est passé en paramètre.
 *
 * @param string elem : form ou sousform
 * @param string action : selecteur de l'action
 * @param string prefix : nom de l'objet
 */
function form_execute_action_direct(elem, action, prefix) {
    //
    real_prefix = "";
    if (elem == "sousform") {
        real_prefix = "#sousform-"+prefix;
    }
    //
    $(real_prefix+' #'+elem+'-message').html(msg_loading);
    //
    $.ajax({
        type: "POST",
        url: $(action).attr('data-href')+"&validation=1&contentonly=true",
        cache: false,
        data: "submit=plop&",
        success: function(html){
            // XXX Il semble nécessaire afin de récupérer la portion de code
            // div.message d'ajouter un container qui contient l'intégralité
            // du code html représentant le contenu du formulaire. Si on ajoute
            // pas ce bloc la récupération du bloc ne se fait pas.
            container_specific_js = '<div id="container-specific-js">'+html+'</div>';
            message = $(container_specific_js).find('div.message').get(0);
            if (message == undefined) {
                message = -1;
            }
            // Ajout du contenu récupéré (uniquement le bloc message)
            $(real_prefix+' #'+elem+'-message').html(message);
            // Rafraichissement du bloc de formulaire
            form_container_refresh(elem, real_prefix);
            // Initialisation JS du nouveau contenu de la page
            om_initialize_content();
        }
    });
}

/**
 * Surcharge de la méthode form_container_refresh qui
 * utilise les données récupérée à partir du href de la balise d'id sousform-real-href plutot
 * que celle du sousform-href pour mettre à jour les données de la page.
 *
 * Dans le cas ou un listing est affiché en dessous d'un formulaire en consultation,
 * cette modification permet d'afficher le code complet de la page et pas uniquement
 * celui du tableau.
 * Le problème étant que le tableau à lui aussi une balise d'id sousform-href et que
 * c'est le href qui lui est associé qui va être utilisé car cette balise est affichée
 * en première.
 *
 * Cette surcharge permet également de passer un prefix en paramètre. Ce préfixe sert
 * à identifier plus précisement dans la page la partie devant être raffraichie.
 * Notamment dans le cas des overlay elle permet de définir que c'est l'objet de overlay
 * qui doit être rechargé et pas le sous formulaire qui contiens l'overlay.
 */
 function form_container_refresh(elem, real_prefix) {
    //
    if (elem == "form") {
        //
        $.get(window.location.href, function(data) {
            //
            $(real_prefix+' #form-container').html(data);
            // Initialisation JS du nouveau contenu de la page
            om_initialize_content();
        });
    } else if (elem == "sousform") {
        //
        $.get($(real_prefix+" #sousform-real-href").attr('data-href')+"&contentonly=true", function(data) {
            //
            $(real_prefix+' #sousform-container').html(data);
            // Initialisation JS du nouveau contenu de la page
            om_initialize_content();
        });
    }
}


function app_initialize_content(tinymce_load) {
    /**
     * Plugin jquery qui bind les actions du formulaire dossier_instruction pour
     * ouvrir des overlay.
     *
     * @param string action Identifiant de l'action
     * @param string obj    Formulaire ouvert en overlay
     *
     * @return void
     */
    (function($){
        //Within this wrapper, $ is the jQuery namespace
        $.fn.bind_action_for_overlay = function(obj, width, height, callback, callbackParams, position) {
            if( typeof(width) == 'undefined' ){
                width = 'auto';
            }
            if( typeof(height) == 'undefined' ){
                height = 'auto';
            }
            if( typeof(callback) == 'undefined' ){
                callback = '';
            }
            if( typeof(callbackParams) == 'undefined' ){
                callbackParams = '';
            }
            if( typeof(position) == 'undefined' ){
                position = 'left top';
            }

            // bind de la function passée en arg sur l'event click des actions portlet
            $(this).off("click").on("click", function(event) {
                //
                elem_href = $(this).attr('href');
                if (elem_href != '#') {
                    $(this).attr('data-href', elem_href);
                    $(this).attr('href', '#');
                }
                //
                popupIt(obj, $(this).attr('data-href'), width, height, callback, callbackParams, position);
            });
            return $(this);
        }
    })(jQuery);
    var modal_height = screen.availHeight * 0.90;
    var modal_position = { my: "center", at: "top", of: window };
    // Bind l'action de consultation des election unité pour permettre l'affichage de cette
    // action dans un overlay
    $('a[id^=action-soustab-election_unite_overlay-left-consulter]').each(function(){
        $(this).bind_action_for_overlay("election_unite", "70%", modal_height, '', '', modal_position);
    });
    // Ferme l'overlay lors du clique sur le bouton "retour" de l'overlay. Seul le bouton
    // retour servant normalement à revenir vers le listing (cas du retour en consultation)
    // est visé.
    // Déclenche également la mise à jour du formulaire de consultation de la centaine
    // Vérifie qu'on se trouve bien dans le contexte de la centaine pour éviter que le formulaire
    // des unités de l'élection soit également impacté
    if ($("#sousform-centaine").length > 0) {
        $("#sousform-election_unite a[id^=sousform-action-election_unite-back].retour-tab").off('click').on('click', function(event) {
            $('#sousform-election_unite').dialog('close').remove();
            // Raffraichissement du formulaire de la centaine
            form_container_refresh('sousform', '#sousform-centaine');
            return false;
        });
        // Déclenche le raffraichissement du formulaire de la centaine lors du clic sur la croix
        // de fermeture de l'overlay
        // TODO : reprendre ce qui existe déjà pour le bouton pour faire ça plus proprement
        $(".ui-dialog-titlebar #ui-dialog-title-sousform-election_unite + a.ui-dialog-titlebar-close").off('click').on('click', function(event) {
            $('#sousform-election_unite').dialog('close').remove();
            // Raffraichissement du formulaire de la centaine
            form_container_refresh('sousform', '#sousform-centaine');
            return false;
        });
    }
    //
    handle__widget_form__select_multiple_permissions();
    adresse_postale(); //
    bind_form_field_hexa();
    // Méthode permettant dans les formulaires de saisie des résultats de l'élection et de ces
    // centaine de surligné le champs de saisie sélectionné.
    highlight_focused_result_field();
}

/**
 * Cette méthode permet si on est dans le contexte du formulaire de saisie
 * des résultats par unité d'une élection ou d'une centaine de changer la
 * classe du label du champs sélection (uniquement les champs de saisie des
 * résultat).
 * En changeant la classe cela permet d'afficher le label du champ saisi en bleu.
 * De la même manière si le champs est déselectionné le label ne sera plus surligné.
 */
function highlight_focused_result_field() {
    // Vérifie si l'on se trouve bien dans le contexte des formulaire de saisie des résultats
    if($('#fieldset-sousform-election_unite-resultats .field div.form-content input').length > 0
        || $('#fieldset-sousform-election_unite_centaine-resultats  .field div.form-content input').length > 0) {

        $(document).ready(function() {
            // Surligne le label du champs sélectionné en lui ajoutant la classe highlight-blue
            // Pour le formulaire de saisie des résultats de l'unité
            $('#fieldset-sousform-election_unite-resultats .field div.form-content input').focus(function(){
                $(this).parents('.field').find('div.form-libelle label').addClass('highlight-blue');
            });
            // Pour le formulaire de saisie des résultats de l'unité pour une centaine
            $('#fieldset-sousform-election_unite_centaine-resultats .field div.form-content input').focus(function(){
                $(this).parents('.field').find('div.form-libelle label').addClass('highlight-blue');
            });
            // Lorsque l'on déselectionne le champ le label n'est plus surligné car on lui supprime la classe highlight-blue
            // Pour le formulaire de saisie des résultats de l'unité
            $('#fieldset-sousform-election_unite-resultats .field div.form-content input').focusout(function(){
                $(this).parents('.field').find('div.form-libelle label').removeClass('highlight-blue'); 
            });
            // Pour le formulaire de saisie des résultats de l'unité pour une centaine
            $('#fieldset-sousform-election_unite_centaine-resultats .field div.form-content input').focusout(function(){
                $(this).parents('.field').find('div.form-libelle label').removeClass('highlight-blue'); 
            });
        });
    }
}

// adresse postale via la BAN 
function adresse_postale() {
// la fonction necessite cp + ville + adresse + 
// latitude arles -> l'ordre des donnees pour adresse est fonction latitude et longitude
// a mettre dans les om_parametre
var lat  =$("input[name='lat']").val();
var lon  = $("input[name='lon']").val();

$("#cp").autocomplete({
    source: function (request, response) {
        $.ajax({
            url: "https://api-adresse.data.gouv.fr/search/?postcode="+$("input[name='cp']").val(),
            data: { q: request.term },
            dataType: "json",
            success: function (data) {
                var postcodes = [];
                response($.map(data.features, function (item) {
                    // Ici on est obligé d'ajouter les CP dans un array pour ne pas avoir plusieurs fois le même
                    if ($.inArray(item.properties.postcode, postcodes) == -1) {
                        postcodes.push(item.properties.postcode);
                        return { label: item.properties.postcode + " - " + item.properties.city, 
                                 city: item.properties.city,
                                 value: item.properties.postcode
                        };
                    }
                }));
            }
        });
    },
    // On remplit aussi la ville
    select: function(event, ui) {
        $('#ville').val(ui.item.city);
    }
});
$("#adresse1").autocomplete({
    source: function (request, response) {
        $.ajax({
            url: "https://api-adresse.data.gouv.fr/search/?postcode="+$("input[name='cp']").val()+"&lat="+lat+"&lon="+lon,
            data: { q: request.term },
            dataType: "json",
            success: function (data) {
                response($.map(data.features, function (item) {
                    return { label: item.properties.name + " - " + item.properties.city,
                             city: item.properties.city,
                             postcode: item.properties.postcode, 
                             coordinates : item.geometry.coordinates,
                             value: item.properties.name};
                }));
            }
        });
    },
    // On remplit ville cp coordonnees
    //var temp = val(ui.item.coordinates).split(',');
    //alert(temp[1]);
    select: function(event, ui) {
        $('#ville').val(ui.item.city);
        $('#cp').val(ui.item.postcode);
        $('#coordinates').val(ui.item.coordinates);
    }
    
});
$("#ville").autocomplete({
    source: function (request, response) {
        $.ajax({
            url: "https://api-adresse.data.gouv.fr/search/?city="+$("input[name='ville']").val(),
            data: { q: request.term },
            dataType: "json",
            success: function (data) {
                var cities = [];
                response($.map(data.features, function (item) {
                    // Ici on est obligé d'ajouter les villes dans un array pour ne pas avoir plusieurs fois la même
                    if ($.inArray(item.properties.postcode, cities) == -1) {
                        cities.push(item.properties.postcode);
                        return { label: item.properties.postcode + " - " + item.properties.city, 
                                 postcode: item.properties.postcode,
                                 value: item.properties.city
                        };
                    }
                }));
            }
        });
    },
    // On remplit aussi le CP
    select: function(event, ui) {
        $('#cp').val(ui.item.postcode);
    }
}); 
}

/**
* Cette fonction permet d'afficher les options d'un select par rapport
* à un autre champ
* 
* @param  int id               Données (identifiant) du champ visé
* @param  string tableName     Nom de la table
* @param  string linkedField   Champ visé
* @param  string formCible     Formulaire visé
*/
function filterSelect(id, tableName, linkedField, formCible) {
    //lien vers script php
    link = "../app/index.php?module=form&snippet=filterselect&idx=" + id + "&tableName=" + tableName +
            "&linkedField=" + linkedField + "&formCible=" + formCible;

    $.ajax({
        type: "GET",
        url: link,
        cache: false,
        dataType: "json",
        success: function(res) {
            $('#'+tableName).empty();

            for ( j=0 ; j < res[0].length ; j++ ){

                $('#'+tableName).append(
                    '<option value="'+res[0][j]+'" >'+res[1][j]+'</option>'
                );
                
            }

        },
        error: function(jqXHR, textStatus, errorThrown) {
            $('#'+tableName).empty();
        },
        async: false
    });
}

/**
* Cette fonction permet d'afficher le champ code_unite comme obligatoire
* selon le type d'unité
*
* @param  integer  champ    Champ visé
*/
function obligatoireSelonType(typeUnite) {
    // lien script php
    var link = "../app/index.php?module=form&obj=unite&action=10&type_unite=" + typeUnite

    $.ajax({
        type: "GET",
        url: link,
        cache: false,
        dataType: "json",
        success: function(res) {
            if ("libelle" in res) {
                $('#lib-code_unite').text(res['libelle']);
            }
        },
        async: false
    });
}

// Sur l'événement "change" du champ "dossier_coordination_type"
function recuperer_perimetre(selected) {
    // lien script php
    link = "../app/index.php?module=form&obj=unite&action=7&idx=0";
    //
    $.ajax({
        type: "POST",
        url: link,
        cache: false,
        data: "selected="+selected+"&",
        dataType: "json",
        success: function(json){
            // Change la valeur du champ "perimetre"
            $("#perimetre").prop('checked', false);
            $("#perimetre").prop('value', '');
            //
            if (json.type_unite_contenu !== "") {
                $("#perimetre").prop('checked', true);
                $("#perimetre").prop('value', 'Oui');
            }
        }
    });
}

/**
 * Calcul et met à jour la valeur des champs correspondant aux périmètres.
 * Récupère un tableau json de cette forme :
 *      [indice_champ_parent] = [indice_champ_enfant1, ..., indice_champ_enfant_n]
 * A partir des indices fournis dans ce tableau, récupère les valeurs des champs
 * enfants et les additionne si ce sont bien des valeurs numériques. Donne ensuite
 * cette valeur au champ parent. Cette action est réalisé pour chaque champs parents
 * du tableau json.
 * 
 * @param integer election identifiant de l'élection en cours qui permet de récupérer
 * les périmètres de l'élection pour remplir le json
 */
function update_participation_perimetre(election) {
    // lien script php
    var link = "../app/index.php?module=form&obj=participation_election&action=20&election="
        + election

    $.ajax({
        type: "GET",
        url: link,
        cache: false,
        dataType: "json",
        success: function(res){
            //Parcours le tableau json et donc chaque indice des unites_parents
            for (parent in res) {
                //Si un parent n'a pas d'enfant alors on passe au suivant
                if (res[parent]['enfants'].length != 0){
                    newValue = 0;
                    //Pour chaque enfant de l'unité parent, récupère la valeur du champ en
                    //utilisant son indice et si, c'est un nombre, l'ajoute au total des participations
                    for (enfant in res[parent]['enfants']) {
                        childValue = parseInt($('#unite'+ res[parent]['enfants'][enfant]).attr('value'))
                        if ($.isNumeric(childValue)) {
                            newValue += childValue;
                        }
                    }
                    //Attribution de la valeur calculé à l'unité parent
                    $('#unite'+ res[parent]['parent']).attr('value', newValue);
                    //Affichage de l'unité calculé pour que l'utilisateur puisse voir l'évolution
                    $('#unite'+ res[parent]['parent'] + ' + p').text(newValue);
                }
            }
        },
        async: false
    });
}


/**
 * A partir des valeurs du modèle récupéré en json, rempli le formulaire
 * de l'animation avec les mêmes valeurs.
 *  
 * @param integer modele identifiant de l'animation servant de modele
 */
function applique_parametrage_modele(modele) {
    // lien script php
    var link = "../app/index.php?module=form&obj=animation&action=10&modele="
        + modele

    $.ajax({
        type: "GET",
        url: link,
        cache: false,
        dataType: "json",
        success: function(res){
            for (parametre in res) {
                //Pour éviter que le selecteur du champ du logo ne récupére
                //la position du logo de l'application par erreur, il est
                //nécessaire de rendre le selecteur plus précis.
                //Ainsi '#content' permet de ne sélectionner que
                //les élements se trouvant dans le formulaire
                var position = $('#content #'+ parametre);
                //Une fois la position obtenue, il faut savoir de quel type
                //champs il s'agit (select, input, ...) pour pouvoir le modifier
                if (position.prop('tagName') == 'SELECT') {
                    //Si c'est un select, il faut déselectionner l'option sélectionnée
                    //et sélectionner l'option du paramétrage
                    var posOptionSelectionnee = $('#content #'+ parametre + ' option[selected]');
                    posOptionSelectionnee.removeAttr('selected');
                    var posOptionVoulue = $('#content #'+ parametre + ' option[value=' + res[parametre] + ']');
                    posOptionVoulue.attr('selected', 'selected');
                } else if (position.prop('tagName') == 'INPUT') {
                    //Si c'est un input, il suffit de remplacer le texte par le texte
                    //issu du paramétrage
                    position.attr('value', res[parametre]);
                } else if (position.prop('tagName') == 'TEXTAREA') {
                    //Si c'est un textarea, il suffit de remplacer le texte par le texte
                    //issu du paramétrage
                    position.text(res[parametre]);
                }
            }
        },
        async: false
    });
}

/**
 * FORM WIDGET - HEX
 * @requires lib miniColors
 */
//
function bind_form_field_hexa() {
    $('input.hexa').not('[disabled="disabled"]').miniColors({});
}

/**
 * Les fonctions suivantes sont similaires aux fonctions qui permettent de gérer
 * la localisation dans les formulaires de paramétrage des éditions du framework
 * mais modifiées pour coller au besoin de la localisation sur plan
 * d'opencimetiere.
 */


$(function() {
   localisation_plan_bind_draggable();
});

function localisation_plan_bind_draggable() {
    $("#plan-draggable").draggable({ containment: "parent" });
    $("#plan-draggable").dblclick(function() {
        infos = $(this).attr("class");
        infos = infos.split(" ");
        infos = infos[0].split(";");
        // on enlève un à l'abscisse et à l'ordonnée pour la bordure de l'image
        // qui décale d'un pixel
        position = $(this).position();
        x = parseInt(position.left);
        y = parseInt(position.top);
        localisation_plan_return(infos[0], infos[1], (x-1), infos[2], (y-1));
        return true;
    });
}
    
// Cette fonction permet de retourner les informations sur le fichier téléchargé
// du formulaire d'upload vers le formulaire d'origine
function localisation_plan_return(form, champ_x, value_x, champ_y, value_y) {
    $("form[name|="+form+"] #"+champ_x, window.opener.document).attr('value', value_x);
    $("form[name|="+form+"] #"+champ_y, window.opener.document).attr('value', value_y);
    window.close();
}

//
function localisation_plan(champ, chplan, positionx) {
    //
    if (fenetreouverte == true) {
        pfenetre.close();
    }
    //
    var plan = document.f2.elements[chplan].value;
    var x = document.f2.elements[positionx].value;
    var y = document.f2.elements[champ].value;
    //
    if (plan == "") {
        //
        alert("Vous devez d'abord selectionner un plan pour pouvoir localiser l'emplacement.");
    } else {
        //
        link = "../app/localisation_plan_edit.php?positiony="+champ+"&positionx="+positionx+"&plan="+plan+"&form=f2"+"&x="+x+"&y="+y+"#draggable";
        //
        pfenetre = window.open(link,"localisation","toolbar=no,scrollbars=yes,width="+screen.availWidth+",height="+screen.availHeight+",top=0,left=0");
        //
        fenetreouverte = true;
    }
}

/**
 * Gère le comportement de la page settings (administration et paramétrage) :
 *  - autofocus sur le champ de recherche
 *  - comportement du livesearch
 */
 function handle_settings_page() {
    // Auto focus sur le champ de recherche au chargement de la page
    $('#settings-live-search #filter').focus();
    // Live search dans le contenu de la page settings
    $("#settings-live-search #filter").keyup(function(){
        var filter = $(this).val(), count = 0;
        $("div.list-group a").each(function(){
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).hide();
            } else {
                $(this).show();
                count++;
            }
        });
    });
}

/**
 *
 */
$(function(){
    /**
     * Gère le comportement de la page settings (administration et paramétrage).
     */
    handle_settings_page();
});

