<?php
/**
 * Ce script permet de ...
 *
 * @package openresultat
 * @version SVN : $Id$
 */

require_once "../obj/openresultat.class.php";
$f = new openresultat("nohtml");

// Cette fonction permet de calculer le lien retour en fonction des paramètres
// récupérés en $_GET.
function calculate_back_link($get, $emplacements) {
    //
    if (isset($get["retour"]) && $get["retour"] == "tab") {
        //
        $link = OM_ROUTE_TAB;
        $link .= "&";
        //
        if (isset($get["specific_origin"])) {
            $get["obj"] = $get["specific_origin"];
        }
        //
        foreach ($get as $key => $value) {
            $link .= "&amp;".$key."=".$value;
        }
    } elseif (isset($get["retour"]) && $get["retour"] == "form" && isset($emplacements[0])) {
        //
        $emplacement = $emplacements[0];
        //
        $link = OM_ROUTE_FORM;
        $link .= "&";
        //
        $link .= "obj=plan_unite&amp;idx=".$emplacement['plan_unite']."&amp;action=3";
    } else {
        //
        $link = OM_ROUTE_DASHBOARD;
    }
    //
    return $link;
}

// Cette fonction permet d'afficher le bouton retour
function display_button_back($link = "") {
    //
    echo "<div class=\"formControls\">\n";
    //
    echo "<a class=\"retour\" ";
    echo "href=\"";
    //
    echo $link;
    //
    echo "\"";
    echo ">";
    //
    echo __("Retour");
    //
    echo "</a>\n";
    //
    echo "</div>\n";
}

//
$emplacements = array();

//
$mode = 0;

// MODE 1 - Visualisation d'un seul objet sur le plan
//
$obj = "";
if (isset($_GET['obj'])) {
    $obj = $_GET['obj'];
}
//
$idx = "";
if (isset($_GET['idx'])) {
    $idx = $_GET['idx'];
}
//
if ($obj != "" && $idx != "") {
    $mode = 1;
}
//
if ($mode == 1 && (strpos($obj, "/") !== false
    || !file_exists("../obj/".$obj.".class.php"))) {
    $class = "error";
    $message = __("L'objet est invalide.");
    $f->addToMessage($class, $message);
    $f->setFlag(NULL);
    $f->display();
    die();
}

// MODE 2 - Visualisation de tous les objets sur le plan
//
$idPlan = "";
if (isset($_GET['plan'])) {
    $idPlan = $_GET['plan'];
}

//
if ($mode == 0 && $idPlan != "") {
    $mode = 2;
}

//
if ($mode == 1) {
    //
    require "../obj/plan_unite.class.php";
    $emplacement = new plan_unite($idx, $f->db, 0);
    //
    $idPlan = $emplacement->getVal("plan");
    //
    $emplacements[] = array(
        "plan_unite" => $emplacement->getVal("plan_unite"),
        "position_x" => $emplacement->getVal("position_x"),
        "position_y" => $emplacement->getVal("position_y"),
        "img_unite_arrivee" => $emplacement->getVal("img_unite_arrivee"),
        "largeur" => $emplacement->getVal("largeur_icone")
    );
}

//
if ($mode == 2) {
    //
    $sql = sprintf(
        "select
            plan_unite,
            position_x,
            position_y,
            img_unite_arrivee,
            largeur_icone
        from
            ".DB_PREFIXE."plan_unite
        where
            plan = %d",
        $idPlan
    );

    $res = $f->db->query($sql);
    $f->addToLog("visualisationplan.php: db->query(\"".$sql."\");", VERBOSE_MODE);
    $f->isDatabaseError($res);
    while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
        $emplacements[] = $row;
    }
}

//
require_once "../obj/plan.class.php";
$plan = new plan($idPlan, $f->db, NULL);
//
$imageplan = $f->storage->getPath($plan->getVal("image_plan"));

//
if (!file_exists($imageplan)) {
    $class = "error";
    $message = __("L'objet est invalide car il n y a pas de plan selectionné pour cet enregistrement.");
    $f->addToMessage($class, $message);
    $f->setFlag(NULL);
    $f->display();
    die();
}

//
$sql = "select libelle from ".DB_PREFIXE."plan where plan='".$idPlan."'";
$plan_libelle = $f->db->getone($sql);
$f->addToLog("app/widget_localisation.php: db->getone(\"".$sql."\");", VERBOSE_MODE);
$f->isDatabaseError($plan_libelle);

//
$html_body = "<body class=\"localisation\">";
$f->setHTMLBody($html_body);

//
$f->setFlag("htmlonly");
$f->display();

//
$f->displayStartContent();
//
$f->setTitle(__("Localisation")." -> ".$plan_libelle." (".$plan->getVal('plan').")");
$f->displayTitle();

/**
 *
 */
//
display_button_back(calculate_back_link($_GET, $emplacements));

/**
 *
 */
//
$size = getimagesize($imageplan);
//
echo "<div";
echo " id=\"localisation-wrapper\"";
echo " style=\"";
//
echo "border: 1px solid #cdcdcd; ";
echo "position:relative; ";
echo "width:".$size[0]."px; ";
echo "height:".$size[1]."px; ";
echo "background-image: url('".OM_ROUTE_FORM."&snippet=file&obj=plan&champ=image_plan&id=".$plan->getVal('plan')."'); ";
//
echo "\"";
echo ">\n";
//

//
require_once "../obj/plan.class.php";

foreach($emplacements as $i => $emplacement) {
    $planUnite = new plan_unite($emplacement['plan_unite'], $f->db, NULL);
    //
    $icone = $f->storage->getPath($planUnite->getVal("img_unite_arrivee"));
    $urlIcone = OM_ROUTE_FORM."&snippet=file&obj=plan_unite&champ=img_unite_arrivee&id=".$emplacement['plan_unite'];
    if (!file_exists($icone)) {
        $icone = $f->storage->getPath($plan->getVal("img_unite_arrivee"));
        $urlIcone = OM_ROUTE_FORM."&snippet=file&obj=plan&champ=img_unite_arrivee&id=".$plan->getVal('plan');
        if (!file_exists($icone)) {
            $urlIcone = '../app/img/img_unite_arrivee.gif';
        }
    }
    //
    echo "<div id=\"camap".$i."\"";
    echo " style=\"";
    echo "position:absolute; ";
    echo "left:".($emplacement['position_x'])."px;";
    echo "top:".($emplacement['position_y'])."px;";
    echo "\">\n";
    //
    echo "\t<a href=\"".OM_ROUTE_FORM."&obj=plan_unite&amp;idx=".$emplacement['plan_unite']."&amp;action=3\">";
    //
    echo "<img name='image$i'";
    echo "src='".$urlIcone."' ";
    //
    if (! empty($emplacement['largeur'])) {
        echo " style='width:".($emplacement['largeur'])."px;' ";
    }
    echo "title=\"".$emplacement['plan_unite']."\" alt=\".\" />";
    //
    echo "</a>\n";
    //
    echo "</div>\n";
}
//
echo "\n</div>\n";
//
echo "<div class=\"visualClear\"><!-- --></div>\n";

/**
 *
 */
//
display_button_back(calculate_back_link($_GET, $emplacements));

/**
 *
 */
//
$f->displayEndContent();