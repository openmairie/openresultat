<?php
/**
 * Ce fichier permet de paramétrer le générateur.
 *
 * @package openresultat
 * @version SVN : $Id$
 */

/**
 * Surcharge applicative de la classe 'om_dbform'.
 */
$om_dbform_path_override = "../obj/om_dbform.class.php";
$om_dbform_class_override = "om_dbform";

/**
 * Ce tableau permet de lister les tables qui ne doivent pas être prises en
 * compte dans le générateur. Elles n'apparaîtront donc pas dans l'interface
 * et ne seront pas automatiquement générées par le 'genfull'.
 */
$tables_to_avoid = array(
    "candidat_resultat",
    "election_cumul_unite",
    "election_exprime",
    "resultat_participation_unite",
    "resultat_participation_tranche"
);

/**
 *
 */
$rubrik_application = "application";
$rubrik_administration = "administration & paramétrage";
$rubrik_parametrage = "administration & paramétrage";
$rubrik_parametrage_metier = "administration & paramétrage";
$rubrik_election = "élections";

/**
 *
 */
$category_affichage = "affichage";
$category_decoupage = "découpage administratif";
$category_editions = "editions";
$category_election = "élection";
$category_gestiondesutilisateurs = "gestion des utilisateurs";
$category_tableauxdebord = "tableaux de bord";

/**
 *
 */
$tables_to_overload = array(
    // A&P
    "acteur" => array(
        "breadcrumb_in_page_title" => array($rubrik_parametrage_metier, $category_election, ),
    ),
    // DATA A&P
    "animation" => array(
        "breadcrumb_in_page_title" => array($rubrik_parametrage_metier, $category_affichage, ),
    ),
    // A&P
    "candidat" => array(
        "breadcrumb_in_page_title" => array($rubrik_parametrage_metier, $category_election, ),
    ),
    // A&P
    "canton" => array(
        "breadcrumb_in_page_title" => array($rubrik_parametrage_metier, $category_decoupage, ),
    ),
    // A&P
    "circonscription" => array(
        "breadcrumb_in_page_title" => array($rubrik_parametrage_metier, $category_decoupage, ),
    ),
    // A&P
    "commune" => array(
        "breadcrumb_in_page_title" => array($rubrik_parametrage_metier, $category_decoupage, ),
    ),
    // DATA
    "delegation" => array(
        "breadcrumb_in_page_title" => array($rubrik_application, "élection", ),
    ),
    // A&P
    "departement" => array(
        "tablename_in_page_title" => "département",
        "breadcrumb_in_page_title" => array($rubrik_parametrage_metier, $category_decoupage, ),
    ),
    //  DATA
    "election" => array(
        "tablename_in_page_title" => "élection",
        "breadcrumb_in_page_title" => array($rubrik_application, ),
    ),
    // DATA
    "election_candidat" => array(
        "breadcrumb_in_page_title" => array($rubrik_application, "élection", ),
        "displayed_fields_in_tableinc" => array(
            "election_candidat", "election", "candidat", "ordre", "prefecture",
        ),
        "specific_config_for_fields" => array(
            "photo" => array(
                "abstract_type" => "file",
            ),
        ),
    ),
    // DATA
    "election_resultat" => array(
        "tablename_in_page_title" => "détail des résultats",
        "breadcrumb_in_page_title" => array($rubrik_application, "élection", ),
    ),
    // DATA
    "election_unite" => array(
        "breadcrumb_in_page_title" => array($rubrik_application, "élection", ),
        "extended_class" => array("election_unite_overlay", ),
    ),
    // A&P
    "lien_unite" => array(
        "tablename_in_page_title" => "lien entre unités",
        "breadcrumb_in_page_title" => array($rubrik_parametrage_metier, $category_election, ),
    ),
    // A&P
    "om_collectivite" => array(
        "breadcrumb_in_page_title" => array($rubrik_administration, "général", ),
    ),
    // A&P
    "om_dashboard" => array(
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_tableauxdebord, ),
    ),
    // A&P
    "om_droit" => array(
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_gestiondesutilisateurs, ),
    ),
    // A&P
    "om_etat" => array(
        "breadcrumb_in_page_title" => array($rubrik_parametrage, $category_editions, ),
    ),
    // A&P
    "om_lettretype" => array(
        "breadcrumb_in_page_title" => array($rubrik_parametrage, $category_editions, ),
    ),
    // A&P
    "om_logo" => array(
        "breadcrumb_in_page_title" => array($rubrik_parametrage, $category_editions, ),
    ),
    // A&P
    "om_parametre" => array(
        "breadcrumb_in_page_title" => array($rubrik_administration, "général", ),
    ),
    // A&P
    "om_permission" => array(
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_gestiondesutilisateurs, ),
    ),
    // A&P
    "om_profil" => array(
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_gestiondesutilisateurs, ),
    ),
    // A&P
    "om_requete" => array(
        "breadcrumb_in_page_title" => array($rubrik_parametrage, $category_editions, ),
    ),
    // A&P
    "om_sig_extent" => array(
        "tablename_in_page_title" => "étendue",
        "breadcrumb_in_page_title" => array($rubrik_administration, "SIG", ),
    ),
    // A&P
    "om_sig_flux" => array(
        "tablename_in_page_title" => "flux",
        "breadcrumb_in_page_title" => array($rubrik_administration, "SIG", ),
    ),
    // A&P
    "om_sig_map" => array(
        "tablename_in_page_title" => "carte",
        "breadcrumb_in_page_title" => array($rubrik_administration, "SIG", ),
    ),
    // A&P
    "om_sig_map_comp" => array(
        "tablename_in_page_title" => "géométrie",
        "breadcrumb_in_page_title" => array($rubrik_administration, "SIG", ),
    ),
    // A&P
    "om_sig_map_flux" => array(
        "tablename_in_page_title" => "flux appliqué à une carte",
        "breadcrumb_in_page_title" => array($rubrik_administration, "SIG", ),
    ),
    // A&P
    "om_sousetat" => array(
        "breadcrumb_in_page_title" => array($rubrik_parametrage, $category_editions, ),
    ),
    // A&P
    "om_utilisateur" => array(
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_gestiondesutilisateurs, ),
        "displayed_fields_in_tableinc" => array(
            "nom", "email", "login", "om_profil",
        ),
    ),
    // A&P
    "om_widget" => array(
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_tableauxdebord, ),
        "displayed_fields_in_tableinc" => array(
            "libelle", "om_profil", "type",
        ),
    ),
    // DATA
    "participation_election" => array(
        "breadcrumb_in_page_title" => array($rubrik_application, "élection", ),
    ),
    // DATA
    "participation_unite" => array(
        "tablename_in_page_title" => "détail des participations",
        "breadcrumb_in_page_title" => array($rubrik_application, "élection", ),
    ),
    // A&P
    "parti_politique" => array(
        "tablename_in_page_title" => "parti politique",
        "breadcrumb_in_page_title" => array($rubrik_parametrage_metier, $category_election, ),
    ),
    // DATA A&P
    "plan" => array(
        "breadcrumb_in_page_title" => array($rubrik_parametrage_metier, $category_affichage, ),
        "displayed_fields_in_tableinc" => array(
            "plan", "libelle", "image_plan",
        ),
        "specific_config_for_fields" => array(
            "image_plan" => array(
                "abstract_type" => "file",
            ),
            "img_unite_arrivee" => array(
                "abstract_type" => "file",
            ),
            "img_unite_non_arrivee" => array(
                "abstract_type" => "file",
            ),
        ),
    ),
    // DATA
    "plan_election" => array(
        "breadcrumb_in_page_title" => array($rubrik_application, "election", ),
    ),
    // DATA A&P
    "plan_unite" => array(
        "breadcrumb_in_page_title" => array($rubrik_parametrage_metier, $category_affichage, ),
        "displayed_fields_in_tableinc" => array(
            "plan_unite", "plan", "unite", "position_x", "position_y",
        ),
        "specific_config_for_fields" => array(
            "img_unite_arrivee" => array(
                "abstract_type" => "file",
            ),
            "img_unite_non_arrivee" => array(
                "abstract_type" => "file",
            ),
        ),
    ),
    // A&P
    "tranche" => array(
        "breadcrumb_in_page_title" => array($rubrik_parametrage_metier, $category_election, ),
        "tabs_in_form" => false,
    ),
    // A&P
    "type_election" => array(
        "tablename_in_page_title" => "type d'élection",
        "breadcrumb_in_page_title" => array($rubrik_parametrage_metier, $category_election, ),
    ),
    // A&P
    "type_unite" => array(
        "tablename_in_page_title" => "type d'unité",
        "breadcrumb_in_page_title" => array($rubrik_parametrage_metier, $category_election, ),
    ),
    // A&P
    "unite" => array(
        "tablename_in_page_title" => "unité",
        "breadcrumb_in_page_title" => array($rubrik_parametrage_metier, $category_election, ),
    ),
    // A&P
    "web" => array(
        "breadcrumb_in_page_title" => array($rubrik_parametrage_metier, $category_affichage, ),
    ),
);
