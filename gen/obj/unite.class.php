<?php
//$Id$ 
//gen openMairie le 07/05/2021 10:44

require_once "../obj/om_dbform.class.php";

class unite_gen extends om_dbform {

    protected $_absolute_class_name = "unite";

    var $table = "unite";
    var $clePrimaire = "unite";
    var $typeCle = "N";
    var $required_field = array(
        "libelle",
        "ordre",
        "type_unite",
        "unite"
    );
    
    var $foreign_keys_extended = array(
        "canton" => array("canton", ),
        "circonscription" => array("circonscription", ),
        "commune" => array("commune", ),
        "departement" => array("departement", ),
        "type_unite" => array("type_unite", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "unite",
            "libelle",
            "type_unite",
            "ordre",
            "adresse1",
            "adresse2",
            "cp",
            "ville",
            "geom",
            "coordinates",
            "om_validite_debut",
            "om_validite_fin",
            "type_unite_contenu",
            "perimetre",
            "canton",
            "circonscription",
            "commune",
            "departement",
            "code_unite",
            "id_reu",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_canton() {
        return "SELECT canton.canton, canton.libelle FROM ".DB_PREFIXE."canton ORDER BY canton.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_canton_by_id() {
        return "SELECT canton.canton, canton.libelle FROM ".DB_PREFIXE."canton WHERE canton = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_circonscription() {
        return "SELECT circonscription.circonscription, circonscription.libelle FROM ".DB_PREFIXE."circonscription ORDER BY circonscription.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_circonscription_by_id() {
        return "SELECT circonscription.circonscription, circonscription.libelle FROM ".DB_PREFIXE."circonscription WHERE circonscription = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_commune() {
        return "SELECT commune.commune, commune.libelle FROM ".DB_PREFIXE."commune ORDER BY commune.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_commune_by_id() {
        return "SELECT commune.commune, commune.libelle FROM ".DB_PREFIXE."commune WHERE commune = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_departement() {
        return "SELECT departement.departement, departement.libelle FROM ".DB_PREFIXE."departement ORDER BY departement.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_departement_by_id() {
        return "SELECT departement.departement, departement.libelle FROM ".DB_PREFIXE."departement WHERE departement = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_type_unite() {
        return "SELECT type_unite.type_unite, type_unite.libelle FROM ".DB_PREFIXE."type_unite ORDER BY type_unite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_type_unite_by_id() {
        return "SELECT type_unite.type_unite, type_unite.libelle FROM ".DB_PREFIXE."type_unite WHERE type_unite = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_type_unite_contenu() {
        return "SELECT type_unite.type_unite, type_unite.libelle FROM ".DB_PREFIXE."type_unite ORDER BY type_unite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_type_unite_contenu_by_id() {
        return "SELECT type_unite.type_unite, type_unite.libelle FROM ".DB_PREFIXE."type_unite WHERE type_unite = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['unite'])) {
            $this->valF['unite'] = ""; // -> requis
        } else {
            $this->valF['unite'] = $val['unite'];
        }
        $this->valF['libelle'] = $val['libelle'];
        if (!is_numeric($val['type_unite'])) {
            $this->valF['type_unite'] = ""; // -> requis
        } else {
            $this->valF['type_unite'] = $val['type_unite'];
        }
        $this->valF['ordre'] = $val['ordre'];
        if ($val['adresse1'] == "") {
            $this->valF['adresse1'] = NULL;
        } else {
            $this->valF['adresse1'] = $val['adresse1'];
        }
        if ($val['adresse2'] == "") {
            $this->valF['adresse2'] = NULL;
        } else {
            $this->valF['adresse2'] = $val['adresse2'];
        }
        if ($val['cp'] == "") {
            $this->valF['cp'] = NULL;
        } else {
            $this->valF['cp'] = $val['cp'];
        }
        if ($val['ville'] == "") {
            $this->valF['ville'] = NULL;
        } else {
            $this->valF['ville'] = $val['ville'];
        }
        if ($val['geom'] == "") {
            unset($this->valF['geom']);
        } else {
            $this->valF['geom'] = $val['geom'];
        }
        if ($val['coordinates'] == "") {
            $this->valF['coordinates'] = NULL;
        } else {
            $this->valF['coordinates'] = $val['coordinates'];
        }
        if ($val['om_validite_debut'] != "") {
            $this->valF['om_validite_debut'] = $this->dateDB($val['om_validite_debut']);
        } else {
            $this->valF['om_validite_debut'] = NULL;
        }
        if ($val['om_validite_fin'] != "") {
            $this->valF['om_validite_fin'] = $this->dateDB($val['om_validite_fin']);
        } else {
            $this->valF['om_validite_fin'] = NULL;
        }
        if (!is_numeric($val['type_unite_contenu'])) {
            $this->valF['type_unite_contenu'] = NULL;
        } else {
            $this->valF['type_unite_contenu'] = $val['type_unite_contenu'];
        }
        if ($val['perimetre'] == 1 || $val['perimetre'] == "t" || $val['perimetre'] == "Oui") {
            $this->valF['perimetre'] = true;
        } else {
            $this->valF['perimetre'] = false;
        }
        if (!is_numeric($val['canton'])) {
            $this->valF['canton'] = NULL;
        } else {
            $this->valF['canton'] = $val['canton'];
        }
        if (!is_numeric($val['circonscription'])) {
            $this->valF['circonscription'] = NULL;
        } else {
            $this->valF['circonscription'] = $val['circonscription'];
        }
        if (!is_numeric($val['commune'])) {
            $this->valF['commune'] = NULL;
        } else {
            $this->valF['commune'] = $val['commune'];
        }
        if (!is_numeric($val['departement'])) {
            $this->valF['departement'] = NULL;
        } else {
            $this->valF['departement'] = $val['departement'];
        }
        if ($val['code_unite'] == "") {
            $this->valF['code_unite'] = NULL;
        } else {
            $this->valF['code_unite'] = $val['code_unite'];
        }
        if (!is_numeric($val['id_reu'])) {
            $this->valF['id_reu'] = NULL;
        } else {
            $this->valF['id_reu'] = $val['id_reu'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }
    /**
     * Methode verifier
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::verifier($val, $this->f->db, null);

        // gestion des dates de validites
        $date_debut = $this->valF['om_validite_debut'];
        $date_fin = $this->valF['om_validite_fin'];

        if ($date_debut != '' and $date_fin != '') {
        
            $date_debut = explode('-', $this->valF['om_validite_debut']);
            $date_fin = explode('-', $this->valF['om_validite_fin']);

            $time_debut = mktime(0, 0, 0, $date_debut[1], $date_debut[2],
                                 $date_debut[0]);
            $time_fin = mktime(0, 0, 0, $date_fin[1], $date_fin[2],
                                 $date_fin[0]);

            if ($time_debut > $time_fin or $time_debut == $time_fin) {
                $this->correct = false;
                $this->addToMessage(__('La date de fin de validite doit etre future a la de debut de validite.'));
            }
        }
    }


    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("unite", "hidden");
            $form->setType("libelle", "text");
            if ($this->is_in_context_of_foreign_key("type_unite", $this->retourformulaire)) {
                $form->setType("type_unite", "selecthiddenstatic");
            } else {
                $form->setType("type_unite", "select");
            }
            $form->setType("ordre", "text");
            $form->setType("adresse1", "text");
            $form->setType("adresse2", "text");
            $form->setType("cp", "text");
            $form->setType("ville", "text");
            $form->setType("geom", "geom");
            $form->setType("coordinates", "text");
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
            if ($this->is_in_context_of_foreign_key("type_unite", $this->retourformulaire)) {
                $form->setType("type_unite_contenu", "selecthiddenstatic");
            } else {
                $form->setType("type_unite_contenu", "select");
            }
            $form->setType("perimetre", "checkbox");
            if ($this->is_in_context_of_foreign_key("canton", $this->retourformulaire)) {
                $form->setType("canton", "selecthiddenstatic");
            } else {
                $form->setType("canton", "select");
            }
            if ($this->is_in_context_of_foreign_key("circonscription", $this->retourformulaire)) {
                $form->setType("circonscription", "selecthiddenstatic");
            } else {
                $form->setType("circonscription", "select");
            }
            if ($this->is_in_context_of_foreign_key("commune", $this->retourformulaire)) {
                $form->setType("commune", "selecthiddenstatic");
            } else {
                $form->setType("commune", "select");
            }
            if ($this->is_in_context_of_foreign_key("departement", $this->retourformulaire)) {
                $form->setType("departement", "selecthiddenstatic");
            } else {
                $form->setType("departement", "select");
            }
            $form->setType("code_unite", "text");
            $form->setType("id_reu", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("unite", "hiddenstatic");
            $form->setType("libelle", "text");
            if ($this->is_in_context_of_foreign_key("type_unite", $this->retourformulaire)) {
                $form->setType("type_unite", "selecthiddenstatic");
            } else {
                $form->setType("type_unite", "select");
            }
            $form->setType("ordre", "text");
            $form->setType("adresse1", "text");
            $form->setType("adresse2", "text");
            $form->setType("cp", "text");
            $form->setType("ville", "text");
            $form->setType("geom", "geom");
            $form->setType("coordinates", "text");
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
            if ($this->is_in_context_of_foreign_key("type_unite", $this->retourformulaire)) {
                $form->setType("type_unite_contenu", "selecthiddenstatic");
            } else {
                $form->setType("type_unite_contenu", "select");
            }
            $form->setType("perimetre", "checkbox");
            if ($this->is_in_context_of_foreign_key("canton", $this->retourformulaire)) {
                $form->setType("canton", "selecthiddenstatic");
            } else {
                $form->setType("canton", "select");
            }
            if ($this->is_in_context_of_foreign_key("circonscription", $this->retourformulaire)) {
                $form->setType("circonscription", "selecthiddenstatic");
            } else {
                $form->setType("circonscription", "select");
            }
            if ($this->is_in_context_of_foreign_key("commune", $this->retourformulaire)) {
                $form->setType("commune", "selecthiddenstatic");
            } else {
                $form->setType("commune", "select");
            }
            if ($this->is_in_context_of_foreign_key("departement", $this->retourformulaire)) {
                $form->setType("departement", "selecthiddenstatic");
            } else {
                $form->setType("departement", "select");
            }
            $form->setType("code_unite", "text");
            $form->setType("id_reu", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("unite", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("type_unite", "selectstatic");
            $form->setType("ordre", "hiddenstatic");
            $form->setType("adresse1", "hiddenstatic");
            $form->setType("adresse2", "hiddenstatic");
            $form->setType("cp", "hiddenstatic");
            $form->setType("ville", "hiddenstatic");
            $form->setType("geom", "geom");
            $form->setType("coordinates", "hiddenstatic");
            $form->setType("om_validite_debut", "hiddenstatic");
            $form->setType("om_validite_fin", "hiddenstatic");
            $form->setType("type_unite_contenu", "selectstatic");
            $form->setType("perimetre", "hiddenstatic");
            $form->setType("canton", "selectstatic");
            $form->setType("circonscription", "selectstatic");
            $form->setType("commune", "selectstatic");
            $form->setType("departement", "selectstatic");
            $form->setType("code_unite", "hiddenstatic");
            $form->setType("id_reu", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("unite", "static");
            $form->setType("libelle", "static");
            $form->setType("type_unite", "selectstatic");
            $form->setType("ordre", "static");
            $form->setType("adresse1", "static");
            $form->setType("adresse2", "static");
            $form->setType("cp", "static");
            $form->setType("ville", "static");
            $form->setType("geom", "geom");
            $form->setType("coordinates", "static");
            $form->setType("om_validite_debut", "datestatic");
            $form->setType("om_validite_fin", "datestatic");
            $form->setType("type_unite_contenu", "selectstatic");
            $form->setType("perimetre", "checkboxstatic");
            $form->setType("canton", "selectstatic");
            $form->setType("circonscription", "selectstatic");
            $form->setType("commune", "selectstatic");
            $form->setType("departement", "selectstatic");
            $form->setType("code_unite", "static");
            $form->setType("id_reu", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('unite','VerifNum(this)');
        $form->setOnchange('type_unite','VerifNum(this)');
        $form->setOnchange('om_validite_debut','fdate(this)');
        $form->setOnchange('om_validite_fin','fdate(this)');
        $form->setOnchange('type_unite_contenu','VerifNum(this)');
        $form->setOnchange('canton','VerifNum(this)');
        $form->setOnchange('circonscription','VerifNum(this)');
        $form->setOnchange('commune','VerifNum(this)');
        $form->setOnchange('departement','VerifNum(this)');
        $form->setOnchange('id_reu','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("unite", 11);
        $form->setTaille("libelle", 30);
        $form->setTaille("type_unite", 11);
        $form->setTaille("ordre", 10);
        $form->setTaille("adresse1", 30);
        $form->setTaille("adresse2", 30);
        $form->setTaille("cp", 10);
        $form->setTaille("ville", 30);
        $form->setTaille("geom", 30);
        $form->setTaille("coordinates", 30);
        $form->setTaille("om_validite_debut", 12);
        $form->setTaille("om_validite_fin", 12);
        $form->setTaille("type_unite_contenu", 11);
        $form->setTaille("perimetre", 1);
        $form->setTaille("canton", 11);
        $form->setTaille("circonscription", 11);
        $form->setTaille("commune", 11);
        $form->setTaille("departement", 11);
        $form->setTaille("code_unite", 10);
        $form->setTaille("id_reu", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("unite", 11);
        $form->setMax("libelle", 100);
        $form->setMax("type_unite", 11);
        $form->setMax("ordre", 10);
        $form->setMax("adresse1", 100);
        $form->setMax("adresse2", 100);
        $form->setMax("cp", 5);
        $form->setMax("ville", 40);
        $form->setMax("geom", 551424);
        $form->setMax("coordinates", 100);
        $form->setMax("om_validite_debut", 12);
        $form->setMax("om_validite_fin", 12);
        $form->setMax("type_unite_contenu", 11);
        $form->setMax("perimetre", 1);
        $form->setMax("canton", 11);
        $form->setMax("circonscription", 11);
        $form->setMax("commune", 11);
        $form->setMax("departement", 11);
        $form->setMax("code_unite", 10);
        $form->setMax("id_reu", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('unite', __('unite'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('type_unite', __('type_unite'));
        $form->setLib('ordre', __('ordre'));
        $form->setLib('adresse1', __('adresse1'));
        $form->setLib('adresse2', __('adresse2'));
        $form->setLib('cp', __('cp'));
        $form->setLib('ville', __('ville'));
        $form->setLib('geom', __('geom'));
        $form->setLib('coordinates', __('coordinates'));
        $form->setLib('om_validite_debut', __('om_validite_debut'));
        $form->setLib('om_validite_fin', __('om_validite_fin'));
        $form->setLib('type_unite_contenu', __('type_unite_contenu'));
        $form->setLib('perimetre', __('perimetre'));
        $form->setLib('canton', __('canton'));
        $form->setLib('circonscription', __('circonscription'));
        $form->setLib('commune', __('commune'));
        $form->setLib('departement', __('departement'));
        $form->setLib('code_unite', __('code_unite'));
        $form->setLib('id_reu', __('id_reu'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // canton
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "canton",
            $this->get_var_sql_forminc__sql("canton"),
            $this->get_var_sql_forminc__sql("canton_by_id"),
            false
        );
        // circonscription
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "circonscription",
            $this->get_var_sql_forminc__sql("circonscription"),
            $this->get_var_sql_forminc__sql("circonscription_by_id"),
            false
        );
        // commune
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "commune",
            $this->get_var_sql_forminc__sql("commune"),
            $this->get_var_sql_forminc__sql("commune_by_id"),
            false
        );
        // departement
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "departement",
            $this->get_var_sql_forminc__sql("departement"),
            $this->get_var_sql_forminc__sql("departement_by_id"),
            false
        );
        // type_unite
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "type_unite",
            $this->get_var_sql_forminc__sql("type_unite"),
            $this->get_var_sql_forminc__sql("type_unite_by_id"),
            false
        );
        // type_unite_contenu
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "type_unite_contenu",
            $this->get_var_sql_forminc__sql("type_unite_contenu"),
            $this->get_var_sql_forminc__sql("type_unite_contenu_by_id"),
            false
        );
        // geom
        if ($maj == 1 || $maj == 3) {
            $contenu = array();
            $contenu[0] = array("unite", $this->getParameter("idx"), "0");
            $form->setSelect("geom", $contenu);
        }
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('canton', $this->retourformulaire))
                $form->setVal('canton', $idxformulaire);
            if($this->is_in_context_of_foreign_key('circonscription', $this->retourformulaire))
                $form->setVal('circonscription', $idxformulaire);
            if($this->is_in_context_of_foreign_key('commune', $this->retourformulaire))
                $form->setVal('commune', $idxformulaire);
            if($this->is_in_context_of_foreign_key('departement', $this->retourformulaire))
                $form->setVal('departement', $idxformulaire);
        }// fin validation
        if ($validation == 0 and $maj == 0) {
            if($this->is_in_context_of_foreign_key('type_unite', $this->retourformulaire))
                $form->setVal('type_unite', $idxformulaire);
            if($this->is_in_context_of_foreign_key('type_unite', $this->retourformulaire))
                $form->setVal('type_unite_contenu', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : animation
        $this->rechercheTable($this->f->db, "animation", "perimetre_aff", $id);
        // Verification de la cle secondaire : delegation
        $this->rechercheTable($this->f->db, "delegation", "unite", $id);
        // Verification de la cle secondaire : election
        $this->rechercheTable($this->f->db, "election", "perimetre", $id);
        // Verification de la cle secondaire : election_unite
        $this->rechercheTable($this->f->db, "election_unite", "unite", $id);
        // Verification de la cle secondaire : lien_unite
        $this->rechercheTable($this->f->db, "lien_unite", "unite_enfant", $id);
        // Verification de la cle secondaire : lien_unite
        $this->rechercheTable($this->f->db, "lien_unite", "unite_parent", $id);
        // Verification de la cle secondaire : plan_unite
        $this->rechercheTable($this->f->db, "plan_unite", "unite", $id);
    }


}
