<?php
//$Id$ 
//gen openMairie le 11/05/2021 14:34

require_once "../obj/om_dbform.class.php";

class election_candidat_gen extends om_dbform {

    protected $_absolute_class_name = "election_candidat";

    var $table = "election_candidat";
    var $clePrimaire = "election_candidat";
    var $typeCle = "N";
    var $required_field = array(
        "candidat",
        "election",
        "election_candidat",
        "ordre"
    );
    var $unique_key = array(
      array("candidat","election"),
      array("election","ordre"),
    );
    var $foreign_keys_extended = array(
        "candidat" => array("candidat", ),
        "election" => array("election", ),
        "parti_politique" => array("parti_politique", ),
    );
    var $abstract_type = array(
        "photo" => "file",
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("election");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "election_candidat",
            "election",
            "candidat",
            "ordre",
            "prefecture",
            "age_moyen",
            "siege",
            "age_moyen_com",
            "siege_com",
            "couleur",
            "photo",
            "parti_politique",
            "age_moyen_mep",
            "siege_mep",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_candidat() {
        return "SELECT candidat.candidat, candidat.libelle FROM ".DB_PREFIXE."candidat ORDER BY candidat.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_candidat_by_id() {
        return "SELECT candidat.candidat, candidat.libelle FROM ".DB_PREFIXE."candidat WHERE candidat = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_election() {
        return "SELECT election.election, election.libelle FROM ".DB_PREFIXE."election ORDER BY election.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_election_by_id() {
        return "SELECT election.election, election.libelle FROM ".DB_PREFIXE."election WHERE election = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_parti_politique() {
        return "SELECT parti_politique.parti_politique, parti_politique.libelle FROM ".DB_PREFIXE."parti_politique ORDER BY parti_politique.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_parti_politique_by_id() {
        return "SELECT parti_politique.parti_politique, parti_politique.libelle FROM ".DB_PREFIXE."parti_politique WHERE parti_politique = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['election_candidat'])) {
            $this->valF['election_candidat'] = ""; // -> requis
        } else {
            $this->valF['election_candidat'] = $val['election_candidat'];
        }
        if (!is_numeric($val['election'])) {
            $this->valF['election'] = ""; // -> requis
        } else {
            $this->valF['election'] = $val['election'];
        }
        if (!is_numeric($val['candidat'])) {
            $this->valF['candidat'] = ""; // -> requis
        } else {
            $this->valF['candidat'] = $val['candidat'];
        }
        if (!is_numeric($val['ordre'])) {
            $this->valF['ordre'] = ""; // -> requis
        } else {
            $this->valF['ordre'] = $val['ordre'];
        }
        if ($val['prefecture'] == "") {
            $this->valF['prefecture'] = NULL;
        } else {
            $this->valF['prefecture'] = $val['prefecture'];
        }
        if (!is_numeric($val['age_moyen'])) {
            $this->valF['age_moyen'] = NULL;
        } else {
            $this->valF['age_moyen'] = $val['age_moyen'];
        }
        if (!is_numeric($val['siege'])) {
            $this->valF['siege'] = NULL;
        } else {
            $this->valF['siege'] = $val['siege'];
        }
        if (!is_numeric($val['age_moyen_com'])) {
            $this->valF['age_moyen_com'] = NULL;
        } else {
            $this->valF['age_moyen_com'] = $val['age_moyen_com'];
        }
        if (!is_numeric($val['siege_com'])) {
            $this->valF['siege_com'] = NULL;
        } else {
            $this->valF['siege_com'] = $val['siege_com'];
        }
        if ($val['couleur'] == "") {
            $this->valF['couleur'] = NULL;
        } else {
            $this->valF['couleur'] = $val['couleur'];
        }
        if ($val['photo'] == "") {
            $this->valF['photo'] = NULL;
        } else {
            $this->valF['photo'] = $val['photo'];
        }
        if (!is_numeric($val['parti_politique'])) {
            $this->valF['parti_politique'] = NULL;
        } else {
            $this->valF['parti_politique'] = $val['parti_politique'];
        }
        if (!is_numeric($val['age_moyen_mep'])) {
            $this->valF['age_moyen_mep'] = 0; // -> default
        } else {
            $this->valF['age_moyen_mep'] = $val['age_moyen_mep'];
        }
        if (!is_numeric($val['siege_mep'])) {
            $this->valF['siege_mep'] = 0; // -> default
        } else {
            $this->valF['siege_mep'] = $val['siege_mep'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("election_candidat", "hidden");
            if ($this->is_in_context_of_foreign_key("election", $this->retourformulaire)) {
                $form->setType("election", "selecthiddenstatic");
            } else {
                $form->setType("election", "select");
            }
            if ($this->is_in_context_of_foreign_key("candidat", $this->retourformulaire)) {
                $form->setType("candidat", "selecthiddenstatic");
            } else {
                $form->setType("candidat", "select");
            }
            $form->setType("ordre", "text");
            $form->setType("prefecture", "text");
            $form->setType("age_moyen", "text");
            $form->setType("siege", "text");
            $form->setType("age_moyen_com", "text");
            $form->setType("siege_com", "text");
            $form->setType("couleur", "text");
            if ($this->retourformulaire == "") {
                $form->setType("photo", "upload");
            } else {
                $form->setType("photo", "upload2");
            }
            if ($this->is_in_context_of_foreign_key("parti_politique", $this->retourformulaire)) {
                $form->setType("parti_politique", "selecthiddenstatic");
            } else {
                $form->setType("parti_politique", "select");
            }
            $form->setType("age_moyen_mep", "text");
            $form->setType("siege_mep", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("election_candidat", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("election", $this->retourformulaire)) {
                $form->setType("election", "selecthiddenstatic");
            } else {
                $form->setType("election", "select");
            }
            if ($this->is_in_context_of_foreign_key("candidat", $this->retourformulaire)) {
                $form->setType("candidat", "selecthiddenstatic");
            } else {
                $form->setType("candidat", "select");
            }
            $form->setType("ordre", "text");
            $form->setType("prefecture", "text");
            $form->setType("age_moyen", "text");
            $form->setType("siege", "text");
            $form->setType("age_moyen_com", "text");
            $form->setType("siege_com", "text");
            $form->setType("couleur", "text");
            if ($this->retourformulaire == "") {
                $form->setType("photo", "upload");
            } else {
                $form->setType("photo", "upload2");
            }
            if ($this->is_in_context_of_foreign_key("parti_politique", $this->retourformulaire)) {
                $form->setType("parti_politique", "selecthiddenstatic");
            } else {
                $form->setType("parti_politique", "select");
            }
            $form->setType("age_moyen_mep", "text");
            $form->setType("siege_mep", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("election_candidat", "hiddenstatic");
            $form->setType("election", "selectstatic");
            $form->setType("candidat", "selectstatic");
            $form->setType("ordre", "hiddenstatic");
            $form->setType("prefecture", "hiddenstatic");
            $form->setType("age_moyen", "hiddenstatic");
            $form->setType("siege", "hiddenstatic");
            $form->setType("age_moyen_com", "hiddenstatic");
            $form->setType("siege_com", "hiddenstatic");
            $form->setType("couleur", "hiddenstatic");
            $form->setType("photo", "filestatic");
            $form->setType("parti_politique", "selectstatic");
            $form->setType("age_moyen_mep", "hiddenstatic");
            $form->setType("siege_mep", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("election_candidat", "static");
            $form->setType("election", "selectstatic");
            $form->setType("candidat", "selectstatic");
            $form->setType("ordre", "static");
            $form->setType("prefecture", "static");
            $form->setType("age_moyen", "static");
            $form->setType("siege", "static");
            $form->setType("age_moyen_com", "static");
            $form->setType("siege_com", "static");
            $form->setType("couleur", "static");
            $form->setType("photo", "file");
            $form->setType("parti_politique", "selectstatic");
            $form->setType("age_moyen_mep", "static");
            $form->setType("siege_mep", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('election_candidat','VerifNum(this)');
        $form->setOnchange('election','VerifNum(this)');
        $form->setOnchange('candidat','VerifNum(this)');
        $form->setOnchange('ordre','VerifNum(this)');
        $form->setOnchange('age_moyen','VerifFloat(this)');
        $form->setOnchange('siege','VerifNum(this)');
        $form->setOnchange('age_moyen_com','VerifFloat(this)');
        $form->setOnchange('siege_com','VerifNum(this)');
        $form->setOnchange('parti_politique','VerifNum(this)');
        $form->setOnchange('age_moyen_mep','VerifNum(this)');
        $form->setOnchange('siege_mep','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("election_candidat", 11);
        $form->setTaille("election", 11);
        $form->setTaille("candidat", 11);
        $form->setTaille("ordre", 11);
        $form->setTaille("prefecture", 10);
        $form->setTaille("age_moyen", 10);
        $form->setTaille("siege", 11);
        $form->setTaille("age_moyen_com", 10);
        $form->setTaille("siege_com", 11);
        $form->setTaille("couleur", 13);
        $form->setTaille("photo", 30);
        $form->setTaille("parti_politique", 11);
        $form->setTaille("age_moyen_mep", 11);
        $form->setTaille("siege_mep", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("election_candidat", 11);
        $form->setMax("election", 11);
        $form->setMax("candidat", 11);
        $form->setMax("ordre", 11);
        $form->setMax("prefecture", 10);
        $form->setMax("age_moyen", -5);
        $form->setMax("siege", 11);
        $form->setMax("age_moyen_com", -5);
        $form->setMax("siege_com", 11);
        $form->setMax("couleur", 13);
        $form->setMax("photo", 100);
        $form->setMax("parti_politique", 11);
        $form->setMax("age_moyen_mep", 11);
        $form->setMax("siege_mep", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('election_candidat', __('election_candidat'));
        $form->setLib('election', __('election'));
        $form->setLib('candidat', __('candidat'));
        $form->setLib('ordre', __('ordre'));
        $form->setLib('prefecture', __('prefecture'));
        $form->setLib('age_moyen', __('age_moyen'));
        $form->setLib('siege', __('siege'));
        $form->setLib('age_moyen_com', __('age_moyen_com'));
        $form->setLib('siege_com', __('siege_com'));
        $form->setLib('couleur', __('couleur'));
        $form->setLib('photo', __('photo'));
        $form->setLib('parti_politique', __('parti_politique'));
        $form->setLib('age_moyen_mep', __('age_moyen_mep'));
        $form->setLib('siege_mep', __('siege_mep'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // candidat
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "candidat",
            $this->get_var_sql_forminc__sql("candidat"),
            $this->get_var_sql_forminc__sql("candidat_by_id"),
            false
        );
        // election
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "election",
            $this->get_var_sql_forminc__sql("election"),
            $this->get_var_sql_forminc__sql("election_by_id"),
            false
        );
        // parti_politique
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "parti_politique",
            $this->get_var_sql_forminc__sql("parti_politique"),
            $this->get_var_sql_forminc__sql("parti_politique_by_id"),
            false
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('candidat', $this->retourformulaire))
                $form->setVal('candidat', $idxformulaire);
            if($this->is_in_context_of_foreign_key('election', $this->retourformulaire))
                $form->setVal('election', $idxformulaire);
            if($this->is_in_context_of_foreign_key('parti_politique', $this->retourformulaire))
                $form->setVal('parti_politique', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : election_resultat
        $this->rechercheTable($this->f->db, "election_resultat", "election_candidat", $id);
    }


}
