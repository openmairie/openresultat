<?php
//$Id$ 
//gen openMairie le 29/04/2021 17:19

require_once "../obj/om_dbform.class.php";

class plan_gen extends om_dbform {

    protected $_absolute_class_name = "plan";

    var $table = "plan";
    var $clePrimaire = "plan";
    var $typeCle = "N";
    var $required_field = array(
        "image_plan",
        "img_unite_arrivee",
        "img_unite_non_arrivee",
        "libelle",
        "plan"
    );
    
    var $foreign_keys_extended = array(
    );
    var $abstract_type = array(
        "image_plan" => "file",
        "img_unite_arrivee" => "file",
        "img_unite_non_arrivee" => "file",
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "plan",
            "libelle",
            "image_plan",
            "img_unite_arrivee",
            "img_unite_non_arrivee",
            "par_defaut",
            "largeur_icone",
        );
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['plan'])) {
            $this->valF['plan'] = ""; // -> requis
        } else {
            $this->valF['plan'] = $val['plan'];
        }
        $this->valF['libelle'] = $val['libelle'];
        $this->valF['image_plan'] = $val['image_plan'];
        $this->valF['img_unite_arrivee'] = $val['img_unite_arrivee'];
        $this->valF['img_unite_non_arrivee'] = $val['img_unite_non_arrivee'];
        if ($val['par_defaut'] == 1 || $val['par_defaut'] == "t" || $val['par_defaut'] == "Oui") {
            $this->valF['par_defaut'] = true;
        } else {
            $this->valF['par_defaut'] = false;
        }
        if (!is_numeric($val['largeur_icone'])) {
            $this->valF['largeur_icone'] = NULL;
        } else {
            $this->valF['largeur_icone'] = $val['largeur_icone'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("plan", "hidden");
            $form->setType("libelle", "text");
            if ($this->retourformulaire == "") {
                $form->setType("image_plan", "upload");
            } else {
                $form->setType("image_plan", "upload2");
            }
            if ($this->retourformulaire == "") {
                $form->setType("img_unite_arrivee", "upload");
            } else {
                $form->setType("img_unite_arrivee", "upload2");
            }
            if ($this->retourformulaire == "") {
                $form->setType("img_unite_non_arrivee", "upload");
            } else {
                $form->setType("img_unite_non_arrivee", "upload2");
            }
            $form->setType("par_defaut", "checkbox");
            $form->setType("largeur_icone", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("plan", "hiddenstatic");
            $form->setType("libelle", "text");
            if ($this->retourformulaire == "") {
                $form->setType("image_plan", "upload");
            } else {
                $form->setType("image_plan", "upload2");
            }
            if ($this->retourformulaire == "") {
                $form->setType("img_unite_arrivee", "upload");
            } else {
                $form->setType("img_unite_arrivee", "upload2");
            }
            if ($this->retourformulaire == "") {
                $form->setType("img_unite_non_arrivee", "upload");
            } else {
                $form->setType("img_unite_non_arrivee", "upload2");
            }
            $form->setType("par_defaut", "checkbox");
            $form->setType("largeur_icone", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("plan", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("image_plan", "filestatic");
            $form->setType("img_unite_arrivee", "filestatic");
            $form->setType("img_unite_non_arrivee", "filestatic");
            $form->setType("par_defaut", "hiddenstatic");
            $form->setType("largeur_icone", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("plan", "static");
            $form->setType("libelle", "static");
            $form->setType("image_plan", "file");
            $form->setType("img_unite_arrivee", "file");
            $form->setType("img_unite_non_arrivee", "file");
            $form->setType("par_defaut", "checkboxstatic");
            $form->setType("largeur_icone", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('plan','VerifNum(this)');
        $form->setOnchange('largeur_icone','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("plan", 11);
        $form->setTaille("libelle", 30);
        $form->setTaille("image_plan", 30);
        $form->setTaille("img_unite_arrivee", 30);
        $form->setTaille("img_unite_non_arrivee", 30);
        $form->setTaille("par_defaut", 1);
        $form->setTaille("largeur_icone", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("plan", 11);
        $form->setMax("libelle", 50);
        $form->setMax("image_plan", 100);
        $form->setMax("img_unite_arrivee", 100);
        $form->setMax("img_unite_non_arrivee", 100);
        $form->setMax("par_defaut", 1);
        $form->setMax("largeur_icone", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('plan', __('plan'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('image_plan', __('image_plan'));
        $form->setLib('img_unite_arrivee', __('img_unite_arrivee'));
        $form->setLib('img_unite_non_arrivee', __('img_unite_non_arrivee'));
        $form->setLib('par_defaut', __('par_defaut'));
        $form->setLib('largeur_icone', __('largeur_icone'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : plan_election
        $this->rechercheTable($this->f->db, "plan_election", "plan", $id);
        // Verification de la cle secondaire : plan_unite
        $this->rechercheTable($this->f->db, "plan_unite", "plan", $id);
    }


}
