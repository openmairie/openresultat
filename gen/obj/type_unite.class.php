<?php
//$Id$ 
//gen openMairie le 05/03/2021 14:09

require_once "../obj/om_dbform.class.php";

class type_unite_gen extends om_dbform {

    protected $_absolute_class_name = "type_unite";

    var $table = "type_unite";
    var $clePrimaire = "type_unite";
    var $typeCle = "N";
    var $required_field = array(
        "hierarchie",
        "libelle",
        "type_unite"
    );
    
    var $foreign_keys_extended = array(
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "type_unite",
            "libelle",
            "hierarchie",
            "bureau_vote",
        );
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['type_unite'])) {
            $this->valF['type_unite'] = ""; // -> requis
        } else {
            $this->valF['type_unite'] = $val['type_unite'];
        }
        $this->valF['libelle'] = $val['libelle'];
        if (!is_numeric($val['hierarchie'])) {
            $this->valF['hierarchie'] = ""; // -> requis
        } else {
            $this->valF['hierarchie'] = $val['hierarchie'];
        }
        if ($val['bureau_vote'] == 1 || $val['bureau_vote'] == "t" || $val['bureau_vote'] == "Oui") {
            $this->valF['bureau_vote'] = true;
        } else {
            $this->valF['bureau_vote'] = false;
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("type_unite", "hidden");
            $form->setType("libelle", "text");
            $form->setType("hierarchie", "text");
            $form->setType("bureau_vote", "checkbox");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("type_unite", "hiddenstatic");
            $form->setType("libelle", "text");
            $form->setType("hierarchie", "text");
            $form->setType("bureau_vote", "checkbox");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("type_unite", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("hierarchie", "hiddenstatic");
            $form->setType("bureau_vote", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("type_unite", "static");
            $form->setType("libelle", "static");
            $form->setType("hierarchie", "static");
            $form->setType("bureau_vote", "checkboxstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('type_unite','VerifNum(this)');
        $form->setOnchange('hierarchie','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("type_unite", 11);
        $form->setTaille("libelle", 30);
        $form->setTaille("hierarchie", 11);
        $form->setTaille("bureau_vote", 1);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("type_unite", 11);
        $form->setMax("libelle", 30);
        $form->setMax("hierarchie", 11);
        $form->setMax("bureau_vote", 1);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('type_unite', __('type_unite'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('hierarchie', __('hierarchie'));
        $form->setLib('bureau_vote', __('bureau_vote'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : animation
        $this->rechercheTable($this->f->db, "animation", "type_aff", $id);
        // Verification de la cle secondaire : unite
        $this->rechercheTable($this->f->db, "unite", "type_unite", $id);
        // Verification de la cle secondaire : unite
        $this->rechercheTable($this->f->db, "unite", "type_unite_contenu", $id);
    }


}
