<?php
//$Id$ 
//gen openMairie le 06/05/2021 21:39

require_once "../obj/om_dbform.class.php";

class participation_election_gen extends om_dbform {

    protected $_absolute_class_name = "participation_election";

    var $table = "participation_election";
    var $clePrimaire = "participation_election";
    var $typeCle = "N";
    var $required_field = array(
        "election",
        "participation_election",
        "tranche"
    );
    var $unique_key = array(
      array("election","tranche"),
    );
    var $foreign_keys_extended = array(
        "election" => array("election", ),
        "tranche" => array("tranche", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("election");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "participation_election",
            "election",
            "tranche",
            "date_derniere_modif",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_election() {
        return "SELECT election.election, election.libelle FROM ".DB_PREFIXE."election ORDER BY election.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_election_by_id() {
        return "SELECT election.election, election.libelle FROM ".DB_PREFIXE."election WHERE election = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_tranche() {
        return "SELECT tranche.tranche, tranche.libelle FROM ".DB_PREFIXE."tranche ORDER BY tranche.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_tranche_by_id() {
        return "SELECT tranche.tranche, tranche.libelle FROM ".DB_PREFIXE."tranche WHERE tranche = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['participation_election'])) {
            $this->valF['participation_election'] = ""; // -> requis
        } else {
            $this->valF['participation_election'] = $val['participation_election'];
        }
        if (!is_numeric($val['election'])) {
            $this->valF['election'] = ""; // -> requis
        } else {
            $this->valF['election'] = $val['election'];
        }
        if (!is_numeric($val['tranche'])) {
            $this->valF['tranche'] = ""; // -> requis
        } else {
            $this->valF['tranche'] = $val['tranche'];
        }
        if (!is_numeric($val['date_derniere_modif'])) {
            $this->valF['date_derniere_modif'] = NULL;
        } else {
            $this->valF['date_derniere_modif'] = $val['date_derniere_modif'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("participation_election", "hidden");
            if ($this->is_in_context_of_foreign_key("election", $this->retourformulaire)) {
                $form->setType("election", "selecthiddenstatic");
            } else {
                $form->setType("election", "select");
            }
            if ($this->is_in_context_of_foreign_key("tranche", $this->retourformulaire)) {
                $form->setType("tranche", "selecthiddenstatic");
            } else {
                $form->setType("tranche", "select");
            }
            $form->setType("date_derniere_modif", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("participation_election", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("election", $this->retourformulaire)) {
                $form->setType("election", "selecthiddenstatic");
            } else {
                $form->setType("election", "select");
            }
            if ($this->is_in_context_of_foreign_key("tranche", $this->retourformulaire)) {
                $form->setType("tranche", "selecthiddenstatic");
            } else {
                $form->setType("tranche", "select");
            }
            $form->setType("date_derniere_modif", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("participation_election", "hiddenstatic");
            $form->setType("election", "selectstatic");
            $form->setType("tranche", "selectstatic");
            $form->setType("date_derniere_modif", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("participation_election", "static");
            $form->setType("election", "selectstatic");
            $form->setType("tranche", "selectstatic");
            $form->setType("date_derniere_modif", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('participation_election','VerifNum(this)');
        $form->setOnchange('election','VerifNum(this)');
        $form->setOnchange('tranche','VerifNum(this)');
        $form->setOnchange('date_derniere_modif','VerifFloat(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("participation_election", 11);
        $form->setTaille("election", 11);
        $form->setTaille("tranche", 11);
        $form->setTaille("date_derniere_modif", 20);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("participation_election", 11);
        $form->setMax("election", 11);
        $form->setMax("tranche", 11);
        $form->setMax("date_derniere_modif", 20);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('participation_election', __('participation_election'));
        $form->setLib('election', __('election'));
        $form->setLib('tranche', __('tranche'));
        $form->setLib('date_derniere_modif', __('date_derniere_modif'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // election
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "election",
            $this->get_var_sql_forminc__sql("election"),
            $this->get_var_sql_forminc__sql("election_by_id"),
            false
        );
        // tranche
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "tranche",
            $this->get_var_sql_forminc__sql("tranche"),
            $this->get_var_sql_forminc__sql("tranche_by_id"),
            false
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('election', $this->retourformulaire))
                $form->setVal('election', $idxformulaire);
            if($this->is_in_context_of_foreign_key('tranche', $this->retourformulaire))
                $form->setVal('tranche', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : participation_unite
        $this->rechercheTable($this->f->db, "participation_unite", "participation_election", $id);
    }


}
