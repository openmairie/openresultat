<?php
//$Id$ 
//gen openMairie le 13/04/2021 16:59

require_once "../obj/om_dbform.class.php";

class candidat_gen extends om_dbform {

    protected $_absolute_class_name = "candidat";

    var $table = "candidat";
    var $clePrimaire = "candidat";
    var $typeCle = "N";
    var $required_field = array(
        "candidat",
        "libelle"
    );
    
    var $foreign_keys_extended = array(
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "candidat",
            "libelle",
            "libelle_liste",
        );
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['candidat'])) {
            $this->valF['candidat'] = ""; // -> requis
        } else {
            $this->valF['candidat'] = $val['candidat'];
        }
        $this->valF['libelle'] = $val['libelle'];
        if ($val['libelle_liste'] == "") {
            $this->valF['libelle_liste'] = NULL;
        } else {
            $this->valF['libelle_liste'] = $val['libelle_liste'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("candidat", "hidden");
            $form->setType("libelle", "text");
            $form->setType("libelle_liste", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("candidat", "hiddenstatic");
            $form->setType("libelle", "text");
            $form->setType("libelle_liste", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("candidat", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("libelle_liste", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("candidat", "static");
            $form->setType("libelle", "static");
            $form->setType("libelle_liste", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('candidat','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("candidat", 11);
        $form->setTaille("libelle", 30);
        $form->setTaille("libelle_liste", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("candidat", 11);
        $form->setMax("libelle", 100);
        $form->setMax("libelle_liste", 100);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('candidat', __('candidat'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('libelle_liste', __('libelle_liste'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : election_candidat
        $this->rechercheTable($this->f->db, "election_candidat", "candidat", $id);
    }


}
