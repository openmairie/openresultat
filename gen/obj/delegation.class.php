<?php
//$Id$ 
//gen openMairie le 06/05/2021 21:39

require_once "../obj/om_dbform.class.php";

class delegation_gen extends om_dbform {

    protected $_absolute_class_name = "delegation";

    var $table = "delegation";
    var $clePrimaire = "delegation";
    var $typeCle = "N";
    var $required_field = array(
        "acteur",
        "delegation",
        "election",
        "unite"
    );
    
    var $foreign_keys_extended = array(
        "acteur" => array("acteur", ),
        "election" => array("election", ),
        "unite" => array("unite", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("election");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "delegation",
            "election",
            "unite",
            "acteur",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_acteur() {
        return "SELECT acteur.acteur, acteur.nom FROM ".DB_PREFIXE."acteur ORDER BY acteur.nom ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_acteur_by_id() {
        return "SELECT acteur.acteur, acteur.nom FROM ".DB_PREFIXE."acteur WHERE acteur = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_election() {
        return "SELECT election.election, election.libelle FROM ".DB_PREFIXE."election ORDER BY election.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_election_by_id() {
        return "SELECT election.election, election.libelle FROM ".DB_PREFIXE."election WHERE election = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_unite() {
        return "SELECT unite.unite, unite.libelle FROM ".DB_PREFIXE."unite WHERE ((unite.om_validite_debut IS NULL AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)) OR (unite.om_validite_debut <= CURRENT_DATE AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE))) ORDER BY unite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_unite_by_id() {
        return "SELECT unite.unite, unite.libelle FROM ".DB_PREFIXE."unite WHERE unite = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['delegation'])) {
            $this->valF['delegation'] = ""; // -> requis
        } else {
            $this->valF['delegation'] = $val['delegation'];
        }
        if (!is_numeric($val['election'])) {
            $this->valF['election'] = ""; // -> requis
        } else {
            $this->valF['election'] = $val['election'];
        }
        if (!is_numeric($val['unite'])) {
            $this->valF['unite'] = ""; // -> requis
        } else {
            $this->valF['unite'] = $val['unite'];
        }
        if (!is_numeric($val['acteur'])) {
            $this->valF['acteur'] = ""; // -> requis
        } else {
            $this->valF['acteur'] = $val['acteur'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("delegation", "hidden");
            if ($this->is_in_context_of_foreign_key("election", $this->retourformulaire)) {
                $form->setType("election", "selecthiddenstatic");
            } else {
                $form->setType("election", "select");
            }
            if ($this->is_in_context_of_foreign_key("unite", $this->retourformulaire)) {
                $form->setType("unite", "selecthiddenstatic");
            } else {
                $form->setType("unite", "select");
            }
            if ($this->is_in_context_of_foreign_key("acteur", $this->retourformulaire)) {
                $form->setType("acteur", "selecthiddenstatic");
            } else {
                $form->setType("acteur", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("delegation", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("election", $this->retourformulaire)) {
                $form->setType("election", "selecthiddenstatic");
            } else {
                $form->setType("election", "select");
            }
            if ($this->is_in_context_of_foreign_key("unite", $this->retourformulaire)) {
                $form->setType("unite", "selecthiddenstatic");
            } else {
                $form->setType("unite", "select");
            }
            if ($this->is_in_context_of_foreign_key("acteur", $this->retourformulaire)) {
                $form->setType("acteur", "selecthiddenstatic");
            } else {
                $form->setType("acteur", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("delegation", "hiddenstatic");
            $form->setType("election", "selectstatic");
            $form->setType("unite", "selectstatic");
            $form->setType("acteur", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("delegation", "static");
            $form->setType("election", "selectstatic");
            $form->setType("unite", "selectstatic");
            $form->setType("acteur", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('delegation','VerifNum(this)');
        $form->setOnchange('election','VerifNum(this)');
        $form->setOnchange('unite','VerifNum(this)');
        $form->setOnchange('acteur','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("delegation", 11);
        $form->setTaille("election", 11);
        $form->setTaille("unite", 11);
        $form->setTaille("acteur", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("delegation", 11);
        $form->setMax("election", 11);
        $form->setMax("unite", 11);
        $form->setMax("acteur", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('delegation', __('delegation'));
        $form->setLib('election', __('election'));
        $form->setLib('unite', __('unite'));
        $form->setLib('acteur', __('acteur'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // acteur
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "acteur",
            $this->get_var_sql_forminc__sql("acteur"),
            $this->get_var_sql_forminc__sql("acteur_by_id"),
            false
        );
        // election
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "election",
            $this->get_var_sql_forminc__sql("election"),
            $this->get_var_sql_forminc__sql("election_by_id"),
            false
        );
        // unite
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "unite",
            $this->get_var_sql_forminc__sql("unite"),
            $this->get_var_sql_forminc__sql("unite_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('acteur', $this->retourformulaire))
                $form->setVal('acteur', $idxformulaire);
            if($this->is_in_context_of_foreign_key('election', $this->retourformulaire))
                $form->setVal('election', $idxformulaire);
            if($this->is_in_context_of_foreign_key('unite', $this->retourformulaire))
                $form->setVal('unite', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
