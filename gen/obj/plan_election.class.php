<?php
//$Id$ 
//gen openMairie le 06/05/2021 21:39

require_once "../obj/om_dbform.class.php";

class plan_election_gen extends om_dbform {

    protected $_absolute_class_name = "plan_election";

    var $table = "plan_election";
    var $clePrimaire = "plan_election";
    var $typeCle = "N";
    var $required_field = array(
        "election",
        "plan",
        "plan_election"
    );
    var $unique_key = array(
      array("election","plan"),
    );
    var $foreign_keys_extended = array(
        "election" => array("election", ),
        "plan" => array("plan", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("plan");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "plan_election",
            "plan",
            "election",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_election() {
        return "SELECT election.election, election.libelle FROM ".DB_PREFIXE."election ORDER BY election.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_election_by_id() {
        return "SELECT election.election, election.libelle FROM ".DB_PREFIXE."election WHERE election = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_plan() {
        return "SELECT plan.plan, plan.libelle FROM ".DB_PREFIXE."plan ORDER BY plan.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_plan_by_id() {
        return "SELECT plan.plan, plan.libelle FROM ".DB_PREFIXE."plan WHERE plan = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['plan_election'])) {
            $this->valF['plan_election'] = ""; // -> requis
        } else {
            $this->valF['plan_election'] = $val['plan_election'];
        }
        if (!is_numeric($val['plan'])) {
            $this->valF['plan'] = ""; // -> requis
        } else {
            $this->valF['plan'] = $val['plan'];
        }
        if (!is_numeric($val['election'])) {
            $this->valF['election'] = ""; // -> requis
        } else {
            $this->valF['election'] = $val['election'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("plan_election", "hidden");
            if ($this->is_in_context_of_foreign_key("plan", $this->retourformulaire)) {
                $form->setType("plan", "selecthiddenstatic");
            } else {
                $form->setType("plan", "select");
            }
            if ($this->is_in_context_of_foreign_key("election", $this->retourformulaire)) {
                $form->setType("election", "selecthiddenstatic");
            } else {
                $form->setType("election", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("plan_election", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("plan", $this->retourformulaire)) {
                $form->setType("plan", "selecthiddenstatic");
            } else {
                $form->setType("plan", "select");
            }
            if ($this->is_in_context_of_foreign_key("election", $this->retourformulaire)) {
                $form->setType("election", "selecthiddenstatic");
            } else {
                $form->setType("election", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("plan_election", "hiddenstatic");
            $form->setType("plan", "selectstatic");
            $form->setType("election", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("plan_election", "static");
            $form->setType("plan", "selectstatic");
            $form->setType("election", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('plan_election','VerifNum(this)');
        $form->setOnchange('plan','VerifNum(this)');
        $form->setOnchange('election','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("plan_election", 11);
        $form->setTaille("plan", 11);
        $form->setTaille("election", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("plan_election", 11);
        $form->setMax("plan", 11);
        $form->setMax("election", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('plan_election', __('plan_election'));
        $form->setLib('plan', __('plan'));
        $form->setLib('election', __('election'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // election
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "election",
            $this->get_var_sql_forminc__sql("election"),
            $this->get_var_sql_forminc__sql("election_by_id"),
            false
        );
        // plan
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "plan",
            $this->get_var_sql_forminc__sql("plan"),
            $this->get_var_sql_forminc__sql("plan_by_id"),
            false
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('election', $this->retourformulaire))
                $form->setVal('election', $idxformulaire);
            if($this->is_in_context_of_foreign_key('plan', $this->retourformulaire))
                $form->setVal('plan', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
