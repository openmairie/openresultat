<?php
//$Id$ 
//gen openMairie le 25/05/2022 15:23

require_once "../obj/om_dbform.class.php";

class participation_unite_gen extends om_dbform {

    protected $_absolute_class_name = "participation_unite";

    var $table = "participation_unite";
    var $clePrimaire = "participation_unite";
    var $typeCle = "N";
    var $required_field = array(
        "election_unite",
        "participation_election",
        "participation_unite"
    );
    var $unique_key = array(
      array("election_unite","participation_election"),
    );
    var $foreign_keys_extended = array(
        "election_unite" => array("election_unite", "election_unite_overlay", ),
        "participation_election" => array("participation_election", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("election_unite");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "participation_unite",
            "election_unite",
            "participation_election",
            "votant",
            "saisie",
            "envoi_aff",
            "envoi_web",
            "date_derniere_modif",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_election_unite() {
        return "SELECT election_unite.election_unite, election_unite.election FROM ".DB_PREFIXE."election_unite ORDER BY election_unite.election ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_election_unite_by_id() {
        return "SELECT election_unite.election_unite, election_unite.election FROM ".DB_PREFIXE."election_unite WHERE election_unite = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_participation_election() {
        return "SELECT participation_election.participation_election, participation_election.election FROM ".DB_PREFIXE."participation_election ORDER BY participation_election.election ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_participation_election_by_id() {
        return "SELECT participation_election.participation_election, participation_election.election FROM ".DB_PREFIXE."participation_election WHERE participation_election = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['participation_unite'])) {
            $this->valF['participation_unite'] = ""; // -> requis
        } else {
            $this->valF['participation_unite'] = $val['participation_unite'];
        }
        if (!is_numeric($val['election_unite'])) {
            $this->valF['election_unite'] = ""; // -> requis
        } else {
            $this->valF['election_unite'] = $val['election_unite'];
        }
        if (!is_numeric($val['participation_election'])) {
            $this->valF['participation_election'] = ""; // -> requis
        } else {
            $this->valF['participation_election'] = $val['participation_election'];
        }
        if (!is_numeric($val['votant'])) {
            $this->valF['votant'] = NULL;
        } else {
            $this->valF['votant'] = $val['votant'];
        }
        if ($val['saisie'] == 1 || $val['saisie'] == "t" || $val['saisie'] == "Oui") {
            $this->valF['saisie'] = true;
        } else {
            $this->valF['saisie'] = false;
        }
        if ($val['envoi_aff'] == 1 || $val['envoi_aff'] == "t" || $val['envoi_aff'] == "Oui") {
            $this->valF['envoi_aff'] = true;
        } else {
            $this->valF['envoi_aff'] = false;
        }
        if ($val['envoi_web'] == 1 || $val['envoi_web'] == "t" || $val['envoi_web'] == "Oui") {
            $this->valF['envoi_web'] = true;
        } else {
            $this->valF['envoi_web'] = false;
        }
        if (!is_numeric($val['date_derniere_modif'])) {
            $this->valF['date_derniere_modif'] = NULL;
        } else {
            $this->valF['date_derniere_modif'] = $val['date_derniere_modif'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("participation_unite", "hidden");
            if ($this->is_in_context_of_foreign_key("election_unite", $this->retourformulaire)) {
                $form->setType("election_unite", "selecthiddenstatic");
            } else {
                $form->setType("election_unite", "select");
            }
            if ($this->is_in_context_of_foreign_key("participation_election", $this->retourformulaire)) {
                $form->setType("participation_election", "selecthiddenstatic");
            } else {
                $form->setType("participation_election", "select");
            }
            $form->setType("votant", "text");
            $form->setType("saisie", "checkbox");
            $form->setType("envoi_aff", "checkbox");
            $form->setType("envoi_web", "checkbox");
            $form->setType("date_derniere_modif", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("participation_unite", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("election_unite", $this->retourformulaire)) {
                $form->setType("election_unite", "selecthiddenstatic");
            } else {
                $form->setType("election_unite", "select");
            }
            if ($this->is_in_context_of_foreign_key("participation_election", $this->retourformulaire)) {
                $form->setType("participation_election", "selecthiddenstatic");
            } else {
                $form->setType("participation_election", "select");
            }
            $form->setType("votant", "text");
            $form->setType("saisie", "checkbox");
            $form->setType("envoi_aff", "checkbox");
            $form->setType("envoi_web", "checkbox");
            $form->setType("date_derniere_modif", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("participation_unite", "hiddenstatic");
            $form->setType("election_unite", "selectstatic");
            $form->setType("participation_election", "selectstatic");
            $form->setType("votant", "hiddenstatic");
            $form->setType("saisie", "hiddenstatic");
            $form->setType("envoi_aff", "hiddenstatic");
            $form->setType("envoi_web", "hiddenstatic");
            $form->setType("date_derniere_modif", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("participation_unite", "static");
            $form->setType("election_unite", "selectstatic");
            $form->setType("participation_election", "selectstatic");
            $form->setType("votant", "static");
            $form->setType("saisie", "checkboxstatic");
            $form->setType("envoi_aff", "checkboxstatic");
            $form->setType("envoi_web", "checkboxstatic");
            $form->setType("date_derniere_modif", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('participation_unite','VerifNum(this)');
        $form->setOnchange('election_unite','VerifNum(this)');
        $form->setOnchange('participation_election','VerifNum(this)');
        $form->setOnchange('votant','VerifNum(this)');
        $form->setOnchange('date_derniere_modif','VerifFloat(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("participation_unite", 11);
        $form->setTaille("election_unite", 11);
        $form->setTaille("participation_election", 11);
        $form->setTaille("votant", 11);
        $form->setTaille("saisie", 1);
        $form->setTaille("envoi_aff", 1);
        $form->setTaille("envoi_web", 1);
        $form->setTaille("date_derniere_modif", 20);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("participation_unite", 11);
        $form->setMax("election_unite", 11);
        $form->setMax("participation_election", 11);
        $form->setMax("votant", 11);
        $form->setMax("saisie", 1);
        $form->setMax("envoi_aff", 1);
        $form->setMax("envoi_web", 1);
        $form->setMax("date_derniere_modif", 20);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('participation_unite', __('participation_unite'));
        $form->setLib('election_unite', __('election_unite'));
        $form->setLib('participation_election', __('participation_election'));
        $form->setLib('votant', __('votant'));
        $form->setLib('saisie', __('saisie'));
        $form->setLib('envoi_aff', __('envoi_aff'));
        $form->setLib('envoi_web', __('envoi_web'));
        $form->setLib('date_derniere_modif', __('date_derniere_modif'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // election_unite
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "election_unite",
            $this->get_var_sql_forminc__sql("election_unite"),
            $this->get_var_sql_forminc__sql("election_unite_by_id"),
            false
        );
        // participation_election
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "participation_election",
            $this->get_var_sql_forminc__sql("participation_election"),
            $this->get_var_sql_forminc__sql("participation_election_by_id"),
            false
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('election_unite', $this->retourformulaire))
                $form->setVal('election_unite', $idxformulaire);
            if($this->is_in_context_of_foreign_key('participation_election', $this->retourformulaire))
                $form->setVal('participation_election', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
