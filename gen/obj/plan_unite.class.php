<?php
//$Id$ 
//gen openMairie le 30/04/2021 10:47

require_once "../obj/om_dbform.class.php";

class plan_unite_gen extends om_dbform {

    protected $_absolute_class_name = "plan_unite";

    var $table = "plan_unite";
    var $clePrimaire = "plan_unite";
    var $typeCle = "N";
    var $required_field = array(
        "plan",
        "plan_unite",
        "unite"
    );
    var $unique_key = array(
      array("plan","unite"),
    );
    var $foreign_keys_extended = array(
        "plan" => array("plan", ),
        "unite" => array("unite", ),
    );
    var $abstract_type = array(
        "img_unite_arrivee" => "file",
        "img_unite_non_arrivee" => "file",
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("plan");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "plan_unite",
            "plan",
            "unite",
            "img_unite_arrivee",
            "img_unite_non_arrivee",
            "position_x",
            "position_y",
            "largeur_icone",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_plan() {
        return "SELECT plan.plan, plan.libelle FROM ".DB_PREFIXE."plan ORDER BY plan.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_plan_by_id() {
        return "SELECT plan.plan, plan.libelle FROM ".DB_PREFIXE."plan WHERE plan = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_unite() {
        return "SELECT unite.unite, unite.libelle FROM ".DB_PREFIXE."unite WHERE ((unite.om_validite_debut IS NULL AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)) OR (unite.om_validite_debut <= CURRENT_DATE AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE))) ORDER BY unite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_unite_by_id() {
        return "SELECT unite.unite, unite.libelle FROM ".DB_PREFIXE."unite WHERE unite = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['plan_unite'])) {
            $this->valF['plan_unite'] = ""; // -> requis
        } else {
            $this->valF['plan_unite'] = $val['plan_unite'];
        }
        if (!is_numeric($val['plan'])) {
            $this->valF['plan'] = ""; // -> requis
        } else {
            $this->valF['plan'] = $val['plan'];
        }
        if (!is_numeric($val['unite'])) {
            $this->valF['unite'] = ""; // -> requis
        } else {
            $this->valF['unite'] = $val['unite'];
        }
        if ($val['img_unite_arrivee'] == "") {
            $this->valF['img_unite_arrivee'] = NULL;
        } else {
            $this->valF['img_unite_arrivee'] = $val['img_unite_arrivee'];
        }
        if ($val['img_unite_non_arrivee'] == "") {
            $this->valF['img_unite_non_arrivee'] = NULL;
        } else {
            $this->valF['img_unite_non_arrivee'] = $val['img_unite_non_arrivee'];
        }
        if (!is_numeric($val['position_x'])) {
            $this->valF['position_x'] = NULL;
        } else {
            $this->valF['position_x'] = $val['position_x'];
        }
        if (!is_numeric($val['position_y'])) {
            $this->valF['position_y'] = NULL;
        } else {
            $this->valF['position_y'] = $val['position_y'];
        }
        if (!is_numeric($val['largeur_icone'])) {
            $this->valF['largeur_icone'] = NULL;
        } else {
            $this->valF['largeur_icone'] = $val['largeur_icone'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("plan_unite", "hidden");
            if ($this->is_in_context_of_foreign_key("plan", $this->retourformulaire)) {
                $form->setType("plan", "selecthiddenstatic");
            } else {
                $form->setType("plan", "select");
            }
            if ($this->is_in_context_of_foreign_key("unite", $this->retourformulaire)) {
                $form->setType("unite", "selecthiddenstatic");
            } else {
                $form->setType("unite", "select");
            }
            if ($this->retourformulaire == "") {
                $form->setType("img_unite_arrivee", "upload");
            } else {
                $form->setType("img_unite_arrivee", "upload2");
            }
            if ($this->retourformulaire == "") {
                $form->setType("img_unite_non_arrivee", "upload");
            } else {
                $form->setType("img_unite_non_arrivee", "upload2");
            }
            $form->setType("position_x", "text");
            $form->setType("position_y", "text");
            $form->setType("largeur_icone", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("plan_unite", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("plan", $this->retourformulaire)) {
                $form->setType("plan", "selecthiddenstatic");
            } else {
                $form->setType("plan", "select");
            }
            if ($this->is_in_context_of_foreign_key("unite", $this->retourformulaire)) {
                $form->setType("unite", "selecthiddenstatic");
            } else {
                $form->setType("unite", "select");
            }
            if ($this->retourformulaire == "") {
                $form->setType("img_unite_arrivee", "upload");
            } else {
                $form->setType("img_unite_arrivee", "upload2");
            }
            if ($this->retourformulaire == "") {
                $form->setType("img_unite_non_arrivee", "upload");
            } else {
                $form->setType("img_unite_non_arrivee", "upload2");
            }
            $form->setType("position_x", "text");
            $form->setType("position_y", "text");
            $form->setType("largeur_icone", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("plan_unite", "hiddenstatic");
            $form->setType("plan", "selectstatic");
            $form->setType("unite", "selectstatic");
            $form->setType("img_unite_arrivee", "filestatic");
            $form->setType("img_unite_non_arrivee", "filestatic");
            $form->setType("position_x", "hiddenstatic");
            $form->setType("position_y", "hiddenstatic");
            $form->setType("largeur_icone", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("plan_unite", "static");
            $form->setType("plan", "selectstatic");
            $form->setType("unite", "selectstatic");
            $form->setType("img_unite_arrivee", "file");
            $form->setType("img_unite_non_arrivee", "file");
            $form->setType("position_x", "static");
            $form->setType("position_y", "static");
            $form->setType("largeur_icone", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('plan_unite','VerifNum(this)');
        $form->setOnchange('plan','VerifNum(this)');
        $form->setOnchange('unite','VerifNum(this)');
        $form->setOnchange('position_x','VerifNum(this)');
        $form->setOnchange('position_y','VerifNum(this)');
        $form->setOnchange('largeur_icone','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("plan_unite", 11);
        $form->setTaille("plan", 11);
        $form->setTaille("unite", 11);
        $form->setTaille("img_unite_arrivee", 30);
        $form->setTaille("img_unite_non_arrivee", 30);
        $form->setTaille("position_x", 11);
        $form->setTaille("position_y", 11);
        $form->setTaille("largeur_icone", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("plan_unite", 11);
        $form->setMax("plan", 11);
        $form->setMax("unite", 11);
        $form->setMax("img_unite_arrivee", 100);
        $form->setMax("img_unite_non_arrivee", 100);
        $form->setMax("position_x", 11);
        $form->setMax("position_y", 11);
        $form->setMax("largeur_icone", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('plan_unite', __('plan_unite'));
        $form->setLib('plan', __('plan'));
        $form->setLib('unite', __('unite'));
        $form->setLib('img_unite_arrivee', __('img_unite_arrivee'));
        $form->setLib('img_unite_non_arrivee', __('img_unite_non_arrivee'));
        $form->setLib('position_x', __('position_x'));
        $form->setLib('position_y', __('position_y'));
        $form->setLib('largeur_icone', __('largeur_icone'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // plan
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "plan",
            $this->get_var_sql_forminc__sql("plan"),
            $this->get_var_sql_forminc__sql("plan_by_id"),
            false
        );
        // unite
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "unite",
            $this->get_var_sql_forminc__sql("unite"),
            $this->get_var_sql_forminc__sql("unite_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('plan', $this->retourformulaire))
                $form->setVal('plan', $idxformulaire);
            if($this->is_in_context_of_foreign_key('unite', $this->retourformulaire))
                $form->setVal('unite', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
