<?php
//$Id$ 
//gen openMairie le 06/05/2021 21:39

require_once "../obj/om_dbform.class.php";

class animation_gen extends om_dbform {

    protected $_absolute_class_name = "animation";

    var $table = "animation";
    var $clePrimaire = "animation";
    var $typeCle = "N";
    var $required_field = array(
        "animation",
        "libelle",
        "refresh"
    );
    
    var $foreign_keys_extended = array(
        "election" => array("election", ),
        "om_logo" => array("om_logo", ),
        "unite" => array("unite", ),
        "type_unite" => array("type_unite", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "animation",
            "election",
            "libelle",
            "refresh",
            "is_modele",
            "actif",
            "titre",
            "sous_titre",
            "couleur_titre",
            "couleur_bandeau",
            "logo",
            "perimetre_aff",
            "type_aff",
            "affichage",
            "is_config",
            "feuille_style",
            "script",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_election() {
        return "SELECT election.election, election.libelle FROM ".DB_PREFIXE."election ORDER BY election.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_election_by_id() {
        return "SELECT election.election, election.libelle FROM ".DB_PREFIXE."election WHERE election = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_logo() {
        return "SELECT om_logo.om_logo, om_logo.libelle FROM ".DB_PREFIXE."om_logo ORDER BY om_logo.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_logo_by_id() {
        return "SELECT om_logo.om_logo, om_logo.libelle FROM ".DB_PREFIXE."om_logo WHERE om_logo = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_perimetre_aff() {
        return "SELECT unite.unite, unite.libelle FROM ".DB_PREFIXE."unite WHERE ((unite.om_validite_debut IS NULL AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)) OR (unite.om_validite_debut <= CURRENT_DATE AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE))) ORDER BY unite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_perimetre_aff_by_id() {
        return "SELECT unite.unite, unite.libelle FROM ".DB_PREFIXE."unite WHERE unite = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_type_aff() {
        return "SELECT type_unite.type_unite, type_unite.libelle FROM ".DB_PREFIXE."type_unite ORDER BY type_unite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_type_aff_by_id() {
        return "SELECT type_unite.type_unite, type_unite.libelle FROM ".DB_PREFIXE."type_unite WHERE type_unite = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['animation'])) {
            $this->valF['animation'] = ""; // -> requis
        } else {
            $this->valF['animation'] = $val['animation'];
        }
        if (!is_numeric($val['election'])) {
            $this->valF['election'] = NULL;
        } else {
            $this->valF['election'] = $val['election'];
        }
        $this->valF['libelle'] = $val['libelle'];
        if (!is_numeric($val['refresh'])) {
            $this->valF['refresh'] = ""; // -> requis
        } else {
            $this->valF['refresh'] = $val['refresh'];
        }
        if ($val['is_modele'] == 1 || $val['is_modele'] == "t" || $val['is_modele'] == "Oui") {
            $this->valF['is_modele'] = true;
        } else {
            $this->valF['is_modele'] = false;
        }
        if ($val['actif'] == 1 || $val['actif'] == "t" || $val['actif'] == "Oui") {
            $this->valF['actif'] = true;
        } else {
            $this->valF['actif'] = false;
        }
        if ($val['titre'] == "") {
            $this->valF['titre'] = NULL;
        } else {
            $this->valF['titre'] = $val['titre'];
        }
        if ($val['sous_titre'] == "") {
            $this->valF['sous_titre'] = NULL;
        } else {
            $this->valF['sous_titre'] = $val['sous_titre'];
        }
        if ($val['couleur_titre'] == "") {
            $this->valF['couleur_titre'] = NULL;
        } else {
            $this->valF['couleur_titre'] = $val['couleur_titre'];
        }
        if ($val['couleur_bandeau'] == "") {
            $this->valF['couleur_bandeau'] = NULL;
        } else {
            $this->valF['couleur_bandeau'] = $val['couleur_bandeau'];
        }
        if (!is_numeric($val['logo'])) {
            $this->valF['logo'] = NULL;
        } else {
            $this->valF['logo'] = $val['logo'];
        }
        if (!is_numeric($val['perimetre_aff'])) {
            $this->valF['perimetre_aff'] = NULL;
        } else {
            $this->valF['perimetre_aff'] = $val['perimetre_aff'];
        }
        if (!is_numeric($val['type_aff'])) {
            $this->valF['type_aff'] = NULL;
        } else {
            $this->valF['type_aff'] = $val['type_aff'];
        }
            $this->valF['affichage'] = $val['affichage'];
        if ($val['is_config'] == 1 || $val['is_config'] == "t" || $val['is_config'] == "Oui") {
            $this->valF['is_config'] = true;
        } else {
            $this->valF['is_config'] = false;
        }
            $this->valF['feuille_style'] = $val['feuille_style'];
            $this->valF['script'] = $val['script'];
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("animation", "hidden");
            if ($this->is_in_context_of_foreign_key("election", $this->retourformulaire)) {
                $form->setType("election", "selecthiddenstatic");
            } else {
                $form->setType("election", "select");
            }
            $form->setType("libelle", "text");
            $form->setType("refresh", "text");
            $form->setType("is_modele", "checkbox");
            $form->setType("actif", "checkbox");
            $form->setType("titre", "text");
            $form->setType("sous_titre", "text");
            $form->setType("couleur_titre", "text");
            $form->setType("couleur_bandeau", "text");
            if ($this->is_in_context_of_foreign_key("om_logo", $this->retourformulaire)) {
                $form->setType("logo", "selecthiddenstatic");
            } else {
                $form->setType("logo", "select");
            }
            if ($this->is_in_context_of_foreign_key("unite", $this->retourformulaire)) {
                $form->setType("perimetre_aff", "selecthiddenstatic");
            } else {
                $form->setType("perimetre_aff", "select");
            }
            if ($this->is_in_context_of_foreign_key("type_unite", $this->retourformulaire)) {
                $form->setType("type_aff", "selecthiddenstatic");
            } else {
                $form->setType("type_aff", "select");
            }
            $form->setType("affichage", "textarea");
            $form->setType("is_config", "checkbox");
            $form->setType("feuille_style", "textarea");
            $form->setType("script", "textarea");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("animation", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("election", $this->retourformulaire)) {
                $form->setType("election", "selecthiddenstatic");
            } else {
                $form->setType("election", "select");
            }
            $form->setType("libelle", "text");
            $form->setType("refresh", "text");
            $form->setType("is_modele", "checkbox");
            $form->setType("actif", "checkbox");
            $form->setType("titre", "text");
            $form->setType("sous_titre", "text");
            $form->setType("couleur_titre", "text");
            $form->setType("couleur_bandeau", "text");
            if ($this->is_in_context_of_foreign_key("om_logo", $this->retourformulaire)) {
                $form->setType("logo", "selecthiddenstatic");
            } else {
                $form->setType("logo", "select");
            }
            if ($this->is_in_context_of_foreign_key("unite", $this->retourformulaire)) {
                $form->setType("perimetre_aff", "selecthiddenstatic");
            } else {
                $form->setType("perimetre_aff", "select");
            }
            if ($this->is_in_context_of_foreign_key("type_unite", $this->retourformulaire)) {
                $form->setType("type_aff", "selecthiddenstatic");
            } else {
                $form->setType("type_aff", "select");
            }
            $form->setType("affichage", "textarea");
            $form->setType("is_config", "checkbox");
            $form->setType("feuille_style", "textarea");
            $form->setType("script", "textarea");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("animation", "hiddenstatic");
            $form->setType("election", "selectstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("refresh", "hiddenstatic");
            $form->setType("is_modele", "hiddenstatic");
            $form->setType("actif", "hiddenstatic");
            $form->setType("titre", "hiddenstatic");
            $form->setType("sous_titre", "hiddenstatic");
            $form->setType("couleur_titre", "hiddenstatic");
            $form->setType("couleur_bandeau", "hiddenstatic");
            $form->setType("logo", "selectstatic");
            $form->setType("perimetre_aff", "selectstatic");
            $form->setType("type_aff", "selectstatic");
            $form->setType("affichage", "hiddenstatic");
            $form->setType("is_config", "hiddenstatic");
            $form->setType("feuille_style", "hiddenstatic");
            $form->setType("script", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("animation", "static");
            $form->setType("election", "selectstatic");
            $form->setType("libelle", "static");
            $form->setType("refresh", "static");
            $form->setType("is_modele", "checkboxstatic");
            $form->setType("actif", "checkboxstatic");
            $form->setType("titre", "static");
            $form->setType("sous_titre", "static");
            $form->setType("couleur_titre", "static");
            $form->setType("couleur_bandeau", "static");
            $form->setType("logo", "selectstatic");
            $form->setType("perimetre_aff", "selectstatic");
            $form->setType("type_aff", "selectstatic");
            $form->setType("affichage", "textareastatic");
            $form->setType("is_config", "checkboxstatic");
            $form->setType("feuille_style", "textareastatic");
            $form->setType("script", "textareastatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('animation','VerifNum(this)');
        $form->setOnchange('election','VerifNum(this)');
        $form->setOnchange('refresh','VerifNum(this)');
        $form->setOnchange('logo','VerifNum(this)');
        $form->setOnchange('perimetre_aff','VerifNum(this)');
        $form->setOnchange('type_aff','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("animation", 11);
        $form->setTaille("election", 11);
        $form->setTaille("libelle", 30);
        $form->setTaille("refresh", 11);
        $form->setTaille("is_modele", 1);
        $form->setTaille("actif", 1);
        $form->setTaille("titre", 30);
        $form->setTaille("sous_titre", 30);
        $form->setTaille("couleur_titre", 13);
        $form->setTaille("couleur_bandeau", 13);
        $form->setTaille("logo", 11);
        $form->setTaille("perimetre_aff", 11);
        $form->setTaille("type_aff", 11);
        $form->setTaille("affichage", 80);
        $form->setTaille("is_config", 1);
        $form->setTaille("feuille_style", 80);
        $form->setTaille("script", 80);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("animation", 11);
        $form->setMax("election", 11);
        $form->setMax("libelle", 100);
        $form->setMax("refresh", 11);
        $form->setMax("is_modele", 1);
        $form->setMax("actif", 1);
        $form->setMax("titre", 100);
        $form->setMax("sous_titre", 100);
        $form->setMax("couleur_titre", 13);
        $form->setMax("couleur_bandeau", 13);
        $form->setMax("logo", 11);
        $form->setMax("perimetre_aff", 11);
        $form->setMax("type_aff", 11);
        $form->setMax("affichage", 6);
        $form->setMax("is_config", 1);
        $form->setMax("feuille_style", 6);
        $form->setMax("script", 6);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('animation', __('animation'));
        $form->setLib('election', __('election'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('refresh', __('refresh'));
        $form->setLib('is_modele', __('is_modele'));
        $form->setLib('actif', __('actif'));
        $form->setLib('titre', __('titre'));
        $form->setLib('sous_titre', __('sous_titre'));
        $form->setLib('couleur_titre', __('couleur_titre'));
        $form->setLib('couleur_bandeau', __('couleur_bandeau'));
        $form->setLib('logo', __('logo'));
        $form->setLib('perimetre_aff', __('perimetre_aff'));
        $form->setLib('type_aff', __('type_aff'));
        $form->setLib('affichage', __('affichage'));
        $form->setLib('is_config', __('is_config'));
        $form->setLib('feuille_style', __('feuille_style'));
        $form->setLib('script', __('script'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // election
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "election",
            $this->get_var_sql_forminc__sql("election"),
            $this->get_var_sql_forminc__sql("election_by_id"),
            false
        );
        // logo
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "logo",
            $this->get_var_sql_forminc__sql("logo"),
            $this->get_var_sql_forminc__sql("logo_by_id"),
            false
        );
        // perimetre_aff
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "perimetre_aff",
            $this->get_var_sql_forminc__sql("perimetre_aff"),
            $this->get_var_sql_forminc__sql("perimetre_aff_by_id"),
            true
        );
        // type_aff
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "type_aff",
            $this->get_var_sql_forminc__sql("type_aff"),
            $this->get_var_sql_forminc__sql("type_aff_by_id"),
            false
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('election', $this->retourformulaire))
                $form->setVal('election', $idxformulaire);
            if($this->is_in_context_of_foreign_key('om_logo', $this->retourformulaire))
                $form->setVal('logo', $idxformulaire);
            if($this->is_in_context_of_foreign_key('unite', $this->retourformulaire))
                $form->setVal('perimetre_aff', $idxformulaire);
            if($this->is_in_context_of_foreign_key('type_unite', $this->retourformulaire))
                $form->setVal('type_aff', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
