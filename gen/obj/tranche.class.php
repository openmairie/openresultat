<?php
//$Id$ 
//gen openMairie le 05/03/2021 14:09

require_once "../obj/om_dbform.class.php";

class tranche_gen extends om_dbform {

    protected $_absolute_class_name = "tranche";

    var $table = "tranche";
    var $clePrimaire = "tranche";
    var $typeCle = "N";
    var $required_field = array(
        "libelle",
        "ordre",
        "tranche"
    );
    
    var $foreign_keys_extended = array(
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "tranche",
            "libelle",
            "ordre",
        );
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['tranche'])) {
            $this->valF['tranche'] = ""; // -> requis
        } else {
            $this->valF['tranche'] = $val['tranche'];
        }
            $this->valF['libelle'] = $val['libelle'];
        if (!is_numeric($val['ordre'])) {
            $this->valF['ordre'] = ""; // -> requis
        } else {
            $this->valF['ordre'] = $val['ordre'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("tranche", "hidden");
            $form->setType("libelle", "text");
            $form->setType("ordre", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("tranche", "hiddenstatic");
            $form->setType("libelle", "text");
            $form->setType("ordre", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("tranche", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("ordre", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("tranche", "static");
            $form->setType("libelle", "static");
            $form->setType("ordre", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('tranche','VerifNum(this)');
        $form->setOnchange('ordre','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("tranche", 11);
        $form->setTaille("libelle", 8);
        $form->setTaille("ordre", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("tranche", 11);
        $form->setMax("libelle", 8);
        $form->setMax("ordre", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('tranche', __('tranche'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('ordre', __('ordre'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : election
        $this->rechercheTable($this->f->db, "election", "heure_fermeture", $id);
        // Verification de la cle secondaire : election
        $this->rechercheTable($this->f->db, "election", "heure_ouverture", $id);
        // Verification de la cle secondaire : participation_election
        $this->rechercheTable($this->f->db, "participation_election", "tranche", $id);
    }


}
