<?php
//$Id$ 
//gen openMairie le 03/05/2021 01:35

$DEBUG=0;
$serie=30;
$ent = __("administration & paramétrage")." -> ".__("affichage")." -> ".__("plan");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."plan";
// SELECT 
$champAffiche = array(
    'plan.plan as "'.__("plan").'"',
    'plan.libelle as "'.__("libelle").'"',
    'plan.image_plan as "'.__("image_plan").'"',
    );
//
$champNonAffiche = array(
    'plan.img_unite_arrivee as "'.__("img_unite_arrivee").'"',
    'plan.img_unite_non_arrivee as "'.__("img_unite_non_arrivee").'"',
    'plan.par_defaut as "'.__("par_defaut").'"',
    'plan.largeur_icone as "'.__("largeur_icone").'"',
    );
//
$champRecherche = array(
    'plan.plan as "'.__("plan").'"',
    'plan.libelle as "'.__("libelle").'"',
    'plan.image_plan as "'.__("image_plan").'"',
    );
$tri="ORDER BY plan.libelle ASC NULLS LAST";
$edition="plan";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'plan_election',
    'plan_unite',
);

