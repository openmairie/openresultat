<?php
//$Id$ 
//gen openMairie le 20/05/2022 11:04

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("élection");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."election
    LEFT JOIN ".DB_PREFIXE."election 
        ON election.election_reference=election.election 
    LEFT JOIN ".DB_PREFIXE."tranche as tranche1 
        ON election.heure_fermeture=tranche1.tranche 
    LEFT JOIN ".DB_PREFIXE."tranche as tranche2 
        ON election.heure_ouverture=tranche2.tranche 
    LEFT JOIN ".DB_PREFIXE."unite 
        ON election.perimetre=unite.unite 
    LEFT JOIN ".DB_PREFIXE."type_election 
        ON election.type_election=type_election.type_election ";
// SELECT 
$champAffiche = array(
    'election.election as "'.__("election").'"',
    'election.libelle as "'.__("libelle").'"',
    'election.code as "'.__("code").'"',
    'election.tour as "'.__("tour").'"',
    'type_election.libelle as "'.__("type_election").'"',
    'to_char(election.date ,\'DD/MM/YYYY\') as "'.__("date").'"',
    'unite.libelle as "'.__("perimetre").'"',
    'election.votant_defaut as "'.__("votant_defaut").'"',
    'tranche2.libelle as "'.__("heure_ouverture").'"',
    'tranche1.libelle as "'.__("heure_fermeture").'"',
    "case election.envoi_initial when 't' then 'Oui' else 'Non' end as \"".__("envoi_initial")."\"",
    "case election.is_centaine when 't' then 'Oui' else 'Non' end as \"".__("is_centaine")."\"",
    'election.libelle as "'.__("election_reference").'"',
    'election.workflow as "'.__("workflow").'"',
    "case election.publication_auto when 't' then 'Oui' else 'Non' end as \"".__("publication_auto")."\"",
    "case election.publication_erreur when 't' then 'Oui' else 'Non' end as \"".__("publication_erreur")."\"",
    "case election.calcul_auto_exprime when 't' then 'Oui' else 'Non' end as \"".__("calcul_auto_exprime")."\"",
    "case election.garder_resultat_simulation when 't' then 'Oui' else 'Non' end as \"".__("garder_resultat_simulation")."\"",
    "case election.delegation_saisie when 't' then 'Oui' else 'Non' end as \"".__("delegation_saisie")."\"",
    "case election.delegation_participation when 't' then 'Oui' else 'Non' end as \"".__("delegation_participation")."\"",
    "case election.validation_avant_publication when 't' then 'Oui' else 'Non' end as \"".__("validation_avant_publication")."\"",
    'election.sieges as "'.__("sieges").'"',
    'election.sieges_com as "'.__("sieges_com").'"',
    'election.sieges_mep as "'.__("sieges_mep").'"',
    "case election.export_emargement_prefecture when 't' then 'Oui' else 'Non' end as \"".__("export_emargement_prefecture")."\"",
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'election.election as "'.__("election").'"',
    'election.libelle as "'.__("libelle").'"',
    'election.code as "'.__("code").'"',
    'election.tour as "'.__("tour").'"',
    'type_election.libelle as "'.__("type_election").'"',
    'unite.libelle as "'.__("perimetre").'"',
    'election.votant_defaut as "'.__("votant_defaut").'"',
    'tranche2.libelle as "'.__("heure_ouverture").'"',
    'tranche1.libelle as "'.__("heure_fermeture").'"',
    'election.libelle as "'.__("election_reference").'"',
    'election.workflow as "'.__("workflow").'"',
    'election.sieges as "'.__("sieges").'"',
    'election.sieges_com as "'.__("sieges_com").'"',
    'election.sieges_mep as "'.__("sieges_mep").'"',
    );
$tri="ORDER BY election.libelle ASC NULLS LAST";
$edition="election";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "election" => array("election", ),
    "tranche" => array("tranche", ),
    "unite" => array("unite", ),
    "type_election" => array("type_election", ),
);
// Filtre listing sous formulaire - election
if (in_array($retourformulaire, $foreign_keys_extended["election"])) {
    $selection = " WHERE (election.election_reference = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - tranche
if (in_array($retourformulaire, $foreign_keys_extended["tranche"])) {
    $selection = " WHERE (election.heure_fermeture = ".intval($idxformulaire)." OR election.heure_ouverture = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - unite
if (in_array($retourformulaire, $foreign_keys_extended["unite"])) {
    $selection = " WHERE (election.perimetre = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - type_election
if (in_array($retourformulaire, $foreign_keys_extended["type_election"])) {
    $selection = " WHERE (election.type_election = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'animation',
    'delegation',
    'election',
    'election_candidat',
    'election_unite',
    'participation_election',
    'plan_election',
);

