<?php
//$Id$ 
//gen openMairie le 29/04/2021 23:02

$DEBUG=0;
$serie=30;
$ent = __("administration & paramétrage")." -> ".__("élection")." -> ".__("tranche");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."tranche";
// SELECT 
$champAffiche = array(
    'tranche.tranche as "'.__("tranche").'"',
    'tranche.libelle as "'.__("libelle").'"',
    'tranche.ordre as "'.__("ordre").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'tranche.tranche as "'.__("tranche").'"',
    'tranche.ordre as "'.__("ordre").'"',
    );
$tri="ORDER BY tranche.libelle ASC NULLS LAST";
$edition="tranche";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'election',
    //'participation_election',
);

