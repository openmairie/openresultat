<?php
//$Id$ 
//gen openMairie le 07/05/2021 10:44

$DEBUG=0;
$serie=30;
$ent = __("administration & paramétrage")." -> ".__("élection")." -> ".__("unité");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."unite
    LEFT JOIN ".DB_PREFIXE."canton 
        ON unite.canton=canton.canton 
    LEFT JOIN ".DB_PREFIXE."circonscription 
        ON unite.circonscription=circonscription.circonscription 
    LEFT JOIN ".DB_PREFIXE."commune 
        ON unite.commune=commune.commune 
    LEFT JOIN ".DB_PREFIXE."departement 
        ON unite.departement=departement.departement 
    LEFT JOIN ".DB_PREFIXE."type_unite as type_unite4 
        ON unite.type_unite=type_unite4.type_unite 
    LEFT JOIN ".DB_PREFIXE."type_unite as type_unite5 
        ON unite.type_unite_contenu=type_unite5.type_unite ";
// SELECT 
$champAffiche = array(
    'unite.unite as "'.__("unite").'"',
    'unite.libelle as "'.__("libelle").'"',
    'type_unite4.libelle as "'.__("type_unite").'"',
    'unite.ordre as "'.__("ordre").'"',
    'unite.adresse1 as "'.__("adresse1").'"',
    'unite.adresse2 as "'.__("adresse2").'"',
    'unite.cp as "'.__("cp").'"',
    'unite.ville as "'.__("ville").'"',
    'unite.geom as "'.__("geom").'"',
    'unite.coordinates as "'.__("coordinates").'"',
    'to_char(unite.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(unite.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
    'type_unite5.libelle as "'.__("type_unite_contenu").'"',
    "case unite.perimetre when 't' then 'Oui' else 'Non' end as \"".__("perimetre")."\"",
    'canton.libelle as "'.__("canton").'"',
    'circonscription.libelle as "'.__("circonscription").'"',
    'commune.libelle as "'.__("commune").'"',
    'departement.libelle as "'.__("departement").'"',
    'unite.code_unite as "'.__("code_unite").'"',
    'unite.id_reu as "'.__("id_reu").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'unite.unite as "'.__("unite").'"',
    'unite.libelle as "'.__("libelle").'"',
    'type_unite4.libelle as "'.__("type_unite").'"',
    'unite.ordre as "'.__("ordre").'"',
    'unite.adresse1 as "'.__("adresse1").'"',
    'unite.adresse2 as "'.__("adresse2").'"',
    'unite.cp as "'.__("cp").'"',
    'unite.ville as "'.__("ville").'"',
    'unite.coordinates as "'.__("coordinates").'"',
    'type_unite5.libelle as "'.__("type_unite_contenu").'"',
    'canton.libelle as "'.__("canton").'"',
    'circonscription.libelle as "'.__("circonscription").'"',
    'commune.libelle as "'.__("commune").'"',
    'departement.libelle as "'.__("departement").'"',
    'unite.code_unite as "'.__("code_unite").'"',
    'unite.id_reu as "'.__("id_reu").'"',
    );
$tri="ORDER BY unite.libelle ASC NULLS LAST";
$edition="unite";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((unite.om_validite_debut IS NULL AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)) OR (unite.om_validite_debut <= CURRENT_DATE AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((unite.om_validite_debut IS NULL AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)) OR (unite.om_validite_debut <= CURRENT_DATE AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)))";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "canton" => array("canton", ),
    "circonscription" => array("circonscription", ),
    "commune" => array("commune", ),
    "departement" => array("departement", ),
    "type_unite" => array("type_unite", ),
);
// Filtre listing sous formulaire - canton
if (in_array($retourformulaire, $foreign_keys_extended["canton"])) {
    $selection = " WHERE (unite.canton = ".intval($idxformulaire).")  AND ((unite.om_validite_debut IS NULL AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)) OR (unite.om_validite_debut <= CURRENT_DATE AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((unite.om_validite_debut IS NULL AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)) OR (unite.om_validite_debut <= CURRENT_DATE AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - circonscription
if (in_array($retourformulaire, $foreign_keys_extended["circonscription"])) {
    $selection = " WHERE (unite.circonscription = ".intval($idxformulaire).")  AND ((unite.om_validite_debut IS NULL AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)) OR (unite.om_validite_debut <= CURRENT_DATE AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((unite.om_validite_debut IS NULL AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)) OR (unite.om_validite_debut <= CURRENT_DATE AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - commune
if (in_array($retourformulaire, $foreign_keys_extended["commune"])) {
    $selection = " WHERE (unite.commune = ".intval($idxformulaire).")  AND ((unite.om_validite_debut IS NULL AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)) OR (unite.om_validite_debut <= CURRENT_DATE AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((unite.om_validite_debut IS NULL AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)) OR (unite.om_validite_debut <= CURRENT_DATE AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - departement
if (in_array($retourformulaire, $foreign_keys_extended["departement"])) {
    $selection = " WHERE (unite.departement = ".intval($idxformulaire).")  AND ((unite.om_validite_debut IS NULL AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)) OR (unite.om_validite_debut <= CURRENT_DATE AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((unite.om_validite_debut IS NULL AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)) OR (unite.om_validite_debut <= CURRENT_DATE AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - type_unite
if (in_array($retourformulaire, $foreign_keys_extended["type_unite"])) {
    $selection = " WHERE (unite.type_unite = ".intval($idxformulaire)." OR unite.type_unite_contenu = ".intval($idxformulaire).")  AND ((unite.om_validite_debut IS NULL AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)) OR (unite.om_validite_debut <= CURRENT_DATE AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((unite.om_validite_debut IS NULL AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)) OR (unite.om_validite_debut <= CURRENT_DATE AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)))";
}
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'animation',
    'delegation',
    'election',
    'election_unite',
    'lien_unite',
    'plan_unite',
);

