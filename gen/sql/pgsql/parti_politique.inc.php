<?php
//$Id$ 
//gen openMairie le 03/05/2021 01:58

$DEBUG=0;
$serie=30;
$ent = __("administration & paramétrage")." -> ".__("élection")." -> ".__("parti politique");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."parti_politique";
// SELECT 
$champAffiche = array(
    'parti_politique.parti_politique as "'.__("parti_politique").'"',
    'parti_politique.libelle as "'.__("libelle").'"',
    'parti_politique.couleur as "'.__("couleur").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'parti_politique.parti_politique as "'.__("parti_politique").'"',
    'parti_politique.libelle as "'.__("libelle").'"',
    'parti_politique.couleur as "'.__("couleur").'"',
    );
$tri="ORDER BY parti_politique.libelle ASC NULLS LAST";
$edition="parti_politique";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'election_candidat',
);

