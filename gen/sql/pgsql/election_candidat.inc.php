<?php
//$Id$ 
//gen openMairie le 11/05/2021 14:34

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("élection")." -> ".__("election_candidat");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."election_candidat
    LEFT JOIN ".DB_PREFIXE."candidat 
        ON election_candidat.candidat=candidat.candidat 
    LEFT JOIN ".DB_PREFIXE."election 
        ON election_candidat.election=election.election 
    LEFT JOIN ".DB_PREFIXE."parti_politique 
        ON election_candidat.parti_politique=parti_politique.parti_politique ";
// SELECT 
$champAffiche = array(
    'election_candidat.election_candidat as "'.__("election_candidat").'"',
    'election.libelle as "'.__("election").'"',
    'candidat.libelle as "'.__("candidat").'"',
    'election_candidat.ordre as "'.__("ordre").'"',
    'election_candidat.prefecture as "'.__("prefecture").'"',
    );
//
$champNonAffiche = array(
    'election_candidat.age_moyen as "'.__("age_moyen").'"',
    'election_candidat.siege as "'.__("siege").'"',
    'election_candidat.age_moyen_com as "'.__("age_moyen_com").'"',
    'election_candidat.siege_com as "'.__("siege_com").'"',
    'election_candidat.couleur as "'.__("couleur").'"',
    'election_candidat.photo as "'.__("photo").'"',
    'election_candidat.parti_politique as "'.__("parti_politique").'"',
    'election_candidat.age_moyen_mep as "'.__("age_moyen_mep").'"',
    'election_candidat.siege_mep as "'.__("siege_mep").'"',
    );
//
$champRecherche = array(
    'election_candidat.election_candidat as "'.__("election_candidat").'"',
    'election.libelle as "'.__("election").'"',
    'candidat.libelle as "'.__("candidat").'"',
    'election_candidat.ordre as "'.__("ordre").'"',
    'election_candidat.prefecture as "'.__("prefecture").'"',
    );
$tri="ORDER BY election.libelle ASC NULLS LAST";
$edition="election_candidat";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "candidat" => array("candidat", ),
    "election" => array("election", ),
    "parti_politique" => array("parti_politique", ),
);
// Filtre listing sous formulaire - candidat
if (in_array($retourformulaire, $foreign_keys_extended["candidat"])) {
    $selection = " WHERE (election_candidat.candidat = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - election
if (in_array($retourformulaire, $foreign_keys_extended["election"])) {
    $selection = " WHERE (election_candidat.election = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - parti_politique
if (in_array($retourformulaire, $foreign_keys_extended["parti_politique"])) {
    $selection = " WHERE (election_candidat.parti_politique = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'election_resultat',
);

