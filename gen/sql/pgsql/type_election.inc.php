<?php
//$Id$ 
//gen openMairie le 01/05/2021 01:27

$DEBUG=0;
$serie=30;
$ent = __("administration & paramétrage")." -> ".__("élection")." -> ".__("type d'élection");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."type_election";
// SELECT 
$champAffiche = array(
    'type_election.type_election as "'.__("type_election").'"',
    'type_election.libelle as "'.__("libelle").'"',
    'type_election.code as "'.__("code").'"',
    'type_election.prefecture as "'.__("prefecture").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'type_election.type_election as "'.__("type_election").'"',
    'type_election.libelle as "'.__("libelle").'"',
    'type_election.code as "'.__("code").'"',
    'type_election.prefecture as "'.__("prefecture").'"',
    );
$tri="ORDER BY type_election.libelle ASC NULLS LAST";
$edition="type_election";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'election',
);

