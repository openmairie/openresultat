<?php
//$Id$ 
//gen openMairie le 01/05/2021 13:25

$DEBUG=0;
$serie=30;
$ent = __("administration & paramétrage")." -> ".__("découpage administratif")." -> ".__("département");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."departement";
// SELECT 
$champAffiche = array(
    'departement.departement as "'.__("departement").'"',
    'departement.libelle as "'.__("libelle").'"',
    'departement.code as "'.__("code").'"',
    'departement.prefecture as "'.__("prefecture").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'departement.departement as "'.__("departement").'"',
    'departement.libelle as "'.__("libelle").'"',
    'departement.code as "'.__("code").'"',
    'departement.prefecture as "'.__("prefecture").'"',
    );
$tri="ORDER BY departement.libelle ASC NULLS LAST";
$edition="departement";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'unite',
);

