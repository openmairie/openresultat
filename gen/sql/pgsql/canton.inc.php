<?php
//$Id$ 
//gen openMairie le 29/04/2021 23:04

$DEBUG=0;
$serie=30;
$ent = __("administration & paramétrage")." -> ".__("découpage administratif")." -> ".__("canton");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."canton";
// SELECT 
$champAffiche = array(
    'canton.canton as "'.__("canton").'"',
    'canton.libelle as "'.__("libelle").'"',
    'canton.code as "'.__("code").'"',
    'canton.prefecture as "'.__("prefecture").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'canton.canton as "'.__("canton").'"',
    'canton.libelle as "'.__("libelle").'"',
    'canton.code as "'.__("code").'"',
    'canton.prefecture as "'.__("prefecture").'"',
    );
$tri="ORDER BY canton.libelle ASC NULLS LAST";
$edition="canton";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'unite',
);

