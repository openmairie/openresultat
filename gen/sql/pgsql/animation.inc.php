<?php
//$Id$ 
//gen openMairie le 06/05/2021 21:39

$DEBUG=0;
$serie=30;
$ent = __("administration & paramétrage")." -> ".__("affichage")." -> ".__("animation");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."animation
    LEFT JOIN ".DB_PREFIXE."election 
        ON animation.election=election.election 
    LEFT JOIN ".DB_PREFIXE."om_logo 
        ON animation.logo=om_logo.om_logo 
    LEFT JOIN ".DB_PREFIXE."unite 
        ON animation.perimetre_aff=unite.unite 
    LEFT JOIN ".DB_PREFIXE."type_unite 
        ON animation.type_aff=type_unite.type_unite ";
// SELECT 
$champAffiche = array(
    'animation.animation as "'.__("animation").'"',
    'election.libelle as "'.__("election").'"',
    'animation.libelle as "'.__("libelle").'"',
    'animation.refresh as "'.__("refresh").'"',
    "case animation.is_modele when 't' then 'Oui' else 'Non' end as \"".__("is_modele")."\"",
    "case animation.actif when 't' then 'Oui' else 'Non' end as \"".__("actif")."\"",
    'animation.titre as "'.__("titre").'"',
    'animation.sous_titre as "'.__("sous_titre").'"',
    'animation.couleur_titre as "'.__("couleur_titre").'"',
    'animation.couleur_bandeau as "'.__("couleur_bandeau").'"',
    'om_logo.libelle as "'.__("logo").'"',
    'unite.libelle as "'.__("perimetre_aff").'"',
    'type_unite.libelle as "'.__("type_aff").'"',
    "case animation.is_config when 't' then 'Oui' else 'Non' end as \"".__("is_config")."\"",
    );
//
$champNonAffiche = array(
    'animation.affichage as "'.__("affichage").'"',
    'animation.feuille_style as "'.__("feuille_style").'"',
    'animation.script as "'.__("script").'"',
    );
//
$champRecherche = array(
    'animation.animation as "'.__("animation").'"',
    'election.libelle as "'.__("election").'"',
    'animation.libelle as "'.__("libelle").'"',
    'animation.refresh as "'.__("refresh").'"',
    'animation.titre as "'.__("titre").'"',
    'animation.sous_titre as "'.__("sous_titre").'"',
    'animation.couleur_titre as "'.__("couleur_titre").'"',
    'animation.couleur_bandeau as "'.__("couleur_bandeau").'"',
    'om_logo.libelle as "'.__("logo").'"',
    'unite.libelle as "'.__("perimetre_aff").'"',
    'type_unite.libelle as "'.__("type_aff").'"',
    );
$tri="ORDER BY animation.libelle ASC NULLS LAST";
$edition="animation";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "election" => array("election", ),
    "om_logo" => array("om_logo", ),
    "unite" => array("unite", ),
    "type_unite" => array("type_unite", ),
);
// Filtre listing sous formulaire - election
if (in_array($retourformulaire, $foreign_keys_extended["election"])) {
    $selection = " WHERE (animation.election = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - om_logo
if (in_array($retourformulaire, $foreign_keys_extended["om_logo"])) {
    $selection = " WHERE (animation.logo = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - unite
if (in_array($retourformulaire, $foreign_keys_extended["unite"])) {
    $selection = " WHERE (animation.perimetre_aff = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - type_unite
if (in_array($retourformulaire, $foreign_keys_extended["type_unite"])) {
    $selection = " WHERE (animation.type_aff = ".intval($idxformulaire).") ";
}

