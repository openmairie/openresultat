<?php
//$Id$ 
//gen openMairie le 03/05/2021 01:39

$DEBUG=0;
$serie=30;
$ent = __("administration & paramétrage")." -> ".__("élection")." -> ".__("acteur");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."acteur";
// SELECT 
$champAffiche = array(
    'acteur.acteur as "'.__("acteur").'"',
    'acteur.nom as "'.__("nom").'"',
    'acteur.login as "'.__("login").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'acteur.acteur as "'.__("acteur").'"',
    'acteur.nom as "'.__("nom").'"',
    'acteur.login as "'.__("login").'"',
    );
$tri="ORDER BY acteur.nom ASC NULLS LAST";
$edition="acteur";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'delegation',
);

