<?php
//$Id$ 
//gen openMairie le 29/04/2021 23:02

$DEBUG=0;
$serie=30;
$ent = __("administration & paramétrage")." -> ".__("élection")." -> ".__("candidat");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."candidat";
// SELECT 
$champAffiche = array(
    'candidat.candidat as "'.__("candidat").'"',
    'candidat.libelle as "'.__("libelle").'"',
    'candidat.libelle_liste as "'.__("libelle_liste").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'candidat.candidat as "'.__("candidat").'"',
    'candidat.libelle as "'.__("libelle").'"',
    'candidat.libelle_liste as "'.__("libelle_liste").'"',
    );
$tri="ORDER BY candidat.libelle ASC NULLS LAST";
$edition="candidat";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'election_candidat',
);

