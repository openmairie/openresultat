<?php
//$Id$ 
//gen openMairie le 25/05/2022 15:23

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("élection")." -> ".__("détail des résultats");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."election_resultat
    LEFT JOIN ".DB_PREFIXE."election_candidat 
        ON election_resultat.election_candidat=election_candidat.election_candidat 
    LEFT JOIN ".DB_PREFIXE."election_unite 
        ON election_resultat.election_unite=election_unite.election_unite ";
// SELECT 
$champAffiche = array(
    'election_resultat.election_resultat as "'.__("election_resultat").'"',
    'election_unite.election as "'.__("election_unite").'"',
    'election_candidat.election as "'.__("election_candidat").'"',
    'election_resultat.resultat as "'.__("resultat").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'election_resultat.election_resultat as "'.__("election_resultat").'"',
    'election_unite.election as "'.__("election_unite").'"',
    'election_candidat.election as "'.__("election_candidat").'"',
    'election_resultat.resultat as "'.__("resultat").'"',
    );
$tri="ORDER BY election_unite.election ASC NULLS LAST";
$edition="election_resultat";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "election_candidat" => array("election_candidat", ),
    "election_unite" => array("election_unite", "election_unite_overlay", ),
);
// Filtre listing sous formulaire - election_candidat
if (in_array($retourformulaire, $foreign_keys_extended["election_candidat"])) {
    $selection = " WHERE (election_resultat.election_candidat = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - election_unite
if (in_array($retourformulaire, $foreign_keys_extended["election_unite"])) {
    $selection = " WHERE (election_resultat.election_unite = ".intval($idxformulaire).") ";
}

