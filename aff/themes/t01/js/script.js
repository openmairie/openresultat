
$(function() {

    // Positionnement de l'entite du listing dans le viewport
    var myDiv = $("div.sidebar");
    var entite = $('ul.nav-sidebar li.active');
    if (entite === null || entite === '') {
        active_element_top = entite.offset().top;
        // 90 = 50 + 40 (première position visible de li)
        if (active_element_top <= 90) {
            myDiv.animate({ scrollTop: 0 }, 1);
        } else {
            if (active_element_top > myDiv.height()) {
                console.log("non visible");
                myDiv.animate({ scrollTop: myDiv.prop("scrollHeight") / 2 }, 1);
            }
        }
    }

});
