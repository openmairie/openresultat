<?php
/**
 * Ce script contient la définition des variables de l'objet *election*.
 *
 * @package openresultat
 * @version SVN : $Id$
 */

include "../gen/sql/pgsql/election.inc.php";

$tab_title = __("élection");

$sousformulaire = array(
    'election_candidat',
    'election_unite',
    'participation_election',
    'centaine',
    'animation',
    'plan_election'
);

$table = DB_PREFIXE."election
    LEFT JOIN ".DB_PREFIXE."tranche as tranche1 
        ON election.heure_fermeture=tranche1.tranche 
    LEFT JOIN ".DB_PREFIXE."tranche as tranche2 
        ON election.heure_ouverture=tranche2.tranche
    LEFT JOIN ".DB_PREFIXE."type_election 
        ON election.type_election=type_election.type_election     
    LEFT JOIN ".DB_PREFIXE."election_exprime 
        ON election.election=election_exprime.election  
    LEFT JOIN ".DB_PREFIXE."election_cumul_unite
        ON election.election=election_cumul_unite.election
    LEFT JOIN ".DB_PREFIXE."unite
        ON election.perimetre=unite.unite ";
// SELECT
$displayed_field__workflow__case = "election.workflow";
$displayed_field__workflow = $displayed_field__workflow__case." as \"".__("workflow")."\"";
$champAffiche = array(
    'election.election as "'.__("id").'"',
    'election.libelle as "'.__("libellé").'"',
    'to_char(election.date ,\'DD/MM/YYYY\') as "'.__("date").'"',
    'election_cumul_unite.inscrit as "'.__("inscrit").'"',
    'election_cumul_unite.votant as "'.__("votant").'"',
    'election_cumul_unite.blanc as "'.__("blanc").'"',
    'election_cumul_unite.nul as "'.__("nul").'"',
    'election_cumul_unite.exprime as "'.__("votant-nuls-blanc").'"',
    'election_exprime.exprime as "'.__("exprime candidat").'"',
    $displayed_field__workflow,
);

$selection = " where is_centaine is not true ";

// SORT
$tri = " ORDER BY election.date DESC, election.libelle ";

$sousformulaire_parameters = array(
    "election_candidat" => array(
        "title" => _("candidat(s)")
    ),
    "election_unite" => array(
        "title" => _("unité(s)")
    ),
    "participation_election" => array(
        "title" => _("participation(s)")
    ),
    "centaine" => array(
        "title" => _("centaine(s)")
    ),
    "animation" => array(
        "title" => _("animation(s)")
    ),
    "plan_election" => array(
        "title" => _("plan(s)")
    )
);

// Affichage des onglets de delegation de saisie selon les options sélectionnées
$optionsDelegation = $this->get_options_delegation_election();
if ($optionsDelegation !== false &&
    array_key_exists('delegation_saisie', $optionsDelegation) &&
    $optionsDelegation['delegation_saisie'] &&
    array_key_exists('workflow', $optionsDelegation) &&
    $optionsDelegation['workflow'] != 'Archivage'
) {
    $sousformulaire[] = 'delegation';
    $sousformulaire_parameters['delegation'] = array(
        "title" => _("délégation")
    );
    // Si il y a délégation de la participation, le formulaire de saisie de la participation
    // par unité est affiché et celui de la participation globale ne doit plus l'être
    if (array_key_exists('delegation_participation', $optionsDelegation) &&
        $optionsDelegation['delegation_participation']
    ) {
        $sousformulaire[] = 'delegation_participation';
        $sousformulaire_parameters['delegation_participation'] = array(
            "title" => _("participation(s)")
        );

        // suppression de l'onglet de la participation par tranche horaire
        $indiceParticipation = array_search('participation_election', $sousformulaire);
        unset($sousformulaire[$indiceParticipation]);
        unset($sousformulaire_parameters['participation_election']);
    }
}

// Gestion des options du listing
if (!isset($options)) {
    $options = array();
}
// Option condition archivee : permet d'ajouter la classe css election-archivee
// sur chaque enregistrement dont l'état de workflow est Archivage.
$option_condition_archivee = array(
    "type" => "condition",
    "field" => $displayed_field__workflow__case,
    "case" => array(
        array(
            "values" => array("Archivage", ),
            "style" => "election-archivee",
        ),
    ),
);
$options[] = $option_condition_archivee;
// Recherche avancée
$champs = array();
$champs["id"] = array(
    "libelle" => __("id"),
    "table" => "election",
    "colonne" =>  "election",
    "type" => "text",
    "max" => 50,
    "taille" => 30,
);
$champs["libelle"] = array(
    "libelle" => __("libelle"),
    "table" => "election",
    "colonne" =>  "libelle",
    "type" => "text",
    "max" => 50,
    "taille" => 30,
);
$champs["date"] = array(
    "libelle" => __("date"),
    "table" => "election",
    "colonne" => "date",
    "type" => "date",
    "taille" => 8,
    "where" => "intervaldate",
);
$champs["type_election"] = array(
    "libelle" => __("type"),
    "table" => "election",
    "colonne" =>  "type_election",
    "type" => "select",
);
$champs["workflow"] = array(
    "libelle" => __("workflow"),
    "table" => "election",
    "colonne" =>  "workflow",
    "type" => "select",
    "subtype" => "manualselect",
    "args" => array(
        0 => array("", "Paramétrage", "Simulation", "Saisie", "Finalisation", "Archivage", ),
        1 => array(__("Tous"), __("Paramétrage"), __("Simulation"), __("Saisie"), __("Finalisation"), __("Archivage"), ),
    ),
);
$options[] = array(
    "type" => "search",
    "display" => true,
    "advanced"  => $champs,
    "default_form"  => "advanced",
    "absolute_object" => "election",
    "export" => array("csv"),
);
