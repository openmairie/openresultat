<?php
//$Id$
//gen openMairie le 30/05/2019 16:10

include "../gen/sql/pgsql/election_unite.inc.php";

// Impossible d'ajouter un enregistrement manuellement, il faut passer par les
// actions de l'élection, donc on cache l'action ajouter
$tab_actions['corner']['ajouter'] = null;

// parametrage du nombre de unite a afficher sur une page
if ($this->getParameter('nombre_unite')) {
    $serie=$this->getParameter('nombre_unite');
}

// SORT
$tri = " ORDER BY election.date, election.libelle, unite.ordre ";

// SELECT
$displayed_field__election = 'election.libelle as "'.__("election").'"';
$displayFieldUnite = 'CONCAT_WS(\' - \', unite.code_unite, unite.libelle) as "'.__("unité").'"';
$champAffiche = array(
    'election_unite.election_unite as "'.__("id").'"',
    $displayed_field__election,
    $displayFieldUnite,
    'unite.perimetre as "'.__("perimetre").'"',
    'election_unite.inscrit as "'.__("inscrit").'"',
    'election_unite.votant as "'.__("votant").'"',
    'election_unite.emargement as "'.__("emargement").'"',
    'election_unite.procuration as "'.__("procuration").'"',
    'election_unite.blanc as "'.__("blanc").'"',
    'election_unite.nul as "'.__("nul").'"',
    'election_unite.exprime as "'.__("exprimé").'"',
    "case election_unite.saisie
        when 'en attente' then 
            '<span class=\"om-icon om-icon-16 wait-election_unite-16\" title=\"en attente\">en attente</span>' 
        when 'correcte' then
            '<span class=\"om-icon om-icon-16 done-election_unite-16\" title=\"arrivée\">arrivée</span>'
        else 
            '<img src=../app/img/erreur.png>' 
        end as 
            \"".__("saisie")."\"",
    "case election_unite.envoi_aff
        when 
            't' 
        then 
            '<img src=../app/img/arrive.png>' else '<img src=../app/img/nonarrive.png>' 
        end 
            as \"".__("aff")."\"",
    "case election_unite.envoi_web 
        when 
            't'
        then 
            '<img src=../app/img/arrive.png>'
            else 
            '<img src=../app/img/nonarrive.png>' 
        end as 
            \"".__("web")."\"",
    "case election_unite.validation 
        when 'en attente' then 
            '<img src=../app/img/erreur.png>'
        when 'validee' then
            '<img src=../app/img/arrive.png>'
        else 
            '<img src=../app/img/nonarrive.png>' 
        end as 
            \"".__("validation")."\"",
    );

// Dans le contexte d'une élection
if (in_array($retourformulaire, $foreign_keys_extended["election"])) {
    // La colonne élection n'est pas nécessaire
    $champAffiche = array_diff($champAffiche, array($displayed_field__election, ));
    $champRecherche = array_diff($champRecherche, array($displayed_field__election, ));
}
