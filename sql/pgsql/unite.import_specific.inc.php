<?php
//$Id$ 
//gen openMairie le 20/01/2021 15:33

$import= "Insertion dans la table unite voir rec/import_utilisateur.inc";
$table= DB_PREFIXE."unite";
$id='unite'; // numerotation automatique
$verrou=1;// =0 pas de mise a jour de la base / =1 mise a jour
$fic_rejet=1; // =0 pas de fichier pour relance / =1 fichier relance traitement
$ligne1=1;// = 1 : 1ere ligne contient nom des champs / o sinon
/**
 *
 */
$fields = array(
    "Id" => array( //code du bureau
        "notnull" => "",
        "type" => "int",
        "len" => "10",
        "colonnesName" => array(
            "﻿\"Id\"",
            'Id',
            'Identifiant du bureau'
        )
    ),
    "Code" => array( //code du bureau
        "notnull" => "1",
        "type" => "int",
        "len" => "3",
        "colonnesName" => array(
            'Code',
            'Code du bureau'
        )
    ),
    "Libelle" => array( // Libelle du Bureau
        "notnull" => "1",
        "type" => "string",
        "len" => "30",
        "colonnesName" => array(
            'Libellé',
            'Libellé du bureau'
        )
    ),//Adresse
    "Adresse" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "100",
        "colonnesName" => array(
            "Adresse",
            "N° d’appartement - d’étage - de couloir - d’escalier",
            "Entrée - bâtiment - résidence",
            "BP, lieu-dit ou ancienne commune",
            "Lieu-dit",
            "Numero de voie",
            "Libellé de voie",
            "Complément 1",
            "Complément 2",
            "Code postal",
            "Commune"
        )
    ),
    "Circonscription" => array( // Circonscription legislative
        "notnull" => "",
        "type" => "string",
        "len" => "200",
        "colonnesName" => array(
            "Code de la circonscription législative",
            "Libellé de la circonscription législative",
            "Circonscription législative"
        )
    ),
    "Canton" => array(  // Canton
        "notnull" => "",
        "type" => "string",
        "len" => "200",
        "colonnesName" => array(
            "Code du canton",
            "Libellé du canton",
            "Canton"
        )
    ),
    "Type" => array(  // Type de l'unité à importer
        "facultatif" => true,
        "notnull" => "",
        "type" => "int",
        "len" => "5",
        "fkey" => array(
            "foreign_table_name" => "type_unite",
            "foreign_column_name" => "type_unite",
            "sql_exist" => "select * from ".DB_PREFIXE."type_unite where type_unite = '",
        ),
    ),
);
