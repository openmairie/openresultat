<?php
/**
 * Ce script contient la définition des variables de l'objet *participation_unite*.
 *
 * @package openresultat
 * @version SVN : $Id$
 */

include "../gen/sql/pgsql/participation_unite.inc.php";

$tab_title = _("détail des participations");

// SORT
$tri = " ORDER BY election.date, election.libelle, unite.ordre ";

// Impossible d'ajouter un enregistrement manuellement, il faut passer par les
// actions de l'élection, donc on cache l'action ajouter
$tab_actions['corner']['ajouter'] = null;

$table = DB_PREFIXE."participation_unite
    LEFT JOIN ".DB_PREFIXE."election_unite 
        ON participation_unite.election_unite=election_unite.election_unite 
    LEFT JOIN ".DB_PREFIXE."participation_election 
        ON participation_unite.participation_election=participation_election.participation_election 
   LEFT JOIN ".DB_PREFIXE."unite 
        ON election_unite.unite=unite.unite 
   LEFT JOIN ".DB_PREFIXE."tranche 
        ON tranche.tranche=participation_election.tranche 
    LEFT JOIN ".DB_PREFIXE."election
        ON election.election=election_unite.election        
        ";

$displayFieldUnite = 'CONCAT_WS(\' - \', unite.code_unite, unite.libelle) as "'.__("unité").'"';
$champAffiche = array(
    'participation_unite.participation_unite as "'.__("id").'"',
    'election.libelle as "'.__("élection").'"',
    $displayFieldUnite,
    'tranche.libelle as "'.__("tranche").'"',
    'participation_unite.votant as "'.__("votant").'"'
    );

// Recherche avancée
$champs = array();
$champs["election"] = array(
    "libelle" => __("élection"),
    "table" => "election",
    "colonne" =>   "election",
    "type" => "select",
);
$champs["unite"] = array(
    "libelle" => __("unité"),
    "table" => "unite",
    "colonne" =>  "unite",
    "type" => "select",
);
$champs["tranche"] = array(
    "libelle" => __("tranche"),
    "table" => "tranche",
    "colonne" =>  "tranche",
    "type" => "select",
);
$options[] = array(
    "type" => "search",
    "display" => true,
    "advanced"  => $champs,
    "default_form"  => "advanced",
    "absolute_object" => "participation_unite",
    "export" => array("csv"),
);
