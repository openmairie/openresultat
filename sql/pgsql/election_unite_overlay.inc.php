<?php
//$Id$
//gen openMairie le 10/11/2020 15:00

include "../sql/pgsql/election_unite.inc.php";

// Declaration du dictionnaire
$tab_actions = array(
    'corner' => array(),
    'left' => array(),
    'content' => array(),
    'specific_content' => array(),
);

// Actions consulter : cette action est bindée par du javascript pour ouvrir son
// contenu dans un overlay. Le formulaire afficher est celui des elections unites
// C'est le paramétre obj qui permet de définir quel formulaire afficher
$tab_actions['left']['consulter'] = array(
    'lien' => OM_ROUTE_SOUSFORM.'&obj=election_unite&amp;action=3&amp;idx=',
    'id' => '&amp;advs_id='.$advs_id.'&amp;premiersf='.$premier.'&amp;trisf='.$tricol.'&amp;valide='.$valide.'&amp;retourformulaire='.$retourformulaire.'&amp;idxformulaire='.$idxformulaire.'&amp;retour=tab',
    'lib' => '<span class="om-icon om-icon-16 om-icon-fix consult-16" title="'.__('Consulter').'">'.__('Consulter').'</span>',
    'rights' => array('list' => array($obj, $obj.'_consulter'), 'operator' => 'OR'),
    'ordre' => 10,
    "ajax" => false,
);
