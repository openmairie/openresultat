<?php
//$Id$ 
//gen openMairie le 17/09/2020 14:11

include "../gen/sql/pgsql/type_unite.inc.php";

$tab_title = _("Type d'unités");

$tri = " order by type_unite.hierarchie";

// SELECT
$champAffiche = array(
    'type_unite.type_unite as "'.__("id").'"',
    'type_unite.libelle as "'.__("libelle").'"',
    'type_unite.hierarchie as "'.__("hiérarchie").'"',
    "CASE type_unite.bureau_vote WHEN 't' THEN 'Oui' ELSE '' END as \"".__("bureau de vote ?")."\"",
);
