<?php
//$Id$
//gen openMairie le 30/05/2019 16:10

include "election.inc.php";

if (in_array($retourformulaire, $foreign_keys_extended["election"])) {
    $selection = " WHERE
                election.is_centaine is true AND
                election.election_reference = ".intval($idxformulaire);
}

$table = DB_PREFIXE."election
    LEFT JOIN ".DB_PREFIXE."election as election_ref
        ON election.election_reference = election_ref.election
        ";

$champAffiche = array(
    'election.election as "'.__("id").'"',
    'election_ref.libelle as "'.__("référence").'"',
    'election.libelle as "'.__("libelle").'"',
    'election.votant_defaut as "'.__("Nombre de votant").'"'
    );

$tab_title = _("centaine");
$ent .= " -> "._("centaine");

$workflow = $this->get_workflow_election();

if ($workflow === false ||
    $workflow === 'Saisie' ||
    $workflow === 'Finalisation' ||
    $workflow === 'Archivage'
) {
    $tab_actions['corner']['ajouter'] = null;
}