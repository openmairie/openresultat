<?php
//$Id$
//gen openMairie le 10/11/2020 15:00

include "../gen/sql/pgsql/animation.inc.php";

// FROM
$table = DB_PREFIXE."animation
    LEFT JOIN ".DB_PREFIXE."election
        ON animation.election=election.election
    LEFT JOIN ".DB_PREFIXE."om_logo
        ON animation.logo=om_logo.om_logo
    LEFT JOIN ".DB_PREFIXE."unite
        ON animation.perimetre_aff=unite.unite
    LEFT JOIN ".DB_PREFIXE."type_unite
        ON animation.type_aff=type_unite.type_unite ";

// Affichage du listing pour les elections archivées et les elections
if (in_array($retourformulaire, $foreign_keys_extended["election"])) {
    $selection = " WHERE (animation.election IN (
            SELECT
                election.election
            FROM
                ".DB_PREFIXE."election
            WHERE
                election.election = ".intval($idxformulaire)." OR
                election.election_reference = ".intval($idxformulaire)."))";

    $workflow = $this->get_workflow_election();
    
    if ($workflow === false ||
        $workflow === 'Archivage'
    ) {
        $tab_actions['corner']['ajouter'] = null;
    }

    // SELECT
    $champAffiche = array(
        'animation.animation as "'.__("animation").'"',
        'election.libelle as "'.__("election").'"',
        'animation.libelle as "'.__("libelle").'"'
    );
} else {
    $selection = " WHERE is_modele = TRUE";

    // SELECT
    $champAffiche = array(
        'animation.animation as "'.__("animation").'"',
        'election.libelle as "'.__("election").'"',
        'animation.libelle as "'.__("libelle").'"',
        'animation.titre as "'.__("titre").'"',
        "case animation.actif when 't' then 'Oui' else 'Non' end as \"".__("actif")."\""
        );
}


$champRecherche = array(
    'animation.animation as "'.__("animation").'"',
    'animation.libelle as "'.__("libelle").'"',
    'animation.titre as "'.__("titre").'"'
    );

$sousformulaire = array();
