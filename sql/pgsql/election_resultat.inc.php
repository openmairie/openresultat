<?php
/**
 * Ce script contient la définition des variables de l'objet *election_resultat*.
 *
 * @package openresultat
 * @version SVN : $Id$
 */

include "../gen/sql/pgsql/election_resultat.inc.php";

$tab_title = _("détail des résultats");

// Impossible d'ajouter un enregistrement manuellement, il faut passer par les
// actions de l'élection, donc on cache l'action ajouter
$tab_actions['corner']['ajouter'] = null;

$table = DB_PREFIXE."election_resultat
    LEFT JOIN ".DB_PREFIXE."election_unite
        ON election_resultat.election_unite = election_unite.election_unite
    LEFT JOIN ".DB_PREFIXE."election_candidat
        ON election_resultat.election_candidat = election_candidat.election_candidat
    LEFT JOIN ".DB_PREFIXE."candidat
        ON election_candidat.candidat = candidat.candidat
    LEFT JOIN ".DB_PREFIXE."unite
        ON election_unite.unite = unite.unite
    LEFT JOIN ".DB_PREFIXE."election
        ON election_unite.election = election.election
        ";

// SORT
$tri = " ORDER BY election.date, election.libelle, unite.ordre, candidat.libelle";
// SELECT
$displayFieldUnite = 'CONCAT_WS(\' - \', unite.code_unite, unite.libelle) as "'.__("unité").'"';
$champAffiche = array(
    'election_resultat.election_resultat as "'.__("id").'"',
    'election.libelle as "'.__("élection").'"',
    'candidat.libelle as "'.__("candidat").'"',
    $displayFieldUnite,
    'election_resultat.resultat as "'.__("résultat").'"',
    );

$champRecherche = array(
    'election_unite.election_unite as "'.__("election_unite").'"',
    'election.libelle as "'.__("election").'"',
    'unite.libelle as "'.__("unite").'"',
    'candidat.libelle as "'.__("candidat").'"',
    );

// Recherche avancée
$champs = array();
$champs["election"] = array(
    "libelle" => __("élection"),
    "table" => "election",
    "colonne" =>   "election",
    "type" => "select",
);
$champs["candidat"] = array(
    "libelle" => __("candidat"),
    "table" => "candidat",
    "colonne" =>  "candidat",
    "type" => "select",
);
$champs["unite"] = array(
    "libelle" => __("unité"),
    "table" => "unite",
    "colonne" =>  "unite",
    "type" => "select",
);
$options[] = array(
    "type" => "search",
    "display" => true,
    "advanced"  => $champs,
    "default_form"  => "advanced",
    "absolute_object" => "election_resultat",
    "export" => array("csv"),
);
