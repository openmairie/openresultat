<?php
//$Id$ 
//gen openMairie le 04/05/2021 16:02

include "../gen/sql/pgsql/plan_unite.inc.php";

// SORT
$tri = " ORDER BY unite.ordre ";
// SELECT 
$displayFieldUnite = 'CONCAT_WS(\' - \', unite.code_unite, unite.libelle) as "'.__("unité").'"';
$champAffiche = array(
    'plan_unite.plan_unite as "'.__("plan_unite").'"',
    'plan.libelle as "'.__("plan").'"',
    $displayFieldUnite,
    'plan_unite.position_x as "'.__("position_x").'"',
    'plan_unite.position_y as "'.__("position_y").'"',
    );