<?php
//$Id$
//gen openMairie le 16/12/2020 17:03

include "../gen/sql/pgsql/plan_election.inc.php";

if (in_array($retourformulaire, $foreign_keys_extended["election"])) {
    $selection = " WHERE (plan_election.election IN (
            SELECT
                election.election
            FROM
                ".DB_PREFIXE."election
            WHERE
                election.election = ".intval($idxformulaire)." OR
                election.election_reference = ".intval($idxformulaire)."
        ))";

    $workflow = $this->get_workflow_election();
    
    if ($workflow === false ||
        $workflow === 'Archivage'
    ) {
        $tab_actions['corner']['ajouter'] = null;
    }
}

// FROM
$table = DB_PREFIXE."plan_election
    LEFT JOIN ".DB_PREFIXE."election
        ON plan_election.election=election.election
    LEFT JOIN ".DB_PREFIXE."plan
        ON plan_election.plan=plan.plan";

$champAffiche = array(
    'plan_election as "'.__("plan election").'"',
    'plan.libelle as "'.__("plan").'"',
    'election.libelle as "'.__("election").'"'
    );

$workflow = $this->get_workflow_election();

if ($workflow === false ||
    $workflow === 'Saisie' ||
    $workflow === 'Finalisation' ||
    $workflow === 'Archivage'
) {
    $tab_actions['corner']['ajouter'] = null;
}
