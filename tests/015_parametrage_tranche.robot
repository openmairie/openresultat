*** Settings ***
Documentation     Test du parametrage d'une tranche horaire
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Constitution du jeu de données
    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.
    Depuis la page d'accueil  admin  admin


Listing des tranches
    [Documentation]  Ce 'Test Case' vérifie l'intégration du listing des
    ...  enregistrements de type 'tranche'.
    # le lien est présent dans le menu
    Depuis l'écran 'Administration & Paramétrage'
    Element Should Contain  css=#settings a.tranche  tranche
    # le lien amène sur la page correcte
    Click Element  css=#settings a.tranche
    Page Title Should Be  Administration & Paramétrage > Élection > Tranche
    La page ne doit pas contenir d'erreur
    # le mot clé raccourci amène sur la page correcte
    Depuis le listing des tranches
    Page Title Should Be  Administration & Paramétrage > Élection > Tranche


Ajout d'un tranche
    [Documentation]  Ce 'Test Case' vérifie l'intégration de l'action 'ajouter'
    ...  depuis le contexte 'tranche'.

    Depuis le listing des tranches
    Click On Add Button
    Page Title Should Be  Administration & Paramétrage > Élection > Tranche
    La page ne doit pas contenir d'erreur

    &{tranche} =  BuiltIn.Create Dictionary
    ...  libelle=23:00:00
    ...  ordre=666
    Ajouter tranche  ${tranche}
    Element Should Contain In Subform  css=div.message  enregistrées

