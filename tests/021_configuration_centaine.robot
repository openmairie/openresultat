*** Settings ***
Documentation     Test des centaines
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Jeu de données
    [Documentation]  L'objet de ce test case est de creer le jeu de données.
    ...  Ce jeu de données consiste à créer une élection. Cette élection est
    ...  composé d'un arrondissement avec deux mairies. La mairie 1 possède
    ...  deux BV et la mairie deux n'en possède qu'un. De plus, deux candidats
    ...  participent à cette élection.

    Depuis la page d'accueil    admin  admin

    # Modèle web permettant d'afficher les centaines et activation de ce modèle
    &{web} =  BuiltIn.Create Dictionary
    ...  libelle=test centaine
    ...  display_simulation=true
    ${id_modele_web} =  Ajouter web  ${web}
    Activer modèle web  ${id_modele_web}

    # Creation d'une election ayant pour périmètre la mairie
    &{election} =  BuiltIn.Create Dictionary
    ...  libelle=Election test centaine
    ...  code=ET100
    ...  principal=true
    ...  type_election=Municipales
    ...  date=09/06/1995
    ...  perimetre=COMMUNE
    ...  heure_ouverture=08:00:00
    ...  heure_fermeture=12:00:00
    ${id_election} =  Ajouter election  ${election}
    Element Should Contain In Subform  css=div.message  enregistrées
    Set Suite Variable  ${id_election}

    # Ajout de deux candidats avec des photos pour voir si les photos pose problème pour les
    # centaine
    Set Suite Variable  ${ordre_cand1}  1
    Set Suite Variable  ${ordre_cand2}  2
    &{candidat} =  BuiltIn.Create Dictionary
    ...  candidat=David G.
    ...  ordre=${ordre_cand1}
    ...  photo=candidat1.png
    ${id_candidat1} =  Ajouter election_candidat  ${candidat}  ${id_election}
    Set Suite Variable  ${id_candidat1}

    &{candidat} =  BuiltIn.Create Dictionary
    ...  candidat=Jean Michel B.
    ...  ordre=${ordre_cand2}
    ...  photo=candidat2.png
    ${id_candidat2} =  Ajouter election_candidat  ${candidat}  ${id_election}

    Depuis le contexte election  ${id_election}
    Passer à l'étape suivante  simulation  Simulation

    # Remplissage des inscrits d'une des unités pour vérifier que le nombre d'inscrit est
    # bien enregistré pour l'unité de la centaine lors de sa création
    Depuis le contexte election_unite  ${id_election}  2 - Salle des fetes 1
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  500
    Click On Submit Button In Subform

Paramétrage centaine
    [Documentation]  Test du paramétrage d'une centaine. Vérifie que lors de
    ...  la création/modification d'une centaine le nombre de votant enregistré
    ...  est le bon. Vérifie également que les fichiers des répertoires aff et web
    ...  sont bien créés

    &{centaine} =  BuiltIn.Create Dictionary
    ...  libelle=Centaine
    ...  votant_defaut=200
    ${id_centaine} =  Ajouter centaine à l'élection  ${centaine}  ${id_election}
    Set Suite Variable  ${id_centaine}
    La page ne doit pas contenir d'erreur

    # Vérification du nombre de votant dans le sous-tableau de la centaine
    Element Should Contain  css=div#sousform-election_unite_overlay table.tab-tab tr:nth-child(2) td:nth-child(6)    200

    # Modification du nombre de votant pour s'assurer qu'ils sont bien mis à jour
    &{centaine} =  BuiltIn.Create Dictionary
    ...  votant_defaut=100
    Modifier centaine de l'élection  ${id_election}  ${id_centaine}  ${centaine}
    # Vérification du nombre de votant dans le sous-tableau de la centaine
    Element Should Contain  css=div#sousform-election_unite_overlay table.tab-tab tr:nth-child(2) td:nth-child(6)    100
    # Vérification du nombre d'inscrit pour l'unité 2
    Element Should Contain  css=div#sousform-election_unite_overlay table.tab-tab tr:nth-child(2) td:nth-child(5)    500

Saisie des résultats des centaines
    [Documentation]  Test servant à valider le fonctionnement de la saisie des résultats
    ...  par unité des centaines.

    # Remplissage du nombre d'inscrit pour vérifier qu'elle est remplis dans la centaine
    Depuis le contexte election_unite  ${id_election}  1 - Salle des Mariages
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  1000
    Click On Submit Button In Subform

    # Accès au formulaire de saisie et saisie des résultats
    Depuis le contexte centaine  ${id_election}  ${id_centaine}
    Click On Link  xpath=//td[normalize-space(text()) = "1 - Salle des Mariages"]//ancestor::tr/td[contains(@class, "icons")]/a
    La page ne doit pas contenir d'erreur
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  blanc  0
    Input Text  nul  2
    Input Text  exprime  98
    Input Text  candidat${ordre_cand1}  88
    Input Text  candidat${ordre_cand2}  10
    Click On Submit Button In Subform
    La page ne doit pas contenir d'erreur

    # Les résultats ne doivent pas être automatiquement envoyés
    File Should Not Exist  ../web/res/00${id_centaine}-0C/b1.html
    File Should Not Exist  ../aff/res/${id_centaine}/bres/1.json

    # Vérification de l'enregistrement des résultats
    Click On Link  css=div.ui-dialog .formControls-top a[id^=sousform-action-election_unite-back]
    Element Should Contain  css=div#sousform-election_unite_overlay table.tab-tab tr:nth-child(1) td:nth-child(5)    1000
    Element Should Contain  css=div#sousform-election_unite_overlay table.tab-tab tr:nth-child(1) td:nth-child(6)    100
    Element Should Contain  css=div#sousform-election_unite_overlay table.tab-tab tr:nth-child(1) td:nth-child(9)    0
    Element Should Contain  css=div#sousform-election_unite_overlay table.tab-tab tr:nth-child(1) td:nth-child(10)    2
    Element Should Contain  css=div#sousform-election_unite_overlay table.tab-tab tr:nth-child(1) td:nth-child(11)    98

Animation des centaines
    [Documentation]  Test servant à valider le fonctionnement des animations des centaines.
    ...  Vérifie que les centaines sont visibles dans le select du formulaire des animations
    ...  que l'envoi des résultats des centaines et que l'accès à l'animation se font
    ...  correctement

    # Envoi des résultats à l'animation
    Envoyer les résultats de la centaine à l'affichage  ${id_centaine}  ${id_election}  1 - Salle des Mariages

    # Ajout d'une animation liée à la centaine
    &{animation} =  BuiltIn.Create Dictionary
    ...  election=Centaine
    ...  libelle=Animation des résultats
    ...  modele=Résultat
    ...  titre=Résultats de la centaine
    ...  type_aff=Bureau de vote
    ${id_animation} =  Ajouter animation à l'élection  ${animation}  ${id_election}
    Set Suite Variable  ${id_animation}
    La page ne doit pas contenir d'erreur
    Le fichier doit exister  ../aff/res/${id_centaine}/perimetres.json
    Le fichier doit exister  ../aff/res/${id_centaine}/photo/2.png
    Le fichier doit exister  ../aff/res/${id_centaine}/photo/1.png

    # Accès à l'animation
    Click On SubForm Portlet Action  animation  animation  new_window
    Go To  http://localhost/openresultat/aff/animation.php?idx=${id_centaine}&identifier=${id_animation}
    La page ne doit pas contenir d'erreur

Mise à jour des candidats
    [Documentation]  Test servant à valider si la modification des candidats
    ...  sur l'élection met bien à jour les centaines et leurs animations

    # Ajout d'un nouveau candidat à l'élection
    Set Suite Variable  ${ordre_cand3}  3
    &{candidat} =  BuiltIn.Create Dictionary
    ...  candidat=Pierre CHENEL
    ...  ordre=${ordre_cand3}
    ...  photo=candidat1.png
    ${id_candidat3} =  Ajouter election_candidat  ${candidat}  ${id_election}
    La page ne doit pas contenir d'erreur

    # Vérification de la présence du candidat sur la centaine
    Depuis le contexte centaine  ${id_election}  ${id_centaine}
    Click On Link   xpath=//td[normalize-space(text()) = "1 - Salle des Mariages"]//ancestor::tr/td[contains(@class, "icons")]/a
    Table Should Contain  css=.ui-dialog fieldset.cadre .tab-tab  Pierre CHENEL

    # Modification du candidat de l'élection
    &{candidat} =  BuiltIn.Create Dictionary
    ...  candidat=Hervé SCHIAVETTI
    ...  ordre=${ordre_cand3}
    ...  photo=plan_centre.gif
    Modifier election_candidat  ${id_election}  ${id_candidat3}  ${candidat}
    La page ne doit pas contenir d'erreur
    # Mise à jour des photos
    File Should Not Exist    ../aff/res/${id_centaine}/photo/${ordre_cand3}.png
    Le fichier doit exister  ../aff/res/${id_centaine}/photo/${ordre_cand3}.gif

    # Vérification de la modification du candidat sur la centaine
    Depuis le contexte centaine  ${id_election}  ${id_centaine}
    Click On Link   xpath=//td[normalize-space(text()) = "1 - Salle des Mariages"]//ancestor::tr/td[contains(@class, "icons")]/a
    Table Should Contain  css=.ui-dialog fieldset.cadre .tab-tab  Hervé SCHIAVETTI

    # Envoi des résultats à l'animation
    Envoyer les résultats de la centaine à l'affichage  ${id_centaine}  ${id_election}  1 - Salle des Mariages
    # Accès à l'animation
    Depuis le contexte animation de l'election  ${id_election}  ${id_animation}
    Click On SubForm Portlet Action  animation  animation  new_window
    Go To  http://localhost/openresultat/aff/animation.php?idx=${id_centaine}&identifier=${id_animation}
    La page ne doit pas contenir d'erreur

    # Suppression du candidat de l'élection
    Supprimer election_candidat  ${id_election}  ${id_candidat3}
    La page ne doit pas contenir d'erreur

    # Vérification de la suppression du candidat sur la centaine
    Depuis le contexte centaine  ${id_election}  ${id_centaine}
    Click On Link   xpath=//td[normalize-space(text()) = "1 - Salle des Mariages"]//ancestor::tr/td[contains(@class, "icons")]/a
    Page Should Not Contain  css=.ui-dialog fieldset.cadre .tab-tab  Hervé SCHIAVETTI

Affichage Web des centaines
    [Documentation]  Test servant à valider le fonctionnement de l'affichage web des centaines.
    ...  Vérifie que l'accès au portail web et l'envoi des résultats à la page web fonctionnent
    ...  sans erreur.

    # Envoi des résultats à la page web
    Envoyer les résultats de la centaine au portail web  ${id_centaine}  ${id_election}  1 - Salle des Mariages

    # Accés au portail web de la centaine
    Depuis le contexte centaine  ${id_election}  ${id_centaine}
    Go To  http://localhost/openresultat/web/index.php?election=00${id_centaine}-0C
    La page ne doit pas contenir d'erreur

    # Vérification des résultats
    Element Should Contain  css=div#resultats-collectivite > table tr:nth-child(1) td:nth-child(2)    1 500
    Element Should Contain  css=div#resultats-collectivite > table tr:nth-child(2) td:nth-child(2)    1 400
    Element Should Contain  css=div#resultats-collectivite > table tr:nth-child(3) td:nth-child(2)    100
    Element Should Contain  css=div#resultats-collectivite > table tr:nth-child(4) td:nth-child(2)    0
    Element Should Contain  css=div#resultats-collectivite > table tr:nth-child(5) td:nth-child(2)    2
    Element Should Contain  css=div#resultats-collectivite > table tr:nth-child(6) td:nth-child(2)    98

Suppression des centaines
    [Documentation]  Test servant à vérifier que la suppression d'une centaine entraine bien la
    ...  suppression de ses résultats dans les répertoires web et aff

    Supprimer centaine de l'élection  ${id_election}  ${id_centaine}
    Message Should Contain  La suppression a été correctement effectuée.

    File Should Not Exist  ../web/res/00${id_centaine}-0C
    File Should Not Exist  ../aff/res/${id_centaine}