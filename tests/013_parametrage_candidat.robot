*** Settings ***
Documentation     Test du parametrage d'un candidat
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Constitution du jeu de données
    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.
    Depuis la page d'accueil  admin  admin


Listing des candidats
    [Documentation]  Ce 'Test Case' vérifie l'intégration du listing des
    ...  enregistrements de type 'candidat'.
    # le lien est présent dans le menu
    Depuis l'écran 'Administration & Paramétrage'
    Element Should Contain  css=#settings a.candidat  candidat
    # le lien amène sur la page correcte
    Click Element  css=#settings a.candidat
    Page Title Should Be  Administration & Paramétrage > Élection > Candidat
    La page ne doit pas contenir d'erreur
    # le mot clé raccourci amène sur la page correcte
    Depuis le listing des candidats
    Page Title Should Be  Administration & Paramétrage > Élection > Candidat


Ajout d'un candidat
    [Documentation]  Ce 'Test Case' vérifie l'intégration de l'action 'ajouter'
    ...  depuis le contexte 'candidat'.

    Depuis le listing des candidats
    Click On Add Button
    Page Title Should Be  Administration & Paramétrage > Élection > Candidat
    La page ne doit pas contenir d'erreur

    &{candidat} =  BuiltIn.Create Dictionary
    ...  libelle=candidatTest
    ...  libelle_liste=test
    Ajouter candidat  ${candidat}
    Element Should Contain In Subform  css=div.message  enregistrées


Avertissement si un candidat de même nom existe déjà
    [Documentation]  L'objet de ce test case est de vérifier qu'un message d'avertissement
    ...  est bien affiché lorsque l'on crée un candidat ayant le même nom qu'un candidat déjà
    ...  existant

    # creation du nouvelle unité ayant le même nom que l'unité créée dans le premier test
    &{candidat} =  BuiltIn.Create Dictionary
    ...  libelle=candidatTest
    ...  libelle_liste=test
    Ajouter candidat  ${candidat}
    La page ne doit pas contenir d'erreur
    Message Should Contain   ATTENTION : un candidat portant ce nom existe déjà !