*** Settings ***
Documentation     Test du parametrage des communes
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Constitution du jeu de données
    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.
    Depuis la page d'accueil  admin  admin


Listing des communes
    [Documentation]  Ce 'Test Case' vérifie l'intégration du listing des
    ...  enregistrements de type 'commune'.
    # le lien est présent dans le menu
    Depuis l'écran 'Administration & Paramétrage'
    Element Should Contain  css=#settings a.commune  commune
    # le lien amène sur la page correcte
    Click Element  css=#settings a.commune
    Page Title Should Be  Administration & Paramétrage > Découpage Administratif > Commune
    La page ne doit pas contenir d'erreur
    # le mot clé raccourci amène sur la page correcte
    Depuis le listing des communes
    Page Title Should Be  Administration & Paramétrage > Découpage Administratif > Commune


Ajout d'une commune
    [Documentation]  Ce 'Test Case' vérifie l'intégration de l'action 'ajouter'
    ...  depuis le contexte 'commune'.

    Depuis le listing des communes
    Click On Add Button
    Page Title Should Be  Administration & Paramétrage > Découpage Administratif > Commune
    La page ne doit pas contenir d'erreur

    # creation de la commune
    &{commune} =  BuiltIn.Create Dictionary
    ...  libelle=ma commune
    ...  code=COMM
    ...  prefecture=42
    Ajouter commune  ${commune}
    La page ne doit pas contenir d'erreur
    # message de reussite
    Element Should Contain In Subform  css=div.message  enregistrées

