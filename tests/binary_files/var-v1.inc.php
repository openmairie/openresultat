<?php
/**
 * Script de configuration de l"affichage WEB.
 *
 * @package openresultat
 * @version SVN : $Id$
 */

/**
 * Affichage ou non des simulations.
 * - true : les centaines seront affichées
 * - false : les centaines ne seront pas affichées
 */
$display_simulation = false;

/**
 * Surcharge du libellé de l'élection.
 * Pour un affichage homogène, il est préférable de surcharger le libellé de 
 * l'élection par le type de l'élection et son année.
 * - true : les libellés sont homogénéisés
 * - false : les libellés originaux sont affichés
 */
$override_election_libelle = true;

/**
 * Liste des plans.
 * Il faut faire une entrée pour chaque plan que possède la collectivité,
 * la clés de cette entrée doit être le nom de l'image. Chaque entrée comporte
 * trois informations
 * - img : lien vers l'image
 * - libelle : nom du plan
 * - id : identifiant du plan
 * Exemple d'utilisation de la variable :
 * $plans = array(
 *     "centre.gif" => array (
 *         'img' => 'img/plan/centre.gif',
 *         'libelle' => 'Secteur Nord',
 *         'id' => 'secteur-nord',
 *     ),
 *     "quartier.gif" => array (
 *         'img' => 'img/plan/quartier.gif',
 *         'libelle' => 'Secteur Sud',
 *         'id' => 'secteur-sud',
 *     ),
 *     "village.gif" => array (
 *          'img' => 'img/plan/village.gif',
 *          'libelle' => 'Secteur Nord-Est',
 *          'id' => 'secteur-nord-est',
 *     )
 * );
 */
$plans = array( "plan.gif" => array ( "img" => "img/plan/plan.gif", "libelle" => "Sur carte", "id" => "sur-carte", ) );

/**
 * Informations sur la collectivité
 * - title : nom de la collectivité
 * - logo : lien vers le logo (img/logo.png)
 * - url : lien vers le site web de la collectivité (doit commencer par http)
 */
$infos_collectivite = array(
    "title" => "Trets",
    "logo" => "img/logo.png",
    "url" => "",
);

/**
 * JavaScript à ajouter à l'affichage web
 * Il ne faut pas mettre les balises HTML pour le javascript
 */
$jscript_stats = "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-5052549-10', 'atreal.fr');
ga('send', 'pageview');
";

?>