<?php
/**
 * Ce script contient la configuration DATABASE.
 *
 * @package openresultat
 * @version SVN : $Id$
 */

$conn = array(
    0 => array(
        "openRésultat", // Titre
        "pgsql", // Type de base
        "pgsql", // Type de base
        "postgres", // Login
        "postgres", // Mot de passe
        "tcp", // Protocole de connexion
        "localhost", // Nom d'hote
        "5432", // Port du serveur
        "", // Socket
        "openresultat", // Nom de la base
        "AAAA-MM-JJ", // Format de la date
        "openresultat", // Nom du schéma
        "", // Préfixe
        null, // Paramétrage pour l'annuaire LDAP
        null, // Paramétrage pour le serveur de mail
        null, // Paramétrage pour le stockage des fichiers
    ),
);

