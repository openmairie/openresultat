*** Settings ***
Documentation     Test du parametrage du type d'election
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Constitution du jeu de données
    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.
    Depuis la page d'accueil  admin  admin


Listing des types d'élection
    [Documentation]  Ce 'Test Case' vérifie l'intégration du listing des
    ...  enregistrements de type 'type_election'.
    # le lien est présent dans le menu
    Depuis l'écran 'Administration & Paramétrage'
    Element Should Contain  css=#settings a.type_election  type d'élection
    # le lien amène sur la page correcte
    Click Element  css=#settings a.type_election
    Page Title Should Be  Administration & Paramétrage > Élection > Type D'élection
    La page ne doit pas contenir d'erreur
    # le mot clé raccourci amène sur la page correcte
    Depuis le listing des types d'élection
    Page Title Should Be  Administration & Paramétrage > Élection > Type D'élection


Ajout d'un type d'élection
    [Documentation]  Ce 'Test Case' vérifie l'intégration de l'action 'ajouter'
    ...  depuis le contexte 'type_election'.

    Depuis le listing des types d'élection
    Click On Add Button
    Page Title Should Be  Administration & Paramétrage > Élection > Type D'élection
    La page ne doit pas contenir d'erreur

    &{type_election} =  BuiltIn.Create Dictionary
    ...  libelle=test
    ...  code=TST
    ...  prefecture=TS
    Ajouter type_election  ${type_election}
    Element Should Contain In Subform  css=div.message  enregistrées
