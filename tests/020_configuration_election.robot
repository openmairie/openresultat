*** Settings ***
Documentation     Test du parametrage et de la configuration d'une election
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Parametrage des tests
    [Documentation]  L'objet de ce test case est de creer le parametrage métier
    ...  qui servira ensuite à tester la configuration de l'élection

    Depuis la page d'accueil    admin  admin

    #creation de trois types d'unites : bureau, mairie et arrondissement
    &{bureau} =  BuiltIn.Create Dictionary
    ...  libelle=bureau
    ...  hierarchie=1
    ...  bureau_vote=true
    Ajouter type_unite  ${bureau}

    &{mairie} =  BuiltIn.Create Dictionary
    ...  libelle=mairie
    ...  hierarchie=3
    ...  bureau_vote=false
    Ajouter type_unite  ${mairie}

    &{arrondissement} =  BuiltIn.Create Dictionary
    ...  libelle=arrondissement
    ...  hierarchie=5
    ...  bureau_vote=false
    Ajouter type_unite  ${arrondissement}

    # Ajout des unites : bureau_a, bureau_b, bureau_c
    # de leurs mairies : mairie_de_ab et mairie_de_c
    # de leur arrondissement : arrondissement_abc
    &{bureau_a} =  BuiltIn.Create Dictionary
    ...  libelle=bureau_a
    ...  type_unite=bureau
    ...  ordre=41
    ...  code_unite=41
    ${id_bureau_a} =  Ajouter unite  ${bureau_a}
    Set Suite Variable  ${id_bureau_a}

    &{bureau_b} =  BuiltIn.Create Dictionary
    ...  libelle=bureau_b
    ...  type_unite=bureau
    ...  ordre=42
    ...  code_unite=42
    ${id_bureau_b} =  Ajouter unite  ${bureau_b}
    Set Suite Variable  ${id_bureau_b}

    &{bureau_c} =  BuiltIn.Create Dictionary
    ...  libelle=bureau_c
    ...  type_unite=bureau
    ...  ordre=43
    ...  code_unite=43
    ${id_bureau_c} =  Ajouter unite  ${bureau_c}
    Set Suite Variable  ${id_bureau_c}

    &{mairie_de_ab} =  BuiltIn.Create Dictionary
    ...  libelle=mairie_de_ab
    ...  type_unite=mairie
    ...  ordre=44
    ...  perimetre=true
    ...  type_unite_contenu=bureau
    Ajouter unite  ${mairie_de_ab}

    &{mairie_de_c} =  BuiltIn.Create Dictionary
    ...  libelle=mairie_de_c
    ...  type_unite=mairie
    ...  ordre=45
    ...  perimetre=true
    ...  type_unite_contenu=bureau
    Ajouter unite  ${mairie_de_c}

    &{arrondissement_abc} =  BuiltIn.Create Dictionary
    ...  libelle=arrondissement_abc
    ...  type_unite=arrondissement
    ...  ordre=46
    ...  perimetre=true
    ...  type_unite_contenu=mairie
    Ajouter unite  ${arrondissement_abc}

    # Lien entre ces unites pour creer le perimetre de l'election
    Ajouter lien_unite  mairie_de_ab  41 - bureau_a
    Ajouter lien_unite  mairie_de_ab  42 - bureau_b
    Ajouter lien_unite  mairie_de_c   43 - bureau_c
    Ajouter lien_unite  arrondissement_abc  mairie_de_ab
    Ajouter lien_unite  arrondissement_abc  mairie_de_c

    # Ajout d'un logo
    Ajouter le logo  logo_test  logo  openResultat.png  logo openresultat

    #creation des candidats
    &{candidat} =  BuiltIn.Create Dictionary
    ...  libelle=candidat1
    Ajouter candidat  ${candidat}

    &{candidat} =  BuiltIn.Create Dictionary
    ...  libelle=candidat2
    Ajouter candidat  ${candidat}

    &{candidat} =  BuiltIn.Create Dictionary
    ...  libelle=candidat3
    Ajouter candidat  ${candidat}

    # Creation de deux plan dont un avec l'attribut par défaut coché
    &{plan} =  BuiltIn.Create Dictionary
    ...  libelle=mairie_de_ab
    ...  image_plan=plan_centre.gif
    ...  img_unite_arrivee=img_unite_arrivee.gif
    ...  img_unite_non_arrivee=img_unite_non_arrivee.gif
    ...  par_defaut=true
    ${id_plan} =  Ajouter plan  ${plan}
    Set Suite Variable  ${id_plan}

    &{plan} =  BuiltIn.Create Dictionary
    ...  libelle=plan_2
    ...  image_plan=plan_centre.gif
    ...  img_unite_arrivee=img_unite_arrivee.gif
    ...  img_unite_non_arrivee=img_unite_non_arrivee.gif
    ${id_plan2} =  Ajouter plan  ${plan}
    Set Suite Variable  ${id_plan2}

    # Ajout de lien entre le plan et les unités
    &{plan_unite} =  BuiltIn.Create Dictionary
    ...  plan=plan_2
    ...  unite=41 - bureau_a
    ...  img_unite_arrivee=bureau_a.png
    ...  img_unite_non_arrivee=bureau_na.png
    ...  position_x=0
    ...  position_y=0
    ${id_plan_unite} =  Ajouter plan_unite  ${plan_unite}
    Set Suite Variable  ${id_plan_unite}

election
    [Documentation]  L'objet de ce test case est de verifier le fonctionnement de
    ...  la creation d'une election.
    ...  Il vérifie également que les unités de saisie ainsi que la participation
    ...  et les plans par défaut sont ajoutées à l'élection lors de sa création.
    ...  Vérifie aussi que les fichiers de paramétrage du portail web et des
    ...  plans ainsi que les images liées au plan et au logo sont ajoutés au portail
    ...  web et que le fichier decrivant la structure des unités est envoyé à l'affichage

    Depuis la page d'accueil  admin  admin

    Go To Submenu In Menu   application  election
    Page Title Should Be   Application > Élection
    Submenu In Menu Should Be Selected    application  election
    La page ne doit pas contenir d'erreur

    &{election} =  BuiltIn.Create Dictionary
    ...  libelle=election maire ab
    ...  code=lyoko
    ...  type_election=Municipales
    ...  date=09/06/1995
    ...  perimetre=arrondissement_abc
    ...  heure_ouverture=08:00:00
    ...  heure_fermeture=10:00:00
    ${id_election_maire} =  Ajouter election  ${election}
    Set Suite Variable  ${id_election_maire}
    La page ne doit pas contenir d'erreur

    # Les unites et la participation sont bien crees
    Valid Message Should Contain  6 unité(s) créée(s)
    Valid Message Should Contain  2 participation(s) élection créées
    Valid Message Should Contain  12 participation(s) par unité créées

    # Vérification que les fichiers ont bien été créés
    Le fichier doit exister  ../web/res/00${id_election_maire}-0/plans.json
    Le fichier doit exister  ../web/res/00${id_election_maire}-0/plan_${id_plan}/img_unite_arrivee.gif
    Le fichier doit exister  ../web/res/00${id_election_maire}-0/plan_${id_plan}/img_unite_non_arrivee.gif
    Le fichier doit exister  ../web/res/00${id_election_maire}-0/plan_${id_plan}/plan.gif

    # Modification de l'attribut par défaut du plan pour éviter qu'il ne soit ajouté
    # a chaque éléction crée dans les tests
    &{plan} =  BuiltIn.Create Dictionary
    ...  par_defaut=false
    Modifier plan  ${id_plan}  ${plan}

candidat de l'élection
    [Documentation]  L'objet de ce test case est de verifier le fonctionnement de
    ...  l'ajout des candidats à l'élection. Vérifie donc qu'il n'y a pas d'erreur
    ...  lors de l'ajout et que les résultats des candidats sont bien créés.
    ...  Valide également la suppression du candidat et de ses résultats en
    ...  utilisant l'action supprimer

    # Le numéro d'ordre doit être conservé pour identifier le champ de saisie
    # des résultats du candidat lors de la saisie des résultats
    Set Suite Variable  ${ordre_cand1}  1
    Set Suite Variable  ${ordre_cand2}  2
    Set Suite Variable  ${ordre_cand3}  3
    &{candidat_test} =  BuiltIn.Create Dictionary
    ...  candidat=candidat1
    ...  ordre=${ordre_cand1}
    ...  image=candidat1.png
    ${id_candidat1} =  Ajouter election_candidat  ${candidat_test}  ${id_election_maire}
    Set Suite Variable  ${id_candidat1}
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Valid Message Should Contain  Les résultats du candidat ont été ajoutés

    &{candidat_test} =  BuiltIn.Create Dictionary
    ...  candidat=candidat2
    ...  ordre=${ordre_cand2}
    ...  image=candidat2.png
    ${id_candidat2} =  Ajouter election_candidat  ${candidat_test}  ${id_election_maire}
    Valid Message Should Contain  Les résultats du candidat ont été ajoutés

    &{candidat_test} =  BuiltIn.Create Dictionary
    ...  candidat=candidat3
    ...  ordre=${ordre_cand3}
    ${id_candidat3} =  Ajouter election_candidat  ${candidat_test}  ${id_election_maire}
    Valid Message Should Contain  Les résultats du candidat ont été ajoutés

    Supprimer election_candidat  ${id_election_maire}  ${id_candidat3}
    Valid Message Should Contain  Suppression des résultats effectuée
    Valid Message Should Contain  La suppression a été correctement effectuée.

animation de l'élection
    [Documentation]  L'objet de ce test case est de verifier le fonctionnement de
    ...  l'ajout d'une animation à l'élection. Vérifie donc qu'il n'y a pas d'erreur
    ...  lors de l'ajout et que les fichiers de paramétrage de l'animation sont
    ...  envoyés dans le repertoire aff

    &{animation} =  BuiltIn.Create Dictionary
    ...  libelle=animation_test_election
    ...  modele=Résultat
    ...  perimetre_aff=arrondissement_abc
    ...  type_aff=bureau
    ${id_animation} =  Ajouter animation à l'élection  ${animation}  ${id_election_maire}
    Set Suite Variable  ${id_animation}
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

    # Vérification que les fichiers ont bien été créés
    Le fichier doit exister  ../aff/res/${id_election_maire}/perimetres.json
    Le fichier doit exister  ../aff/res/${id_election_maire}/animation_${id_animation}/blocs.ini
    Le fichier doit exister  ../aff/res/${id_election_maire}/animation_${id_animation}/param.ini

plans de l'élection
    [Documentation]  L'objet de ce test case est de verifier le fonctionnement de
    ...  l'ajout d'un plan à l'élection. Vérifie donc qu'il n'y a pas d'erreur
    ...  lors de l'ajout et que les fichiers de paramétrage du plan sont
    ...  envoyés dans le repertoire aff

    &{plan_election} =  BuiltIn.Create Dictionary
    ...  plan=plan_2
    ${id_plan2} =  Ajouter plan_election  ${plan_election}  ${id_election_maire}
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

    # Vérification que les fichiers ont bien été créés
    Le fichier doit exister  ../web/res/00${id_election_maire}-0/plan_${id_plan2}/plan.gif
    Le fichier doit exister  ../web/res/00${id_election_maire}-0/plan_${id_plan2}/img_unite_arrivee.gif
    Le fichier doit exister  ../web/res/00${id_election_maire}-0/plan_${id_plan2}/img_unite_non_arrivee.gif
    Le fichier doit exister  ../web/res/00${id_election_maire}-0/plan_${id_plan2}/${id_plan_unite}/img_unite_arrivee.png
    Le fichier doit exister  ../web/res/00${id_election_maire}-0/plan_${id_plan2}/${id_plan_unite}/img_unite_arrivee.png

Verification du parametrage
    [Documentation]  L'objet de ce test case est de valider le fonctionnement de la
    ...  fonction de vérification du parametrage

    Depuis le contexte election  ${id_election_maire}

    # Verification du parametrage
    Click On Form Portlet Action  election  verification  message  Nombre d'unités : 6

    # La participation de l'election par tranche est cree
    Valid Message Should Contain  nombre de candidat : 2
    Valid Message Should Contain  Les candidats sont correctement enregistrés
    Valid Message Should Contain  Nombre de tranche horaire : 2
    Valid Message Should Contain  La participation est correctement enregistrée
    Valid Message Should Contain  Nombre de plans : 2
    Valid Message Should Contain  Nombre d'animations : 1

saisie des resultats
    [Documentation]  L'objet de ce test case est de valider le fonctionnement de la
    ...  saisie des résultats. La saisie et l'enregistrement des résultats doit être
    ...  réalisé sans erreur et les résultats des périmètres doivent être mis à jour.
    ...  Test également que la vérification de la saisie fonctionne.
    ...  Test aussi la saisie automatique et l'envoi des résultats en erreur selon les
    ...  options sélectionnées

    Click On Form Portlet Action  election  simulation  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer

    # Test de la saisie avec saisie manuelle des votes exprimés
    Depuis le contexte election_unite  ${id_election_maire}  41 - bureau_a
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  100
    Input Text  votant  97
    Input Text  emargement  97
    Input Text  procuration  3
    Input Text  blanc  3
    Input Text  nul  4
    Input Text  exprime  95
    Input Text  candidat${ordre_cand1}  90
    Input Text  candidat${ordre_cand2}  5
    Click On Submit Button In Subform

    La page ne doit pas contenir d'erreur
    Valid Message Should Contain    Total exprimé : 95

    # Test de la saisie automatique
    &{election} =  BuiltIn.Create Dictionary
    ...  calcul_auto_exprime=true
    Modifier election  ${id_election_maire}  ${election}

    Depuis le contexte election_unite  ${id_election_maire}  42 - bureau_b
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  100
    Input Text  votant  97
    Input Text  emargement  97
    Input Text  procuration  3
    Input Text  blanc  3
    Input Text  nul  4
    Input Text  candidat${ordre_cand1}  47
    Input Text  candidat${ordre_cand2}  50
    Click On Submit Button In Subform

    La page ne doit pas contenir d'erreur
    Valid Message Should Contain    Total exprimé : 90

    # Validation de la vérification de la saisie
    # Les éléments vérifiés sont :
    #  -> exprimes = votants - (nuls + blancs)
    #  -> exprimes = total obtenus par les candidats
    #  -> votants < inscrits
    #  -> votants > blancs + nuls
    #  -> emargements > votants
    &{election} =  BuiltIn.Create Dictionary
    ...  calcul_auto_exprime=false
    Modifier election  ${id_election_maire}  ${election}

    Depuis le contexte election_unite  ${id_election_maire}  43 - bureau_c
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  1000
    Input Text  votant  1200
    Input Text  emargement  1500
    Input Text  procuration  1001
    Input Text  blanc  1200
    Input Text  nul  300
    Input Text  exprime  300
    Input Text  candidat${ordre_cand1}  500
    Input Text  candidat${ordre_cand2}  0
    Click On Submit Button In Subform
    # Verification des erreurs
    La page ne doit pas contenir d'erreur
    Message Should Contain    ATTENTION : il y a un écart de 600 entre le nombre de vote exprimé : 300 et le nombre de votant - (blanc + nuls) : -300
    Message Should Contain    ATTENTION : il y a un écart de -200 entre le nombre de vote exprimé : 300 et le nombre de vote obtenu par les candidats : 500
    Message Should Contain    ATTENTION : Il y a plus de votants (1200) que d'inscrits (1000)
    Message Should Contain    ATTENTION : Il y a plus d'émargements (1500) que d'inscrits (1000)
    Message Should Contain    ATTENTION : Il y a plus de procurations (1001) que d'inscrits (1000)
    Message Should Contain    ATTENTION : Il y a plus de votes blancs (1200) et nuls (300) que de votants (1200)

    # Vérification de la mise à jour des résultats des périmètres
    # Résultats dans l'ordre du listing :
    # inscrit, votant, emargement, procuration, blanc, nul, exprime, candidat
    @{resultats_arr}         Create List    1200  1394  1694  1007  1206  308  485
    @{resultats_mairie_c}    Create List    1000  1200  1500  1001  1200  300  300
    @{resultats_mairie_ab}   Create List    200  194  194  6  6  8  185

    # Pour chaque résultats, vérifie qu'il est présent dans la case correspondante
    # Il y a 8 résultats à tester pour chaque liste.
    Depuis le contexte election  ${id_election_maire}
    Click On Tab  election_unite  unité(s)
    :FOR  ${index}  IN RANGE    7
    # Il y a 5 colonnes avant les colonnes des résultats dans le listing, il faut
    # donc ajouter 5 à l'index pour avoir la position de la colonne
    \    ${position_td} =  evaluate  ${index} + ${5}
    # Test des résultats de l'arrondissement
    \    ${value_arr} =  Get From List    ${resultats_arr}    ${index}
    \    Element Should Contain  css=div#sousform-election_unite table.tab-tab tbody tr:nth-child(6) td:nth-child(${position_td})   ${value_arr}
    # Test des résultats de la mairie de ab
    \    ${value_mairie_ab} =  Get From List    ${resultats_mairie_ab}    ${index}
    \    Element Should Contain  css=div#sousform-election_unite table.tab-tab tbody tr:nth-child(4) td:nth-child(${position_td})   ${value_mairie_ab}
    # Test des résultats de la mairie de c
    \    ${value_mairie_c} =  Get From List    ${resultats_mairie_c}    ${index}
    \    Element Should Contain  css=div#sousform-election_unite table.tab-tab tbody tr:nth-child(5) td:nth-child(${position_td})   ${value_mairie_c}

validation des résultats
    [Documentation]  L'objet de ce test case est de valider le fonctionnement de la
    ...  validation des résultats. Vérifie que le passage des différentes étapes de
    ...  validation fonctionne et qu'elles ont les comportements attendus.
    ...  Vérifie également que si l'option de validation obligatoire est sélectionnée
    ...  les options de publication ne s'affiche pas pour une unité non validé

    # Workflow de la validation des résultats : en cours de saisie -> en attente de validation -> valider
    Depuis le contexte election_unite  ${id_election_maire}  41 - bureau_a
    Demander la validation des résultats
	@{actions}    Create List    demander_validation  modifier
    Le portlet du sous formulaire ne doit pas contenir  election_unite  ${actions}
    # Si la validation n'est pas obligatoire les actions de publication sont présentes
	@{actions}    Create List    affichage  web  valider  reprendre_saisie
    Le portlet du sous formulaire doit contenir  election_unite  ${actions}

    # Activation de la demande de validation obligatoire
    &{election} =  BuiltIn.Create Dictionary
    ...  validation_avant_publication=true
    Modifier election  ${id_election_maire}  ${election}
    # Les actions de publication ne doivent plus être présente
    Depuis le contexte election_unite  ${id_election_maire}  41 - bureau_a
	@{actions}    Create List    affichage  web
    Le portlet du sous formulaire ne doit pas contenir  election_unite  ${actions}

    Valider la saisie des résultats
    # La publication automatique n'est pas active les résultats ne sont pas publiés suite à la
    # validation
    Element should not contain  css=div#sousform-message div.message  Les résultats ont été envoyés à l'animation
    Element should not contain  css=div#sousform-message div.message  Erreur lors de l'envoi à l'animation
    Element should not contain  css=div#sousform-message div.message  Les résultats ont été envoyés au portail web
    Element should not contain  css=div#sousform-message div.message  Erreur lors de l'envoi au portail web
	@{actions}    Create List    valider  demander_validation  modifier
    Le portlet du sous formulaire ne doit pas contenir  election_unite  ${actions}
	@{actions}    Create List    reprendre_saisie  affichage  web
    Le portlet du sous formulaire doit contenir  election_unite  ${actions}

    # Réinitialise la fenêtre modale
    Depuis le contexte election_unite  ${id_election_maire}  41 - bureau_a
    Reprendre la saisie des résultats
	@{actions}    Create List    affichage  web  valider  reprendre_saisie
    Le portlet du sous formulaire ne doit pas contenir  election_unite  ${actions}
	@{actions}    Create List    demander_validation
    Le portlet du sous formulaire doit contenir  election_unite  ${actions}

    # Lorsque la publication auto est active les résultats sont publié lors de la validation
    &{election} =  BuiltIn.Create Dictionary
    ...  publication_auto=true
    Modifier election  ${id_election_maire}  ${election}
    Depuis le contexte election_unite  ${id_election_maire}  41 - bureau_a
    Demander la validation des résultats
    # Réinitialise la fenêtre modale
    Depuis le contexte election_unite  ${id_election_maire}  41 - bureau_a
    Valider la saisie des résultats
    Valid Message Should Contain  Les résultats ont été envoyés à l'animation
    Valid Message Should Contain  Les résultats ont été envoyés au portail web

saisie de la participation
    [Documentation]  L'objet de ce test case est de valider le fonctionnement de la
    ...  saisie de la participation. Il vérifie également que le remplissage automatique
    ...  des champs de saisie des périmètres fonctionne correctement.

    Depuis le contexte election  ${id_election_maire}

    # Saisie de la participation
    Click On Tab  participation_election  participation(s)
    Click On Link  09:00:00
    Click On SubForm Portlet Action  participation_election  modifier
    # Remplissage du formulaire et test de la saisie automatique
    Input Text  unite${id_bureau_a}  50
    Input Text  unite${id_bureau_b}  25
    Input Text  unite${id_bureau_c}  100
    Click On Submit Button In Subform
    # Validation de la saisie
    La page ne doit pas contenir d'erreur
    Valid Message Should Contain    Participations enregistrées

    # Saisie de la participation
    Click On Back Button In Subform
    Click On Link  10:00:00
    Click On SubForm Portlet Action  participation_election  modifier
    # Remplissage du formulaire et test de la saisie automatique
    Input Text  unite${id_bureau_a}  75
    Input Text  unite${id_bureau_b}  26
    Input Text  unite${id_bureau_c}  100
    Click On Submit Button In Subform
    # Validation de la saisie
    La page ne doit pas contenir d'erreur

delegation
    [Documentation]  L'objet de ce test case est de valider le fonctionnement de la
    ...  delegation pour la saisie des résultats et de la participation.
    ...  Vérifie le fonctionnement du formulaire de création de délégation.
    ...  Vérifie que l'affichage des sous-formulaire se fait bien selon les options
    ...  choisie.
    ...  Vérifie que seuls les acteurs autorisés peuvent saisir les résultats et la
    ...  participation.
    ...  Test également la création de délégation multiple : vérifie que toutes les
    ...  délégation sont crées et que les délégations existante ne sont pas recréées

    Ajouter l'utilisateur  test1  test1@test1.test1  test1  test1  ADMINISTRATEUR
    Ajouter l'utilisateur  test2  test2@test2.test2  test2  test2  ADMINISTRATEUR

    # L'onglet de la délégation n'apparait que si l'option de délégation est séléctionnée
    # Si l'option de délégation de la participation est séléctionnée le formulaire de
    # saisie de la participation par unité s'affiche à la place de celui de la participation
    # par heure
    Depuis le contexte election  ${id_election_maire}
    Page Should Not Contain Link  delegation
    Page Should Contain Link  participation_election
    Page Should Not Contain Link  delegation_participation

    &{election} =  BuiltIn.Create Dictionary
    ...  delegation_saisie=true
    Modifier election  ${id_election_maire}  ${election}
    Page Should Contain Link  delegation
    Page Should Contain Link  participation_election
    Page Should Not Contain Link  delegation_participation

    &{election} =  BuiltIn.Create Dictionary
    ...  delegation_participation=true
    Modifier election  ${id_election_maire}  ${election}
    Page Should Contain Link  delegation
    Page Should Contain Link  delegation_participation
    Page Should Not Contain Link  participation_election

    # Création des autorisations de saisie des 2 acteurs
    &{delegation} =  BuiltIn.Create Dictionary
    ...  acteur=test1
    ...  unite=41 - bureau_a
    ${id_delegation1} =  Ajouter delegation  ${delegation}  ${id_election_maire}
    Set Suite Variable  ${id_delegation1}

    &{delegation} =  BuiltIn.Create Dictionary
    ...  acteur=test2
    ...  unite=42 - bureau_b
    ${id_delegation2} =  Ajouter delegation  ${delegation}  ${id_election_maire}
    Set Suite Variable  ${id_delegation2}

    # Vérifie que les acteurs n'ont accés qu'aux unités qui leurs sont attribuées
    Depuis la page d'accueil    test1  test1
    Depuis le contexte de la participation par unite  ${id_election_maire}  41 - bureau_a
    Portlet Action Should Be In SubForm  delegation_participation  modifier

    Depuis le contexte de la participation par unite  ${id_election_maire}  42 - bureau_b
    Portlet Action Should Not Be In SubForm  delegation_participation  modifier

    Depuis la page d'accueil    test2  test2
    Depuis le contexte de la participation par unite  ${id_election_maire}  41 - bureau_a
    Portlet Action Should Not Be In SubForm  delegation_participation  modifier

    Depuis le contexte de la participation par unite  ${id_election_maire}  42 - bureau_b
    Portlet Action Should Be In SubForm  delegation_participation  modifier

    # Test de l'affichage du nombre de votant dans le listin de la participation par unité
    # On accède à l'élection
    Depuis le contexte election  ${id_election_maire}
    # On accède au tableau
    Click On Tab  delegation_participation  participation(s)
    Table Cell Should Contain  css=div.soustab-container table.tab-tab  2  4  75
    Table Cell Should Contain  css=div.soustab-container table.tab-tab  3  4  26
    Table Cell Should Contain  css=div.soustab-container table.tab-tab  4  4  100
    Table Cell Should Contain  css=div.soustab-container table.tab-tab  5  4  101
    Table Cell Should Contain  css=div.soustab-container table.tab-tab  6  4  100
    Table Cell Should Contain  css=div.soustab-container table.tab-tab  7  4  201

    # Création des autorisations de saisie des 2 acteurs
    @{acteurs} =  Create List  test1  test2
    @{unites} =   Create List  41 - bureau_a  43 - bureau_c
    Ajouter delegation multiple  ${acteurs}  ${unites}  ${id_election_maire}

    # Vérifie que les délégations ont bien été créées
    Valid Message Should Contain  delegation : test1 -> bureau_c
    Valid Message Should Contain  delegation : test2 -> bureau_a
    Valid Message Should Contain  delegation : test2 -> bureau_c

    # Vérifie que la délégation n'a pas été recréée
    Element Should Not Contain  css=div.soustab-message > div.message span.text  delegation : test1 -> bureau_a

Envoi des fichiers
    [Documentation]  L'objet de ce test case est de valider le fonctionnement
    ...  des envois de fichiers à l'animation et au portail web au cours de
    ...  l'évolution du paramétrage de l'élection. Les éléments à tester sont
    ...  les suivants :
    ...     - Mise à jour des photos des candidats pour l'élection et ses centaines
    ...       en cas de modification des candidats (animation)
    ...     - Suppression de la participation envoyée en cas de modification des
    ...       horaire de l'élection
    ...     - Suppression des résultats et de la participation envoyés en cas de
    ...       modification du périmètre de l'élection

    # Enleve la délégation si elle est active
    &{election} =  BuiltIn.Create Dictionary
    ...  delegation_saisie=false
    Modifier election  ${id_election_maire}  ${election}

    # Maj des photos des candidats. Pour vérifier ça des photos avec des extensions différentes
    # sont utilisées
    &{candidat} =  BuiltIn.Create Dictionary
    ...  photo=plan_centre.gif
    Modifier election_candidat  ${id_election_maire}  ${id_candidat1}  ${candidat}
    La page ne doit pas contenir d'erreur
    Le fichier doit exister  ../aff/res/${id_election_maire}/photo/${ordre_cand1}.gif
    File Should Not Exist    ../aff/res/${id_election_maire}/photo/${ordre_cand1}.png

    # Envoi des résultats d'un bureau et la de la participation pour une tranche horaire
    Envoyer les résultats à l'affichage  ${id_election_maire}  41 - bureau_a
    Le fichier doit exister  ../aff/res/${id_election_maire}/bres/b${id_bureau_a}.json
    Envoyer les résultats à la page web  ${id_election_maire}  41 - bureau_a
    Le fichier doit exister  ../web/res/00${id_election_maire}-0/b41.html
    Envoyer la participation à l'affichage  ${id_election_maire}  09:00:00
    Le fichier doit exister  ../aff/res/${id_election_maire}/bpar/b${id_bureau_a}.json
    Envoyer la participation à la page web  ${id_election_maire}  09:00:00
    Le fichier doit exister  ../web/res/00${id_election_maire}-0/participation.inc

    # Modification des horaires de l'élection
    &{election} =  BuiltIn.Create Dictionary
    ...  heure_ouverture=09:00:00
    ...  heure_fermeture=11:00:00
    Modifier election  ${id_election_maire}  ${election}
    File Should Not Exist  ../aff/res/${id_election_maire}/bpar
    File Should Not Exist  ../web/res/00${id_election_maire}-0/participation.inc

    # Renvoie de la participation
    Envoyer la participation à l'affichage  ${id_election_maire}  10:00:00
    Le fichier doit exister  ../aff/res/${id_election_maire}/bpar/b${id_bureau_a}.json
    Envoyer la participation à la page web  ${id_election_maire}  10:00:00
    Le fichier doit exister  ../web/res/00${id_election_maire}-0/participation.inc

    # Modification du périmètre de l'élection
    &{election} =  BuiltIn.Create Dictionary
    ...  perimetre=COMMUNE
    Modifier election  ${id_election_maire}  ${election}
    File Should Not Exist  ../aff/res/${id_election_maire}/bpar
    File Should Not Exist  ../web/res/00${id_election_maire}-0/participation.inc
    File Should Not Exist  ../aff/res/${id_election_maire}/bres
    File Should Not Exist  ../web/res/00${id_election_maire}-0/b41.html

    # Remise du perimètre d'origine
    &{election} =  BuiltIn.Create Dictionary
    ...  perimetre=arrondissement_abc
    Modifier election  ${id_election_maire}  ${election}

Reset d'une election
    [Documentation]  L'objet de ce test case est de verifier que le reset de
    ...  l'élection supprime bien de la base de données tous les candidats,
    ...  les plans, les centaines et les animations qui lui sont liés

    Depuis le contexte election  ${id_election_maire}

    # Pour pouvoir tester la suppression de tous les éléments, la délégation
    # doit être active
    &{election} =  BuiltIn.Create Dictionary
    ...  delegation_saisie=true
    Modifier election  ${id_election_maire}  ${election}

    # Utilisation de l'action reset
    Depuis le contexte election  ${id_election_maire}
    Retourner à l'étape précédente  parametrage  Paramétrage
    Reload Page
    Click On Form Portlet Action  election  reset  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    La page ne doit pas contenir d'erreur

    # Vérification qu'il n'y a plus de candidats, de plans, de centaine, d'animation et de délégation
    Click On Tab  election_candidat  candidat(s)
    @{candidats_supprimes}    Create List    candidat1
    Le tableau ne doit pas contenir les éléments  ${candidats_supprimes}

    Click On Tab  plan_election  plan(s)
    @{plans_supprimes}   Create List    mairie_de_ab
    Le tableau ne doit pas contenir les éléments  ${plans_supprimes}

    Click On Tab  centaine  centaine(s)
    @{centaines_supprimes}   Create List    centaine_test_election
    Le tableau ne doit pas contenir les éléments  ${centaines_supprimes}

    Click On Tab  animation  animation(s)
    @{animations_supprimes}   Create List    animation_test_election
    Le tableau ne doit pas contenir les éléments  ${animations_supprimes}

    Click On Tab  delegation  délégation
    @{delegation_supprimes}   Create List    test1  test2
    Le tableau ne doit pas contenir les éléments  ${delegation_supprimes}

    # Vérification de la mise à jour des fichiers des répertoires aff et web
    File Should Not Exist  ../web/res/00${id_election_maire}-0/plan_${id_plan}
    File Should Not Exist  ../web/res/00${id_election_maire}-0/plan_${id_plan2}
    File Should Not Exist  ../aff/res/${id_election_maire}/animation_${id_animation}

Suppression d'une election
    [Documentation]  L'objet de ce test case est de valider le fonctionnement
    ...  de la suppression de l'élection et de tous les éléments qui y sont
    ...  liées.

    # Pour pouvoir tester la suppression de tous les éléments, la délégation
    # doit être active
    &{election} =  BuiltIn.Create Dictionary
    ...  delegation_saisie=true
    Modifier election  ${id_election_maire}  ${election}

    # Ajout d'un candidat, un plan, une animation et une délégation pour remplacer ceux supprimés dans
    # le test précédent
    &{candidat_test} =  BuiltIn.Create Dictionary
    ...  candidat=candidat1
    ...  ordre=${ordre_cand1}
    Ajouter election_candidat  ${candidat_test}  ${id_election_maire}

    &{animation} =  BuiltIn.Create Dictionary
    ...  libelle=animation_test_election
    ...  modele=Résultat
    ...  perimetre_aff=arrondissement_abc
    ...  type_aff=bureau
    Ajouter animation à l'élection  ${animation}  ${id_election_maire}

    &{plan_election} =  BuiltIn.Create Dictionary
    ...  plan=plan_2
    Ajouter plan_election  ${plan_election}  ${id_election_maire}

    &{delegation} =  BuiltIn.Create Dictionary
    ...  acteur=test1
    ...  unite=41 - bureau_a
    ${id_delegation} =  Ajouter delegation  ${delegation}  ${id_election_maire}
    Set Suite Variable  ${id_delegation}

    # Suppression de l'élection
    Supprimer election  ${id_election_maire}
    Valid Message Should Contain  La suppression a été correctement effectuée.

    # Vérification de la suppression des répertoires aff et web
    File Should Not Exist  ../web/res/00${id_election_maire}-0
    File Should Not Exist  ../aff/res/${id_election_maire}

Passage des unités en fin de validité
    [Documentation]  Pour éviter que les unités créées dans ce test ne provoquent
    ...  des erreurs liées à la vérification que le code unité est unique pour
    ...  les unités valides, on passe toutes les unités en fin de validité

    Passer l'unité en fin de validité  41
    Passer l'unité en fin de validité  42
    Passer l'unité en fin de validité  43