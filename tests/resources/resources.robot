*** Settings ***
Documentation  Ressources (librairies, keywords et variables)

# Keywords framework
Library  openmairie.robotframework.Library

# Mots-clefs métier
Resource  app${/}application.robot
Resource  app${/}fenetre_modale.robot
Resource  app${/}export.robot

*** Variables ***
${SERVER}          localhost
${PROJECT_NAME}    openresultat
${BROWSER}         firefox
${DELAY}           0
${ADMIN_USER}      admin
${ADMIN_PASSWORD}  admin
${PROJECT_URL}     http://${SERVER}/${PROJECT_NAME}/
${PATH_BIN_FILES}  ${EXECDIR}${/}binary_files${/}
${TITLE}           :: openMairie :: openRésultat
${SESSION_COOKIE}  openresultat

*** Keywords ***
For Suite Setup
    Reload Library  openmairie.robotframework.Library
    # Les keywords définit dans le resources.robot sont prioritaires
    Set Library Search Order  resources
    Ouvrir le navigateur
    Tests Setup

For Suite Teardown
    Fermer le navigateur
