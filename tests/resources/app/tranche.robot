*** Settings ***
Documentation    CRUD de la table tranche
...    @author  generated
...    @package openRésultat
...    @version 25/06/2020 10:06

*** Keywords ***
Depuis le listing des tranches
    [Tags]  tranche
    [Documentation]  Accède au listing des enregistrements de type 'tranche'.
    Depuis le listing  tranche


Depuis le formulaire d'ajout d'une tranche
    [Tags]  tranche
    [Documentation]  Accède directement via URL au formulaire d'ajout d'un
    ...  enregistrement de type 'tranche'.
    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=tranche&action=0&retour=form
    La page ne doit pas contenir d'erreur


Depuis le contexte tranche
    [Documentation]  Accède au formulaire
    [Arguments]  ${tranche}

    Depuis le listing des tranches
    # On recherche l'enregistrement
    Use Simple Search  tranche  ${tranche}
    # On clique sur le résultat
    Click On Link  ${tranche}
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur

Ajouter tranche
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    Depuis le formulaire d'ajout d'une tranche
    # On saisit des valeurs
    Saisir tranche  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${tranche} =  Get Text  css=div.form-content span#tranche
    # On le retourne
    [Return]  ${tranche}

Modifier tranche
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${tranche}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte tranche  ${tranche}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  tranche  modifier
    # On saisit des valeurs
    Saisir tranche  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer tranche
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${tranche}

    # On accède à l'enregistrement
    Depuis le contexte tranche  ${tranche}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  tranche  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir tranche
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}

    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ordre" existe dans "${values}" on execute "Input Text" dans le formulaire

