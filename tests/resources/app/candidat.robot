*** Settings ***
Documentation    CRUD de la table candidat
...    @author  generated
...    @package openRésultat
...    @version 25/06/2020 10:06

*** Keywords ***
Depuis le listing des candidats
    [Tags]  candidat
    [Documentation]  Accède au listing des enregistrements de type 'candidat'.
    Depuis le listing  candidat


Depuis le formulaire d'ajout d'un candidat
    [Tags]  candidat
    [Documentation]  Accède directement via URL au formulaire d'ajout d'un enregistrement de type 'candidat'.
    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=candidat&action=0&retour=form
    La page ne doit pas contenir d'erreur


Depuis le contexte candidat
    [Documentation]  Accède au formulaire
    [Arguments]  ${candidat}

    Depuis le listing des candidats
    # On recherche l'enregistrement
    Use Simple Search  candidat  ${candidat}
    # On clique sur le résultat
    Click On Link  ${candidat}
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur


Ajouter candidat
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    Depuis le formulaire d'ajout d'un candidat
    # On saisit des valeurs
    Saisir candidat  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${candidat} =  Get Text  css=div.form-content span#candidat
    # On le retourne
    [Return]  ${candidat}

Modifier candidat
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${candidat}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte candidat  ${candidat}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  candidat  modifier
    # On saisit des valeurs
    Saisir candidat  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer candidat
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${candidat}

    # On accède à l'enregistrement
    Depuis le contexte candidat  ${candidat}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  candidat  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir candidat
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}

    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle_liste" existe dans "${values}" on execute "Input Text" dans le formulaire