*** Settings ***
Documentation    CRUD de la table unite
...    @author  generated
...    @package openRésultat
...    @version 10/08/2020 09:08

*** Keywords ***
Depuis le listing des unités
    [Tags]  unite
    [Documentation]  Accède au listing des enregistrements de type 'unite'.
    Depuis le listing  unite


Depuis le formulaire d'ajout d'une unité
    [Tags]  unite
    [Documentation]  Accède directement via URL au formulaire d'ajout d'un enregistrement de type 'unite'.
    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=unite&action=0&retour=form
    La page ne doit pas contenir d'erreur


Depuis le formulaire d'import des unités
    [Tags]  unite
    [Documentation]  Accède directement via URL au formulaire d'ajout d'un enregistrement de type 'unite'.
    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=unite&action=9&idx=0&retour=tab
    La page ne doit pas contenir d'erreur


Depuis le contexte unite
    [Documentation]  Accède au formulaire
    [Arguments]  ${unite}

    Depuis le listing des unités
    # On recherche l'enregistrement
    Use Simple Search  unite  ${unite}
    # On clique sur le résultat
    Click On Link  ${unite}
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur

Ajouter unite
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    Depuis le formulaire d'ajout d'une unité
    # On saisit des valeurs
    Saisir unite  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${unite} =  Get Text  css=div.form-content span#unite
    # On le retourne
    [Return]  ${unite}

Modifier unite
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${unite}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte unite  ${unite}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  unite  modifier
    # On saisit des valeurs
    Saisir unite  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer unite
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${unite}

    # On accède à l'enregistrement
    Depuis le contexte unite  ${unite}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  unite  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir unite
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}

    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "type_unite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "ordre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cp" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ville" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "geom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "coordinates" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "type_unite_contenu" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "perimetre" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "canton" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "circonscription" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "commune" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "departement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "code_unite" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "id_reu" existe dans "${values}" on execute "Input Text" dans le formulaire

L'unite a été correctement importée
    [Documentation]  Vérifie si l'unité existe et si ses valeurs sont bien celles
    ...  attendues
    [Arguments]  ${values}  ${id_reu}

    # On accède à l'unité à l'aide de son libellé
    Depuis le listing des unités
    Use Simple Search  id_reu  ${id_reu}
    Click On Link  ${values.libelle}

    # Pour chacune des valeurs enregistrées vérifie si la valeur du champ est correcte
    ${keys} =  Get Dictionary Keys  ${values}
    :FOR    ${key}    IN    @{keys}
    \    Element Should Contain  css=span#${key}  ${values.${key}}

Passer l'unité en fin de validité
    [Documentation]  Modifie la date de fin de validité de l'unité pour la passer
    ...  en unité expirée
    [Arguments]  ${code_unite}

    # Date du jour qui permettra de passer l'unité en fin de validité
    ${current_date} =  Get Current Date  result_format=%d/%m/%Y
    &{args_modif_unite} =  BuiltIn.Create Dictionary
    ...  om_validite_fin=${current_date}
    # On accède à l'unité voulue
    Depuis le listing des unités
    Use Simple Search  code_unite  ${code_unite}
    Click On Link  ${code_unite}
    # On passe sa date de fin de validité à la date du jour et on valide la modification
    Click On Form Portlet Action  unite  modifier
    Saisir unite  ${args_modif_unite}
    Click On Submit Button