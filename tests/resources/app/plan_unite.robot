*** Settings ***
Documentation    CRUD de la table plan_unite
...    @author  generated
...    @package openRésultat
...    @version 16/12/2020 16:12

*** Keywords ***

Depuis le contexte plan_unite
    [Documentation]  Accède au formulaire
    [Arguments]  ${plan_unite}

    # On accède au tableau
    Depuis le listing  plan_unite
    # On recherche l'enregistrement
    Use Simple Search  plan_unite  ${plan_unite}
    # On clique sur le résultat
    Click On Link  ${plan_unite}
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur

Ajouter plan_unite
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Depuis le listing  plan_unite
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir plan_unite  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${plan_unite} =  Get Text  css=div.form-content span#plan_unite
    # On le retourne
    [Return]  ${plan_unite}

Modifier plan_unite
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${plan_unite}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte plan_unite  ${plan_unite}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  plan_unite  modifier
    # On saisit des valeurs
    Saisir plan_unite  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer plan_unite
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${plan_unite}

    # On accède à l'enregistrement
    Depuis le contexte plan_unite  ${plan_unite}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  plan_unite  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir plan_unite
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "plan" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "unite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Selectionner un fichier si img_unite_arrivee existe et n'est pas vide dans ${values} sinon deselectionner
    Selectionner un fichier si img_unite_non_arrivee existe et n'est pas vide dans ${values} sinon deselectionner
    Si "position_x" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "position_y" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "largeur_icone" existe dans "${values}" on execute "Input Text" dans le formulaire

Ouvrir la fenêtre de prévisualisation
    [Documentation]  Clique sur le bouton de prévisualisation pour afficher la fenêtre
    [Arguments]

    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Link  css=div.field-type-localisation_plan_static a

Fermer la fenêtre de prévisualisation
    [Documentation]  Clique sur le bouton de fermer de la fenêtre
    [Arguments]

    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Link  css=a.retour

Verifier la position de l'icone
    [Documentation]  Vérifie la position de l'icone de coordonnées (x, y) sur le plan
    [Arguments]  ${coordonnees_x}  ${coordonnees_y}  ${plan}  ${icone}

    ${position_x_unite} =  Get Horizontal Position  ${icone}
    ${position_x_plan} =  Get Horizontal Position  ${plan}
    ${position_x} =  Evaluate  ${position_x_unite} - ${position_x_plan}
    Should Be Equal As Numbers  ${coordonnees_x}    ${position_x}

    ${position_y_unite} =  Get Vertical Position  ${icone}
    ${position_y_plan} =  Get Vertical Position  ${plan}
    ${position_y} =  Evaluate  ${position_y_unite} - ${position_y_plan}
    Should Be Equal As Numbers  ${coordonnees_y}    ${position_y}