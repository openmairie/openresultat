*** Settings ***
Documentation    CRUD de la table participation_election
...    @author  generated
...    @package openRésultat
...    @version 15/03/2021 17:03

*** Keywords ***

Depuis le contexte participation_election
    [Documentation]  Accède au formulaire
    [Arguments]  ${participation_election}

    # On accède au tableau
    Go To Tab  participation_election
    # On recherche l'enregistrement
    Use Simple Search  participation_election  ${participation_election}
    # On clique sur le résultat
    Click On Link  ${participation_election}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter participation_election
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  participation_election
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir participation_election  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${participation_election} =  Get Text  css=div.form-content span#participation_election
    # On le retourne
    [Return]  ${participation_election}

Modifier participation_election
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${participation_election}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte participation_election  ${participation_election}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  participation_election  modifier
    # On saisit des valeurs
    Saisir participation_election  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer participation_election
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${participation_election}

    # On accède à l'enregistrement
    Depuis le contexte participation_election  ${participation_election}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  participation_election  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir participation_election
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "election" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "tranche" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "date_derniere_modif" existe dans "${values}" on execute "Input Text" dans le formulaire