*** Settings ***
Documentation    CRUD de la table election_unite
...    @author  generated
...    @package openRésultat
...    @version 13/08/2020 14:08

*** Keywords ***

Depuis le contexte election_unite
    [Documentation]  Accède au formulaire
    [Arguments]  ${election_unite}

    # On accède au tableau
    Go To Tab  election_unite
    # On recherche l'enregistrement
    Use Simple Search  election_unite  ${election_unite}
    # On clique sur le résultat
    Click On Link  ${election_unite}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter election_unite
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  election_unite
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir election_unite  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${election_unite} =  Get Text  css=div.form-content span#election_unite
    # On le retourne
    [Return]  ${election_unite}

Modifier election_unite
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${election_unite}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte election_unite  ${election_unite}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  election_unite  modifier
    # On saisit des valeurs
    Saisir election_unite  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer election_unite
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${election_unite}

    # On accède à l'enregistrement
    Depuis le contexte election_unite  ${election_unite}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  election_unite  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir election_unite
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "election" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "unite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "inscrit" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "votant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "blanc" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "nul" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "exprime" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "envoi_aff" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "envoi_web" existe dans "${values}" on execute "Set Checkbox" dans le formulaire