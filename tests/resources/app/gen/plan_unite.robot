*** Settings ***
Documentation    CRUD de la table plan_unite
...    @author  generated
...    @package openRésultat
...    @version 30/04/2021 10:04

*** Keywords ***

Depuis le contexte plan_unite
    [Documentation]  Accède au formulaire
    [Arguments]  ${plan_unite}

    # On accède au tableau
    Go To Tab  plan_unite
    # On recherche l'enregistrement
    Use Simple Search  plan_unite  ${plan_unite}
    # On clique sur le résultat
    Click On Link  ${plan_unite}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter plan_unite
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  plan_unite
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir plan_unite  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${plan_unite} =  Get Text  css=div.form-content span#plan_unite
    # On le retourne
    [Return]  ${plan_unite}

Modifier plan_unite
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${plan_unite}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte plan_unite  ${plan_unite}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  plan_unite  modifier
    # On saisit des valeurs
    Saisir plan_unite  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer plan_unite
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${plan_unite}

    # On accède à l'enregistrement
    Depuis le contexte plan_unite  ${plan_unite}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  plan_unite  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir plan_unite
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "plan" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "unite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "img_unite_arrivee" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "img_unite_non_arrivee" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "position_x" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "position_y" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "largeur_icone" existe dans "${values}" on execute "Input Text" dans le formulaire