*** Settings ***
Documentation    CRUD de la table election
...    @author  generated
...    @package openRésultat
...    @version 07/04/2021 16:04

*** Keywords ***

Depuis le contexte election
    [Documentation]  Accède au formulaire
    [Arguments]  ${election}

    # On accède au tableau
    Go To Tab  election
    # On recherche l'enregistrement
    Use Simple Search  election  ${election}
    # On clique sur le résultat
    Click On Link  ${election}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter election
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  election
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir election  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${election} =  Get Text  css=div.form-content span#election
    # On le retourne
    [Return]  ${election}

Modifier election
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${election}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte election  ${election}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  election  modifier
    # On saisit des valeurs
    Saisir election  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer election
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${election}

    # On accède à l'enregistrement
    Depuis le contexte election  ${election}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  election  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir election
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tour" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "type_election" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "date" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "perimetre" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "votant_defaut" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "heure_ouverture" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "heure_fermeture" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "envoi_initial" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "is_centaine" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "election_reference" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "workflow" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "publication_auto" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "publication_erreur" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "calcul_auto_exprime" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "garder_resultat_simulation" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "delegation_saisie" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "delegation_participation" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "validation_avant_publication" existe dans "${values}" on execute "Set Checkbox" dans le formulaire