*** Settings ***
Documentation    CRUD de la table delegation
...    @author  generated
...    @package openRésultat
...    @version 05/03/2021 17:03

*** Keywords ***

Depuis le contexte delegation
    [Documentation]  Accède au formulaire
    [Arguments]  ${delegation}

    # On accède au tableau
    Go To Tab  delegation
    # On recherche l'enregistrement
    Use Simple Search  delegation  ${delegation}
    # On clique sur le résultat
    Click On Link  ${delegation}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter delegation
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  delegation
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir delegation  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${delegation} =  Get Text  css=div.form-content span#delegation
    # On le retourne
    [Return]  ${delegation}

Modifier delegation
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${delegation}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte delegation  ${delegation}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  delegation  modifier
    # On saisit des valeurs
    Saisir delegation  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer delegation
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${delegation}

    # On accède à l'enregistrement
    Depuis le contexte delegation  ${delegation}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  delegation  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir delegation
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "election" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "unite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "acteur" existe dans "${values}" on execute "Select From List By Label" dans le formulaire