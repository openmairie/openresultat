*** Settings ***
Documentation    CRUD de la table participation_unite
...    @author  generated
...    @package openRésultat
...    @version 10/08/2020 09:08

*** Keywords ***

Depuis le contexte participation_unite
    [Documentation]  Accède au formulaire
    [Arguments]  ${participation_unite}

    # On accède au tableau
    Go To Tab  participation_unite
    # On recherche l'enregistrement
    Use Simple Search  participation_unite  ${participation_unite}
    # On clique sur le résultat
    Click On Link  ${participation_unite}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter participation_unite
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  participation_unite
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir participation_unite  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${participation_unite} =  Get Text  css=div.form-content span#participation_unite
    # On le retourne
    [Return]  ${participation_unite}

Modifier participation_unite
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${participation_unite}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte participation_unite  ${participation_unite}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  participation_unite  modifier
    # On saisit des valeurs
    Saisir participation_unite  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer participation_unite
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${participation_unite}

    # On accède à l'enregistrement
    Depuis le contexte participation_unite  ${participation_unite}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  participation_unite  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir participation_unite
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "election_unite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "participation_election" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "votant" existe dans "${values}" on execute "Input Text" dans le formulaire