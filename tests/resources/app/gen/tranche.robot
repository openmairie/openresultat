*** Settings ***
Documentation    CRUD de la table tranche
...    @author  generated
...    @package openRésultat
...    @version 25/06/2020 10:06

*** Keywords ***

Depuis le contexte tranche
    [Documentation]  Accède au formulaire
    [Arguments]  ${tranche}

    # On accède au tableau
    Go To Tab  tranche
    # On recherche l'enregistrement
    Use Simple Search  tranche  ${tranche}
    # On clique sur le résultat
    Click On Link  ${tranche}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter tranche
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  tranche
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir tranche  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${tranche} =  Get Text  css=div.form-content span#tranche
    # On le retourne
    [Return]  ${tranche}

Modifier tranche
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${tranche}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte tranche  ${tranche}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  tranche  modifier
    # On saisit des valeurs
    Saisir tranche  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer tranche
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${tranche}

    # On accède à l'enregistrement
    Depuis le contexte tranche  ${tranche}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  tranche  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir tranche
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ordre" existe dans "${values}" on execute "Input Text" dans le formulaire