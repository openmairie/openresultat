*** Settings ***
Documentation    CRUD de la table animation
...    @author  generated
...    @package openRésultat
...    @version 13/04/2021 16:04

*** Keywords ***

Depuis le contexte animation
    [Documentation]  Accède au formulaire
    [Arguments]  ${animation}

    # On accède au tableau
    Go To Tab  animation
    # On recherche l'enregistrement
    Use Simple Search  animation  ${animation}
    # On clique sur le résultat
    Click On Link  ${animation}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter animation
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  animation
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir animation  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${animation} =  Get Text  css=div.form-content span#animation
    # On le retourne
    [Return]  ${animation}

Modifier animation
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${animation}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte animation  ${animation}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  animation  modifier
    # On saisit des valeurs
    Saisir animation  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer animation
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${animation}

    # On accède à l'enregistrement
    Depuis le contexte animation  ${animation}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  animation  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir animation
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "election" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "refresh" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "is_modele" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "actif" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "titre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "sous_titre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "couleur_titre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "couleur_bandeau" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "logo" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "perimetre_aff" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "type_aff" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "affichage" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "is_config" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "feuille_style" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "script" existe dans "${values}" on execute "Input Text" dans le formulaire