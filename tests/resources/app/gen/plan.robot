*** Settings ***
Documentation    CRUD de la table plan
...    @author  generated
...    @package openRésultat
...    @version 16/12/2020 16:12

*** Keywords ***

Depuis le contexte plan
    [Documentation]  Accède au formulaire
    [Arguments]  ${plan}

    # On accède au tableau
    Go To Tab  plan
    # On recherche l'enregistrement
    Use Simple Search  plan  ${plan}
    # On clique sur le résultat
    Click On Link  ${plan}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter plan
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  plan
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir plan  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${plan} =  Get Text  css=div.form-content span#plan
    # On le retourne
    [Return]  ${plan}

Modifier plan
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${plan}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte plan  ${plan}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  plan  modifier
    # On saisit des valeurs
    Saisir plan  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer plan
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${plan}

    # On accède à l'enregistrement
    Depuis le contexte plan  ${plan}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  plan  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir plan
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "image_plan" existe dans "${values}" on execute "Input Text" dans le formulaire