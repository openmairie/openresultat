*** Settings ***
Documentation    CRUD de la table canton
...    @author  generated
...    @package openRésultat
...    @version 25/09/2020 17:09

*** Keywords ***

Depuis le contexte canton
    [Documentation]  Accède au formulaire
    [Arguments]  ${canton}

    # On accède au tableau
    Go To Tab  canton
    # On recherche l'enregistrement
    Use Simple Search  canton  ${canton}
    # On clique sur le résultat
    Click On Link  ${canton}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter canton
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  canton
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir canton  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${canton} =  Get Text  css=div.form-content span#canton
    # On le retourne
    [Return]  ${canton}

Modifier canton
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${canton}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte canton  ${canton}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  canton  modifier
    # On saisit des valeurs
    Saisir canton  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer canton
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${canton}

    # On accède à l'enregistrement
    Depuis le contexte canton  ${canton}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  canton  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir canton
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prefecture" existe dans "${values}" on execute "Input Text" dans le formulaire