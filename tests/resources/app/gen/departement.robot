*** Settings ***
Documentation    CRUD de la table departement
...    @author  generated
...    @package openRésultat
...    @version 25/09/2020 17:09

*** Keywords ***

Depuis le contexte departement
    [Documentation]  Accède au formulaire
    [Arguments]  ${departement}

    # On accède au tableau
    Go To Tab  departement
    # On recherche l'enregistrement
    Use Simple Search  departement  ${departement}
    # On clique sur le résultat
    Click On Link  ${departement}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter departement
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  departement
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir departement  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${departement} =  Get Text  css=div.form-content span#departement
    # On le retourne
    [Return]  ${departement}

Modifier departement
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${departement}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte departement  ${departement}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  departement  modifier
    # On saisit des valeurs
    Saisir departement  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer departement
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${departement}

    # On accède à l'enregistrement
    Depuis le contexte departement  ${departement}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  departement  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir departement
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prefecture" existe dans "${values}" on execute "Input Text" dans le formulaire