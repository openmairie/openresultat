*** Settings ***
Documentation    CRUD de la table election_candidat
...    @author  generated
...    @package openRésultat
...    @version 25/06/2020 10:06

*** Keywords ***

Depuis le listing election_candidat
    [Documentation]  Accède au formulaire
    [Arguments]  ${election}

    # On accède à l'élection
    Depuis le contexte election  ${election}
    # On accéde à l'onglet des election_candidat
    Click On Tab  election_candidat  candidat(s)

Depuis le contexte election_candidat
    [Documentation]  Accède au formulaire
    [Arguments]  ${election}  ${election_candidat}

    # On accède au listing des candidats de l'élection
    Depuis le listing election_candidat  ${election}
    # On clique sur le candidat voulu
    Click On Link  css=a#action-soustab-election_candidat-left-consulter-${election_candidat}
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur

Ajouter election_candidat
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}  ${election}

    # On accède au listing des candidats de l'élection
    Depuis le listing election_candidat  ${election}
    # On clique sur le bouton ajouter
    Click On Add Button In SubForm
    # On saisit des valeurs
    Saisir election_candidat  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${election_candidat} =  Get Text  css=div.form-content span#election_candidat
    # On le retourne
    [Return]  ${election_candidat}

Modifier election_candidat
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${election}  ${election_candidat}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte election_candidat  ${election}  ${election_candidat}
    # On clique sur le bouton modifier
    Click On SubForm Portlet Action  election_candidat  modifier
    # On saisit des valeurs
    Saisir election_candidat  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer election_candidat
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${election}  ${election_candidat}

    # On accède à l'enregistrement
    Depuis le contexte election_candidat  ${election}  ${election_candidat}
    # On clique sur le bouton supprimer
    Click On SubForm Portlet Action  election_candidat  supprimer
    # On valide le formulaire
    Click On Submit Button In SubForm

Saisir election_candidat
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}

    Open Fieldset In SubForm  election_candidat  listes-municipales
    Si "election" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "candidat" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "ordre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prefecture" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "age_moyen" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "siege" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "age_moyen_com" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "siege_com" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "age_moyen_mep" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "siege_mep" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "photo" existe dans "${values}" on execute "Add File" sur "photo"
    Si "couleur" existe dans "${values}" on execute "Input Text" dans le formulaire

Créer les résultats du candidats
    [Documentation]  Utilise l'action de création des résultats d'un candidat.
    [Arguments]  ${id_candidat}

    Click On Tab  election_candidat  candidat(s)
    Click On Link  ${id_candidat}
    Click On SubForm Portlet Action  election_candidat  resultat  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    #Reinitialise la fenêtre modale
    Reload Page
