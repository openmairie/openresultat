*** Settings ***
Documentation  Actions spécifiques aux fenêtre de validation

*** Keywords ***
Cliquer sur le bouton de la fenêtre modale
 	    [Documentation]  Clic sur un bouton de la fenêtre modale d'après son texte
 	    [Arguments]  ${texte}
 	
 	    # construit le sélecteur
 	    ${selector}=  Set Variable  //div[contains(@class, 'ui-dialog')]/descendant::div[contains(@class, 'ui-dialog-buttonset')]/button/span[text()='${texte}']
 	
 	    # attend que la fenêtre modale soit remplie et le bouton présent
 	    Sleep  1
 	    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain Element  xpath=//div[contains(@class, 'ui-dialog')]
 	    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain Element  xpath=${selector}
 	
 	    # attend que la fenêtre modale soit visible et le bouton aussi
 	    Wait Until Element Is Visible  xpath=//div[contains(@class, 'ui-dialog')]
 	    Wait Until Element Is Visible  xpath=${selector}
 	
 	    # clic sur le bouton
 	    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  xpath=${selector}
 	
 	    # attend que la fenêtre modale soit invisible
 	    Wait Until Element Is Not Visible  xpath=//div[contains(@class, 'ui-dialog')]
 	
 	    # délai pour que l'action soit effectuée
 	    Sleep  2.5