*** Settings ***
Documentation    CRUD de la table animation
...    @author  generated
...    @package openRésultat
...    @version 12/11/2020 11:11

*** Keywords ***

Depuis le contexte animation
    [Documentation]  Accède au formulaire
    [Arguments]  ${animation}

    # On accède au tableau
    Depuis le listing  animation
    # On recherche l'enregistrement
    Use Simple Search  animation  ${animation}
    # On clique sur le résultat
    Click On Link  ${animation}
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur

Depuis le contexte animation de l'election
    [Documentation]  Accède au formulaire
    [Arguments]  ${election}  ${animation}

    # On accède à l'élection
    Depuis le contexte election  ${election}
    # On accède au tableau
    Click On Tab  animation  animation(s)
    # On clique sur le résultat
    Click On Link  ${animation}
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur

Ajouter animation à l'élection
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}  ${election}

    # On accède au tableau
    Depuis le contexte election  ${election}
    Click On Tab  animation  animation(s)
    La page ne doit pas contenir d'erreur
    # On clique sur le bouton ajouter
    Click On Add Button In SubForm
    # On saisit des valeurs
    Saisir animation de l'élection  ${values}
    # On valide le formulaire
    Click On Submit Button
    La page ne doit pas contenir d'erreur
    # On récupère l'ID du nouvel enregistrement
    ${animation} =  Get Text  css=div.form-content span#animation
    # On le retourne
    [Return]  ${animation}

Ajouter animation
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Depuis le listing  animation
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir animation  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${animation} =  Get Text  css=div.form-content span#animation
    # On le retourne
    [Return]  ${animation}

Modifier animation
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${values}  ${animation}

    # On accède à l'enregistrement
    Depuis le contexte animation  ${animation}
    # On clique sur le bouton modifier
    Click On SubForm Portlet Action  animation  modifier
    # On saisit des valeurs
    Saisir animation  ${values}
    # On valide le formulaire
    Click On Submit Button

Modifier animation de l'élection
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${values}  ${election}  ${animation}

    # On accède à l'enregistrement
    Depuis le contexte animation de l'election  ${election}  ${animation}
    # On clique sur le bouton modifier
    Click On SubForm Portlet Action  animation  modifier
    # On saisit des valeurs
    Saisir animation de l'élection  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer animation
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${animation}

    # On accède à l'enregistrement
    Depuis le contexte animation  ${animation}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  animation  supprimer
    # On valide le formulaire
    Click On Submit Button

Supprimer animation de l'élection
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${election}  ${animation}

    # On accède à l'enregistrement
    Depuis le contexte animation de l'election  ${election}  ${animation}
    # On clique sur le bouton supprimer
    Click On SubForm Portlet Action  animation  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir animation
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}

    Si "election" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "refresh" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "modele" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "is_modele" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "titre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "sous_titre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "couleur_titre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "couleur_bandeau" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "logo" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "perimetre_aff" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "type_aff" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "affichage" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "script" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "feuille_style" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "election_comparaison" existe dans "${values}" on execute "Select From List By Label" dans le formulaire

Saisir animation de l'élection
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}

    Si "election" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans "animation"
    Si "refresh" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "modele" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "is_modele" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "titre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "sous_titre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "couleur_titre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "couleur_bandeau" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "logo" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "perimetre_aff" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "type_aff" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "affichage" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "script" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "feuille_style" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "election_comparaison" existe dans "${values}" on execute "Select Multiple By Label" dans le formulaire

Activer animation
    [Documentation]  Utilise l'action d'activation de l'animation
    [Arguments]  ${animation}

    # On accède à l'enregistrement
    Depuis le contexte animation  ${animation}
    Click On Form Portlet Action  animation  activer  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    # Réinitialise la fenêtre modale
    Reload Page

Désactiver animation
    [Documentation]  Utilise l'action de désactivation de l'animation
    [Arguments]  ${animation}

    # On accède à l'enregistrement
    Depuis le contexte animation  ${animation}
    Click On Form Portlet Action  animation  desactiver  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    # Réinitialise la fenêtre modale
    Reload Page