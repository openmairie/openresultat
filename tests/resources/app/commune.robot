*** Settings ***
Documentation    CRUD de la table commune
...    @author  generated
...    @package openRésultat
...    @version 25/09/2020 17:09

*** Keywords ***
Depuis le listing des communes
    [Tags]  commune
    [Documentation]  Accède au listing des enregistrements de type 'commune'.
    Depuis le listing  commune


Depuis le formulaire d'ajout d'une commune
    [Tags]  commune
    [Documentation]  Accède directement via URL au formulaire d'ajout d'un enregistrement de type 'commune'.
    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=commune&action=0&retour=form
    La page ne doit pas contenir d'erreur


Depuis le contexte commune
    [Documentation]  Accède au formulaire
    [Arguments]  ${commune}

    Depuis le listing des communes
    # On recherche l'enregistrement
    Use Simple Search  commune  ${commune}
    # On clique sur le résultat
    Click On Link  ${commune}
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur

Ajouter commune
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    Depuis le formulaire d'ajout d'une commune
    # On saisit des valeurs
    Saisir commune  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${commune} =  Get Text  css=div.form-content span#commune
    # On le retourne
    [Return]  ${commune}

Modifier commune
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${commune}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte commune  ${commune}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  commune  modifier
    # On saisit des valeurs
    Saisir commune  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer commune
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${commune}

    # On accède à l'enregistrement
    Depuis le contexte commune  ${commune}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  commune  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir commune
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}

    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prefecture" existe dans "${values}" on execute "Input Text" dans le formulaire