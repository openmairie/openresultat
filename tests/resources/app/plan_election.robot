*** Settings ***
Documentation    CRUD de la table plan_election
...    @author  generated
...    @package openRésultat
...    @version 17/12/2020 10:12

*** Keywords ***

Depuis le contexte plan_election
    [Documentation]  Accède au formulaire
    [Arguments]  ${lib_plan}  ${election}

    # On accède à l'élection
    Depuis le contexte election  ${election}
    # On accède au tableau
    Click On Tab  plan_election  plan(s)
    # On clique sur le résultat
    Click On Link  ${lib_plan}
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur

Ajouter plan_election
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}  ${election}

    # On accède au tableau
    Depuis le contexte election  ${election}
    Click On Tab  plan_election  plan(s)
    La page ne doit pas contenir d'erreur
    # On clique sur le bouton ajouter
    Click On Add Button In SubForm
    # On saisit des valeurs
    Saisir plan_election  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${plan_election} =  Get Text  css=div.form-content span#plan_election
    # On le retourne
    [Return]  ${plan_election}

Modifier plan_election
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${lib_plan}  ${election}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte plan_election  ${lib_plan}  ${election}
    # On clique sur le bouton modifier
    Click On SubForm Portlet Action  plan_election  modifier
    # On saisit des valeurs
    Saisir plan_election  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer plan_election
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${lib_plan}  ${election}

    # On accède à l'enregistrement
    Depuis le contexte plan_election  ${lib_plan}  ${election}
    # On clique sur le bouton supprimer
    Click On SubForm Portlet Action  plan_election  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir plan_election
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}

    Si "plan" existe dans "${values}" on execute "Select From List By Label" dans le formulaire

Envoyer le plan à la page web
    [Documentation]  Utilise l'action "envoi plan" servant à envoyer les informations
    ...  relatives au plan à la page web
    [Arguments]  ${lib_plan}  ${election}

    # On accède à l'enregistrement
    Depuis le contexte plan_election  ${lib_plan}  ${election}
    # On clique sur le bouton envoi plan
    Click On SubForm Portlet Action  plan_election  plan  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur
