*** Settings ***
Documentation    CRUD de la table type_election
...    @author  generated
...    @package openRésultat
...    @version 25/06/2020 10:06

*** Keywords ***
Depuis le listing des types d'élection
    [Tags]  type_election
    [Documentation]  Accède au listing des enregistrements de type 'type_election'.
    Depuis le listing  type_election


Depuis le formulaire d'ajout d'un type d'élection
    [Tags]  type_election
    [Documentation]  Accède directement via URL au formulaire d'ajout d'un
    ...  enregistrement de type 'type_election'.
    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=type_election&action=0&retour=form
    La page ne doit pas contenir d'erreur


Depuis le contexte type_election
    [Documentation]  Accède au formulaire
    [Arguments]  ${type_election}

    Depuis le listing des types d'élection
    # On recherche l'enregistrement
    Use Simple Search  type_election  ${type_election}
    # On clique sur le résultat
    Click On Link  ${type_election}
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur

Ajouter type_election
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    Depuis le formulaire d'ajout d'un type d'élection
    # On saisit des valeurs
    Saisir type_election  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${type_election} =  Get Text  css=div.form-content span#type_election
    # On le retourne
    [Return]  ${type_election}

Modifier type_election
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${type_election}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte type_election  ${type_election}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  type_election  modifier
    # On saisit des valeurs
    Saisir type_election  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer type_election
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${type_election}

    # On accède à l'enregistrement
    Depuis le contexte type_election  ${type_election}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  type_election  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir type_election
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}

    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prefecture" existe dans "${values}" on execute "Input Text" dans le formulaire

