*** Settings ***
Documentation    CRUD de la table plan
...    @author  generated
...    @package openRésultat
...    @version 16/12/2020 16:12

*** Keywords ***
Depuis le listing des plans
    [Tags]  plan
    [Documentation]  Accède au listing des enregistrements de type 'plan'.
    Depuis le listing  plan


Depuis le formulaire d'ajout d'un plan
    [Tags]  plan
    [Documentation]  Accède directement via URL au formulaire d'ajout d'un enregistrement de type 'plan'.
    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=plan&action=0&retour=form
    La page ne doit pas contenir d'erreur


Depuis le contexte plan
    [Documentation]  Accède au formulaire
    [Arguments]  ${plan}

    Depuis le listing des plans
    # On recherche l'enregistrement
    Use Simple Search  plan  ${plan}
    # On clique sur le résultat
    Click On Link  ${plan}
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur

Ajouter plan
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    Depuis le formulaire d'ajout d'un plan
    # On saisit des valeurs
    Saisir plan  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${plan} =  Get Text  css=div.form-content span#plan
    # On le retourne
    [Return]  ${plan}

Modifier plan
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${plan}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte plan  ${plan}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  plan  modifier
    # On saisit des valeurs
    Saisir plan  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer plan
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${plan}

    # On accède à l'enregistrement
    Depuis le contexte plan  ${plan}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  plan  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir plan
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}

    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "image_plan" existe dans "${values}" on execute "Add File" sur "image_plan"
    Si "img_unite_arrivee" existe dans "${values}" on execute "Add File" sur "img_unite_arrivee"
    Si "img_unite_non_arrivee" existe dans "${values}" on execute "Add File" sur "img_unite_non_arrivee"
    Si "par_defaut" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "largeur_icone" existe dans "${values}" on execute "Input Text" dans le formulaire