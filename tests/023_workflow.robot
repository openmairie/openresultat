*** Settings ***
Documentation     Test du workflow
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Jeu de données
    [Documentation]  L'objet de ce test case est de creer le jeu de données
    ...  qui servira ensuite à tester le fonctionnement du workflow

    Depuis la page d'accueil    admin  admin

    # Création de deux candidat
    &{candidat} =  BuiltIn.Create Dictionary
    ...  libelle=candidat1_workflow
    Ajouter candidat  ${candidat}

    &{candidat} =  BuiltIn.Create Dictionary
    ...  libelle=candidat2_workflow
    Ajouter candidat  ${candidat}

    # Création d'un plan
    &{plan} =  BuiltIn.Create Dictionary
    ...  libelle=plan_test_workflow
    ...  image_plan=plan_centre.gif
    ...  img_unite_arrivee=img_unite_arrivee.gif
    ...  img_unite_non_arrivee=img_unite_non_arrivee.gif
    ${id_plan} =  Ajouter plan  ${plan}

    # Creation d'une election
    &{election} =  BuiltIn.Create Dictionary
    ...  libelle=Election Test Workflow
    ...  code=lyoko
    ...  type_election=Municipales
    ...  date=09/06/1995
    ...  perimetre=COMMUNE
    ...  heure_ouverture=08:00:00
    ...  heure_fermeture=12:00:00
    ...  publication_auto=true
    ...  publication_erreur=true
    ...  calcul_auto_exprime=true
    ${id_election} =  Ajouter election  ${election}
    Set Suite Variable  ${id_election}
    Element Should Contain In Subform  css=div.message  enregistrées

    &{candidat_test} =  BuiltIn.Create Dictionary
    ...  candidat=candidat1_workflow
    ${id_candidat1} =  Ajouter election_candidat  ${candidat_test}  ${id_election}
    Set Suite Variable  ${id_candidat1}

    &{candidat_test} =  BuiltIn.Create Dictionary
    ...  candidat=candidat2_workflow
    ${id_candidat2} =  Ajouter election_candidat  ${candidat_test}  ${id_election}
    Set Suite Variable  ${id_candidat2}

    &{centaine} =  BuiltIn.Create Dictionary
    ...  libelle=centaine_test_workflow
    ...  votant_defaut=100
    ${id_centaine} =  Ajouter centaine à l'élection  ${centaine}  ${id_election}
    Set Suite Variable  ${id_centaine}

    &{plan_election} =  BuiltIn.Create Dictionary
    ...  plan=plan_test_workflow
    ${id_plan} =  Ajouter plan_election  ${plan_election}  ${id_election}
    Set Suite Variable  ${id_plan}

    &{animation} =  BuiltIn.Create Dictionary
    ...  libelle=animation_test_workflow
    ...  modele=Résultat
    ...  perimetre_aff=COMMUNE
    ...  type_aff=Bureau de vote
    ${id_animation} =  Ajouter animation à l'élection  ${animation}  ${id_election}
    Set Suite Variable  ${id_animation}

    # Liste des actions liées aux éditions
	@{editions1}    Create List    pdf_proclamation_bureau  pdf_proclamation_siege  pdf_proclamation_perimetre  pdf_participation
	@{editions2}    Create List    pdf_resultat_perimetre  pdf_prefecture  pdf_resultats_globaux  edition  proclamation
    @{editions} =  Combine Lists  ${editions1}  ${editions2}
    Set Suite Variable  ${editions}

Etape de paramétrage
    [Documentation]  L'objet de ce test case est de valider le fonctionnement
    ...  de l'étape de paramétrage.
    ...  Durant cette étape il doit être possible d'ajouter/supprimer/modifier
    ...  les candidats, centaines, plans et animation. L'élection doit également
    ...  être modifiable.
    ...  En revanche, la saisie des résultats et de la participation ne doit pas
    ...  être possible.
    ...  Teste également l'affichage du tableau de bord du paramétrage

    # Vérification de l'étape et des actions de l'élection
    Depuis le contexte election  ${id_election}
    Page Should Contain Element  css=input[value="Paramétrage"]
	@{actions}    Create List    modifier  supprimer  reset  simulation  verification  import_inscrits
    Le portlet du formulaire doit contenir  election  ${actions}

	@{actions}    Create List    saisie  retour_parametrage  extraction  prefecture  siege
    @{actions} =  Combine Lists  ${actions}  ${editions}
    Le portlet du formulaire ne doit pas contenir  election  ${actions}

    # Vérification des actions liés aux candidats
	@{actions}    Create List    modifier  supprimer
    Depuis le contexte election_candidat  ${id_election}  ${id_candidat1}
    Le portlet du sous formulaire doit contenir  election_candidat  ${actions}
    Click On Back Button In SubForm
    L'icone d'ajout doit être présente dans le sous tableau  election_candidat

    # Vérification des actions liés aux plans
    Depuis le contexte plan_election  ${id_election}  ${id_plan}
    Le portlet du sous formulaire doit contenir  plan_election  ${actions}
    Click On Back Button In SubForm
    L'icone d'ajout doit être présente dans le sous tableau  plan_election

    # Vérification des actions liés aux centaines
	@{actions}    Create List    modifier  supprimer  extraction  pdf_resultat_perimetre  pdf_resultats_globaux  edition
    Depuis le contexte centaine  ${id_election}  ${id_centaine}
    Le portlet du sous formulaire doit contenir  centaine  ${actions}
    Click On Back Button In SubForm
    L'icone d'ajout doit être présente dans le sous tableau  centaine

    # Vérification des actions liés aux animations
	@{actions}    Create List    modifier  supprimer  animation  creer_modele
    Depuis le contexte animation de l'election  ${id_election}  ${id_animation}
    Le portlet du sous formulaire doit contenir  animation  ${actions}
    Click On Back Button In SubForm
    L'icone d'ajout doit être présente dans le sous tableau  animation

    # L'ajout, la saisie et la suppression des résultats et de la participation par tranche horaire
    # ne doit pas être possible
    Depuis le contexte election_unite  ${id_election}  1 - Salle des Mariages
    Le portlet d'action ne doit pas être présent dans le sous-formulaire
    Click On Back Button In SubForm
    L'icone d'ajout ne doit pas être présente dans le sous tableau  election_unite

    # Permet d'accéder au formulaire de la participation par tranche horaire
    Depuis le contexte participation_election  ${id_election}  09:00:00
    Le portlet d'action ne doit pas être présent dans le sous-formulaire
    Click On Back Button In SubForm
    L'icone d'ajout ne doit pas être présente dans le sous tableau  participation_election

    # Vérification des informations issus du tableau de bord
    Depuis le contexte election  ${id_election}
    # Vérification du titre du tableau et des informations contenues
    Element Should Contain  css=div#affichage_article legend  Paramétrage
    Table cell should contain  css=div#affichage_article table.tab-tab  2  2  2 candidat(s)
    Table cell should contain  css=div#affichage_article table.tab-tab  2  3  candidat1_workflow\n candidat2_workflow
    Table cell should contain  css=div#affichage_article table.tab-tab  3  2  7 unite(s)
    Table cell should contain  css=div#affichage_article table.tab-tab  3  3  Bureau de vote : 6\nCommune : 1
    Table cell should contain  css=div#affichage_article table.tab-tab  4  2  4 tranche(s) horaire
    Table cell should contain  css=div#affichage_article table.tab-tab  4  3  horaire : 8 h - 12 h
    Table cell should contain  css=div#affichage_article table.tab-tab  5  2  1 centaines
    Table cell should contain  css=div#affichage_article table.tab-tab  5  3  centaine_test_workflow
    Table cell should contain  css=div#affichage_article table.tab-tab  6  2  1 animation(s)
    Table cell should contain  css=div#affichage_article table.tab-tab  6  3  animation_test_workflow
    Table cell should contain  css=div#affichage_article table.tab-tab  7  2  1 plan(s)
    Table cell should contain  css=div#affichage_article table.tab-tab  7  3  plan_test_workflow

Etape de simulation
    [Documentation]  L'objet de ce test case est de valider le fonctionnement
    ...  de l'étape de simulation.
    ...  Durant cette étape il doit être possible d'ajouter/supprimer/modifier
    ...  les candidats, centaines, plans et animation. L'élection doit également
    ...  être modifiable. La saisie des résultats et de la participation sont
    ...  elles aussi possible.
    ...  Teste également l'affichage du tableau de bord de la simulation

    # Passage à l'étape de simulation. La vérification du paramétrage
    # doit être effectué lors du passage.
    Depuis le contexte election  ${id_election}
    Passer à l'étape suivante  simulation  Simulation
    Valid Message Should Contain  Nombre d'unités : 7
    Valid Message Should Contain  nombre de candidat : 2
    Valid Message Should Contain  Les candidats sont correctement enregistrés
    Valid Message Should Contain  Nombre de tranche horaire : 4
    Valid Message Should Contain  La participation est correctement enregistrée
    Valid Message Should Contain  Nombre de plans : 1
    Valid Message Should Contain  Nombre d'animations : 1
    Valid Message Should Contain  Nombre de centaines : 1
    Valid Message Should Contain  Passage à l'étape de simulation
    Page Should Contain Element  css=input[value="Simulation"]

    # Vérification de l'étape et des actions de l'élection
    # Découpé en plusieurs ligne pour être plus lisible
	@{actions1}   Create List    modifier  retour_parametrage  saisie  extraction  prefecture  siege  verification  import_inscrits
    @{actions} =  Combine Lists  ${actions1}  ${editions}
    Le portlet du formulaire doit contenir  election  ${actions}

	@{actions}    Create List    simulation  finalisation  archiver  supprimer  reset
    Le portlet du formulaire ne doit pas contenir  election  ${actions}

    # Vérification des actions liés aux candidats
	@{actions}    Create List    modifier  supprimer
    Depuis le contexte election_candidat  ${id_election}  ${id_candidat1}
    Le portlet du sous formulaire doit contenir  election_candidat  ${actions}
    Click On Back Button In SubForm
    L'icone d'ajout doit être présente dans le sous tableau  election_candidat

    # Vérification des actions liés aux plans
    Depuis le contexte plan_election  ${id_election}  ${id_plan}
    Le portlet du sous formulaire doit contenir  plan_election  ${actions}
    Click On Back Button In SubForm
    L'icone d'ajout doit être présente dans le sous tableau  plan_election

    # Vérification des actions liés aux centaines
	@{actions}    Create List    modifier  supprimer  extraction  pdf_resultat_perimetre  pdf_resultats_globaux  edition
    Depuis le contexte centaine  ${id_election}  ${id_centaine}
    Le portlet du sous formulaire doit contenir  centaine  ${actions}
    Click On Back Button In SubForm
    L'icone d'ajout doit être présente dans le sous tableau  centaine

    # Vérification des actions liés aux animations
	@{actions}    Create List    modifier  supprimer  creer_modele  animation
    Depuis le contexte animation de l'election  ${id_election}  ${id_animation}
    Le portlet du sous formulaire doit contenir  animation  ${actions}
    Click On Back Button In SubForm
    L'icone d'ajout doit être présente dans le sous tableau  animation

    #  Vérification des actions liés à la saisie de la participation
    # Saisie et publication auto de la participation -> permet d'afficher toutes les actions du portlet
    Depuis le contexte participation_election  ${id_election}  10:00:00
    Click On SubForm Portlet Action  participation_election  modifier
    Input Text  unite1  10
    Input Text  unite2  20
    Input Text  unite3  30
    Input Text  unite4  40
    Input Text  unite5  50
    Input Text  unite6  60
    Click On Submit Button In Subform

	@{actions}    Create List    modifier  affichage  desafficher  web  depublier_web
    Le portlet du sous formulaire doit contenir  participation_election  ${actions}
    Click On Back Button In SubForm
    L'icone d'ajout ne doit pas être présente dans le sous tableau  participation_election

    # Saisie de résultat pour tester le tableau de bord et envoyer les résultats
    # afin de rendre les actions de dépublications visibles
    Depuis le contexte election_unite  ${id_election}  2 - Salle des fetes 1
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  150
    Input Text  votant  70
    Input Text  emargement  70
    Input Text  procuration  0
    Input Text  blanc  6
    Input Text  nul  4
    Input Text  candidat1  30
    Input Text  candidat2  30
    Click On Submit Button In Subform
    Le fichier doit exister  ../aff/res/${id_election}/bres/b2.json
    Le fichier doit exister  ../web/res/00${id_election}-0/b2.html

    #  Vérification des actions liés à la saisie des résultats
    Depuis le contexte election_unite  ${id_election}  2 - Salle des fetes 1
	@{actions}    Create List    modifier  affichage  depublier_aff  web  depublier_web  demander_validation
    Le portlet du sous formulaire doit contenir  election_unite  ${actions}
    Click On Back Button In SubForm
    L'icone d'ajout ne doit pas être présente dans le sous tableau  election_unite

    # Vérification des informations issus du tableau de bord
    Depuis le contexte election  ${id_election}
    # Met à jour le tableau de bord
    Reload Page
    # Vérification du titre du tableau et des informations contenues
    Element Should Contain  css=fieldset#resultats legend  Résultats
    @{resultats_1}  Create List    150  70  0  70  6  4  60
    @{resultats_2}  Create List    0  0  0  0  0  0  0
    :FOR  ${index}  IN RANGE    7
    \   ${position_td} =  evaluate  ${index} + ${2}
    \   ${resultat} =   Get From List  ${resultats_1}  ${index}
    \   Table cell should contain  css=fieldset#resultats table.tab-tab  3  ${position_td}  ${resultat}
    \   ${resultat} =   Get From List  ${resultats_2}  ${index}
    \   Table cell should contain  css=fieldset#resultats table.tab-tab  4  ${position_td}  ${resultat}
    # Vérification des icones d'état de l'envoi
    Page Should Contain Element  css=fieldset#resultats tr:nth-child(3) td:nth-child(9) img[src="../app/img/arrive.png"]
    Page Should Contain Element  css=fieldset#resultats tr:nth-child(3) td:nth-child(10) img[src="../app/img/arrive.png"]
    Page Should Contain Element  css=fieldset#resultats tr:nth-child(3) td:nth-child(11) img[src="../app/img/arrive.png"]
    Page Should Contain Element  css=fieldset#resultats tr:nth-child(4) td:nth-child(9) img[src="../app/img/nonarrive.png"]
    Page Should Contain Element  css=fieldset#resultats tr:nth-child(4) td:nth-child(10) img[src="../app/img/nonarrive.png"]
    Page Should Contain Element  css=fieldset#resultats tr:nth-child(4) td:nth-child(11) img[src="../app/img/nonarrive.png"]

    Element Should Contain  css=fieldset#participation legend  Participation
    Table cell should contain  css=fieldset#participation table.tab-tab  2  2  0
    Table cell should contain  css=fieldset#participation table.tab-tab  3  2  210
    # Vérification des icones d'état de l'envoi
    Page Should Contain Element  css=fieldset#participation tr:nth-child(2) td:nth-child(3) img[src="../app/img/nonarrive.png"]
    Page Should Contain Element  css=fieldset#participation tr:nth-child(2) td:nth-child(4) img[src="../app/img/nonarrive.png"]
    Page Should Contain Element  css=fieldset#participation tr:nth-child(2) td:nth-child(5) img[src="../app/img/nonarrive.png"]
    Page Should Contain Element  css=fieldset#participation tr:nth-child(3) td:nth-child(3) img[src="../app/img/arrive.png"]
    Page Should Contain Element  css=fieldset#participation tr:nth-child(3) td:nth-child(4) img[src="../app/img/arrive.png"]
    Page Should Contain Element  css=fieldset#participation tr:nth-child(3) td:nth-child(5) img[src="../app/img/arrive.png"]

Etape de saisie
    [Documentation]  L'objet de ce test case est de valider le fonctionnement
    ...  de l'étape de saisie.
    ...  Durant cette étape il n'est plus possible d'ajouter/supprimer/modifier
    ...  les centaines et plans. Les candidats ne peuvent plus être supprimer/ajouter
    ...  et il est uniquement possible de modifier leur couleur et leur photo.
    ...  La saisie des résultats et de la participation est toujours possible.
    ...  Seul le choix du modèle web est modifiable pour l'élection et certaines
    ...  actions ne sont plus disponible
    ...  Teste également l'affichage du tableau de bord de la saisie

    # Saisie de résultat pour tester la réinitailisation des résultats lors du changement d'étape
    Depuis le contexte election_unite  ${id_election}  1 - Salle des Mariages
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  100
    Input Text  votant  97
    Input Text  emargement  97
    Input Text  procuration  3
    Input Text  blanc  3
    Input Text  nul  4
    Input Text  candidat1  50
    Input Text  candidat2  40
    Click On Submit Button In Subform
    Le fichier doit exister  ../aff/res/${id_election}/bres/b1.json
    Le fichier doit exister  ../web/res/00${id_election}-0/b1.html

    Depuis le contexte participation_election  ${id_election}  09:00:00
    Click On SubForm Portlet Action  participation_election  modifier
    Input Text  unite1  97
    Click On Submit Button In Subform
    # publication de la participation
    Envoyer la participation à l'affichage  ${id_election}  09:00:00
    Envoyer la participation à la page web  ${id_election}  09:00:00

    # Validation du passage à l'étape de saisie
    Depuis le contexte election  ${id_election}
    Passer à l'étape suivante  saisie  Saisie
    Wait Until Element Is Visible  css=div.message.ui-state-valid p span.text
    Valid Message Should Contain  Nombre d'unités : 7
    Valid Message Should Contain  nombre de candidat : 2
    Valid Message Should Contain  Les candidats sont correctement enregistrés
    Valid Message Should Contain  Nombre de tranche horaire : 4
    Valid Message Should Contain  La participation est correctement enregistrée
    Valid Message Should Contain  Nombre de plans : 1
    Valid Message Should Contain  Nombre d'animations : 1
    Valid Message Should Contain  Nombre de centaines : 1
    Valid Message Should Contain  Passage à l'étape de saisie
    Page Should Contain Element  css=input[value="Saisie"]

    # Vérification de la remise à zéro des résultats et de la participation
    Depuis le contexte election_unite  ${id_election}  1 - Salle des Mariages
    &{resultats_attendus} =  BuiltIn.Create Dictionary
    ...  inscrit=100
    ...  votant=\
    ...  emargement=\
    ...  procuration=\
    ...  perimetre=\
    ...  blanc=\
    ...  nul=\
    Depuis le contexte election_unite  ${id_election}  1 - Salle des Mariages
    Vérifier les valeurs des champs  ${resultats_attendus}

    File Should Not Exist  ../web/res/00${id_election}-0/b1.html
    File Should Not Exist  ../aff/res/${id_election}/bres/1.json

    # Vérification dans le listing de la participation que le nombre de votant est le bon
    # et que la participation n'est plus publiée
    Depuis le contexte election  ${id_election}
    Click On Tab  participation_election  participation(s)
    Element Should Contain  css=div#sousform-participation_election td.col-3    ${EMPTY}
    # aff non publié
    Page Should Contain Element  css=div#sousform-participation_election td.col-4 img[src="../app/img/nonarrive.png"]
    # web non publié
    Page Should Contain Element  css=div#sousform-participation_election td.col-5 img[src="../app/img/nonarrive.png"]

    # Vérification de la remise à zéro et de la dépublication des résultats des centaines
    Depuis le contexte centaine  ${id_election}  ${id_centaine}
    # Résultats enregistrés visible dans le tableau de bord de la centaine
    Element Should Contain  css=div#sousform-container table.tab-tab tr:nth-child(2) td:nth-child(5)    0
    Element Should Contain  css=div#sousform-container table.tab-tab tr:nth-child(2) td:nth-child(6)    100
    Element Should Contain  css=div#sousform-container table.tab-tab tr:nth-child(2) td:nth-child(7)    ${EMPTY}
    Element Should Contain  css=div#sousform-container table.tab-tab tr:nth-child(2) td:nth-child(8)    ${EMPTY}
    Element Should Contain  css=div#sousform-container table.tab-tab tr:nth-child(2) td:nth-child(9)    ${EMPTY}
    Element Should Contain  css=div#sousform-container table.tab-tab tr:nth-child(2) td:nth-child(10)   ${EMPTY}
    Element Should Contain  css=div#sousform-container table.tab-tab tr:nth-child(2) td:nth-child(11)   ${EMPTY}
    File Should Not Exist  ../web/res/${id_centaine}/b1.html
    File Should Not Exist  ../aff/res/${id_centaine}/bres/1.json

    # Vérification des actions de l'élection
	@{actions1}    Create List    modifier  retour_simulation  finalisation  import_inscrits  prefecture  extraction
    @{actions} =  Combine Lists  ${actions1}  ${editions}
    Le portlet du formulaire doit contenir  election  ${actions}
    # Découpé en plusieurs ligne pour être plus lisible
    @{actions}   Create List    siege  verification  retour_parametrage  simulation  saisie  reset  supprimer
    Le portlet du formulaire ne doit pas contenir  election  ${actions}

    # Vérification des champs pouvant être saisie pour l'élection
    Depuis le contexte election  ${id_election}
    Click On Form Portlet Action  election  modifier
    &{type_champs_a_tester} =  BuiltIn.Create Dictionary
    ...  libelle=hiddenstatic
    ...  code=hiddenstatic
    ...  tour=hiddenstatic
    ...  type_election=hiddenstatic
    ...  date=hiddenstatic
    ...  perimetre=hiddenstatic
    ...  votant_defaut=hidden
    ...  heure_ouverture=hiddenstatic
    ...  heure_fermeture=hiddenstatic
    ...  publication_auto=hiddenstatic
    ...  publication_erreur=hiddenstatic
    ...  calcul_auto_exprime=hiddenstatic
    ...  garder_resultat_simulation=hiddenstatic

    ${champs} =  Get Dictionary Keys  ${type_champs_a_tester}
    :FOR  ${champ}  IN  @{champs}
    \   Le champ ${champ} doit être ${type_champs_a_tester.${champ}}

    # Vérification des actions liés aux plans
    Depuis le contexte plan_election  ${id_election}  ${id_plan}
    Portlet Action Should Be In SubForm  plan_election  parametrage_election_plan
    Click On Back Button In SubForm
    L'icone d'ajout ne doit pas être présente dans le sous tableau  plan_election

    # Vérification des actions liés aux candidats
    Depuis le contexte election_candidat  ${id_election}  ${id_candidat1}
    Portlet Action Should Not Be In SubForm  election_candidat  supprimer
    Portlet Action Should Be In SubForm  election_candidat  modifier
    # Vérification des champs pouvant être saisie
    Click On SubForm Portlet Action  election_candidat  modifier
    Open Fieldset In SubForm  election_candidat  listes-municipales
    &{type_champs_a_tester} =  BuiltIn.Create Dictionary
    ...  election_candidat=hiddenstatic
    ...  election=hiddenstatic
    ...  candidat=hiddenstatic
    ...  ordre=hiddenstatic
    ...  prefecture=hiddenstatic
    ...  age_moyen=hiddenstatic
    ...  siege=hiddenstatic
    ...  age_moyen_com=hiddenstatic
    ...  siege_com=hiddenstatic
    ...  couleur=hexa
    ...  photo=upload2

    ${champs} =  Get Dictionary Keys  ${type_champs_a_tester}
    :FOR  ${champ}  IN  @{champs}
    \   Le champ ${champ} doit être ${type_champs_a_tester.${champ}}

    Depuis le listing election_candidat  ${id_election}
    L'icone d'ajout ne doit pas être présente dans le sous tableau  election_candidat

    # Vérification des actions liés aux centaines
	@{actions}    Create List    extraction  pdf_resultat_perimetre  pdf_resultats_globaux  edition
    Depuis le contexte centaine  ${id_election}  ${id_centaine}
    Le portlet du sous formulaire doit contenir  centaine  ${actions}
    Click On Back Button In SubForm
    L'icone d'ajout ne doit pas être présente dans le sous tableau  centaine

    # Vérification des actions liés aux animations
	@{actions}    Create List    modifier  supprimer  creer_modele  animation
    Depuis le contexte animation de l'election  ${id_election}  ${id_animation}
    Le portlet du sous formulaire doit contenir  animation  ${actions}
    Click On Back Button In SubForm
    L'icone d'ajout doit être présente dans le sous tableau  animation

    #  Vérification des actions liés à la saisie de la participation par tranche horaire
	@{actions}    Create List    modifier  affichage  desafficher  web  depublier_web
    Depuis le contexte participation_election  ${id_election}  11:00:00
    Click On SubForm Portlet Action  participation_election  modifier
    Input Text  unite1  97
    Input Text  unite2  34
    Input Text  unite3  108
    Click On Submit Button In Subform
    # Permet d'afficher toutes les actions (publication + dépublication)
    Envoyer la participation à l'affichage  ${id_election}  11:00:00
    Envoyer la participation à la page web  ${id_election}  11:00:00
    Le portlet du sous formulaire doit contenir  participation_election  ${actions}
    Click On Back Button In SubForm
    L'icone d'ajout ne doit pas être présente dans le sous tableau  participation_election

    # Saisie de résultat pour tester le tableau de bord et publier les résultats
    # afin de rendre visible les actions de dépublication
    Depuis le contexte election_unite  ${id_election}  3 - Ecole A
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  10
    Input Text  votant  546
    Input Text  emargement  546
    Input Text  procuration  6
    Input Text  blanc  43
    Input Text  nul  57
    Input Text  candidat1  146
    Input Text  candidat2  300
    Click On Submit Button In Subform
    Le fichier doit exister  ../aff/res/${id_election}/bres/b3.json
    Le fichier doit exister  ../web/res/00${id_election}-0/b3.html

    #  Vérification des actions liés à la saisie des résultats
    Depuis le contexte election_unite  ${id_election}  3 - Ecole A
	@{actions}    Create List    modifier  affichage  depublier_aff  web  depublier_web  demander_validation
    Le portlet du sous formulaire doit contenir  election_unite  ${actions}
    Click On Back Button In SubForm
    L'icone d'ajout ne doit pas être présente dans le sous tableau  election_unite

    # Vérification des informations issus du tableau de bord
    Depuis le contexte election  ${id_election}
    # Vérification du titre du tableau et des informations contenues
    Element Should Contain  css=div#affichage_article > fieldset > legend  Saisie
    # Icones de saisie et d'envoi des résultats
    # Résultats : Salle des Mariages
    Page Should Contain Element  css=div#affichage_article tr:nth-child(3) td:nth-child(2) img[src="../app/img/nonarrive.png"]
    Page Should Contain Element  css=div#affichage_article tr:nth-child(3) td:nth-child(3) img[src="../app/img/nonarrive.png"]
    Page Should Contain Element  css=div#affichage_article tr:nth-child(3) td:nth-child(4) img[src="../app/img/nonarrive.png"]
    # Résultats : Ecole A
    Page Should Contain Element  css=div#affichage_article tr:nth-child(4) td:nth-child(2) img[src="../app/img/erreur.png"]
    Page Should Contain Element  css=div#affichage_article tr:nth-child(4) td:nth-child(3) img[src="../app/img/arrive.png"]
    Page Should Contain Element  css=div#affichage_article tr:nth-child(4) td:nth-child(4) img[src="../app/img/arrive.png"]
    # Icones de saisie et d'envoi de la participation
    # Horaire : 11:00:00
    Page Should Contain Element  css=div#affichage_article tr:nth-child(12) td:nth-child(2) img[src="../app/img/nonarrive.png"]
    Page Should Contain Element  css=div#affichage_article tr:nth-child(12) td:nth-child(3) img[src="../app/img/arrive.png"]
    Page Should Contain Element  css=div#affichage_article tr:nth-child(12) td:nth-child(4) img[src="../app/img/arrive.png"]
    # Horaire : 10:00:00
    Page Should Contain Element  css=div#affichage_article tr:nth-child(11) td:nth-child(2) img[src="../app/img/nonarrive.png"]
    Page Should Contain Element  css=div#affichage_article tr:nth-child(11) td:nth-child(3) img[src="../app/img/nonarrive.png"]
    Page Should Contain Element  css=div#affichage_article tr:nth-child(11) td:nth-child(4) img[src="../app/img/nonarrive.png"]

Etape de finalisation
    [Documentation]  L'objet de ce test case est de valider le fonctionnement
    ...  de l'étape de finalisation.
    ...  L'étape de finalisation est celle qui va servir à faire les éditions, traitement
    ...  et exports des résultats de l'élection. Durant cette étape la saisie n'est plus
    ...  possible. Il est également possible d'accéder aux animations et à la page web et
    ...  de les paramétrer.
    ...  Teste également l'affichage du tableau de bord de la finalisation

    # Passage à l'étape de finalisation
    Depuis le contexte election  ${id_election}
    Passer à l'étape suivante  finalisation  Finalisation
    Valid Message Should Contain  Passage à l'étape de finalisation
    Page Should Contain Element  css=input[value="Finalisation"]

    # Vérification de l'étape et des actions de l'élection
    # Découpé en plusieurs ligne pour être plus lisible
	@{actions}    Create List    modifier  retour_saisie  archiver  import_inscrits  extraction  prefecture  siege
    @{actions} =  Combine Lists  ${actions}  ${editions}
    Le portlet du formulaire doit contenir  election  ${actions}

    @{actions}   Create List    retour_simulation  finalisation  verification  reset  supprimer
    Le portlet du formulaire ne doit pas contenir  election  ${actions}

    # Vérification des champs pouvant être saisie
    Click On Form Portlet Action  election  modifier
    &{type_champs_a_tester} =  BuiltIn.Create Dictionary
    ...  libelle=hiddenstatic
    ...  code=hiddenstatic
    ...  tour=hiddenstatic
    ...  type_election=hiddenstatic
    ...  date=hiddenstatic
    ...  perimetre=hiddenstatic
    ...  votant_defaut=hidden
    ...  heure_ouverture=hiddenstatic
    ...  heure_fermeture=hiddenstatic
    ...  publication_auto=hiddenstatic
    ...  publication_erreur=hiddenstatic
    ...  calcul_auto_exprime=hiddenstatic
    ...  garder_resultat_simulation=hiddenstatic

    ${champs} =  Get Dictionary Keys  ${type_champs_a_tester}
    :FOR  ${champ}  IN  @{champs}
    \   Le champ ${champ} doit être ${type_champs_a_tester.${champ}}

    # Vérification des actions liés aux plans
    Depuis le contexte plan_election  ${id_election}  ${id_plan}
    Portlet Action Should Be In SubForm  plan_election  parametrage_election_plan
    Click On Back Button In SubForm
    L'icone d'ajout ne doit pas être présente dans le sous tableau  plan_election

    # Vérification des actions liés aux candidats
    Depuis le contexte election_candidat  ${id_election}  ${id_candidat1}
    Portlet Action Should Not Be In SubForm  election_candidat  supprimer
    Portlet Action Should Be In SubForm  election_candidat  modifier
    # Vérification des champs pouvant être saisie
    Click On SubForm Portlet Action  election_candidat  modifier
    Open Fieldset In SubForm  election_candidat  listes-municipales
    &{type_champs_a_tester} =  BuiltIn.Create Dictionary
    ...  election_candidat=hiddenstatic
    ...  election=hiddenstatic
    ...  candidat=hiddenstatic
    ...  ordre=hiddenstatic
    ...  prefecture=hiddenstatic
    ...  age_moyen=text
    ...  siege=text
    ...  age_moyen_com=text
    ...  siege_com=text
    ...  couleur=hexa
    ...  photo=upload2

    ${champs} =  Get Dictionary Keys  ${type_champs_a_tester}
    :FOR  ${champ}  IN  @{champs}
    \   Le champ ${champ} doit être ${type_champs_a_tester.${champ}}

    Depuis le listing election_candidat  ${id_election}
    L'icone d'ajout ne doit pas être présente dans le sous tableau  election_candidat

    # Vérification des actions liés aux centaines
	@{actions}    Create List    extraction  pdf_resultat_perimetre  pdf_resultats_globaux  edition
    Depuis le contexte centaine  ${id_election}  ${id_centaine}
    Le portlet du sous formulaire doit contenir  centaine  ${actions}
    Click On Back Button In SubForm
    L'icone d'ajout ne doit pas être présente dans le sous tableau  centaine

    # Vérification des actions liés aux animations
	@{actions}    Create List    modifier  supprimer  creer_modele  animation
    Depuis le contexte animation de l'election  ${id_election}  ${id_animation}
    Le portlet du sous formulaire doit contenir  animation  ${actions}
    Click On Back Button In SubForm
    L'icone d'ajout doit être présente dans le sous tableau  animation

    #  Vérification des actions liés à la saisie des résultats
    Depuis le contexte election_unite  ${id_election}  1 - Salle des Mariages
	@{actions}    Create List    affichage  web  demander_validation
    Le portlet du sous formulaire doit contenir  election_unite  ${actions}
	@{actions}    Create List    supprimer  modifier
    Le portlet du sous formulaire ne doit pas contenir  election_unite  ${actions}
    Click On Back Button In SubForm
    L'icone d'ajout ne doit pas être présente dans le sous tableau  election_unite

    #  Vérification des actions liés à la saisie de la participation
    Depuis le contexte participation_election  ${id_election}  09:00:00
	@{actions}    Create List    affichage  web
    Le portlet du sous formulaire doit contenir  participation_election  ${actions}
	@{actions}    Create List    supprimer  modifier
    Le portlet du sous formulaire ne doit pas contenir  participation_election  ${actions}
    Click On Back Button In SubForm
    L'icone d'ajout ne doit pas être présente dans le sous tableau  participation_election

    # Vérification des informations issus du tableau de bord
    Depuis le contexte election  ${id_election}
    # Vérification du titre du tableau et des informations contenues
    Element Should Contain  css=div#affichage_article > fieldset > legend  Résultats - Election Test Workflow
    @{resultatsU3}  Create List    10   546  546  6  43  57  446  146  300
    @{resultats}    Create List    0    0    0    0  0   0   0    0    0
    @{total}        Create List    260   546  546  6  43  57  446  146  300
    :FOR  ${index}  IN RANGE    9
    \   ${position_td} =  evaluate  ${index} + ${2}
    \   ${resultat} =   Get From List  ${resultatsU3}  ${index}
    \   Table cell should contain  css=div#affichage_article table.tab-tab  4  ${position_td}  ${resultat}
    \   ${resultat} =   Get From List  ${resultats}  ${index}
    \   Table cell should contain  css=div#affichage_article table.tab-tab  5  ${position_td}  ${resultat}
    \   ${resultat} =   Get From List  ${total}  ${index}
    \   Table cell should contain  css=div#affichage_article table.tab-tab  8  ${position_td}  ${resultat}

Etape d'archivage
    [Documentation]  L'objet de ce test case est de valider le fonctionnement
    ...  de l'étape d'archivage.
    ...  Lorsqu'elle est archivée, l'election, ses animations, centaines,
    ...  résultats, participation et plans ne peuvent plus être modifié et supprimé.
    ...  L'accés aux animations et au portail web est toujours possible
    ...  Teste également l'affichage du tableau de bord de l'archivage (idem finalisation)

    # Archivage de l'élection
    Depuis le contexte election  ${id_election}
    Passer à l'étape suivante  archiver  Archivage
    Valid Message Should Contain  Election archivée
    Page Should Contain Element  css=input[value="Archivage"]

    Depuis le listing  election
    Input Text  css=div#adv-search-adv-fields input#id  ${id_election}
    Input Text  css=div#adv-search-adv-fields input#libelle  Election Test Workflow
    Click On Search Button
    Wait Until Element Is Visible  css=tr.election-archivee td a#action-tab-election-left-consulter-${id_election}

    # Accès à l'élection archivée et vérification de l'étape et des actions de l'élection
    Depuis le contexte election  ${id_election}
    # Découpé en plusieurs ligne pour être plus lisible
	@{actions}    Create List    retour_finalisation  import_inscrits  extraction  prefecture
    @{actions} =  Combine Lists  ${actions}  ${editions}
    Le portlet du formulaire doit contenir  election  ${actions}

    @{actions}   Create List    retour_saisie  archiver  verification  reset  siege  modifier  supprimer
    Le portlet du formulaire ne doit pas contenir  election  ${actions}

    # Vérification des actions liés aux candidats
    Depuis le contexte election_candidat  ${id_election}  ${id_candidat1}
    Le portlet d'action ne doit pas être présent dans le sous-formulaire
    Click On Back Button In SubForm
    L'icone d'ajout ne doit pas être présente dans le sous tableau  election_candidat

    #  Vérification des actions liés à la saisie des résultats
    Depuis le contexte election_unite  ${id_election}  1 - Salle des Mariages
    Le portlet d'action ne doit pas être présent dans le sous-formulaire
    Click On Back Button In SubForm
    L'icone d'ajout ne doit pas être présente dans le sous tableau  election_unite

    #  Vérification des actions liés à la saisie de la participation
    Depuis le contexte participation_election  ${id_election}  09:00:00
    Le portlet d'action ne doit pas être présent dans le sous-formulaire
    Click On Back Button In SubForm
    L'icone d'ajout ne doit pas être présente dans le sous tableau  participation_election

    # Vérification des actions liés aux centaines
	@{actions}    Create List    extraction  pdf_resultat_perimetre  pdf_resultats_globaux  edition
    Depuis le contexte centaine  ${id_election}  ${id_centaine}
    Le portlet du sous formulaire doit contenir  centaine  ${actions}
    L'icone d'ajout ne doit pas être présente dans le sous tableau  centaine

    # Vérification des actions liés aux animations
	@{actions}    Create List    creer_modele  animation
    Depuis le contexte animation de l'election  ${id_election}  ${id_animation}
    Le portlet du sous formulaire doit contenir  animation  ${actions}
    Click On Back Button In SubForm
    L'icone d'ajout ne doit pas être présente dans le sous tableau  animation

    # Vérification des actions liés aux plans
    Depuis le contexte plan_election  ${id_election}  ${id_plan}
    Portlet Action Should Be In SubForm  plan_election  parametrage_election_plan
    Click On Back Button In SubForm
    L'icone d'ajout ne doit pas être présente dans le sous tableau  plan_election

    # Vérification des informations issus du tableau de bord
    Depuis le contexte election  ${id_election}
    # Vérification du titre du tableau et des informations contenues
    Element Should Contain  css=div#affichage_article > fieldset > legend  Résultats - Election Test Workflow
    @{resultatsU3}  Create List    10   546  546  6  43  57  446  146  300
    @{resultats}    Create List    0    0    0    0  0   0   0    0    0
    @{total}        Create List    260   546  546  6  43  57  446  146  300
    :FOR  ${index}  IN RANGE    9
    \   ${position_td} =  evaluate  ${index} + ${2}
    \   ${resultat} =   Get From List  ${resultatsU3}  ${index}
    \   Table cell should contain  css=div#affichage_article table.tab-tab  4  ${position_td}  ${resultat}
    \   ${resultat} =   Get From List  ${resultats}  ${index}
    \   Table cell should contain  css=div#affichage_article table.tab-tab  5  ${position_td}  ${resultat}
    \   ${resultat} =   Get From List  ${total}  ${index}
    \   Table cell should contain  css=div#affichage_article table.tab-tab  8  ${position_td}  ${resultat}

Passage de l'étape de simulation à l'étape de paramétrage
    [Documentation]  L'objet de ce test case est de vérifier que les résultats
    ...  enregistrés sont correctement supprimés lors du passage de la
    ...  simulation au paramétrage

    Depuis le contexte election  ${id_election}
    Click On Form Portlet Action  election  retour_finalisation  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    # Reinitialise la fenêtre modale
    Depuis le contexte election  ${id_election}
    Retourner à l'étape précédente  saisie  Saisie
    Reload Page
    Retourner à l'étape précédente  simulation  Simulation
    Reload Page

    # Saisie de résultats/participation
    Depuis le contexte election_unite  ${id_election}  1 - Salle des Mariages
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  100
    Input Text  votant  97
    Input Text  emargement  97
    Input Text  procuration  3
    Input Text  blanc  3
    Input Text  nul  4
    Input Text  candidat1  90
    Click On Submit Button In Subform
    Le fichier doit exister  ../aff/res/${id_election}/bres/b1.json
    Le fichier doit exister  ../web/res/00${id_election}-0/b1.html

    Depuis le contexte participation_election  ${id_election}  09:00:00
    Click On SubForm Portlet Action  participation_election  modifier
    Input Text  unite1  97
    Click On Submit Button In Subform
    # publication de la participation
    Envoyer la participation à l'affichage  ${id_election}  09:00:00
    Envoyer la participation à la page web  ${id_election}  09:00:00

    # passage de l'étape de simulation à l'étape de paramétrage
    Depuis le contexte election  ${id_election}
    Retourner à l'étape précédente  parametrage  Paramétrage

    &{resultats_attendus} =  BuiltIn.Create Dictionary
    ...  inscrit=100
    ...  votant=\
    ...  emargement=\
    ...  procuration=\
    ...  perimetre=\
    ...  blanc=\
    ...  nul=\
    Depuis le contexte election_unite  ${id_election}  1 - Salle des Mariages
    Vérifier les valeurs des champs  ${resultats_attendus}

    File Should Not Exist  ../web/res/00${id_election}-0/b1.html
    File Should Not Exist  ../aff/res/${id_election}/bres/1.json

    # Vérification dans le listing de la participation que le nombre de votant est le bon
    # et que la participation est toujours publiée
    Depuis le contexte election  ${id_election}
    Click On Tab  participation_election  participation(s)
    Element Should Contain  css=div#sousform-participation_election td.col-3    ${EMPTY}
    # aff non publié
    Page Should Contain Element  css=div#sousform-participation_election td.col-4 img[src="../app/img/nonarrive.png"]
    # web non publié
    Page Should Contain Element  css=div#sousform-participation_election td.col-5 img[src="../app/img/nonarrive.png"]

Conservation des résultats de la simulation
    [Documentation]  L'objet de ce test case est de vérifier que les résultats
    ...  sont bien conservé lorsque l'option de conservation des résultats est
    ...  cochée et qu'ils sont supprimés sinon

    # selection de l'option de conservation des résultats
    &{election} =  BuiltIn.Create Dictionary
    ...  garder_resultat_simulation=true
    Modifier election  ${id_election}  ${election}

    Depuis le contexte election  ${id_election}
    Passer à l'étape suivante  simulation  Simulation

    # Saisie de résultats/participation
    Depuis le contexte election_unite  ${id_election}  1 - Salle des Mariages
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  100
    Input Text  votant  97
    Input Text  emargement  97
    Input Text  procuration  3
    Input Text  blanc  3
    Input Text  nul  4
    Input Text  candidat1  90
    Click On Submit Button In Subform
    Le fichier doit exister  ../aff/res/${id_election}/bres/b1.json
    Le fichier doit exister  ../web/res/00${id_election}-0/b1.html

    Depuis le contexte participation_election  ${id_election}  09:00:00
    Click On SubForm Portlet Action  participation_election  modifier
    Input Text  unite1  97
    Click On Submit Button In Subform
    # publication de la participation
    Envoyer la participation à l'affichage  ${id_election}  09:00:00
    Envoyer la participation à la page web  ${id_election}  09:00:00

    # passage de l'étape de simulation à l'étape de saisie
    # les resultats et les fichiers des répertoires web et aff doivent
    # toujours exister
    Depuis le contexte election  ${id_election}
    Passer à l'étape suivante  saisie  Saisie

    &{resultats_attendus} =  BuiltIn.Create Dictionary
    ...  inscrit=100
    ...  votant=97
    ...  emargement=97
    ...  procuration=3
    ...  blanc=3
    ...  nul=4
    ...  exprime=90
    Depuis le contexte election_unite  ${id_election}  1 - Salle des Mariages
    Vérifier les valeurs des champs  ${resultats_attendus}

    Le fichier doit exister  ../web/res/00${id_election}-0/b1.html
    Le fichier doit exister  ../aff/res/${id_election}/bres/b1.json

    # Vérification dans le listing de la participation que le nombre de votant est le bon
    # et que la participation est toujours publiée
    Depuis le contexte election  ${id_election}
    Click On Tab  participation_election  participation(s)
    Element Should Contain  css=div#sousform-participation_election td.col-2    97
    # aff publié
    Page Should Contain Element  css=div#sousform-participation_election td.col-4 img[src="../app/img/arrive.png"]
    # web publié
    Page Should Contain Element  css=div#sousform-participation_election td.col-5 img[src="../app/img/arrive.png"]

Workflow de la délégation de saisie
    [Documentation]  L'objet de ce test case est de valider le workflow pour
    ...  l'élection si l'option de délégation est activée.
    ...  Test uniquement les formulaires de la délégation et de la saisie de la
    ...  participation par unité

    # Retour à l'étape de paramétrage
    Depuis le contexte election  ${id_election}
    Retourner à l'étape précédente  simulation  Simulation
    Reload Page
    Retourner à l'étape précédente  parametrage  Paramétrage

    # Préparation de l'élection et de la délégation
    &{election} =  BuiltIn.Create Dictionary
    ...  delegation_saisie=true
    ...  delegation_participation=true
    Modifier election  ${id_election}  ${election}

    Ajouter l'utilisateur  test  test@test.test  test  test  ADMINISTRATEUR

    &{delegation} =  BuiltIn.Create Dictionary
    ...  acteur=test
    ...  unite=1 - Salle des Mariages
    ${id_delegation} =  Ajouter delegation  ${delegation}  ${id_election}
    Set Suite Variable  ${id_delegation}

    # PARAMÉTRAGE

    # Nécéssaire car la saisie est déléguée à l'acteur test
    Depuis la page d'accueil    test  test

    # Vérification des actions liés à la délégation à l'étape de paramétrage
	@{actions}    Create List    modifier  supprimer
    Depuis le contexte delegation  ${id_delegation}  ${id_election}
    Le portlet du sous formulaire doit contenir  delegation  ${actions}
    Click On Back Button In SubForm
    L'icone d'ajout doit être présente dans le sous tableau  delegation

    # Vérification des actions liés à la saisie de la participation par unité
    Depuis le contexte de la participation par unite  ${id_election}  1 - Salle des Mariages
    Le portlet d'action ne doit pas être présent dans le sous-formulaire
    Click On Back Button In SubForm
    L'icone d'ajout ne doit pas être présente dans le sous tableau  delegation_participation

    # SIMULATION
    Depuis le contexte election  ${id_election}
    Passer à l'étape suivante  simulation  Simulation

    # Vérification des actions liés à la délégation
    Depuis le contexte delegation  ${id_delegation}  ${id_election}
    Le portlet du sous formulaire doit contenir  delegation  ${actions}
    Click On Back Button In SubForm
    L'icone d'ajout doit être présente dans le sous tableau  delegation

    # Vérification des actions liés à la saisie de la participation par unité
	@{actions}    Create List    modifier  affichage  desafficher  web  depublier_web
    # Permet d'afficher toutes les actions (publication + dépublication)
    Envoyer la participation de l'unité à l'affichage  ${id_election}  1 - Salle des Mariages
    Envoyer la participation de l'unité à la page web  ${id_election}  1 - Salle des Mariages

    Depuis le contexte de la participation par unite  ${id_election}  1 - Salle des Mariages
    Le portlet du sous formulaire doit contenir  delegation_participation  ${actions}
    Click On Back Button In SubForm
    L'icone d'ajout ne doit pas être présente dans le sous tableau  delegation_participation

    # SAISIE
    Depuis le contexte election  ${id_election}
    Passer à l'étape suivante  saisie  Saisie

    # Vérification des actions liés à la délégation
	@{actions}    Create List    modifier  supprimer
    Depuis le contexte delegation  ${id_delegation}  ${id_election}
    Le portlet du sous formulaire doit contenir  delegation  ${actions}
    Click On Back Button In SubForm
    L'icone d'ajout doit être présente dans le sous tableau  delegation

    # Vérification des actions liés à la saisie de la participation par unité
	@{actions}    Create List    modifier  affichage  desafficher  web  depublier_web
    # Permet d'afficher toutes les actions (publication + dépublication)
    Envoyer la participation de l'unité à l'affichage  ${id_election}  1 - Salle des Mariages
    Envoyer la participation de l'unité à la page web  ${id_election}  1 - Salle des Mariages

    Depuis le contexte de la participation par unite  ${id_election}  1 - Salle des Mariages
    Le portlet du sous formulaire doit contenir  delegation_participation  ${actions}
    Click On Back Button In SubForm
    L'icone d'ajout ne doit pas être présente dans le sous tableau  delegation_participation

    # FINALISATION
    Depuis le contexte election  ${id_election}
    Passer à l'étape suivante  finalisation  Finalisation

    # Vérification des actions liés à la délégation
    Depuis le contexte delegation  ${id_delegation}  ${id_election}
    Le portlet d'action ne doit pas être présent dans le sous-formulaire
    Click On Back Button In SubForm
    L'icone d'ajout ne doit pas être présente dans le sous tableau  delegation

    # Vérification des actions liés à la saisie de la participation par unité
	@{actions}    Create List    affichage  web
    Depuis le contexte de la participation par unite  ${id_election}  1 - Salle des Mariages
    Le portlet du sous formulaire doit contenir  delegation_participation  ${actions}
    Click On Back Button In SubForm
    L'icone d'ajout ne doit pas être présente dans le sous tableau  delegation_participation

    # ARCHIVAGE
    Depuis le contexte election  ${id_election}
    Passer à l'étape suivante  archiver  Archivage

    # Les sous-formulaires liées à la délégation ne doivent pas être présents
    Depuis le contexte election  ${id_election}
    Page Should Not Contain Element  css=a#delegation
    Page Should Not Contain Element  css=a#delegation_participation
