*** Settings ***
Documentation     TestSuite "Documentation" : cette suite permet d'extraire
...    automatiquement les captures à destination de la documentation.
# On inclut les mots-clefs
Resource    resources/resources.robot
# On ouvre et on ferme le navigateur respectivement au début et à la fin
# du Test Suite.
Suite Setup    For Suite Setup
Suite Teardown    For Suite Teardown
# A chaque début de Test Case on positionne la taille de la fenêtre
# pour obtenir des captures homogènes
Test Setup    Set Window Size  ${1280}  ${1024}




*** Keywords ***
Highlight heading
    [Arguments]  ${locator}

    Update element style  ${locator}  margin-top  0.75em
    Highlight  ${locator}

Capturer le menu et le dashboard des profils
    [Arguments]  ${logins}

    #
    :FOR  ${login}  IN  @{logins}
    #
    \  Depuis la page d'accueil  ${login}  ${login}
    \  Go To Dashboard
    #
    \  Capture and crop page screenshot  screenshots/profils/dashboard_${login}.png
    \  ...  content
    #
    \  Capture and crop page screenshot  screenshots/profils/menu_${login}.png
    \  ...  menu-list


Capturer le menu des profils
    [Arguments]  ${logins}

    #
    :FOR  ${login}  IN  @{logins}
    #
    \  Depuis la page d'accueil  ${login}  ${login}
    \  Capture and crop page screenshot  screenshots/profils/menu_${login}.png
    \  ...  menu-list



*** Test Cases ***
Prérequis

    [Documentation]  L'objet de ce 'Test Case' est de respecter les prérequis
    ...    nécessaires aux captures d'écran.

    [Tags]  doc

    # Création des répertoires destinés à recevoir les captures d'écran
    # selon le respect de l'architecture de la documentation
    Create Directory    results/screenshots
    Create Directory    results/screenshots/ergonomie
    Create Directory    results/screenshots/parametrage
    Create Directory    results/screenshots/fiche
    Create Directory    results/screenshots/utilisation

Jeu de données

    [Documentation]  L'objet de ce test case est de creer le jeu de données
    ...  qui servira ensuite à prendre les captures d'écrans

    [Tags]  doc

    Depuis la page d'accueil    admin  admin

    # ajout d'un utilisateur et donc d'un acteur pour la délégation de saisie
    Ajouter l'utilisateur  test  test@test.test  test  test  ADMINISTRATEUR

    # Ajout d'un logo
    Ajouter le logo  logo  openresultat  openResultat.png  logo openresultat

    # Création d'un modéle pour le portail web
    Set Suite Variable  ${lib_web}  documentation
    &{web} =  BuiltIn.Create Dictionary
    ...  libelle=${lib_web}
    ...  entete=Résultat Electoraux Libreville
    ...  url_collectivite=url_exemple
    ...  libelle_url=Portail Libreville
    ...  logo=openresultat
    ${id_web} =  Ajouter web  ${web}
    Activer modèle web  ${id_web}

    # Ajout d'un plan et positionnement des unités dessus
    &{plan} =  BuiltIn.Create Dictionary
    ...  libelle=centre
    ...  image_plan=plan_centre.gif
    ...  img_unite_arrivee=img_unite_arrivee.gif
    ...  img_unite_non_arrivee=img_unite_non_arrivee.gif
    ...  par_defaut=true
    ${plan_id} =  Ajouter plan  ${plan}
    Set Suite Variable  ${plan_id}

    &{plan_unite} =  BuiltIn.Create Dictionary
    ...  plan=centre
    ...  unite=1 - Salle des Mariages
    ...  position_x=50
    ...  position_y=50
    ...  largeur_icone=80
    ...  hauteur_icone=50
    ${plan_unite1} =  Ajouter plan_unite  ${plan_unite}
    Set Suite Variable  ${plan_unite1}

    &{plan_unite} =  BuiltIn.Create Dictionary
    ...  plan=centre
    ...  unite=2 - Salle des fetes 1
    ...  position_x=300
    ...  position_y=300
    ${plan_unite2} =  Ajouter plan_unite  ${plan_unite}

    &{plan_unite} =  BuiltIn.Create Dictionary
    ...  plan=centre
    ...  unite=3 - Ecole A
    ...  position_x=500
    ...  position_y=200
    ${plan_unite3} =  Ajouter plan_unite  ${plan_unite}

    &{plan_unite} =  BuiltIn.Create Dictionary
    ...  plan=centre
    ...  unite=4 - Salle des fêtes 2
    ...  position_x=400
    ...  position_y=100
    ${plan_unite4} =  Ajouter plan_unite  ${plan_unite}

    &{plan_unite} =  BuiltIn.Create Dictionary
    ...  plan=centre
    ...  unite=5 - Gymnase
    ...  position_x=0
    ...  position_y=0
    ${plan_unite5} =  Ajouter plan_unite  ${plan_unite}

    &{plan_unite} =  BuiltIn.Create Dictionary
    ...  plan=centre
    ...  unite=6 - Ecole B
    ...  position_x=200
    ...  position_y=50
    ${plan_unite6} =  Ajouter plan_unite  ${plan_unite}

    # Creation d'un nouveau candidat
    &{candidat} =  BuiltIn.Create Dictionary
    ...  libelle=nouveau
    ...  libelle_liste=candidat
    Ajouter candidat  ${candidat}

    &{election} =  BuiltIn.Create Dictionary
    ...  libelle=Municipales 2020 1er tour
    ...  code=MUN20-1
    ...  tour=1
    ...  principal=true
    ...  type_election=Municipales
    ...  date=15/03/2020
    ...  perimetre=COMMUNE
    ...  heure_ouverture=08:00:00
    ...  heure_fermeture=11:00:00
    ...  calcul_auto_exprime=true
    ...  publication_auto=true
    ${id_election} =  Ajouter election  ${election}
    Set Suite Variable  ${id_election}
    Element Should Contain In Subform  css=div.message  enregistrées

    # Ajout des candidats
    &{candidat} =  BuiltIn.Create Dictionary
    ...  candidat=Hervé SCHIAVETTI
    ...  prefecture=1
    ...  age_moyen=42
    ...  age_moyen_com=40
    ...  couleur=#0000ff
    ${id_candidat1} =  Ajouter election_candidat  ${candidat}  ${id_election}

    &{candidat} =  BuiltIn.Create Dictionary
    ...  candidat=Pierre CHENEL
    ...  prefecture=2
    ...  age_moyen=53
    ...  age_moyen_com=45
    ...  couleur=#ff0000
    ${id_candidat2} =  Ajouter election_candidat  ${candidat}  ${id_election}

    # Passage à l'étape de simulation
    Depuis le contexte election  ${id_election}
    Passer à l'étape suivante  simulation  Simulation

    # Saisie des résultats
    Depuis le contexte election_unite  ${id_election}  1 - Salle des Mariages
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  760
    Input Text  votant  438
    Input Text  emargement  438
    Input Text  procuration  0
    Input Text  blanc  8
    Input Text  nul  1
    Input Text  candidat1  251
    Input Text  candidat2  178
    Click On Submit Button In Subform
    Click On Back Button In Subform

    Click On Link   2 - Salle des fetes 1
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  742
    Input Text  votant  385
    Input Text  emargement  385
    Input Text  procuration  0
    Input Text  blanc  5
    Input Text  nul  6
    Input Text  candidat1  234
    Input Text  candidat2  140
    Click On Submit Button In Subform
    Click On Back Button In Subform

    Click On Link   3 - Ecole A
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  919
    Input Text  votant  471
    Input Text  emargement  471
    Input Text  procuration  0
    Input Text  blanc  12
    Input Text  nul  8
    Input Text  candidat1  264
    Input Text  candidat2  187
    Click On Submit Button In SubForm
    Click On Back Button In Subform

    Click On Link   4 - Salle des fêtes 2
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  915
    Input Text  votant  462
    Input Text  emargement  462
    Input Text  procuration  0
    Input Text  blanc  7
    Input Text  nul  10
    Input Text  candidat1  300
    Input Text  candidat2  145
    Click On Submit Button In Subform
    Click On Back Button In Subform

    Click On Link   5 - Gymnase
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  761
    Input Text  votant  379
    Input Text  emargement  379
    Input Text  procuration  0
    Input Text  blanc  4
    Input Text  nul  5
    Input Text  candidat1  207
    Input Text  candidat2  163
    Click On Submit Button In Subform
    Click On Back Button In Subform

    Click On Link   6 - Ecole B
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  680
    Input Text  votant  297
    Input Text  emargement  297
    Input Text  procuration  0
    Input Text  blanc  1
    Input Text  nul  3
    Input Text  candidat1  182
    Input Text  candidat2  111
    Click On Submit Button In Subform

    # Saisie de la participation
    Click On Tab  participation_election  participation(s)
    Click On Link  09:00:00
    Click On SubForm Portlet Action  participation_election  modifier
    Input Text  unite1  146
    Input Text  unite2  80
    Input Text  unite3  48
    Input Text  unite4  0
    Input Text  unite5  99
    Input Text  unite6  20
    Click On Submit Button In Subform
    Envoyer la participation à l'affichage  ${id_election}  09:00:00
    Envoyer la participation à la page web  ${id_election}  09:00:00
    Click On Back Button In SubForm

    Click On Link  10:00:00
    Click On SubForm Portlet Action  participation_election  modifier
    Input Text  unite1  146
    Input Text  unite2  90
    Input Text  unite3  126
    Input Text  unite4  199
    Input Text  unite5  100
    Input Text  unite6  47
    Click On Submit Button In Subform
    Envoyer la participation à l'affichage  ${id_election}  10:00:00
    Envoyer la participation à la page web  ${id_election}  10:00:00
    Click On Back Button In SubForm

    Click On Link  11:00:00
    Click On SubForm Portlet Action  participation_election  modifier
    Input Text  unite1  146
    Input Text  unite2  137
    Input Text  unite3  142
    Input Text  unite4  263
    Input Text  unite5  110
    Input Text  unite6  103
    Click On Submit Button In Subform
    Envoyer la participation à l'affichage  ${id_election}  11:00:00
    Envoyer la participation à la page web  ${id_election}  11:00:00

    # Création d'une nouvelle animation pour chaque modèle d'animation
    &{animation} =  BuiltIn.Create Dictionary
     ...  election=Municipales 2020 1er tour
     ...  libelle=liste
     ...  titre=Aff liste municipale
     ...  modele=Liste municipale
     ...  perimetre_aff=COMMUNE
     ...  type_aff=Bureau de vote
    ${liste}=  Ajouter animation à l'élection  ${animation}  ${id_election}
    Set Suite Variable  ${liste}

    &{animation} =  BuiltIn.Create Dictionary
     ...  election=Municipales 2020 1er tour
     ...  libelle=resultat
     ...  titre=Aff résultat
     ...  modele=Résultat
     ...  perimetre_aff=COMMUNE
     ...  type_aff=Bureau de vote
    ${resultat}=  Ajouter animation à l'élection  ${animation}  ${id_election}
    Set Suite Variable  ${resultat}

    &{animation} =  BuiltIn.Create Dictionary
     ...  election=Municipales 2020 1er tour
     ...  libelle=res_lg
     ...  titre=Aff résultat liste gauche
     ...  modele=Résultat liste gauche
     ...  perimetre_aff=COMMUNE
     ...  type_aff=Bureau de vote
    ${res_lg}=  Ajouter animation à l'élection  ${animation}  ${id_election}
    Set Suite Variable  ${res_lg}

    &{animation} =  BuiltIn.Create Dictionary
     ...  election=Municipales 2020 1er tour
     ...  libelle=res_cumul_lg
     ...  titre=Aff résultat avec graphique cumul liste gauche
     ...  modele=Résultat avec graphique cumul liste gauche
     ...  perimetre_aff=COMMUNE
     ...  type_aff=Bureau de vote
    ${res_cumul_lg}=  Ajouter animation à l'élection  ${animation}  ${id_election}
    Set Suite Variable  ${res_cumul_lg}

    &{animation} =  BuiltIn.Create Dictionary
     ...  election=Municipales 2020 1er tour
     ...  libelle=res_sans_cumul_lg
     ...  titre=Aff résultat sans cumul liste gauche
     ...  modele=Résultat sans cumul liste gauche
     ...  perimetre_aff=COMMUNE
     ...  type_aff=Bureau de vote
    ${res_sans_cumul_lg}=  Ajouter animation à l'élection  ${animation}  ${id_election}
    Set Suite Variable  ${res_sans_cumul_lg}

    &{animation} =  BuiltIn.Create Dictionary
     ...  election=Municipales 2020 1er tour
     ...  libelle=Test Participation
     ...  titre=Aff Participation
     ...  modele=Participation
     ...  perimetre_aff=COMMUNE
     ...  type_aff=Bureau de vote
    ${participation}=  Ajouter animation à l'élection  ${animation}  ${id_election}
    Set Suite Variable  ${participation}

    # Calcul du nombre de siège
    Depuis le contexte election  ${id_election}
    Click On Form Portlet Action  election  siege  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer

Manuel d'usage - Ergonomie
    [Documentation]  Section 'Ergonomie'.
    [Tags]  doc

    # Les méthodes Suite Setup et Suite Teardown gèrent l'ouverture et la
    # fermeture du navigateur. Dans le cas de ce TestSuite on a besoin de
    # travailler sur un navigateur fraichement ouvert pour être sûr que la
    # variable de session est neuve.
    Fermer le navigateur
    Ouvrir le navigateur
    Depuis la page de login
    Capture viewport screenshot  screenshots/ergonomie/a_connexion_formulaire.png
    #
    Input Username    admin
    Input Password    plop
    Click Button    login.action.connect
    Wait Until Keyword Succeeds    ${TIMEOUT}    ${RETRY_INTERVAL}    Error Message Should Be    Votre identifiant ou votre mot de passe est incorrect.
    Capture and crop page screenshot  screenshots/ergonomie/a_connexion_message_erreur.png
    ...  css=div.message
    #
    Input Username    admin
    Input Password    admin
    Click Button    login.action.connect
    Wait Until Element Is Visible    css=#actions a.actions-logout
    Capture and crop page screenshot  screenshots/ergonomie/a_connexion_message_ok.png
    ...  css=div.message
    #
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_actions_globales.png
    ...  footer
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_actions_personnelles.png
    ...  actions
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_logo.png
    ...  logo
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_raccourcis.png
    ...  shortlinks
    #
    Highlight heading  css=li.actions-logout
    Capture and crop page screenshot  screenshots/ergonomie/a_deconnexion_action.png
    ...  header
    #
    Go To Dashboard
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_menu.png
    ...  menu
    #
    Go To Dashboard
    Se déconnecter
    Capture and crop page screenshot  screenshots/ergonomie/a_deconnexion_message_ok.png
    ...  css=div.message
    #
    Depuis la page d'accueil  admin  admin
    Go To Dashboard
    Remove element  dashboard
    Update element style  css=#content  height  300px
    Add pointy note  css=#logo  Logo  position=right
    Highlight heading  css=#menu
    Add note  css=#menu  Menu  position=right
    Add pointy note  css=#shortlinks  Raccourcis  position=bottom
    Add pointy note  css=#actions  Actions personnelles  position=left
    Highlight heading  css=#footer
    Add note  css=#footer  Actions globales  position=top
    Capture viewport screenshot  screenshots/ergonomie/a_ergonomie_generale_detail.png
    Depuis le listing des paramètres
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_icone_recherche_possible_sur_cette_colonne.png
    ...  css=span.ui-icon-search
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_exemple_listing_recherche_standard.png
    ...  css=.tab-search
    # Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_icone_pdf_listing.png
    # ...  css=span.print-25
    Depuis le listing des profils
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_icone_ajouter.png
    ...  css=span.add-16
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_icone_visualiser.png
    ...  css=span.consult-16
    Depuis le listing des profils

    Click Link  ADMINISTRATEUR
    # Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_exemple_fiche_visualisation.png
    # ...  content
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_portlet_actions_contextuelles.png
    ...  css=#portlet-actions
    Click On Form Portlet Action  om_profil  modifier
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_exemple_formulaire_modification.png
    ...  content
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_lien_retour.png
    ...  css=a.retour
    Click On Submit Button
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_exemple_formulaire_modification_message_validation.png
    ...  css=div#content div.message
    On clique sur l'onglet  om_utilisateur  utilisateur
    # Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_exemple_onglet_listing.png
    # ...  content
    On clique sur l'onglet  om_droit  droit
    Input Text  css=#recherchedyn  logo
    Sleep  1
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_exemple_onglet_listing_recherche_live.png
    ...  content
    # Recherche avancée
    Depuis le listing  election
    Element Should Be Visible  css=#advanced-form
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_icone_exporter_csv.png
    ...  css=span.csv-25
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_exemple_listing_recherche_avancee.png
    ...  css=#advanced-form
    Click Element  css=#toggle-advanced-display
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_exemple_listing_recherche_simple.png
    ...  css=#advanced-form
    # Fieldset
    Capture and crop page screenshot  screenshots/ergonomie/a_ergonomie_fieldset_closed.png
    ...  css=#toggle-advanced-display

    Go To Dashboard
    :FOR  ${index_widget}  IN RANGE  1  5
    \    Capture and crop page screenshot  screenshots/ergonomie/widget_${index_widget}.png
    \    ...  widget_${index_widget}


parametrage
    [Documentation]  L'objet de ce 'Test Case' est de recuperer les capture d'ecran
    ...    qui concernent la partie parametrage de la documentation.

    [Tags]  doc

    #tableau de bord
    Depuis la page d'accueil  admin  admin
    Remove element
    ...  footer
    Capture page screenshot  screenshots/parametrage/tableau_de_bord.png

    # formulaire d'import
    Depuis le formulaire d'import des unités
    Capture and crop page screenshot  screenshots/parametrage/form_import.png
    ...  content
    Click On Back Button In Import CSV
    Capture and crop page screenshot  screenshots/parametrage/icone_import.png
    ...  css=a#action-tab-unite-corner-import-unites

    # Menu de paramétrage
    Click On Link  css=a.shortlinks-settings
    Capture and crop page screenshot  screenshots/parametrage/menu_parametrage.png
    ...  content

    # formulaire de la collectivité
    Depuis le contexte de la collectivité  LIBREVILLE
    Click On Form Portlet Action  om_collectivite  modifier
    Capture and crop page screenshot  screenshots/parametrage/form_collectivite.png
    ...  content

    # listing des unites et formulaire
    Depuis le listing  unite
    Capture and crop page screenshot  screenshots/parametrage/tab_unite.png
    ...  content

    Depuis le contexte unite  1
    Click On Form Portlet Action  unite  modifier
    Capture and crop page screenshot  screenshots/parametrage/form_unite.png
    ...  content

    # listing des candidats et formulaire
    Depuis le listing  candidat
    Capture and crop page screenshot  screenshots/parametrage/tab_candidat.png
    ...  content

    Depuis le contexte candidat  1
    Click On Form Portlet Action  candidat  modifier
    Capture and crop page screenshot  screenshots/parametrage/form_candidat.png
    ...  content

    # listing des lien_unites et formulaire
    Depuis le contexte unite  7
    Click On Link  lien_unite
    Wait Until Page Contains  unité enfant
    Capture and crop page screenshot  screenshots/parametrage/icone_lien_multiple.png
    ...  css=a#action-soustab-lien_unite-corner-ajouter_multiple

    Click On Link  css=a#action-soustab-lien_unite-corner-ajouter_multiple
    Wait Until Page Contains  unité contenue
    Capture and crop page screenshot  screenshots/parametrage/form_lien_unite_multiple.png
    ...  content

    Depuis le contexte lien_unite  1
    Click On Form Portlet Action  lien_unite  modifier
    Capture and crop page screenshot  screenshots/parametrage/form_lien_unite.png
    ...  content

    Depuis le listing  lien_unite
    Capture and crop page screenshot  screenshots/parametrage/tab_lien_unite.png
    ...  content

    # listing des type_unites et formulaire
    Depuis le listing  type_unite
    Capture and crop page screenshot  screenshots/parametrage/tab_type_unite.png
    ...  content

    Depuis le contexte type_unite  1
    Click On Form Portlet Action  type_unite  modifier
    Capture and crop page screenshot  screenshots/parametrage/form_type_unite.png
    ...  content

    # listing des type_elections et formulaire
    Depuis le listing  type_election
    Capture and crop page screenshot  screenshots/parametrage/tab_type_election.png
    ...  content

    Depuis le contexte type_election  1
    Click On Form Portlet Action  type_election  modifier
    Capture and crop page screenshot  screenshots/parametrage/form_type_election.png
    ...  content

    # listing des tranches et formulaire
    Depuis le listing  tranche
    Capture and crop page screenshot  screenshots/parametrage/tab_tranche.png
    ...  content

    Depuis le contexte tranche  1
    Click On Form Portlet Action  tranche  modifier
    Capture and crop page screenshot  screenshots/parametrage/form_tranche.png
    ...  content

    # listing des cantons et formulaire
    Depuis le listing  canton
    Capture and crop page screenshot  screenshots/parametrage/tab_canton.png
    ...  content

    Click On Add Button
    Capture and crop page screenshot  screenshots/parametrage/form_canton.png
    ...  content


    # listing des communes et formulaire
    Depuis le listing  commune
    Capture and crop page screenshot  screenshots/parametrage/tab_commune.png
    ...  content

    Click On Add Button
    Capture and crop page screenshot  screenshots/parametrage/form_commune.png
    ...  content


    # listing des départements et formulaire
    Depuis le listing  departement
    Capture and crop page screenshot  screenshots/parametrage/tab_departement.png
    ...  content

    Click On Add Button
    Capture and crop page screenshot  screenshots/parametrage/form_departement.png
    ...  content

    # listing des circonscription et formulaire
    Depuis le listing  circonscription
    Capture and crop page screenshot  screenshots/parametrage/tab_circonscription.png
    ...  content

    Click On Add Button
    Capture and crop page screenshot  screenshots/parametrage/form_circonscription.png
    ...  content

    # listing des animation et formulaire
    Depuis le listing  animation
    Capture and crop page screenshot  screenshots/parametrage/tab_animation.png
    ...  content

    Click On Add Button
    Capture and crop page screenshot  screenshots/parametrage/form_animation.png
    ...  content

    # Capture d'écran des affichages pré-configurés
	@{modeles}    Create List    liste  resultat  res_lg  res_sans_cumul_lg  res_cumul_lg  participation

    :FOR  ${modele}  IN  @{modeles}
    \  Depuis le contexte animation de l'election  ${id_election}  ${${modele}}
    \  Click On SubForm Portlet Action  animation  animation  new_window
    \  Go To  http://localhost/openresultat/aff/animation.php?idx=${id_election}&identifier=${${modele}}
    # Délai permettant aux animations des diagrammes de se terminer et donc
    # d'être visible sur le screenshot
    \  Sleep  5
    \  Capture Page screenshot  screenshots/parametrage/${modele}.png

    # listing des modèles web et formulaire
    Depuis le listing  web
    Capture and crop page screenshot  screenshots/parametrage/tab_web.png
    ...  content

    Click On Add Button
    Capture and crop page screenshot  screenshots/parametrage/form_web.png
    ...  content

    # Capture d'écran du formulaire d'ajout des plans, du listing des plans, du listing
    # et du formulaire liant les plans et les unités et de la fenêtre de prévisualisation
    Depuis le listing  plan
    Capture and crop page screenshot  screenshots/parametrage/tab_plan.png
    ...  content

    Click On Add Button
    Capture and crop page screenshot  screenshots/parametrage/form_plan.png
    ...  content

    Depuis le listing  plan_unite
    Capture and crop page screenshot  screenshots/parametrage/tab_plan_unite.png
    ...  content

    Click On Add Button
    Capture and crop page screenshot  screenshots/parametrage/form_plan_unite.png
    ...  content
    Click On Back Button

    Depuis le contexte plan_unite  ${plan_unite1}
    Ouvrir la fenêtre de prévisualisation
    Capture and crop page screenshot  screenshots/parametrage/previsualisation.png
    ...  content

utilisation
    [Documentation]  L'objet de ce 'Test Case' est de recuperer les capture d'ecran
    ...    qui concernent la partie fiche pratique de la documentation.

    [Tags]  doc

    # PARAMETRAGE

    # CE du formulaire election
    Depuis le listing  election
    Capture and crop page screenshot  screenshots/utilisation/tab_election.png
    ...  formulaire

    &{election} =  BuiltIn.Create Dictionary
    ...  libelle=Municipales 2020 1er tour
    ...  code=MUN20-1
    ...  tour=1
    ...  principal=true
    ...  type_election=Municipales
    ...  date=15/03/2020
    ...  perimetre=COMMUNE
    ...  heure_ouverture=08:00:00
    ...  heure_fermeture=11:00:00
    Click On Add Button
    Saisir election  ${election}
    Capture and crop page screenshot  screenshots/utilisation/form_election.png
    ...  formulaire
    Click On Submit Button
    ${id_election2} =  Get Text  css=div.form-content span#election

    Depuis le contexte election  ${id_election2}
    Capture and crop page screenshot  screenshots/utilisation/portlet_parametrage.png
    ...  portlet-actions

    # Capture du formulaire de saisie des candidats de l'élection
    &{candidat} =  BuiltIn.Create Dictionary
    ...  candidat=David G.
    ...  ordre=1
    ...  prefecture=100
    ...  couleur=#ff0000
    ...  photo=candidat1.png
    ${id_candidat1} =  Ajouter election_candidat  ${candidat}  ${id_election2}

    &{candidat} =  BuiltIn.Create Dictionary
    ...  couleur=#0000ff
    ...  candidat=Jean Michel B.
    ...  ordre=2
    ...  photo=candidat2.png
    ...  prefecture=101
    Depuis le listing election_candidat  ${id_election2}
    Click Element  css=a#action-soustab-election_candidat-corner-ajouter

    Capture and crop page screenshot  screenshots/utilisation/form_candidat.png
    ...  formulaire
    # Evite d'avoir le color picker sur la capture d'écran
    Saisir election_candidat  ${candidat}
    Click On Submit Button
    ${id_candidat2} =  Get Text  css=div.form-content span#election_candidat

    # formulaire et tableau de bord des centaines
    &{centaine} =  BuiltIn.Create Dictionary
    ...  libelle=Municipales 2020 1er tour 1ère centaine
    ...  votant_defaut=100
    Depuis le listing centaine  ${id_election2}
    Click On Add Button In SubForm
    Saisir centaine  ${centaine}
    Capture and crop page screenshot  screenshots/utilisation/form_centaine.png
    ...  formulaire
    Click On Submit Button
    ${id_centaine} =  Get Text  css=div#sousform-container div.form-content span#election

    Depuis le contexte centaine  ${id_election2}  ${id_centaine}
    Capture and crop page screenshot  screenshots/utilisation/tab_centaine.png
    ...  css=#sousform-resultat_centaine

    # formulaire des animations de l'élection
    &{animation} =  BuiltIn.Create Dictionary
     ...  election=Municipales 2020 1er tour
     ...  libelle=liste
     ...  titre=Aff liste municipale
     ...  modele=Liste municipale
     ...  perimetre_aff=COMMUNE
     ...  type_aff=Bureau de vote
    Click On Tab  animation  animation(s)
    Click Element  css=a#action-soustab-animation-corner-ajouter
    Saisir animation de l'élection  ${animation}
    Capture and crop page screenshot  screenshots/utilisation/form_animation.png
    ...  formulaire
    Click On Submit Button
    ${id_animation} =  Get Text  css=div.form-content span#animation

    # formulaire d'ajout des plans à l'élection
    Depuis le contexte plan_election  centre  ${id_election2}
    Click On SubForm Portlet Action  plan_election  modifier
    Capture and crop page screenshot  screenshots/utilisation/form_plan_election.png
    ...  formulaire

    Depuis le contexte election  ${id_election2}
    Capture and crop page screenshot  screenshots/utilisation/tb_parametrage.png
    ...  css=div#form-container div#affichage_article

    # formulaire d'import des inscrits
    Depuis le contexte election  ${id_election2}
    Click On Form Portlet Action  election  import_inscrits
    Capture and crop page screenshot  screenshots/utilisation/form_import_inscrit.png
    ...  formulaire

    Depuis le contexte election  ${id_election2}
    Capture and crop page screenshot  screenshots/utilisation/tb_parametrage.png
    ...  css=div#form-container div#affichage_article

    # SIMULATION

    # Passage à l'étape de simulation
    Depuis le contexte election  ${id_election2}
    Passer à l'étape suivante  simulation  Simulation
    Capture and crop page screenshot  screenshots/utilisation/portlet_simulation.png
    ...  portlet-actions
    Capture and crop page screenshot  screenshots/utilisation/tb_simulation.png
    ...  css=div#form-container div#affichage_article

    # Modification de l'élection pour mettre en place la délégation de la participation
    # et récupérer une image du formulaire de saisie de la participation par unité
    &{election} =  BuiltIn.Create Dictionary
    ...  delegation_saisie=true
    ...  delegation_participation=true
    Modifier election  ${id_election2}  ${election}

    &{delegation} =  BuiltIn.Create Dictionary
    ...  acteur=test
    ...  unite=1 - Salle des Mariages
    Ajouter delegation  ${delegation}  ${id_election2}

    # Formulaire de delégation multiple
    Click On Back Button In SubForm
    Click Element  css=a#action-soustab-delegation-corner-ajouter_multiple
    Capture and crop page screenshot  screenshots/utilisation/form_delegation_multiple.png
    ...  formulaire

    # Accés au formulaire en utilisant l'utilisateur test qui a le droit de saisie
    Depuis la page d'accueil  test  test

    Depuis le contexte de la participation par unite  ${id_election2}  1 - Salle des Mariages
    Click On SubForm Portlet Action  delegation_participation  modifier
    Capture and crop page screenshot  screenshots/utilisation/form_saisie_participation_unite.png
    ...  formulaire

    &{election} =  BuiltIn.Create Dictionary
    ...  delegation_saisie=false
    ...  delegation_participation=false
    Modifier election  ${id_election2}  ${election}

    # SAISIE

    # Passage à l'étape de saisie
    Depuis le contexte election  ${id_election2}
    Passer à l'étape suivante  saisie  Saisie
    Capture and crop page screenshot  screenshots/utilisation/portlet_saisie.png
    ...  portlet-actions

    # Capture d'écran du formulaire de saisie des centaines et du tableau de bord des centaines
    Depuis le contexte election_unite  ${id_election2}  1 - Salle des Mariages
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du nombre d'inscrit
    Input Text  inscrit  760
    Click On Submit Button In Subform
    # Saisie de la centaine
    Depuis le contexte centaine  ${id_election2}  ${id_centaine}
    Click On Link  xpath=//td[normalize-space(text()) = "1 - Salle des Mariages"]//ancestor::tr/td[contains(@class, "icons")]/a
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  exprime  99
    Input Text  blanc  1
    Input Text  nul  0
    Input Text  candidat1  51
    Input Text  candidat2  48
    Capture and crop page screenshot  screenshots/utilisation/form_saisie_centaine.png
    ...  formulaire
    Click On Submit Button In Subform
    Click On Link  css=div.ui-dialog .formControls-top a[id^=sousform-action-election_unite-back]
    Capture and crop page screenshot  screenshots/utilisation/tab_centaine.png
    ...  css=#sousform-resultat_centaine

    # Saisie des résultats, capture d'écran du formulaire de saisie et du portlet
    Depuis le contexte election_unite  ${id_election2}  1 - Salle des Mariages
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  760
    Input Text  votant  438
    Input Text  emargement  438
    Input Text  procuration  0
    Input Text  exprime  429
    Input Text  blanc  8
    Input Text  nul  1
    Input Text  candidat1  251
    Input Text  candidat2  178
    Set Checkbox  envoi_web  true
    Set Checkbox  envoi_aff  true
    Capture and crop page screenshot  screenshots/utilisation/form_saisie_resultats.png
    ...  formulaire
    Click On Submit Button In Subform
    Demander la validation des résultats
    Capture and crop page screenshot  screenshots/utilisation/portlet_unites.png
    ...  css=div#sousform-container > div.formEntete > div#portlet-actions
    Click On Back Button In SubForm

    Click On Link   3 - Ecole A
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  919
    Input Text  votant  471
    Input Text  emargement  471
    Input Text  procuration  0
    Input Text  blanc  12
    Input Text  exprime  0
    Input Text  nul  8
    Input Text  candidat1  264
    Input Text  candidat2  187
    Click On Submit Button In SubForm
    Click On Back Button In Subform

    # Saisie de la participation
    Click On Tab  participation_election  participation(s)
    Click On Link  09:00:00
    Click On SubForm Portlet Action  participation_election  modifier
    Input Text  unite1  146
    Input Text  unite2  158
    Input Text  unite3  203
    Input Text  unite5  141
    Input Text  unite6  147
    Input Text  unite4  0
    Capture and crop page screenshot  screenshots/utilisation/form_saisie_participation.png
    ...  formulaire
    Click On Submit Button In Subform
    Capture and crop page screenshot  screenshots/utilisation/portlet_participation.png
    ...  css=div#sousform-container > div.formEntete > div#portlet-actions
    Envoyer la participation à l'affichage  ${id_election2}  09:00:00
    Envoyer la participation à la page web  ${id_election2}  09:00:00

    # tableau de bord de l'élection à l'étape de saisie
    Depuis le contexte election  ${id_election2}
    Capture and crop page screenshot  screenshots/utilisation/tb_saisie.png
    ...  css=div#form-container div#affichage_article

    # FINALISATION

    # tableau de bord et portlet de l'étape de finalisation
    Passer à l'étape suivante  finalisation  Finalisation
    Capture and crop page screenshot  screenshots/utilisation/portlet_finalisation.png
    ...  portlet-actions
    Capture and crop page screenshot  screenshots/utilisation/tb_finalisation.png
    ...  css=div#form-container div#affichage_article

    # CE de la page web
    Go To  http://localhost/openresultat/web/index.php?election=00${id_election2}-0#resultats
    Wait Until Page Contains  Résultat Electoraux Libreville
    Capture Page screenshot  screenshots/utilisation/web.png

    # Affichage du portail web
    Depuis le contexte animation de l'election  ${id_election2}  ${id_animation}
    Capture and crop page screenshot  screenshots/utilisation/portlet_animation.png
    ...  css=div#sousform-container > div.formEntete > div#portlet-actions
    # Calcul du nombre de sièges
    Depuis le contexte election  ${id_election2}
    Click On Form Portlet Action  election  siege  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    Capture and crop page screenshot  screenshots/utilisation/sieges_elu.png
    ...  form-message

    # ARCHIVAGE

    # tableau de bord et portlet de l'étape de finalisation
    Depuis le contexte election  ${id_election2}
    Passer à l'étape suivante  archiver  Archivage
    Capture and crop page screenshot  screenshots/utilisation/portlet_election_archivee.png
    ...  portlet-actions
    Capture and crop page screenshot  screenshots/utilisation/tb_archivage.png
    ...  css=div#form-container div#affichage_article

Fiche
    [Documentation]  L'objet de ce 'Test Case' est de recuperer les capture d'ecran
    ...    qui concernent la partie fiche pratique de la documentation.

    [Tags]  doc

    # capture d'écran du paramétrage d'une unité, du périmètre et du lien entre les deux
    Depuis le contexte unite  1
    Click On Form Portlet Action  unite  modifier
    Capture and crop page screenshot  screenshots/fiche/parametrage_unite.png
    ...  formulaire

    Depuis le contexte unite  7
    Click On Form Portlet Action  unite  modifier
    Capture and crop page screenshot  screenshots/fiche/parametrage_perimetre.png
    ...  formulaire

    # Accès au formulaire d'ajout de liens multiple de l'unité
    Click On Back Button
    Click On Link  lien_unite
    Click On Link  action-soustab-lien_unite-corner-ajouter_multiple
    @{unites} =   Create List  1 - Salle des Mariages  2 - Salle des fetes 1  3 - Ecole A  4 - Salle des fêtes 2  5 - Gymnase  6 - Ecole B
    Select Multiple By Label   unite_enfant  ${unites}
    Capture and crop page screenshot  screenshots/fiche/parametrage_lien.png
    ...  formulaire

    # creation de deux candidat et capture d'écran du parametrage d'un des candidats
    Depuis le contexte candidat  3
    Click On Form Portlet Action  candidat  modifier
    Capture and crop page screenshot  screenshots/fiche/parametrage_candidat.png
    ...  formulaire

    # Paramétrage du plan et positionnement des unités
    # Les unités sont celles définies dans la partie jeu de donné, il n'y
    # a donc pas besoin de tous re-paramétrer
    Depuis le contexte plan  ${plan_id}
    Click On Form Portlet Action  plan  modifier
    Capture and crop page screenshot  screenshots/fiche/parametrage_plan.png
    ...  formulaire

    Depuis le contexte plan_unite  ${plan_unite1}
    Click On Form Portlet Action  plan_unite  modifier
    Capture and crop page screenshot  screenshots/fiche/parametrage_plan_unite.png
    ...  formulaire
    Depuis le contexte plan_unite  ${plan_unite1}
    Ouvrir la fenêtre de prévisualisation
    Capture and crop page screenshot  screenshots/fiche/previsualisation.png
    ...  content

    #screenshot de l'élection
    &{election} =  BuiltIn.Create Dictionary
    ...  libelle=Municipales 2019 1er tour
    ...  code=MUN19-1
    ...  tour=1
    ...  type_election=Municipales
    ...  date=09/06/2019
    ...  perimetre=COMMUNE
    ...  heure_ouverture=08:00:00
    ...  heure_fermeture=18:00:00
    ...  calcul_auto_exprime=true
    ...  publication_auto=true
    Depuis le listing  election
    Click On Add Button
    Saisir election  ${election}
    Capture and crop page screenshot  screenshots/fiche/configuration_election.png
    ...  formulaire
    ${id_election_ex} =  Ajouter election  ${election}

    # Ajout des candidats
    &{candidat} =  BuiltIn.Create Dictionary
    ...  candidat=Hervé SCHIAVETTI
    ...  ordre=1
    ...  prefecture=1
    ...  age_moyen=42
    ...  age_moyen_com=40
    ...  couleur=#0000ff
    ${id_candidat1} =  Ajouter election_candidat  ${candidat}  ${id_election_ex}

    &{candidat} =  BuiltIn.Create Dictionary
    ...  candidat=Pierre CHENEL
    ...  ordre=2
    ...  prefecture=2
    ...  age_moyen=53
    ...  couleur=#ff0000
    ...  age_moyen_com=45
    Depuis le listing election_candidat  ${id_election_ex}
    Click Element  css=a#action-soustab-election_candidat-corner-ajouter
    Saisir election_candidat  ${candidat}
    Capture and crop page screenshot  screenshots/fiche/ajout_candidat.png
    ...  formulaire
    Click On Submit Button

    # Ajout d'une centaine
    &{centaine} =  BuiltIn.Create Dictionary
    ...  libelle=Municipales 2019 1er tour 1ère centaine
    ...  votant_defaut=100
    Depuis le listing centaine  ${id_election_ex}
    Click Element  css=a#action-soustab-centaine-corner-ajouter
    Saisir centaine  ${centaine}
    Capture and crop page screenshot  screenshots/fiche/creation_centaine.png
    ...  formulaire
    Click On Submit Button
    ${id_centaine} =  Get Text  css=div#sousform-container div.form-content span#election

    # Ajout des animations des résultats, de la participation et de la liste municipale
    &{animation} =  BuiltIn.Create Dictionary
     ...  election=Municipales 2019 1er tour
     ...  libelle=resultat
     ...  titre=Résultats
     ...  modele=Résultat
     ...  perimetre_aff=COMMUNE
     ...  type_aff=Bureau de vote
    Depuis le contexte election  ${id_election_ex}
    Click On Tab  animation  animation(s)
    Click Element  css=a#action-soustab-animation-corner-ajouter
    Saisir animation de l'élection  ${animation}
    Capture and crop page screenshot  screenshots/fiche/creation_animation.png
    ...  formulaire
    Click On Submit Button
    ${id_aff_res} =  Get Text  css=div.form-content span#animation

    &{animation} =  BuiltIn.Create Dictionary
     ...  libelle=participation
     ...  titre=Participation
     ...  modele=Participation
     ...  perimetre_aff=COMMUNE
     ...  type_aff=Bureau de vote
    ${id_aff_part} =  Ajouter animation à l'élection  ${animation}  ${id_election_ex}

    &{animation} =  BuiltIn.Create Dictionary
     ...  libelle=liste
     ...  titre=Liste municipale
     ...  modele=Liste municipale
     ...  perimetre_aff=COMMUNE
     ...  type_aff=Bureau de vote
    ${id_aff_liste} =  Ajouter animation à l'élection  ${animation}  ${id_election_ex}

    # Passage à l'étape de saisie
    Depuis le contexte election  ${id_election_ex}
    Passer à l'étape suivante  simulation  Simulation
    Reload Page
    Passer à l'étape suivante  saisie  Saisie

    # Saisie des inscrits
    Depuis le contexte election_unite  ${id_election_ex}  1 - Salle des Mariages
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  760
    Click On Submit Button In Subform
    Click On Back Button In Subform

    Click On Link   2 - Salle des fetes 1
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  742
    Click On Submit Button In Subform
    Click On Back Button In Subform

    Click On Link   3 - Ecole A
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  919
    Click On Submit Button In SubForm
    Click On Back Button In Subform

    Click On Link   4 - Salle des fêtes 2
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  915
    Click On Submit Button In Subform
    Click On Back Button In Subform

    Click On Link   5 - Gymnase
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  761
    Click On Submit Button In Subform
    Click On Back Button In Subform

    Click On Link   6 - Ecole B
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  680
    Click On Submit Button In Subform

    # Saisie de la première centaine
    Depuis le contexte centaine  ${id_election_ex}  ${id_centaine}
    Click On Link  xpath=//td[normalize-space(text()) = "1 - Salle des Mariages"]//ancestor::tr/td[contains(@class, "icons")]/a
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  blanc  8
    Input Text  nul  1
    Input Text  candidat1  67
    Input Text  candidat2  24
    Capture and crop page screenshot  screenshots/fiche/formulaire_saisie_centaine.png
    ...  formulaire
    Click On Submit Button In Subform
    Click On Link  css=div.ui-dialog .formControls-top a[id^=sousform-action-election_unite-back]

    Capture and crop page screenshot  screenshots/fiche/tableau_saisie_centaine.png
    ...  css=#sousform-resultat_centaine

    # Saisie des résultats
    Depuis le contexte election_unite  ${id_election_ex}  1 - Salle des Mariages
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  760
    Input Text  votant  438
    Input Text  emargement  438
    Input Text  procuration  0
    Input Text  blanc  8
    Input Text  nul  1
    Input Text  candidat1  251
    Input Text  candidat2  178
    Capture and crop page screenshot  screenshots/fiche/formulaire_saisie_resultats.png
    ...  formulaire
    Click On Submit Button In Subform
    Click On Back Button In Subform

    Click On Link   2 - Salle des fetes 1
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  742
    Input Text  votant  385
    Input Text  emargement  385
    Input Text  procuration  0
    Input Text  blanc  5
    Input Text  nul  6
    Input Text  candidat1  234
    Input Text  candidat2  140
    Click On Submit Button In Subform
    Click On Back Button In Subform

    Click On Link   3 - Ecole A
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  919
    Input Text  votant  471
    Input Text  emargement  471
    Input Text  procuration  0
    Input Text  blanc  12
    Input Text  nul  8
    Input Text  candidat1  264
    Input Text  candidat2  187
    Click On Submit Button In SubForm
    Click On Back Button In Subform

    Click On Link   4 - Salle des fêtes 2
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  915
    Input Text  votant  462
    Input Text  emargement  462
    Input Text  procuration  0
    Input Text  blanc  7
    Input Text  nul  10
    Input Text  candidat1  300
    Input Text  candidat2  145
    Click On Submit Button In Subform
    Click On Back Button In Subform

    Click On Link   5 - Gymnase
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  761
    Input Text  votant  379
    Input Text  emargement  379
    Input Text  procuration  0
    Input Text  blanc  4
    Input Text  nul  5
    Input Text  candidat1  207
    Input Text  candidat2  163
    Click On Submit Button In Subform
    Click On Back Button In Subform

    Click On Link   6 - Ecole B
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  680
    Input Text  votant  297
    Input Text  emargement  297
    Input Text  procuration  0
    Input Text  blanc  1
    Input Text  nul  3
    Input Text  candidat1  182
    Input Text  candidat2  111
    Click On Submit Button In Subform

    # Saisie de la participation
    Click On Tab  participation_election  participation(s)
    Click On Link  09:00:00
    Click On SubForm Portlet Action  participation_election  modifier
    Input Text  unite1  146
    Input Text  unite2  158
    Input Text  unite3  203
    Input Text  unite4  0
    Input Text  unite5  141
    Input Text  unite6  147
    Capture and crop page screenshot  screenshots/fiche/formulaire_saisie_participation.png
    ...  formulaire
    Click On Submit Button In Subform
    Envoyer la participation à l'affichage  ${id_election_ex}  09:00:00
    Envoyer la participation à la page web  ${id_election_ex}  09:00:00

    Click On Back Button In SubForm
    Click On Link  10:00:00
    Click On SubForm Portlet Action  participation_election  modifier
    Input Text  unite1  146
    Input Text  unite2  90
    Input Text  unite3  126
    Input Text  unite4  199
    Input Text  unite5  128
    Input Text  unite6  47
    Click On Submit Button In Subform
    Envoyer la participation à l'affichage  ${id_election_ex}  10:00:00
    Envoyer la participation à la page web  ${id_election_ex}  10:00:00

    Click On Back Button In SubForm
    Click On Link  11:00:00
    Click On SubForm Portlet Action  participation_election  modifier
    Input Text  unite1  146
    Input Text  unite2  137
    Input Text  unite3  142
    Input Text  unite4  263
    Input Text  unite5  110
    Input Text  unite6  103
    Click On Submit Button In Subform
    Envoyer la participation à l'affichage  ${id_election_ex}  11:00:00
    Envoyer la participation à la page web  ${id_election_ex}  11:00:00

    # Passage à l'étape de finalisation
    Depuis le contexte election  ${id_election_ex}
    Passer à l'étape suivante  finalisation  Finalisation

    # calcul des sièges
    Depuis le contexte election  ${id_election_ex}
    Click On Form Portlet Action  election  siege  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    Capture and crop page screenshot  screenshots/fiche/sieges_elu.png
    ...  form-message

    # Accés aux animations
    Depuis le contexte animation de l'election  ${id_election_ex}  liste
    Click On SubForm Portlet Action  animation  animation  new_window
    Go To  http://localhost/openresultat/aff/animation.php?idx=${id_election_ex}&identifier=${id_aff_liste}
    # Laisse le temps aux animations de se charger
    sleep  5
    Capture Page screenshot  screenshots/fiche/affichage_liste.png

    Depuis le contexte animation de l'election  ${id_election_ex}  resultat
    Click On SubForm Portlet Action  animation  animation  new_window
    Go To  http://localhost/openresultat/aff/animation.php?idx=${id_election_ex}&identifier=${id_aff_res}
    sleep  5
    Capture Page screenshot  screenshots/fiche/affichage_resultats.png

    Depuis le contexte animation de l'election  ${id_election_ex}  participation
    Click On SubForm Portlet Action  animation  animation  new_window
    Go To  http://localhost/openresultat/aff/animation.php?idx=${id_election_ex}&identifier=${id_aff_part}
    sleep  5
    Capture Page screenshot  screenshots/fiche/affichage_participation.png

    # CE de l'affichage web des resultats
    Go To  http://localhost/openresultat/web/index.php?election=00${id_election_ex}-0
    Click Link  css=a[href="#participation"]
    Capture and crop page screenshot  screenshots/fiche/participation_web.png
    ...  css=div#participation

    # Accés au plan et au résultat d'une unité
    Click Link  css=a[href="#resultats"]
    Capture and crop page screenshot  screenshots/fiche/resultats_web.png
    ...  css=div.tab-content

    Click Link  css=a#plan-${plan_id}
    Capture and crop page screenshot  screenshots/fiche/plan_web.png
    ...  css=div.tab-content

    Click Element  css=img[title="Résultats de l'unité 1 Salle des Mariages"]
    Capture and crop page screenshot  screenshots/fiche/affichage_web_u1.png
    ...  css=div.modal-dialog
