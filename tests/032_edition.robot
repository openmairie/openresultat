*** Settings ***
Documentation     Test des editions
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Jeu de données
    [Documentation]  L'objet de ce test case est de creer le jeu de données
    ...  qui servira ensuite à tester le fonctionnement des éditions


    Depuis la page d'accueil    admin  admin

    # Ajout d'une commune et d'un canton ce qui permet de voir si le total enregistré
    # dans les éditions des résultats est correct
    &{unite} =  Create Dictionary
    ...  libelle=COMMUNE 2
    ...  type_unite=Commune
    ...  ordre=12
    Ajouter unite  ${unite}

    &{unite} =  Create Dictionary
    ...  libelle=CANTON
    ...  type_unite=Canton
    ...  ordre=13
    ...  perimetre=true
    ...  type_unite_contenu=Commune
    ...  canton=Exemple
    ...  circonscription=13-53
    ...  commune=Exemple
    ...  departement=Bouches-du-Rhône
    Ajouter unite  ${unite}

    Ajouter lien_unite  CANTON  COMMUNE 2
    Ajouter lien_unite  CANTON  COMMUNE

    &{election} =  BuiltIn.Create Dictionary
    ...  libelle=Municipales 2020 1er tour
    ...  code=MUN20-1
    ...  tour=1
    ...  type_election=Municipales
    ...  date=15/03/2020
    ...  perimetre=CANTON
    ...  heure_ouverture=08:00:00
    ...  heure_fermeture=18:00:00
    ...  calcul_auto_exprime=true
    ${id_election} =  Ajouter election  ${election}
    Set Suite Variable  ${id_election}
    Element Should Contain In Subform  css=div.message  enregistrées

    # Ajout des candidats
    &{candidat} =  BuiltIn.Create Dictionary
    ...  candidat=David G.
    ...  ordre=1
    ...  prefecture=100
    ...  age_moyen=45
    ...  age_moyen_com=46
    ${id_candidat1} =  Ajouter election_candidat  ${candidat}  ${id_election}
    &{candidat} =  BuiltIn.Create Dictionary
    ...  candidat=Jean Michel B.
    ...  ordre=2
    ...  prefecture=101
    ...  age_moyen=38
    ...  age_moyen_com=41
    ${id_candidat2} =  Ajouter election_candidat  ${candidat}  ${id_election}

    # Durant l'étape de simulation, la saisie des résultats, l'accés aux éditions
    # et le paramétrage de l'élection sont possible, c'est donc l'étape la plus pratique
    # pour ce test
    Depuis le contexte election  ${id_election}
    Passer à l'étape suivante  simulation  Simulation

    # Saisie des résultats
    Depuis le contexte election  ${id_election}
    Click On Tab  election_unite  unité(s)
    Click On Link   1 - Salle des Mariages
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  760
    Input Text  votant  438
    Input Text  emargement  438
    Input Text  procuration  0
    Input Text  blanc  8
    Input Text  nul  1
    Input Text  candidat1  251
    Input Text  candidat2  178
    Click On Submit Button In Subform
    La page ne doit pas contenir d'erreur

    # Acces au Bureau B et saisie de ses résultats
    Click On Back Button In Subform
    Click On Link   2 - Salle des fetes 1
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  742
    Input Text  votant  385
    Input Text  emargement  385
    Input Text  procuration  0
    Input Text  blanc  5
    Input Text  nul  6
    Input Text  candidat1  234
    Input Text  candidat2  140
    Click On Submit Button In Subform
    Click On Back Button In Subform

    Click On Link   3 - Ecole A
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  919
    Input Text  votant  471
    Input Text  emargement  471
    Input Text  procuration  0
    Input Text  blanc  12
    Input Text  nul  8
    Input Text  candidat1  264
    Input Text  candidat2  187
    Click On Submit Button In SubForm

    Click On Back Button In Subform
    Click On Link   4 - Salle des fêtes 2
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  915
    Input Text  votant  462
    Input Text  emargement  462
    Input Text  procuration  0
    Input Text  blanc  7
    Input Text  nul  10
    Input Text  candidat1  300
    Input Text  candidat2  145
    Click On Submit Button In Subform
    Click On Back Button In Subform

    Click On Link   5 - Gymnase
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  761
    Input Text  votant  379
    Input Text  emargement  379
    Input Text  procuration  0
    Input Text  blanc  4
    Input Text  nul  5
    Input Text  candidat1  207
    Input Text  candidat2  163
    Click On Submit Button In Subform
    Click On Back Button In Subform

    Click On Link   6 - Ecole B
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  680
    Input Text  votant  297
    Input Text  emargement  297
    Input Text  procuration  0
    Input Text  blanc  1
    Input Text  nul  3
    Input Text  candidat1  182
    Input Text  candidat2  111
    Click On Submit Button In Subform
    Click On Back Button In Subform

    Click On Link   COMMUNE 2
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  2000
    Input Text  emargement  1100
    Input Text  votant  1000
    Input Text  procuration  500
    Input Text  blanc  100
    Input Text  nul  200
    Input Text  candidat1  300
    Input Text  candidat2  400
    Click On Submit Button In Subform
    La page ne doit pas contenir d'erreur

    Depuis le contexte participation_election  ${id_election}  09:00:00
    Click On SubForm Portlet Action  participation_election  modifier
    Input Text  unite1  50
    Input Text  unite2  25
    Input Text  unite3  100
    Input Text  unite4  30
    Input Text  unite5  45
    Input Text  unite6  92
    Click On Submit Button In Subform
    La page ne doit pas contenir d'erreur

    Depuis le contexte participation_election  ${id_election}  10:00:00
    Click On SubForm Portlet Action  participation_election  modifier
    Input Text  unite1  75
    Input Text  unite2  138
    Input Text  unite3  259
    Input Text  unite4  74
    Input Text  unite5  59
    Input Text  unite6  72
    Click On Submit Button In Subform
    La page ne doit pas contenir d'erreur

    Depuis le contexte election  ${id_election}
    Click On Form Portlet Action  election  siege  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer

Proclamation des résultats
    [Documentation]  L'objet de ce test case est de vérifier que la proclamation
    ...  des résultats contiens bien les éléments voulus :
    ...  code, libellé, numéro de tour, date, nom des candidats et résultats

    Depuis le contexte election  ${id_election}

    Click On Form Portlet Action  election  proclamation  new_window
    La page ne doit pas contenir d'erreur
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Open PDF  ${OM_PDF_TITLE}
    La page ne doit pas contenir d'erreur

    # Elément à trouver : code, libellé, tour, circonscription, canton, commune, departement
    # date, candidat, resultat du candidat
    ${contenu_pdf} =  Create List
    ...  RÉSULTATS DÉFINITIFS
    ...  Commune : LIBREVILLE
    ...  Municipales 2020
    ...  1
    ...  15/03/2020
    ...  13-53
    ...  13
    ...  13110
    ...  Inscrits :
    ...  6777
    ...  Abstentions :
    ...  3345
    ...  49,36%
    ...  David G.
    ...  1 738
    ...  56,76%
    ...  Jean Michel B.
    ...  1 324
    ...  43,24%


    La page du fichier PDF doit contenir les chaînes de caractères  ${OM_PDF_TITLE}  ${contenu_pdf}


Proclamation des résultats par bureau
    [Documentation]  L'objet de ce test case est de vérifier que la proclamation
    ...  des résultats par bureau contiens bien les éléments voulus :
    ...  bureau, code, libellé, numéro de tour, date, nom des candidats et résultats

    Depuis le contexte election  ${id_election}

    Click On Form Portlet Action  election  pdf_proclamation_bureau  new_window
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Open PDF  ${OM_PDF_TITLE}
    La page ne doit pas contenir d'erreur

    # Elément à trouver : code, libellé, tour, circonscription, canton, commune, departement
    # date, candidat, resultat du candidat
    ${contenu_pdf} =  Create List
    ...  RÉSULTATS DÉFINITIFS
    ...  Commune : LIBREVILLE
    ...  Municipales 2020
    ...  1
    ...  15/03/2020
    ...  13-53
    ...  13
    ...  13110
    ...  CANTON : 13-24 Exemple
    ...  BUREAU : 1 Salle des Mariages
    ...  Inscrits :
    ...  760
    ...  Abstentions :
    ...  322
    ...  42,37%
    ...  David G.
    ...  251
    ...  58,51%
    ...  Jean Michel B.
    ...  178
    ...  41,49%

    La page du fichier PDF doit contenir les chaînes de caractères  ${OM_PDF_TITLE}  ${contenu_pdf}


Proclamation des résultats repartition des sièges
    [Documentation]  L'objet de ce test case est de vérifier que la proclamation
    ...  des résultats contiens bien les éléments voulus :
    ...  code, libellé, numéro de tour, date, nom des candidats, résultats et nombre de siège

    Depuis le contexte election  ${id_election}

    Click On Form Portlet Action  election  pdf_proclamation_siege  new_window
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Open PDF  ${OM_PDF_TITLE}
    La page ne doit pas contenir d'erreur

    # Elément à trouver : code, libellé, tour, circonscription, canton, commune, departement
    # date, candidat, resultat du candidat
    ${contenu_pdf} =  Create List
    ...  RÉSULTATS DÉFINITIFS
    ...  Commune : LIBREVILLE
    ...  Municipales 2020
    ...  1
    ...  15/03/2020
    ...  13-53
    ...  13
    ...  13110
    ...  Inscrits :
    ...  6777
    ...  Abstentions :
    ...  3345
    ...  49,36%
    ...  1 - C'est pas si mal
    ...  1 738
    ...  56,76%
    ...  24
    ...  1
    ...  2 - Liste 2
    ...  1 324
    ...  43,24%
    ...  5
    ...  0

    La page du fichier PDF doit contenir les chaînes de caractères  ${OM_PDF_TITLE}  ${contenu_pdf}


Proclamation des résultats par périmètre
    [Documentation]  L'objet de ce test case est de vérifier que la proclamation
    ...  des résultats par périmètre contiens bien les éléments voulus :
    ...  code, libellé, numéro de tour, date, nom des candidats et résultats

    Depuis le contexte election  ${id_election}

    Click On Form Portlet Action  election  pdf_proclamation_perimetre  new_window
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Open PDF  ${OM_PDF_TITLE}
    La page ne doit pas contenir d'erreur

    # Elément à trouver : code, libellé, tour, circonscription, canton, commune, departement
    # date, candidat, resultat du candidat
    ${contenu_pdf} =  Create List
    ...  RÉSULTATS DÉFINITIFS
    ...  Commune : LIBREVILLE
    ...  Municipales 2020
    ...  1
    ...  15/03/2020
    ...  13-53
    ...  13
    ...  13110
    ...  PÉRIMÈTRE : CANTON
    ...  Inscrits :
    ...  6777
    ...  Abstentions :
    ...  3345
    ...  49,36%
    ...  David G.
    ...  1 738
    ...  56,76%
    ...  Jean Michel B.
    ...  1 324
    ...  43,24%

    La page du fichier PDF doit contenir les chaînes de caractères  ${OM_PDF_TITLE}  ${contenu_pdf}

Edition des résultats
    [Documentation]  L'objet de ce test case est de vérifier que la proclamation
    ...  des résultats contiens bien les éléments voulus :
    ...  nom des candidats, unité et résultats

    # Accés à l'élection préparamétrée lors de l'initialisation de la base de données
    Depuis le contexte election  ${id_election}

    Click On Form Portlet Action  election  edition  new_window
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Open PDF  ${OM_PDF_TITLE}
    La page ne doit pas contenir d'erreur

    # Liste des eléments à touver : nom election, candidats, nom des unités de saisie
    # nombre total d'inscrit et total des votes enregistrés pour chaque candidat
    ${contenu_pdf} =  Create List
    ...  Municipales 2020 - Tour 1 - 15/03/2020
    ...  David G.
    ...  Jean Michel B.
    ...  1 Salle des Mariage
    ...  2 Salle des fetes 1
    ...  3 Ecole A
    ...  4 Salle des fêtes 2
    ...  5 Gymnase
    ...  6 Ecole B
    ...  4777
    ...  1438
    ...  924
    La page du fichier PDF doit contenir les chaînes de caractères  ${OM_PDF_TITLE}  ${contenu_pdf}

Edition de la participation
    [Documentation]  L'objet de ce test case est de vérifier que l'édition de la participation
    ...  fonctionne sans erreur et affiche les résultats attendus

    # Accés à l'élection préparamétrée lors de l'initialisation de la base de données
    Depuis le contexte election  ${id_election}

    Click On Form Portlet Action  election  pdf_participation  new_window
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Open PDF  ${OM_PDF_TITLE}
    La page ne doit pas contenir d'erreur

    # Liste des eléments à touver : nom des unités, horaires, résultats à 10:00:00 + total + total %
    ${contenu_pdf} =  Create List
    ...  1 Salle des Mariage
    ...  2 Salle des fetes 1
    ...  3 Ecole A
    ...  4 Salle des fêtes 2
    ...  5 Gymnase
    ...  6 Ecole B
    ...  09:00
    ...  10:00
    ...  18:00
    ...  75
    ...  138
    ...  259
    ...  74
    ...  59
    ...  72
    ...  677
    ...  14,17
    La page du fichier PDF doit contenir les chaînes de caractères  ${OM_PDF_TITLE}  ${contenu_pdf}

Edition des résultats par périmètre
    [Documentation]  L'objet de ce test case est de vérifier que l'édition des résultats par
    ...  périmètre fonctionne sans erreur et affiche les résultats attendus

    # Accés à l'élection préparamétrée lors de l'initialisation de la base de données
    Depuis le contexte election  ${id_election}

    Click On Form Portlet Action  election  pdf_resultat_perimetre  new_window
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Open PDF  ${OM_PDF_TITLE}
    La page ne doit pas contenir d'erreur

    # Liste des eléments à touver : liste des noms des candidats, périmètre affiché, nom des unités
    # Le tableau du total, le total d'inscrit et le total de chaque candidat
    ${contenu_pdf} =  Create List
    ...  1-David G.; 2-Jean Michel B.;
    ...  - COMMUNE
    ...  1 Salle des Mariage
    ...  2 Salle des fetes 1
    ...  3 Ecole A
    ...  4 Salle des fêtes 2
    ...  5 Gymnase
    ...  6 Ecole B
    ...  - Total général
    ...  4 777
    ...  1 438
    ...  924
    La page du fichier PDF doit contenir les chaînes de caractères  ${OM_PDF_TITLE}  ${contenu_pdf}

Edition préfecture
    [Documentation]  L'objet de ce test case est de vérifier que l'édition préfecture
    ...  fonctionne sans erreur et affiche les résultats attendus

    # Accés à l'élection préparamétrée lors de l'initialisation de la base de données
    Depuis le contexte election  ${id_election}

    Click On Form Portlet Action  election  pdf_prefecture  new_window
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Open PDF  ${OM_PDF_TITLE}
    La page ne doit pas contenir d'erreur

    # Liste des eléments à touver : nom des colonnes, total inscrits, total des candidats
    ${contenu_pdf} =  Create List
    ...  NO BUREAU VOTE
    ...  INSCRITS
    ...  VOTANTS EMARGEMENT (1)
    ...  ENVELOPPES URNES (2)
    ...  BLANCS
    ...  NULS (3)
    ...  SUFFRAGES EXPRIMES
    ...  David G.
    ...  Jean Michel B.
    ...  4777
    ...  1438
    ...  924
    La page du fichier PDF doit contenir les chaînes de caractères  ${OM_PDF_TITLE}  ${contenu_pdf}


Edition préfecture (résultat par périmètre)
    [Documentation]  L'objet de ce test case est de vérifier que l'édition préfecture
    ...  fonctionne sans erreur et affiche les résultats attendus en utilisant l'état
    ...  prefecture_par_perimetre

    # Activation de l'édition prefecture par périmètre
    Depuis le contexte de l'état  null  prefecture_par_perimetre
    Click On Form Portlet Action  om_etat  activer  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer

    # Accés à l'élection préparamétrée lors de l'initialisation de la base de données
    Depuis le contexte election  ${id_election}

    Click On Form Portlet Action  election  pdf_prefecture  new_window
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Open PDF  ${OM_PDF_TITLE}
    La page ne doit pas contenir d'erreur

    # Liste des eléments à touver : nom des colonnes, total inscrits, total des candidats
    ${contenu_pdf} =  Create List
    ...  COMMUNE
    ...  NO BUREAU VOTE
    ...  INSCRITS
    ...  VOTANTS EMARGEMENT (1)
    ...  ENVELOPPES URNES (2)
    ...  BLANCS
    ...  NULS (3)
    ...  SUFFRAGE EXPRIMES
    ...  David G.
    ...  Jean Michel B.
    ...  4777
    ...  1438
    ...  924
    La page du fichier PDF doit contenir les chaînes de caractères  ${OM_PDF_TITLE}  ${contenu_pdf}  2

Résultats globaux option 2 (option par défaut)
    [Documentation]  L'objet de ce test case est de vérifier que l'édition des résultats globaux
    ...  fonctionne sans erreur et affiche les résultats attendus

    # Accés à l'élection préparamétrée lors de l'initialisation de la base de données
    Depuis le contexte election  ${id_election}

    Click On Form Portlet Action  election  pdf_resultats_globaux  new_window
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Open PDF  ${OM_PDF_TITLE}
    La page ne doit pas contenir d'erreur

    # Liste des eléments à touver : nom de certaines colonnes, nom des unités, total inscrits et par candidat
    ${contenu_pdf} =  Create List
    ...  BUREAUX DE VOTE
    ...  INS
    ...  ABS
    ...  1 Salle des Mariage
    ...  2 Salle des fetes 1
    ...  3 Ecole A
    ...  4 Salle des fêtes 2
    ...  5 Gymnase
    ...  6 Ecole B
    ...  4 777
    ...  1 438
    ...  924
    La page du fichier PDF doit contenir les chaînes de caractères  ${OM_PDF_TITLE}  ${contenu_pdf}

Résultats globaux option 1
    [Documentation]  L'objet de ce test case est de vérifier que l'édition des résultats globaux
    ...  fonctionne sans erreur et affiche les résultats attendus

    # Activation des résultats globaux option 1
    Depuis le contexte de l'état  null  resultats_globaux_opt1
    Click On Form Portlet Action  om_etat  activer  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer

    # Accés à l'élection préparamétrée lors de l'initialisation de la base de données
    Depuis le contexte election  ${id_election}

    Click On Form Portlet Action  election  pdf_resultats_globaux  new_window
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Open PDF  ${OM_PDF_TITLE}
    La page ne doit pas contenir d'erreur

    # Liste des eléments à touver : nom de certaines colonnes, nom des unités, total inscrits et par candidat
    ${contenu_pdf} =  Create List
    ...  BUREAUX DE VOTE
    ...  INS
    ...  ABS
    ...  1 Salle des Mariage
    ...  2 Salle des fetes 1
    ...  3 Ecole A
    ...  4 Salle des fêtes 2
    ...  5 Gymnase
    ...  6 Ecole B
    ...  4 777  #inscrit
    ...  2 432  #votant
    ...  50,91%
    ...  2 432  #emargement
    ...  50,91%
    ...  0     #procuration
    ...  0,00%
    ...  37    #blanc
    ...  1,52%
    ...  33    #nul
    ...  1,36%
    ...  2 362  #exprime
    ...  97,12%
    ...  1 438  #candidat1
    ...  60,88%
    ...  924   #candidat2
    ...  39,12%
    La page du fichier PDF doit contenir les chaînes de caractères  ${OM_PDF_TITLE}  ${contenu_pdf}

Editions sans résultats
    [Documentation]  Même si aucun résultat n'a été saisie les éditions doivent
    ...  fonctionner sans erreur

    # Accés à l'élection préparamétrée lors de l'initialisation de la base de données
    &{election} =  BuiltIn.Create Dictionary
    ...  libelle=Municipales 2024 1er tour
    ...  code=MUN24-1
    ...  tour=1
    ...  type_election=Municipales
    ...  date=15/03/2020
    ...  perimetre=COMMUNE
    ...  heure_ouverture=08:00:00
    ...  heure_fermeture=18:00:00
    ...  calcul_auto_exprime=true
    ${id_election2} =  Ajouter election  ${election}

    &{candidat} =  BuiltIn.Create Dictionary
    ...  candidat=Jean Michel B.
    ...  ordre=2
    ...  prefecture=101
    ...  age_moyen=38
    ...  age_moyen_com=41
    ${id_candidat2} =  Ajouter election_candidat  ${candidat}  ${id_election2}

    Depuis le contexte election  ${id_election2}
    Passer à l'étape suivante  simulation  Simulation
    Click On Form Portlet Action  election  proclamation  new_window
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Open PDF  ${OM_PDF_TITLE}
    La page ne doit pas contenir d'erreur

    Depuis le contexte election  ${id_election2}
    Click On Form Portlet Action  election  edition  new_window
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Open PDF  ${OM_PDF_TITLE}
    La page ne doit pas contenir d'erreur

    Depuis le contexte election  ${id_election2}
    Click On Form Portlet Action  election  pdf_participation  new_window
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Open PDF  ${OM_PDF_TITLE}
    La page ne doit pas contenir d'erreur

    Depuis le contexte election  ${id_election2}
    Click On Form Portlet Action  election  pdf_resultat_perimetre  new_window
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Open PDF  ${OM_PDF_TITLE}
    La page ne doit pas contenir d'erreur

    Depuis le contexte election  ${id_election2}
    Click On Form Portlet Action  election  pdf_prefecture  new_window
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Open PDF  ${OM_PDF_TITLE}
    La page ne doit pas contenir d'erreur

    Depuis le contexte election  ${id_election2}
    Click On Form Portlet Action  election  pdf_resultats_globaux  new_window
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Open PDF  ${OM_PDF_TITLE}
    La page ne doit pas contenir d'erreur
