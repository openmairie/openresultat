-- ------------------------------------------------------------------------------
-- Script d'initialisation du paramétrage.

-- @package openresultat
-- @version SVN : $Id$
-- ------------------------------------------------------------------------------

-- proposition de parametrage
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES
-- heure ouverture et fermeture par defaut  (province)
(nextval('om_parametre_seq'), 'id_default_heure_ouverture', '1', 1),
(nextval('om_parametre_seq'), 'id_default_heure_fermeture', '11', 1),
-- nombre de sieges municipale et communautaire
(nextval('om_parametre_seq'), 'nb_sieges_default', '29', 1),
(nextval('om_parametre_seq'), 'nb_sieges_com_default', '1', 1),
-- nombre d'unite a afficher dans la liste election_unite (onglet election)
(nextval('om_parametre_seq'), 'nombre_unite', '35', 1);

--
INSERT INTO type_unite (type_unite, libelle, hierarchie, bureau_vote) VALUES
(nextval('type_unite_seq'), 'Bureau de vote', 1, TRUE),
(nextval('type_unite_seq'), 'Groupe de bureau', 2, FALSE),
(nextval('type_unite_seq'), 'Mairie', 3, FALSE),
(nextval('type_unite_seq'), 'Groupe de Mairie', 4, FALSE),
(nextval('type_unite_seq'), 'Arrondissement', 5, FALSE),
(nextval('type_unite_seq'), 'Circonscription', 6, FALSE),
(nextval('type_unite_seq'), 'Commune', 7, FALSE),
(nextval('type_unite_seq'), 'Groupe de Commune', 8, FALSE),
(nextval('type_unite_seq'), 'Canton', 9, FALSE),
(nextval('type_unite_seq'), 'Groupe de Canton', 10, FALSE),
(nextval('type_unite_seq'), 'Departement', 11, FALSE),
(nextval('type_unite_seq'), 'Groupe de Departement', 12, FALSE),
(nextval('type_unite_seq'), 'Region', 13, FALSE),
(nextval('type_unite_seq'), 'Groupe de Region', 14, FALSE),
(nextval('type_unite_seq'), 'Election', 15, FALSE);

--
INSERT INTO tranche (tranche, libelle, ordre) VALUES
(nextval('tranche_seq'),  '08:00:00',1),
(nextval('tranche_seq'),  '09:00:00',2),
(nextval('tranche_seq'),  '10:00:00',3),
(nextval('tranche_seq'),  '11:00:00',4),
(nextval('tranche_seq'),  '12:00:00',5),
(nextval('tranche_seq'),  '13:00:00',6),
(nextval('tranche_seq'),  '14:00:00',7),
(nextval('tranche_seq'),  '15:00:00',8),
(nextval('tranche_seq'),  '16:00:00',9),
(nextval('tranche_seq'),  '17:00:00',10),
(nextval('tranche_seq'),  '18:00:00',11),
(nextval('tranche_seq'),  '19:00:00',12),
(nextval('tranche_seq'),  '20:00:00',13),
(nextval('tranche_seq'),  '21:00:00',14),
(nextval('tranche_seq'),  '22:00:00',15);

--
INSERT INTO type_election (type_election, code, libelle, prefecture) VALUES
(nextval('type_election_seq'), 'CAN',  'Cantonales','CN'),
(nextval('type_election_seq'), 'DEP',  'Departementale', 'DP'),
(nextval('type_election_seq'), 'EUR',  'Europeenes', 'ER'),
(nextval('type_election_seq'), 'LEG',  'Legislatives', 'LG'),
(nextval('type_election_seq'), 'MUN',  'Municipales', 'MN'),
(nextval('type_election_seq'), 'PRE',  'Presidentielles', 'PR'),
(nextval('type_election_seq'), 'REF',  'Referendum', 'RF'),
(nextval('type_election_seq'), 'REG',  'Regionales', 'RG2'),
(nextval('type_election_seq'), 'MEP',  'Metropolitaines', 'MP');


-- BEGIN /  [#9433] Personnalisation de l'animation

-- Permettre la personnalisation des animations.


INSERT INTO animation (animation, libelle, refresh, is_modele,
    sous_titre, couleur_titre, couleur_bandeau,
    affichage,
    is_config, actif) VALUES
(nextval('animation_seq'), 'Résultat', 60, TRUE,
    'Résultats provisoires - nb_unite_arrivees unitées arrivées sur nb_unite', '#ffffff', '#000000',
'[sidebar]
position=droite
cliquable=non
taille=3
[bloc1]
type=resultat_global
taille=5
photo=oui
[bloc2]
type=resultat_unite
diagramme=camembert
taille=4',
    TRUE, TRUE
),
(nextval('animation_seq'), 'Participation', 60, TRUE,
    'Participation', '#ffffff', '#000000',
'[sidebar]
position=droite
cliquable=oui
taille=3
[bloc1]
type=participation_globale
taille=5
[bloc2]
type=participation_unite
diagramme=camembert
taille=4',
    TRUE, TRUE
),
(nextval('animation_seq'), 'Liste municipale', 60, TRUE,
    'Résultats provisoires - nb_unite_total_arrivees unitées arrivées sur nb_unite_total', '#ffffff', '#000000',
'[bloc1]
type=resultat_global
taille=7
photo=oui
[bloc2]
type=sieges_municipal
taille=5
diagramme=donut
[bloc3]
type=sieges_communautaire
taille=5
diagramme=donut',
    TRUE, TRUE
),
(nextval('animation_seq'), 'Résultat liste gauche', 60, TRUE,
    'Résultats provisoires - nb_unite_arrivees unitées arrivées sur nb_unite', '#ffffff', '#000000',
'[sidebar]
position=gauche
cliquable=non
taille=3
[bloc1]
type=resultat_unite
diagramme=camembert
taille=4
offset=3
[bloc2]
type=resultat_global
taille=5
photo=oui',
    TRUE, TRUE
),
(nextval('animation_seq'), 'Résultat sans cumul liste gauche', 60, TRUE,
    'Résultats provisoires - nb_unite_arrivees unitées arrivées sur nb_unite', '#ffffff', '#000000',
'[sidebar]
position=gauche
cliquable=non
taille=3
[bloc1]
type=resultat_unite
taille=9
offset=3
diagramme=camembert',
    TRUE, TRUE
),
(nextval('animation_seq'), 'Résultat avec graphique cumul liste gauche', 60, TRUE,
    'Résultats provisoires - nb_unite_arrivees unitées arrivées sur nb_unite', '#ffffff', '#000000',
'[sidebar]
position=gauche
cliquable=non
taille=3
[bloc1]
type=resultat_unite
taille=4
offset=3
diagramme=camembert
[bloc2]
type=resultat_global
taille=5
photo=oui
diagramme=camembert',
    TRUE, TRUE
);

-- END /  [#9433] Personnalisation de l'animation

INSERT INTO web (web, libelle) VALUES (nextval('web_seq'), 'Défaut');
