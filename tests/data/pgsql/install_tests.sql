--------------------------------------------------------------------------------
-- Script d'installation des jeux de données pour les tests
--
-- ATTENTION ce script est prévu pour être appliqué après le install.sql
--
-- @package openresultat
-- @version SVN : $Id$
--------------------------------------------------------------------------------
--
START TRANSACTION;
\set ON_ERROR_STOP on
--
\set schema 'openresultat'
--
SET search_path = :schema, public, pg_catalog;
--
\i 'tests/data/pgsql/init_parametrage.sql'
\i 'tests/data/pgsql/init_parametrage_editions.sql'
\i 'tests/data/pgsql/init_data.sql'
\i 'tests/data/pgsql/init_data_exemple_election_mun14.sql'
--
COMMIT;

