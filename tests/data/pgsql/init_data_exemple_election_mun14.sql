-- 19 décembre 2019 fr-arles - data 
-- contient les résultats de l'élection municipale d'Arles 2eme tour
-- utilisés pour le TEST en "réel" de la version 2.0.0b1
-- election sur 33 unitex en 2014 et non 35 en 2019 (unitex 34 et 35 sans résultats)

-- SET search_path = openresultat, public, pg_catalog;



-- Data for Name: candidat; Type: TABLE DATA; Schema: openresultat; Owner: francois

INSERT INTO candidat (candidat, libelle, libelle_liste) VALUES (3, 'Pierre CHENEL', 'Arles bleu marine');
INSERT INTO candidat (candidat, libelle, libelle_liste) VALUES (4, 'Hervé SCHIAVETTI', 'Pour Arles');
INSERT INTO candidat (candidat, libelle, libelle_liste) VALUES (5, 'Cyril JUGLARET', 'Les Républicains - UDI');
SELECT pg_catalog.setval('candidat_seq', 6, false);
