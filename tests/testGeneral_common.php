<?php
/**
 * Ce script contient la définition de la classe 'GeneralCommon'.
 *
 * @package openresultat
 * @version SVN : $Id$
 */

require_once "../tests/resources/omtestcase.class.php";

/**
 * Cette classe permet de tester unitairement les fonctions de l'application.
 */
abstract class GeneralCommon extends OMTestCase {
    /**
     * Méthode lancée en fin de traitement
     */
    public function common_tearDown() {
        parent::common_tearDown();
        //
        $this->clean_session();
    }
    /**
     * Test template
     */
    public function test_om_application__authenticated() {
        // Instanciation de la classe *om_application*
        $f = $this->get_inst_om_application("admin", "admin");
        $f->disableLog();
        // Test
        $this->assertEquals($f->authenticated, true);
        // Destruction de la classe *om_application*
        $f->__destruct();
    }
}
