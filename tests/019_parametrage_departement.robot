*** Settings ***
Documentation     Test du parametrage des départements
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Constitution du jeu de données
    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.
    Depuis la page d'accueil  admin  admin


Listing des départements
    [Documentation]  Ce 'Test Case' vérifie l'intégration du listing des
    ...  enregistrements de type 'département'.
    # le lien est présent dans le menu
    Depuis l'écran 'Administration & Paramétrage'
    Element Should Contain  css=#settings a.departement  département
    # le lien amène sur la page correcte
    Click Element  css=#settings a.departement
    Page Title Should Be  Administration & Paramétrage > Découpage Administratif > Département
    La page ne doit pas contenir d'erreur
    # le mot clé raccourci amène sur la page correcte
    Depuis le listing des départements
    Page Title Should Be  Administration & Paramétrage > Découpage Administratif > Département


Ajout d'un département
    [Documentation]  Ce 'Test Case' vérifie l'intégration de l'action 'ajouter'
    ...  depuis le contexte 'département'.

    Depuis le listing des départements
    Click On Add Button
    Page Title Should Be  Administration & Paramétrage > Découpage Administratif > Département
    La page ne doit pas contenir d'erreur

    # creation du departement
    &{departement} =  BuiltIn.Create Dictionary
    ...  libelle=mon departement
    ...  code=DEP
    ...  prefecture=42
    Ajouter departement  ${departement}
    La page ne doit pas contenir d'erreur
    # message de reussite
    Element Should Contain In Subform  css=div.message  enregistrées