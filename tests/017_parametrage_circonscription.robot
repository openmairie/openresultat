*** Settings ***
Documentation     Test du parametrage des circonscriptions
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Constitution du jeu de données
    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.
    Depuis la page d'accueil  admin  admin


Listing des circonscriptions
    [Documentation]  Ce 'Test Case' vérifie l'intégration du listing des
    ...  enregistrements de type 'circonscription'.
    # le lien est présent dans le menu
    Depuis l'écran 'Administration & Paramétrage'
    Element Should Contain  css=#settings a.circonscription  circonscription
    # le lien amène sur la page correcte
    Click Element  css=#settings a.circonscription
    Page Title Should Be  Administration & Paramétrage > Découpage Administratif > Circonscription
    La page ne doit pas contenir d'erreur
    # le mot clé raccourci amène sur la page correcte
    Depuis le listing des circonscriptions
    Page Title Should Be  Administration & Paramétrage > Découpage Administratif > Circonscription


Ajout d'une circonscription
    [Documentation]  Ce 'Test Case' vérifie l'intégration de l'action 'ajouter'
    ...  depuis le contexte 'circonscription'.

    Depuis le listing des circonscriptions
    Click On Add Button
    Page Title Should Be  Administration & Paramétrage > Découpage Administratif > Circonscription
    La page ne doit pas contenir d'erreur

    # creation d'une circonscription
    &{circonscription} =  BuiltIn.Create Dictionary
    ...  libelle=ma circonscription
    ...  code=CIRC
    ...  prefecture=42
    Ajouter circonscription  ${circonscription}
    La page ne doit pas contenir d'erreur
    # message de reussite
    Element Should Contain In Subform  css=div.message  enregistrées