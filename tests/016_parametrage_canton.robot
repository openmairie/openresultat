*** Settings ***
Documentation     Test du parametrage des cantons
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Constitution du jeu de données
    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.
    Depuis la page d'accueil  admin  admin


Listing des cantons
    [Documentation]  Ce 'Test Case' vérifie l'intégration du listing des
    ...  enregistrements de type 'canton'.
    # le lien est présent dans le menu
    Depuis l'écran 'Administration & Paramétrage'
    Element Should Contain  css=#settings a.canton  canton
    # le lien amène sur la page correcte
    Click Element  css=#settings a.canton
    Page Title Should Be  Administration & Paramétrage > Découpage Administratif > Canton
    La page ne doit pas contenir d'erreur
    # le mot clé raccourci amène sur la page correcte
    Depuis le listing des cantons
    Page Title Should Be  Administration & Paramétrage > Découpage Administratif > Canton


Ajout d'un canton
    [Documentation]  Ce 'Test Case' vérifie l'intégration de l'action 'ajouter'
    ...  depuis le contexte 'canton'.

    Depuis le listing des cantons
    Click On Add Button
    Page Title Should Be  Administration & Paramétrage > Découpage Administratif > Canton
    La page ne doit pas contenir d'erreur

    &{canton} =  BuiltIn.Create Dictionary
    ...  libelle=mon canton
    ...  code=CAN
    ...  prefecture=42
    Ajouter canton  ${canton}
    La page ne doit pas contenir d'erreur
    # message de reussite
    Element Should Contain In Subform  css=div.message  enregistrées

