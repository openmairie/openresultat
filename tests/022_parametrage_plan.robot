*** Settings ***
Documentation     Test du parametrage des plans
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Constitution du jeu de données
    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.
    Depuis la page d'accueil  admin  admin


Listing des plans
    [Documentation]  Ce 'Test Case' vérifie l'intégration du listing des
    ...  enregistrements de type 'plan'.
    # le lien est présent dans le menu
    Depuis l'écran 'Administration & Paramétrage'
    Element Should Contain  css=#settings a.plan  plan
    # le lien amène sur la page correcte
    Click Element  css=#settings a.plan
    Page Title Should Be  Administration & Paramétrage > Affichage > Plan
    La page ne doit pas contenir d'erreur
    # le mot clé raccourci amène sur la page correcte
    Depuis le listing des plans
    Page Title Should Be  Administration & Paramétrage > Affichage > Plan


Ajout d'un plan
    [Documentation]  Ce 'Test Case' vérifie l'intégration de l'action 'ajouter'
    ...  depuis le contexte 'plan'.

    Depuis le listing des plans
    Click On Add Button
    Page Title Should Be  Administration & Paramétrage > Affichage > Plan
    La page ne doit pas contenir d'erreur

    # Test de l'ajout d'un plan
    &{plan} =  BuiltIn.Create Dictionary
    ...  libelle=monPlan
    ...  image_plan=plan_centre.gif
    ...  img_unite_arrivee=bureau_a.png
    ...  img_unite_non_arrivee=bureau_na.png
    Ajouter plan  ${plan}
    Element Should Contain In Subform  css=div.message  enregistrées


Placement des unités sur le plan
    [Documentation]  L'objet de ce test case est de tester le formulaire
    ...  de liaison des plans aux unités et le placement des points sur
    ...  le plan

    &{unite_test} =  BuiltIn.Create Dictionary
    ...  libelle=testPlan
    ...  type_unite=Bureau de vote
    ...  ordre=41
    ...  code_unite=41
    Ajouter unite  ${unite_test}

    Depuis le listing  plan_unite

    # Test de l'ajout d'un lien plan_unite
    ${x} =  Set Variable  100
    ${y} =  Set Variable  150
    &{plan} =  BuiltIn.Create Dictionary
    ...  plan=monPlan
    ...  unite=41 - testPlan
    ...  position_x=${x}
    ...  position_y=${y}
    ${plan_unite_id} =  Ajouter plan unite  ${plan}
    Element Should Contain In Subform  css=div.message  enregistrées

    # Test de la prévisualisation
    Depuis le contexte plan_unite  ${plan_unite_id}
    Ouvrir la fenêtre de prévisualisation
    La page ne doit pas contenir d'erreur
    # Ajouter 1 car il y a un décalage de 1 pixel
    Verifier la position de l'icone  ${${x} + 1}  ${${y} + 1}  css=div#localisation-wrapper  css=div#camap0

    # Passage de l'unité utilisée dans ce test en fin de validité
    Passer l'unité en fin de validité  41

