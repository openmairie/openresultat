*** Settings ***
Documentation     Test des exports csv
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Jeu de données
    [Documentation]  L'objet de ce test case est de creer le jeu de données
    ...  qui servira ensuite à tester le fonctionnement des exports


    Depuis la page d'accueil    admin  admin

    &{election} =  BuiltIn.Create Dictionary
    ...  libelle=Municipales 2020 1er tour
    ...  code=MUN20-1
    ...  tour=1
    ...  principal=true
    ...  type_election=Municipales
    ...  date=15/03/2020
    ...  perimetre=COMMUNE
    ...  heure_ouverture=08:00:00
    ...  heure_fermeture=18:00:00
    ...  calcul_auto_exprime=true
    ${id_election} =  Ajouter election  ${election}
    Set Suite Variable  ${id_election}
    Element Should Contain In Subform  css=div.message  enregistrées

    # Ajout des candidats
    &{candidat} =  BuiltIn.Create Dictionary
    ...  candidat=David G.
    ...  ordre=1
    ...  prefecture=1
    ...  age_moyen=45
    ...  age_moyen_com=46
    ${id_candidat1} =  Ajouter election_candidat  ${candidat}  ${id_election}
    &{candidat} =  BuiltIn.Create Dictionary
    ...  candidat=Jean Michel B.
    ...  ordre=2
    ...  prefecture=2
    ...  age_moyen=38
    ...  age_moyen_com=41
    ${id_candidat2} =  Ajouter election_candidat  ${candidat}  ${id_election}

    # Passage à l'étape de simulation pour saisir les résultats
    Depuis le contexte election  ${id_election}
    Passer à l'étape suivante  simulation  Simulation

    # Saisie des résultats
    Depuis le contexte election  ${id_election}
    Click On Tab  election_unite  unité(s)
    Click On Link   1 - Salle des Mariages
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  760
    Input Text  votant  438
    Input Text  emargement  438
    Input Text  procuration  0
    Input Text  blanc  8
    Input Text  nul  1
    Input Text  candidat1  251
    Input Text  candidat2  178
    Click On Submit Button In Subform
    Click On Back Button In Subform

    Click On Link   2 - Salle des fetes 1
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  742
    Input Text  votant  385
    Input Text  emargement  385
    Input Text  procuration  0
    Input Text  blanc  5
    Input Text  nul  6
    Input Text  candidat1  234
    Input Text  candidat2  140
    Click On Submit Button In Subform
    Click On Back Button In Subform

    Click On Link   3 - Ecole A
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  919
    Input Text  votant  471
    Input Text  emargement  471
    Input Text  procuration  0
    Input Text  blanc  12
    Input Text  nul  8
    Input Text  candidat1  264
    Input Text  candidat2  187
    Click On Submit Button In SubForm
    Click On Back Button In Subform

    Click On Link   4 - Salle des fêtes 2
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  915
    Input Text  votant  462
    Input Text  emargement  462
    Input Text  procuration  0
    Input Text  blanc  7
    Input Text  nul  10
    Input Text  candidat1  300
    Input Text  candidat2  145
    Click On Submit Button In Subform
    Click On Back Button In Subform

    Click On Link   5 - Gymnase
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  761
    Input Text  votant  379
    Input Text  emargement  379
    Input Text  procuration  0
    Input Text  blanc  4
    Input Text  nul  5
    Input Text  candidat1  207
    Input Text  candidat2  163
    Click On Submit Button In Subform
    Click On Back Button In Subform

    Click On Link   6 - Ecole B
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  680
    Input Text  votant  297
    Input Text  emargement  297
    Input Text  procuration  0
    Input Text  blanc  1
    Input Text  nul  3
    Input Text  candidat1  182
    Input Text  candidat2  111
    Click On Submit Button In Subform

Extraction en CSV
    [Documentation]  L'objet de ce test case est de vérifier que le fichier
    ...  csv généré par l'action d'extraction en csv est correctement 
    ...  construit en le comparant à un fichier modèle

    Depuis le contexte election  ${id_election}
    # On clique sur l'action
    Click On Form Portlet Action  election  extraction  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    # Télécharge le fichier d'export CSV sur le disque
    ${link} =  Get Element Attribute  css=div.message a   href
    ${content_file} =  Contenu CSV  ${link}
    # On vérifie dans le fichier téléchargé que l'entête correspond à ce qui est attendu
    ${header_csv_file} =  Set Variable  election;code;unite;inscrit;votant;emargement;procuration;exprime;nul;blanc;David G.;Jean Michel B.;
    Should Contain  ${content_file}  ${header_csv_file}
    # On vérifie dans le fichier téléchargé que la première ligne correspond à ce qui est attendu
    ${firstline_csv_file} =  Set Variable  ${id_election};1;Salle des Mariages;760;438;438;0;429;1;8;251;178
    Should Contain  ${content_file}  ${firstline_csv_file}

Envoi à la préfecture
    [Documentation]  L'objet de ce test case est de vérifier que le fichier
    ...  csv généré pour l'envoi à la préfecture correctement 
    ...  construit en le comparant à un fichier modèle

    # Utilisation de l'election pré-paramétrée pour les tests
    Depuis le contexte election  ${id_election}
    # On clique sur l'action
    Click On Form Portlet Action  election  prefecture
    # Vérifie le contenu de la page
    # Dans le cas testé tous les éléments ont été paramétré. Il ne doit pas
    # rester de bureaux à paramétrer et les différents codes doivent être
    # visible sur le recapitulatif
    Page Should Not Contain  Bureaux à paramétrer
    Element Should Contain  css=ul#parametrage-candidats  David G. : 1
    Element Should Contain  css=ul#parametrage-candidats  Jean Michel B. : 2
    Element Should Contain  css=ul#parametrage-bureaux  Code département : 13
    Element Should Contain  css=ul#parametrage-bureaux  Code commune : 110
    Element Should Contain  css=ul#parametrage-bureaux  Code canton : 24
    Element Should Contain  css=ul#parametrage-bureaux  Code circonscription : 53
    # On clique sur le bouton de transfert
    Click On Submit Button
    # Télécharge le fichier d'export CSV sur le disque
    ${link} =  Get Element Attribute  css=div.message a   href
    ${content_file} =  Contenu CSV  ${link}
    # On vérifie dans le fichier téléchargé que la première ligne correspond à ce qui est attendu
    ${header_csv_file} =  Set Variable    1;MN;2020;V;1;13;110;0001;I;760;322;438;438;8;1;429;2;001;251;002;178
    Should Contain  ${content_file}  ${header_csv_file}

Envoi à la préfecture (régionales)
    [Documentation]  L'objet de ce test case est de vérifier que le fichier
    ...  csv généré pour l'envoi à la préfecture correctement 
    ...  construit en le comparant à un fichier modèle

    # Utilisation de l'election pré-paramétrée pour les tests
    &{election} =  BuiltIn.Create Dictionary
    ...  type_election=Regionales
    Modifier election  ${id_election}  ${election}
    Depuis le contexte election  ${id_election}
    # On clique sur l'action puis sur le bouton de génération de l'export
    Click On Form Portlet Action  election  prefecture
    Click On Submit Button
    # Télécharge le fichier d'export CSV sur le disque
    ${link} =  Get Element Attribute  css=div.message a   href
    ${content_file} =  Contenu CSV  ${link}
    # On vérifie dans le fichier téléchargé que la première ligne correspond à ce qui est attendu
    ${header_csv_file} =  Set Variable    1;RG2;2020;1;13;110;0001;24;53;I;760;322;438;438;8;1;429;2;001;251;002;178
    Should Contain  ${content_file}  ${header_csv_file}

Envoi à la préfecture (metropolitain)
    [Documentation]  L'objet de ce test case est de vérifier que le fichier
    ...  csv généré pour l'envoi à la préfecture est correctement 
    ...  construit en le comparant à un fichier modèle, pour le cas d'une
    ...  election metropolitaine

    # Utilisation de l'election pré-paramétrée pour les tests

    &{election} =  BuiltIn.Create Dictionary
    ...  type_election=Metropolitaines
    Modifier election  ${id_election}  ${election}
    Depuis le contexte election  ${id_election}
    # On clique sur l'action puis sur le bouton de génération de l'export
    Click On Form Portlet Action  election  prefecture
    Click On Submit Button
    # Télécharge le fichier d'export CSV sur le disque
    ${link} =  Get Element Attribute  css=div.message a   href
    ${content_file} =  Contenu CSV  ${link}
    # On vérifie dans le fichier téléchargé que la première ligne correspond à ce qui est attendu
    ${header_csv_file} =  Set Variable    1;MP;2020;1;13;110;0001;I;760;322;438;438;8;1;429;2;001;251;002;178
    Should Contain  ${content_file}  ${header_csv_file}

Détail des résultats
    [Documentation]  L'objet de ce test case est de vérifier que le fichier
    ...  csv du détail des résultats est correctement construit en le comparant 
    ...  à un fichier modèle
    
    # Detail des résultats
    Depuis le listing  election_resultat
    # Télécharge le fichier d'export CSV sur le disque
    ${link} =  Get Element Attribute  css=a[title="Export csv"]   href
    ${content_file} =  Contenu CSV  ${link}
    # On vérifie dans le fichier téléchargé que l'entête correspond à ce qui est attendu
    ${header_csv_file} =  Set Variable  id;élection;candidat;unité;résultat
    Should Contain  ${content_file}  ${header_csv_file}
    # On vérifie dans le fichier téléchargé que la première ligne correspond à ce qui est attendu
    ${firstline_csv_file} =  Set Variable  ;"Municipales 2020 1er tour";"David G.";"1 - Salle des Mariages";251
    Should Contain  ${content_file}  ${firstline_csv_file}

Envoi à la préfecture (présidentielles)
    [Documentation]  L'objet de ce test case est de vérifier que le fichier
    ...  csv généré pour l'envoi à la préfecture des présidentielles est correctement 
    ...  construit en le comparant à un fichier modèle

    # Utilisation de l'election pré-paramétrée pour les tests
    &{election} =  BuiltIn.Create Dictionary
    ...  type_election=Presidentielles
    Modifier election  ${id_election}  ${election}
    Depuis le contexte election  ${id_election}
    # On clique sur l'action puis sur le bouton de génération de l'export
    Click On Form Portlet Action  election  prefecture
    Click On Submit Button
    # Télécharge le fichier d'export CSV sur le disque
    ${link} =  Get Element Attribute  css=div.message a   href
    ${content_file} =  Contenu CSV  ${link}
    # On vérifie dans le fichier téléchargé que la première ligne correspond à ce qui est attendu
    ${header_csv_file} =  Set Variable    1;PR;2020;1;13;110;0001;24;53;I;760;322;438;438;8;1;429;2;0001;251;0002;178
    Should Contain  ${content_file}  ${header_csv_file}
