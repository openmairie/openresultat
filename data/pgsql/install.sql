--------------------------------------------------------------------------------
-- Script d'installation
--
-- ATTENTION ce script peut supprimer des données de votre base de données
-- il n'est à utiliser qu'en connaissance de cause
--
-- Usage :
-- cd data/pgsql/
-- dropdb openresultat && createdb openresultat && psql openresultat -f install.sql
--
-- @package openresultat
-- @version SVN : $Id$
--------------------------------------------------------------------------------

-- Force l'encoding client à UTF8
SET client_encoding = 'UTF8';

--
START TRANSACTION;
\set ON_ERROR_STOP on

-- Initialisation de postgis
CREATE EXTENSION IF NOT EXISTS postgis;

-- Suppression, Création et Utilisation du schéma
\set schema 'openresultat'
DROP SCHEMA IF EXISTS :schema CASCADE;
CREATE SCHEMA :schema;
SET search_path = :schema, public, pg_catalog;

-- Instructions de base du framework openmairie
\i ../../core/data/pgsql/init.sql

-- Instructions de base de l'applicatif
\i init_metier.sql

-- Initialisation du paramétrage
\i init_permissions.sql
\i ../../core/data/pgsql/init_parametrage.sql
\i init_sequences.sql

-- Mise à jour depuis la dernière version (en cours de développement)
\i v2.2.2.dev0.sql
\i v2.2.2.dev0.init_data.sql



--
COMMIT;

