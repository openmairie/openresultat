-------------------------------------------------------------------------------
-- Script de mise à jour vers la version v2.2.0 depuis la version v2.1.0
--
-- @package openresultat
-- @version SVN : $Id$
-------------------------------------------------------------------------------

--
-- BEGIN /  [#9494] 
--
ALTER TABLE
    election
ADD
    COLUMN export_emargement_prefecture BOOLEAN DEFAULT TRUE;

-- ON fait un update des valeurs de la nouvelle colonne pour s'assurer d'avoir
-- une valeur pour cette colonne pour chaque election enregistrée.
UPDATE election SET export_emargement_prefecture = TRUE;

COMMENT ON COLUMN election.export_emargement_prefecture IS 'Option permettant d''afficher la colonne contenant le nombre d''émargement sur l''export préfecture';
--
-- END /  [#9494] 
--
