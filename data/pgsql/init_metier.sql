--
-- PostgreSQL database dump
--

-- Dumped from database version 12.13 (Ubuntu 12.13-1.pgdg20.04+1)
-- Dumped by pg_dump version 12.13 (Ubuntu 12.13-1.pgdg20.04+1)

-- SET statement_timeout = 0;
-- SET lock_timeout = 0;
-- SET idle_in_transaction_session_timeout = 0;
-- SET client_encoding = 'UTF8';
-- SET standard_conforming_strings = on;
-- SELECT pg_catalog.set_config('search_path', '', false);
-- SET check_function_bodies = false;
-- SET xmloption = content;
-- SET client_min_messages = warning;
-- SET row_security = off;

--
-- Name: openresultat; Type: SCHEMA; Schema: -; Owner: -
--

-- CREATE SCHEMA openresultat;


--
-- Name: pr_maj_bureau(); Type: FUNCTION; Schema: openresultat; Owner: -
--

CREATE FUNCTION pr_maj_bureau() RETURNS trigger
    LANGUAGE plpgsql
    SET search_path TO 'openresultat', 'public', 'pg_catalog'
    AS $$
DECLARE
BEGIN
   IF (TG_OP='INSERT' AND NEW.coordinates IS NOT NULL) THEN
    NEW.geom=ST_Transform(ST_SetSRID(cast(ST_MakePoint(cast(substr(NEW.coordinates,1,strpos( NEW.coordinates, ',')-1) as float), 
      cast(substr(NEW.coordinates,strpos( NEW.coordinates, ',')+1) as float)) as geometry),4326),2154);
   ELSEIF(TG_OP='UPDATE') THEN
        IF (NEW.coordinates is null) THEN
      NEW.geom=NULL;
    ELSEIF (OLD.coordinates <> NEW.coordinates OR OLD.coordinates IS NULL) THEN
      NEW.geom=ST_Transform(ST_SetSRID(cast(ST_MakePoint(cast(substr(NEW.coordinates,1,strpos( NEW.coordinates, ',')-1) as float), 
      cast(substr(NEW.coordinates,strpos( NEW.coordinates, ',')+1) as float)) as geometry),4326),2154);
    END IF;
   END IF;
   RETURN NEW;
END;
$$;


-- SET default_tablespace = '';

-- SET default_table_access_method = heap;

--
-- Name: acteur; Type: TABLE; Schema: openresultat; Owner: -
--

CREATE TABLE acteur (
    acteur integer NOT NULL,
    nom character varying(30),
    login character varying(30)
);


--
-- Name: COLUMN acteur.acteur; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN acteur.acteur IS 'Identifiant de l''acteur';


--
-- Name: COLUMN acteur.nom; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN acteur.nom IS 'Nom de l''acteur';


--
-- Name: acteur_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE acteur_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: animation; Type: TABLE; Schema: openresultat; Owner: -
--

CREATE TABLE animation (
    animation integer NOT NULL,
    election integer,
    libelle character varying(100) NOT NULL,
    refresh integer NOT NULL,
    is_modele boolean DEFAULT false,
    actif boolean DEFAULT false,
    titre character varying(100),
    sous_titre character varying(100),
    couleur_titre character varying(13),
    couleur_bandeau character varying(13),
    logo integer,
    perimetre_aff integer,
    type_aff integer,
    affichage text,
    is_config boolean DEFAULT false,
    feuille_style text,
    script text
);


--
-- Name: COLUMN animation.animation; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN animation.animation IS 'Identifiant de l''animation';


--
-- Name: COLUMN animation.election; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN animation.election IS 'Identifiant de l''élection pour laquelle l''affichage est paramétré';


--
-- Name: COLUMN animation.libelle; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN animation.libelle IS 'Libelle de l''animation';


--
-- Name: COLUMN animation.refresh; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN animation.refresh IS 'Définit l''intervalle de temps entre chaque raffraichissement de l''animation';


--
-- Name: COLUMN animation.is_modele; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN animation.is_modele IS 'Permet d''enregistrer la configuration de l''animation comme modèle';


--
-- Name: COLUMN animation.actif; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN animation.actif IS 'Permet de définir si les animations sont actives/utilisables ou pas';


--
-- Name: COLUMN animation.titre; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN animation.titre IS 'Titre de la page de l''affichage';


--
-- Name: COLUMN animation.sous_titre; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN animation.sous_titre IS 'Sous-titre de la page de l''affichage';


--
-- Name: COLUMN animation.couleur_titre; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN animation.couleur_titre IS 'Définit la couleur du titre';


--
-- Name: COLUMN animation.couleur_bandeau; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN animation.couleur_bandeau IS 'Définit la couleur du bandeau de titre';


--
-- Name: COLUMN animation.logo; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN animation.logo IS 'Logo à afficher';


--
-- Name: COLUMN animation.perimetre_aff; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN animation.perimetre_aff IS 'Périmètre choisit pour l''affichage';


--
-- Name: COLUMN animation.type_aff; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN animation.type_aff IS 'Type des unités à afficher';


--
-- Name: COLUMN animation.affichage; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN animation.affichage IS 'Paramétrage de l''animation, choix des blocs à afficher, des diagrammes, etc.';


--
-- Name: COLUMN animation.is_config; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN animation.is_config IS 'Différencie les animations pré-configurée de celle créées par les utilisateurs';


--
-- Name: COLUMN animation.feuille_style; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN animation.feuille_style IS 'Feuille de style de l''animation';


--
-- Name: COLUMN animation.script; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN animation.script IS 'Script a ajouter à l''animation';


--
-- Name: animation_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE animation_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: candidat; Type: TABLE; Schema: openresultat; Owner: -
--

CREATE TABLE candidat (
    candidat integer NOT NULL,
    libelle character varying(100) NOT NULL,
    libelle_liste character varying(100)
);


--
-- Name: COLUMN candidat.candidat; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN candidat.candidat IS 'Identifiant numérique unique du candidat';


--
-- Name: COLUMN candidat.libelle; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN candidat.libelle IS 'Nom du candidat, de la liste ou autre';


--
-- Name: COLUMN candidat.libelle_liste; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN candidat.libelle_liste IS 'Nom de la liste auquelle appartient le candidat';


--
-- Name: election_resultat; Type: TABLE; Schema: openresultat; Owner: -
--

CREATE TABLE election_resultat (
    election_resultat integer NOT NULL,
    election_unite integer NOT NULL,
    election_candidat integer NOT NULL,
    resultat integer
);


--
-- Name: COLUMN election_resultat.election_resultat; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_resultat.election_resultat IS 'Identifiant numérique unique du résultat d''un candidat dans une unité de l''élection';


--
-- Name: COLUMN election_resultat.election_unite; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_resultat.election_unite IS 'Identifiant numérique unique faisant référence à une unité de l''élection';


--
-- Name: COLUMN election_resultat.election_candidat; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_resultat.election_candidat IS 'Identifiant numérique unique faisant référence à un candidat de l''élection';


--
-- Name: COLUMN election_resultat.resultat; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_resultat.resultat IS 'Nombre de voix obtenues par le candidat';


--
-- Name: election_unite; Type: TABLE; Schema: openresultat; Owner: -
--

CREATE TABLE election_unite (
    election_unite integer NOT NULL,
    election integer NOT NULL,
    unite integer NOT NULL,
    inscrit integer,
    votant integer,
    blanc integer,
    nul integer,
    exprime integer,
    envoi_aff boolean,
    envoi_web boolean,
    emargement integer,
    procuration integer,
    saisie character varying(10),
    validation character varying(30),
    date_derniere_modif double precision
);


--
-- Name: COLUMN election_unite.election_unite; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_unite.election_unite IS 'Identifiant numérique unique d''une unité de l''élection';


--
-- Name: COLUMN election_unite.election; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_unite.election IS 'Identifiant numérique unique faisant référence à l''élection';


--
-- Name: COLUMN election_unite.unite; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_unite.unite IS 'Identifiant numérique unique faisant référence à l''unité';


--
-- Name: COLUMN election_unite.inscrit; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_unite.inscrit IS 'Nombre d''inscrit';


--
-- Name: COLUMN election_unite.votant; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_unite.votant IS 'Nombre de votant';


--
-- Name: COLUMN election_unite.blanc; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_unite.blanc IS 'Nombre de vote blanc';


--
-- Name: COLUMN election_unite.nul; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_unite.nul IS 'Nombre de vote nul';


--
-- Name: COLUMN election_unite.exprime; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_unite.exprime IS 'Nombre de vote exprimé';


--
-- Name: COLUMN election_unite.envoi_aff; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_unite.envoi_aff IS 'Indique si les résultats de l''unité, pour cette élection, ont été envoyé à l''animation';


--
-- Name: COLUMN election_unite.envoi_web; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_unite.envoi_web IS 'Indique si les résultats de l''unité, pour cette élection, ont été envoyé à la page web';


--
-- Name: COLUMN election_unite.emargement; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_unite.emargement IS 'Nombre de personne ayant émargées';


--
-- Name: COLUMN election_unite.procuration; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_unite.procuration IS 'Nombre de procuration';


--
-- Name: COLUMN election_unite.saisie; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_unite.saisie IS 'Etat de la saisie (en attente, en defaut, correcte)';


--
-- Name: COLUMN election_unite.validation; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_unite.validation IS 'Statut de validation de la saisie : preparation, en attente de validation ou validee';


--
-- Name: type_unite; Type: TABLE; Schema: openresultat; Owner: -
--

CREATE TABLE type_unite (
    type_unite integer NOT NULL,
    libelle character varying(30) NOT NULL,
    hierarchie integer NOT NULL,
    bureau_vote boolean DEFAULT true
);


--
-- Name: COLUMN type_unite.type_unite; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN type_unite.type_unite IS 'Identifiant numérique unique du type d''unité';


--
-- Name: COLUMN type_unite.libelle; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN type_unite.libelle IS 'Nom du type d''unité';


--
-- Name: COLUMN type_unite.hierarchie; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN type_unite.hierarchie IS 'Hierarchie du type permettant de structurer les périmètres, une unité avec un type d''unité de faible hiérarchie ne pourra pas contenir une unité de type ayant une hiérarchie plus importante';


--
-- Name: COLUMN type_unite.bureau_vote; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN type_unite.bureau_vote IS 'Détermine si un type d''unité à le comportement d''un bureau de vote';


--
-- Name: unite; Type: TABLE; Schema: openresultat; Owner: -
--

CREATE TABLE unite (
    unite integer NOT NULL,
    libelle character varying(100) NOT NULL,
    type_unite integer NOT NULL,
    ordre character varying(10) NOT NULL,
    adresse1 character varying(100),
    adresse2 character varying(100),
    cp character(5),
    ville character varying(40),
    geom public.geometry(Point,2154),
    coordinates character varying(100),
    om_validite_debut date,
    om_validite_fin date,
    type_unite_contenu integer,
    perimetre boolean DEFAULT false,
    canton integer,
    circonscription integer,
    commune integer,
    departement integer,
    code_unite character varying(10),
    id_reu integer
);


--
-- Name: COLUMN unite.unite; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN unite.unite IS 'Identifiant numérique unique de l''unité';


--
-- Name: COLUMN unite.libelle; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN unite.libelle IS 'Nom de l''unité';


--
-- Name: COLUMN unite.type_unite; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN unite.type_unite IS 'Type de l''unité';


--
-- Name: COLUMN unite.ordre; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN unite.ordre IS 'Ordonne les unités dans la saisie et les affichages';


--
-- Name: COLUMN unite.adresse1; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN unite.adresse1 IS 'Adresse de l''unité';


--
-- Name: COLUMN unite.adresse2; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN unite.adresse2 IS 'Complément d''adresse';


--
-- Name: COLUMN unite.cp; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN unite.cp IS 'Code postal';


--
-- Name: COLUMN unite.ville; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN unite.ville IS 'Nom de la ville de l''unité';


--
-- Name: COLUMN unite.geom; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN unite.geom IS 'Point représentant l''unité sur la carte (SIG)';


--
-- Name: COLUMN unite.coordinates; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN unite.coordinates IS 'Coordonnées de l''unité (SIG)';


--
-- Name: COLUMN unite.om_validite_debut; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN unite.om_validite_debut IS 'Date de début de validité';


--
-- Name: COLUMN unite.om_validite_fin; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN unite.om_validite_fin IS 'Date d''expiration';


--
-- Name: COLUMN unite.type_unite_contenu; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN unite.type_unite_contenu IS 'Type d''unité que l''unité contiens';


--
-- Name: COLUMN unite.perimetre; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN unite.perimetre IS 'Précise si l''unité est un périmètre ou pas, c''est à dire si elle contiens d''autres unités ou pas';


--
-- Name: COLUMN unite.canton; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN unite.canton IS 'Canton d''appartenance de l'' unité';


--
-- Name: COLUMN unite.circonscription; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN unite.circonscription IS 'Circonscription d''appartenance de l''unité, si elle se comporte comme un bureau de vote';


--
-- Name: COLUMN unite.commune; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN unite.commune IS 'Commune d''appartenance de l''unité, si elle se comporte comme un bureau de vote';


--
-- Name: COLUMN unite.departement; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN unite.departement IS 'Département d''apartenance de l''unité, si elle se comporte comme un bureau de vote';


--
-- Name: COLUMN unite.code_unite; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN unite.code_unite IS 'Code de l''unité, si elle se comporte comme un bureau de vote';


--
-- Name: COLUMN unite.id_reu; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN unite.id_reu IS 'Identifiant du bureau sur le REU';


--
-- Name: candidat_resultat; Type: VIEW; Schema: openresultat; Owner: -
--

CREATE VIEW candidat_resultat AS
 SELECT election_resultat.election_candidat,
    sum(election_resultat.resultat) AS resultat
   FROM (((election_resultat
     JOIN election_unite ON ((election_resultat.election_unite = election_unite.election_unite)))
     JOIN unite ON ((election_unite.unite = unite.unite)))
     JOIN type_unite ON ((unite.type_unite = type_unite.type_unite)))
  WHERE (type_unite.bureau_vote = true)
  GROUP BY election_resultat.election_candidat;


--
-- Name: COLUMN candidat_resultat.election_candidat; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN candidat_resultat.election_candidat IS 'Identifiant numérique unique faisant référence à un candidat de l''élection';


--
-- Name: COLUMN candidat_resultat.resultat; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN candidat_resultat.resultat IS 'Total des voix du candidat';


--
-- Name: candidat_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE candidat_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: canton; Type: TABLE; Schema: openresultat; Owner: -
--

CREATE TABLE canton (
    canton integer NOT NULL,
    libelle character varying(30) NOT NULL,
    code character varying(10) NOT NULL,
    prefecture character varying(10) NOT NULL
);


--
-- Name: COLUMN canton.canton; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN canton.canton IS 'Identifiant du canton';


--
-- Name: COLUMN canton.libelle; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN canton.libelle IS 'Libellé du canton';


--
-- Name: COLUMN canton.code; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN canton.code IS 'Code du canton';


--
-- Name: COLUMN canton.prefecture; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN canton.prefecture IS 'Code préfecture du canton';


--
-- Name: canton_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE canton_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: circonscription; Type: TABLE; Schema: openresultat; Owner: -
--

CREATE TABLE circonscription (
    circonscription integer NOT NULL,
    libelle character varying(30) NOT NULL,
    code character varying(10) NOT NULL,
    prefecture character varying(10) NOT NULL
);


--
-- Name: COLUMN circonscription.circonscription; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN circonscription.circonscription IS 'Identifiant de la circonscription';


--
-- Name: COLUMN circonscription.libelle; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN circonscription.libelle IS 'Libellé de la circonscription';


--
-- Name: COLUMN circonscription.code; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN circonscription.code IS 'Code de la circonscription';


--
-- Name: COLUMN circonscription.prefecture; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN circonscription.prefecture IS 'Code préfecture de la circonscription';


--
-- Name: circonscription_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE circonscription_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: commune; Type: TABLE; Schema: openresultat; Owner: -
--

CREATE TABLE commune (
    commune integer NOT NULL,
    libelle character varying(30) NOT NULL,
    code character varying(10) NOT NULL,
    prefecture character varying(10) NOT NULL
);


--
-- Name: COLUMN commune.commune; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN commune.commune IS 'Identifiant de la commune';


--
-- Name: COLUMN commune.libelle; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN commune.libelle IS 'Libellé de la commune';


--
-- Name: COLUMN commune.code; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN commune.code IS 'Code de la commune';


--
-- Name: COLUMN commune.prefecture; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN commune.prefecture IS 'Code préfecture de la commune';


--
-- Name: commune_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE commune_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: delegation; Type: TABLE; Schema: openresultat; Owner: -
--

CREATE TABLE delegation (
    delegation integer NOT NULL,
    election integer NOT NULL,
    unite integer NOT NULL,
    acteur integer NOT NULL
);


--
-- Name: COLUMN delegation.delegation; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN delegation.delegation IS 'Identifiant de la delegation';


--
-- Name: COLUMN delegation.election; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN delegation.election IS 'Identifiant de l''élection concernée';


--
-- Name: COLUMN delegation.unite; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN delegation.unite IS 'Identifiant de l''unite concernée';


--
-- Name: COLUMN delegation.acteur; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN delegation.acteur IS 'Identifiant de l''acteur concerné';


--
-- Name: delegation_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE delegation_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: departement; Type: TABLE; Schema: openresultat; Owner: -
--

CREATE TABLE departement (
    departement integer NOT NULL,
    libelle character varying(30) NOT NULL,
    code character varying(10) NOT NULL,
    prefecture character varying(10) NOT NULL
);


--
-- Name: COLUMN departement.departement; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN departement.departement IS 'Identifiant du departement';


--
-- Name: COLUMN departement.libelle; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN departement.libelle IS 'Libellé du departement';


--
-- Name: COLUMN departement.code; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN departement.code IS 'Code du departement';


--
-- Name: COLUMN departement.prefecture; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN departement.prefecture IS 'Code préfecture du departement';


--
-- Name: departement_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE departement_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: election; Type: TABLE; Schema: openresultat; Owner: -
--

CREATE TABLE election (
    election integer NOT NULL,
    libelle character varying(100) NOT NULL,
    code character varying(10),
    tour integer,
    type_election integer NOT NULL,
    date date NOT NULL,
    perimetre integer NOT NULL,
    votant_defaut integer,
    heure_ouverture integer NOT NULL,
    heure_fermeture integer NOT NULL,
    envoi_initial boolean,
    is_centaine boolean DEFAULT false,
    election_reference integer,
    workflow character varying(15),
    publication_auto boolean,
    publication_erreur boolean,
    calcul_auto_exprime boolean,
    garder_resultat_simulation boolean,
    delegation_saisie boolean,
    delegation_participation boolean,
    validation_avant_publication boolean,
    sieges integer,
    sieges_com integer,
    sieges_mep integer DEFAULT 0 NOT NULL,
    export_emargement_prefecture boolean DEFAULT true,
    CONSTRAINT etape_workflow CHECK (((workflow)::text = ANY (ARRAY[('Paramétrage'::character varying)::text, ('Simulation'::character varying)::text, ('Saisie'::character varying)::text, ('Finalisation'::character varying)::text, ('Archivage'::character varying)::text])))
);


--
-- Name: COLUMN election.election; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election.election IS 'Identifiant numérique unique de l''élection';


--
-- Name: COLUMN election.libelle; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election.libelle IS 'Nom de l''élection';


--
-- Name: COLUMN election.code; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election.code IS 'Code associé à l''élection';


--
-- Name: COLUMN election.tour; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election.tour IS 'Tour de l''élection';


--
-- Name: COLUMN election.type_election; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election.type_election IS 'Type de l''élection (Municipale, Départementale, ...)';


--
-- Name: COLUMN election.date; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election.date IS 'Date à laquelle se déroule l''élection';


--
-- Name: COLUMN election.perimetre; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election.perimetre IS 'Périmètre sur lequel se déroule l''élection et qui va déterminer les unités concernées';


--
-- Name: COLUMN election.votant_defaut; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election.votant_defaut IS 'Nombre de votant par défaut pour une élection non principale. 100 permet de faire une saisie de la première centaine';


--
-- Name: COLUMN election.heure_ouverture; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election.heure_ouverture IS 'Heure à laquelle démarre l''élection. Sert à déterminer les tranches horaires pour la saisie de la participation';


--
-- Name: COLUMN election.heure_fermeture; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election.heure_fermeture IS 'Heure à laquelle s''arrête l''élection. Sert à déterminer les tranches horaires pour la saisie de la participation';


--
-- Name: COLUMN election.envoi_initial; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election.envoi_initial IS 'Permet lors de l''envoi à la préfecture d''indiquer si il s''agit d''un envoi initial ou pas';


--
-- Name: COLUMN election.is_centaine; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election.is_centaine IS 'Paramètre permettant de savoir si il s''agit d''une simulation ou d''une élection';


--
-- Name: COLUMN election.election_reference; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election.election_reference IS 'Paramètre permettant de savoir quelle élection est concerné par la saisie par centaine';


--
-- Name: COLUMN election.workflow; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election.workflow IS 'Workflow de l''élection, les étapes sont : Paramétrage, Simulation, Saisie, Finalisation et Archivage';


--
-- Name: COLUMN election.publication_auto; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election.publication_auto IS 'Si cette option est activée, les résultats sont automatiquement publiés suite à leur saisie/modification';


--
-- Name: COLUMN election.publication_erreur; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election.publication_erreur IS 'Si cette option n''est pas activé, seul les résultats corrects sont publiés';


--
-- Name: COLUMN election.calcul_auto_exprime; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election.calcul_auto_exprime IS 'Défini si le nombre de vote exprimé est saisie par l''utilisateur ou automatiquement calculé';


--
-- Name: COLUMN election.garder_resultat_simulation; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election.garder_resultat_simulation IS 'Défini si les résultats saisis pendant la simulation doivent être conservé par la suite';


--
-- Name: COLUMN election.delegation_saisie; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election.delegation_saisie IS 'Option permettant d''activer la délégation de saisie';


--
-- Name: COLUMN election.delegation_participation; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election.delegation_participation IS 'Option permettant d''activer la délégation de saisie de la validation';


--
-- Name: COLUMN election.validation_avant_publication; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election.validation_avant_publication IS 'Option permettant de rendre la validation obligatoire avant de publier les résultats';


--
-- Name: COLUMN election.export_emargement_prefecture; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election.export_emargement_prefecture IS 'Option permettant d''afficher la colonne contenant le nombre d''émargement sur l''export préfecture';


--
-- Name: election_candidat; Type: TABLE; Schema: openresultat; Owner: -
--

CREATE TABLE election_candidat (
    election_candidat integer NOT NULL,
    election integer NOT NULL,
    candidat integer NOT NULL,
    ordre integer NOT NULL,
    prefecture character varying(10),
    age_moyen numeric,
    siege integer,
    age_moyen_com numeric,
    siege_com integer,
    couleur character varying(13),
    photo character varying(100),
    parti_politique integer,
    age_moyen_mep integer DEFAULT 0 NOT NULL,
    siege_mep integer DEFAULT 0 NOT NULL
);


--
-- Name: COLUMN election_candidat.election_candidat; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_candidat.election_candidat IS 'Identifiant numérique unique d''un candidat de l''élection';


--
-- Name: COLUMN election_candidat.election; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_candidat.election IS 'Identifiant numérique unique faisant référence à l''élection';


--
-- Name: COLUMN election_candidat.candidat; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_candidat.candidat IS 'Identifiant numérique unique faisant référence au candidat';


--
-- Name: COLUMN election_candidat.ordre; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_candidat.ordre IS 'Ordre du candidat dans l''élection';


--
-- Name: COLUMN election_candidat.prefecture; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_candidat.prefecture IS 'Code préfecture du candidat ou de la liste';


--
-- Name: COLUMN election_candidat.age_moyen; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_candidat.age_moyen IS 'Age moyen de la liste municipale';


--
-- Name: COLUMN election_candidat.siege; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_candidat.siege IS 'Nombre de siège obtenu par la liste municipale';


--
-- Name: COLUMN election_candidat.age_moyen_com; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_candidat.age_moyen_com IS 'Age_moyen de la liste communautaire';


--
-- Name: COLUMN election_candidat.siege_com; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_candidat.siege_com IS 'Nombre de siège obtenu par la liste communautaire';


--
-- Name: COLUMN election_candidat.couleur; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_candidat.couleur IS 'Couleur associée au candidat dans l''animation';


--
-- Name: COLUMN election_candidat.photo; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_candidat.photo IS 'Photo du candidat';


--
-- Name: COLUMN election_candidat.parti_politique; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_candidat.parti_politique IS 'Parti du candidat';


--
-- Name: election_candidat_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE election_candidat_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: election_cumul_unite; Type: VIEW; Schema: openresultat; Owner: -
--

CREATE VIEW election_cumul_unite AS
 SELECT election_unite.election,
    sum(election_unite.inscrit) AS inscrit,
    sum(election_unite.votant) AS votant,
    sum(election_unite.nul) AS nul,
    sum(election_unite.blanc) AS blanc,
    sum(election_unite.exprime) AS exprime
   FROM ((election_unite
     JOIN unite ON ((election_unite.unite = unite.unite)))
     JOIN type_unite ON ((unite.type_unite = type_unite.type_unite)))
  WHERE (type_unite.bureau_vote = true)
  GROUP BY election_unite.election;


--
-- Name: COLUMN election_cumul_unite.election; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_cumul_unite.election IS 'Identifiant numérique unique faisant référence à l''élection';


--
-- Name: COLUMN election_cumul_unite.inscrit; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_cumul_unite.inscrit IS 'Total des inscrits à l''élection';


--
-- Name: COLUMN election_cumul_unite.votant; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_cumul_unite.votant IS 'Total des votants de l''élection';


--
-- Name: COLUMN election_cumul_unite.nul; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_cumul_unite.nul IS 'Total des votes nuls de l''élection';


--
-- Name: COLUMN election_cumul_unite.blanc; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_cumul_unite.blanc IS 'Total des votes blancs de l''élection';


--
-- Name: COLUMN election_cumul_unite.exprime; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_cumul_unite.exprime IS 'Total des votes exprimés de l''élection';


--
-- Name: election_exprime; Type: VIEW; Schema: openresultat; Owner: -
--

CREATE VIEW election_exprime AS
 SELECT election_unite.election,
    sum(election_resultat.resultat) AS exprime
   FROM (election_resultat
     JOIN election_unite ON ((election_unite.election_unite = election_resultat.election_unite)))
  GROUP BY election_unite.election;


--
-- Name: COLUMN election_exprime.election; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_exprime.election IS 'Identifiant numérique unique faisant référence à l''élection';


--
-- Name: COLUMN election_exprime.exprime; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN election_exprime.exprime IS 'Total des votes exprimés de l''élection';


--
-- Name: election_resultat_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE election_resultat_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: election_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE election_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: election_unite_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE election_unite_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lien_unite; Type: TABLE; Schema: openresultat; Owner: -
--

CREATE TABLE lien_unite (
    lien_unite integer NOT NULL,
    unite_enfant integer NOT NULL,
    unite_parent integer NOT NULL
);


--
-- Name: COLUMN lien_unite.lien_unite; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN lien_unite.lien_unite IS 'Identifiant numérique unique du lien entre les unités';


--
-- Name: COLUMN lien_unite.unite_enfant; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN lien_unite.unite_enfant IS 'Identifiant de l''unité qui est contenue';


--
-- Name: COLUMN lien_unite.unite_parent; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN lien_unite.unite_parent IS 'Identifiant de l''unité qui contiens l''autre unité';


--
-- Name: lien_unite_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE lien_unite_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: parti_politique; Type: TABLE; Schema: openresultat; Owner: -
--

CREATE TABLE parti_politique (
    parti_politique integer NOT NULL,
    libelle character varying(50) NOT NULL,
    couleur character varying(8)
);


--
-- Name: COLUMN parti_politique.parti_politique; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN parti_politique.parti_politique IS 'Identifiant du parti';


--
-- Name: COLUMN parti_politique.libelle; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN parti_politique.libelle IS 'Nom du parti';


--
-- Name: COLUMN parti_politique.couleur; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN parti_politique.couleur IS 'Couleur associée au parti';


--
-- Name: parti_politique_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE parti_politique_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: participation_election; Type: TABLE; Schema: openresultat; Owner: -
--

CREATE TABLE participation_election (
    participation_election integer NOT NULL,
    election integer NOT NULL,
    tranche integer NOT NULL,
    date_derniere_modif double precision
);


--
-- Name: COLUMN participation_election.participation_election; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN participation_election.participation_election IS 'Identifiant numérique unique d''une tranche horaire de participation à une élection';


--
-- Name: COLUMN participation_election.election; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN participation_election.election IS 'Identifiant numérique unique faisant référence à l''élection';


--
-- Name: COLUMN participation_election.tranche; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN participation_election.tranche IS 'Identifiant numérique unique faisant référence à la tranche horaire';


--
-- Name: participation_election_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE participation_election_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: participation_unite; Type: TABLE; Schema: openresultat; Owner: -
--

CREATE TABLE participation_unite (
    participation_unite integer NOT NULL,
    election_unite integer NOT NULL,
    participation_election integer NOT NULL,
    votant integer,
    saisie boolean,
    envoi_aff boolean,
    envoi_web boolean,
    date_derniere_modif double precision
);


--
-- Name: COLUMN participation_unite.participation_unite; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN participation_unite.participation_unite IS 'Identifiant numérique unique de la participation pour une unité de l''élection';


--
-- Name: COLUMN participation_unite.election_unite; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN participation_unite.election_unite IS 'Identifiant numérique unique faisant référence à une unité de l''élection';


--
-- Name: COLUMN participation_unite.participation_election; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN participation_unite.participation_election IS 'Identifiant numérique unique faisant référence à une tranche horaire de participation à l''élection';


--
-- Name: COLUMN participation_unite.votant; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN participation_unite.votant IS 'Nombre de personnes ayant voté';


--
-- Name: COLUMN participation_unite.saisie; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN participation_unite.saisie IS 'Indique si la saisie a été réalisée ou pas';


--
-- Name: COLUMN participation_unite.envoi_aff; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN participation_unite.envoi_aff IS 'Indique si la participation a été envoyée à l''affichage';


--
-- Name: COLUMN participation_unite.envoi_web; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN participation_unite.envoi_web IS 'Indique si la participation a été envoyée à la page web';


--
-- Name: participation_unite_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE participation_unite_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: perimetre_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE perimetre_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: plan; Type: TABLE; Schema: openresultat; Owner: -
--

CREATE TABLE plan (
    plan integer NOT NULL,
    libelle character varying(50) NOT NULL,
    image_plan character varying(100) NOT NULL,
    img_unite_arrivee character varying(100) NOT NULL,
    img_unite_non_arrivee character varying(100) NOT NULL,
    par_defaut boolean,
    largeur_icone integer
);


--
-- Name: COLUMN plan.plan; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN plan.plan IS 'Identifiant du plan';


--
-- Name: COLUMN plan.libelle; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN plan.libelle IS 'Nom du plan';


--
-- Name: COLUMN plan.image_plan; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN plan.image_plan IS 'Image du plan';


--
-- Name: COLUMN plan.img_unite_arrivee; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN plan.img_unite_arrivee IS 'Image par défaut des unités arrivées';


--
-- Name: COLUMN plan.img_unite_non_arrivee; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN plan.img_unite_non_arrivee IS 'Image par défaut des unités non arrivées';


--
-- Name: COLUMN plan.par_defaut; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN plan.par_defaut IS 'Permet de savoir si le plan doit être ajouté par défaut à l''élecrtion';


--
-- Name: plan_election; Type: TABLE; Schema: openresultat; Owner: -
--

CREATE TABLE plan_election (
    plan_election integer NOT NULL,
    plan integer NOT NULL,
    election integer NOT NULL
);


--
-- Name: COLUMN plan_election.plan_election; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN plan_election.plan_election IS 'Identifiant du lien entre le plan et l''election';


--
-- Name: COLUMN plan_election.plan; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN plan_election.plan IS 'Identifiant du plan';


--
-- Name: COLUMN plan_election.election; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN plan_election.election IS 'Identifiant de l''election';


--
-- Name: plan_election_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE plan_election_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: plan_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE plan_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: plan_unite; Type: TABLE; Schema: openresultat; Owner: -
--

CREATE TABLE plan_unite (
    plan_unite integer NOT NULL,
    plan integer NOT NULL,
    unite integer NOT NULL,
    img_unite_arrivee character varying(100),
    img_unite_non_arrivee character varying(100),
    position_x integer,
    position_y integer,
    largeur_icone integer
);


--
-- Name: COLUMN plan_unite.plan_unite; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN plan_unite.plan_unite IS 'Identifiant du plan';


--
-- Name: COLUMN plan_unite.plan; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN plan_unite.plan IS 'Nom du plan';


--
-- Name: COLUMN plan_unite.unite; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN plan_unite.unite IS 'Image du plan';


--
-- Name: COLUMN plan_unite.img_unite_arrivee; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN plan_unite.img_unite_arrivee IS 'Image affichée si l''unité est arrivée';


--
-- Name: COLUMN plan_unite.img_unite_non_arrivee; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN plan_unite.img_unite_non_arrivee IS 'Image affichée si l''unité n''est pas arrivée';


--
-- Name: COLUMN plan_unite.position_x; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN plan_unite.position_x IS 'Coordonnée en x, en pixel, de l''image sur le plan';


--
-- Name: COLUMN plan_unite.position_y; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN plan_unite.position_y IS 'Coordonnée en y, en pixel, de l''image sur le plan';


--
-- Name: COLUMN plan_unite.largeur_icone; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN plan_unite.largeur_icone IS 'Largeur de l''icone sur le plan';


--
-- Name: plan_unite_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE plan_unite_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: resultat_participation_tranche; Type: VIEW; Schema: openresultat; Owner: -
--

CREATE VIEW resultat_participation_tranche AS
 SELECT participation_election.participation_election,
    every(participation_unite.saisie) AS saisie,
    every(participation_unite.envoi_aff) AS envoi_aff,
    every(participation_unite.envoi_web) AS envoi_web,
    sum(participation_unite.votant) AS votant
   FROM ((((participation_election
     JOIN participation_unite ON ((participation_election.participation_election = participation_unite.participation_election)))
     JOIN election_unite ON ((participation_unite.election_unite = election_unite.election_unite)))
     JOIN unite ON ((election_unite.unite = unite.unite)))
     JOIN type_unite ON ((unite.type_unite = type_unite.type_unite)))
  WHERE (type_unite.bureau_vote = true)
  GROUP BY participation_election.participation_election;


--
-- Name: resultat_participation_unite; Type: VIEW; Schema: openresultat; Owner: -
--

CREATE VIEW resultat_participation_unite AS
 SELECT election_unite.election_unite,
    every(participation_unite.saisie) AS saisie,
    every(participation_unite.envoi_aff) AS envoi_aff,
    every(participation_unite.envoi_web) AS envoi_web,
    max(participation_unite.votant) AS votant
   FROM (election_unite
     JOIN participation_unite ON ((election_unite.election_unite = participation_unite.election_unite)))
  GROUP BY election_unite.election_unite;


--
-- Name: tranche; Type: TABLE; Schema: openresultat; Owner: -
--

CREATE TABLE tranche (
    tranche integer NOT NULL,
    libelle time without time zone NOT NULL,
    ordre integer NOT NULL
);


--
-- Name: COLUMN tranche.tranche; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN tranche.tranche IS 'Identifiant numérique unique de la tranche horaire';


--
-- Name: COLUMN tranche.libelle; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN tranche.libelle IS 'Nom de la tranche horaire';


--
-- Name: COLUMN tranche.ordre; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN tranche.ordre IS 'Ordre des tranches dans la saisie et les affichages';


--
-- Name: tranche_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE tranche_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: type_election; Type: TABLE; Schema: openresultat; Owner: -
--

CREATE TABLE type_election (
    type_election integer NOT NULL,
    libelle character varying(30) NOT NULL,
    code character varying(8) NOT NULL,
    prefecture character varying(3) NOT NULL
);


--
-- Name: COLUMN type_election.type_election; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN type_election.type_election IS 'Identifiant numérique unique du type d''élection';


--
-- Name: COLUMN type_election.libelle; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN type_election.libelle IS 'Nom du type d''élection';


--
-- Name: COLUMN type_election.code; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN type_election.code IS 'Champ de codification interne pour identifier le type';


--
-- Name: COLUMN type_election.prefecture; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN type_election.prefecture IS 'Code donné par la préfecture et qui sert dans le(s) envoi(s) à la préfecture';


--
-- Name: type_election_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE type_election_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: type_unite_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE type_unite_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: unite_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE unite_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: web; Type: TABLE; Schema: openresultat; Owner: -
--

CREATE TABLE web (
    web integer NOT NULL,
    libelle character varying(30) NOT NULL,
    logo integer,
    entete character varying(100),
    url_collectivite character varying(100),
    libelle_url character varying(30),
    feuille_style text,
    jscript_stats text,
    display_simulation boolean DEFAULT false NOT NULL,
    actif boolean DEFAULT false NOT NULL
);


--
-- Name: COLUMN web.web; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN web.web IS 'Identifiant de l''affichage web';


--
-- Name: COLUMN web.libelle; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN web.libelle IS 'Nom du modèle';


--
-- Name: COLUMN web.logo; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN web.logo IS 'Logo a afficher sur la page';


--
-- Name: COLUMN web.entete; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN web.entete IS 'Titre de l''entête de l''affichage web';


--
-- Name: COLUMN web.url_collectivite; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN web.url_collectivite IS 'url vers le site de la collectivité';


--
-- Name: COLUMN web.libelle_url; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN web.libelle_url IS 'texte du lien / url';


--
-- Name: COLUMN web.feuille_style; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN web.feuille_style IS 'Paramétrage de la feuille de style';


--
-- Name: COLUMN web.jscript_stats; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN web.jscript_stats IS 'JavaScript à ajouter à l''affichage web';


--
-- Name: COLUMN web.display_simulation; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN web.display_simulation IS 'Option permettant de définir si les centaines doivent être affichée ou pas dans le listing du portail web';


--
-- Name: COLUMN web.actif; Type: COMMENT; Schema: openresultat; Owner: -
--

COMMENT ON COLUMN web.actif IS 'Indique quel modèle web est utilisé';


--
-- Name: web_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE web_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: acteur acteur_pkey; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY acteur
    ADD CONSTRAINT acteur_pkey PRIMARY KEY (acteur);


--
-- Name: animation animation_pkey; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY animation
    ADD CONSTRAINT animation_pkey PRIMARY KEY (animation);


--
-- Name: unite bureau_pkey; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY unite
    ADD CONSTRAINT bureau_pkey PRIMARY KEY (unite);


--
-- Name: candidat candidat_pkey; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY candidat
    ADD CONSTRAINT candidat_pkey PRIMARY KEY (candidat);


--
-- Name: canton canton_pkey; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY canton
    ADD CONSTRAINT canton_pkey PRIMARY KEY (canton);


--
-- Name: circonscription circonscription_pkey; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY circonscription
    ADD CONSTRAINT circonscription_pkey PRIMARY KEY (circonscription);


--
-- Name: commune commune_pkey; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY commune
    ADD CONSTRAINT commune_pkey PRIMARY KEY (commune);


--
-- Name: delegation delegation_pkey; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY delegation
    ADD CONSTRAINT delegation_pkey PRIMARY KEY (delegation);


--
-- Name: departement departement_pkey; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY departement
    ADD CONSTRAINT departement_pkey PRIMARY KEY (departement);


--
-- Name: election_resultat election_bureau_election_candidat_unique; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY election_resultat
    ADD CONSTRAINT election_bureau_election_candidat_unique UNIQUE (election_unite, election_candidat);


--
-- Name: election_unite election_bureau_pkey; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY election_unite
    ADD CONSTRAINT election_bureau_pkey PRIMARY KEY (election_unite);


--
-- Name: election_unite election_bureau_unique; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY election_unite
    ADD CONSTRAINT election_bureau_unique UNIQUE (election, unite);


--
-- Name: election_candidat election_candidat_pkey; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY election_candidat
    ADD CONSTRAINT election_candidat_pkey PRIMARY KEY (election_candidat);


--
-- Name: election_candidat election_candidat_unique; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY election_candidat
    ADD CONSTRAINT election_candidat_unique UNIQUE (election, candidat);


--
-- Name: election election_pkey; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY election
    ADD CONSTRAINT election_pkey PRIMARY KEY (election);


--
-- Name: election_resultat election_resultat_pkey; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY election_resultat
    ADD CONSTRAINT election_resultat_pkey PRIMARY KEY (election_resultat);


--
-- Name: participation_election election_tranche_unique; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY participation_election
    ADD CONSTRAINT election_tranche_unique UNIQUE (election, tranche);


--
-- Name: election_candidat ordre_unique; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY election_candidat
    ADD CONSTRAINT ordre_unique UNIQUE (ordre, election);


--
-- Name: parti_politique parti_politique_pkey; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY parti_politique
    ADD CONSTRAINT parti_politique_pkey PRIMARY KEY (parti_politique);


--
-- Name: participation_unite participation_bureau_pkey; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY participation_unite
    ADD CONSTRAINT participation_bureau_pkey PRIMARY KEY (participation_unite);


--
-- Name: participation_unite participation_election_election_bureau_unique; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY participation_unite
    ADD CONSTRAINT participation_election_election_bureau_unique UNIQUE (participation_election, election_unite);


--
-- Name: participation_election participation_election_pkey; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY participation_election
    ADD CONSTRAINT participation_election_pkey PRIMARY KEY (participation_election);


--
-- Name: lien_unite perimetre_bureau_pkey; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY lien_unite
    ADD CONSTRAINT perimetre_bureau_pkey PRIMARY KEY (lien_unite);


--
-- Name: plan_election plan_election_pkey; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY plan_election
    ADD CONSTRAINT plan_election_pkey PRIMARY KEY (plan_election);


--
-- Name: plan plan_pkey; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY plan
    ADD CONSTRAINT plan_pkey PRIMARY KEY (plan);


--
-- Name: plan_election plan_unique; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY plan_election
    ADD CONSTRAINT plan_unique UNIQUE (plan, election);


--
-- Name: plan_unite plan_unite_pkey; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY plan_unite
    ADD CONSTRAINT plan_unite_pkey PRIMARY KEY (plan_unite);


--
-- Name: plan_unite plan_unite_unique; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY plan_unite
    ADD CONSTRAINT plan_unite_unique UNIQUE (plan, unite);


--
-- Name: tranche tranche_pkey; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY tranche
    ADD CONSTRAINT tranche_pkey PRIMARY KEY (tranche);


--
-- Name: type_unite type_bureau_pkey; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY type_unite
    ADD CONSTRAINT type_bureau_pkey PRIMARY KEY (type_unite);


--
-- Name: type_election type_election_pkey; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY type_election
    ADD CONSTRAINT type_election_pkey PRIMARY KEY (type_election);


--
-- Name: web web_pkey; Type: CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY web
    ADD CONSTRAINT web_pkey PRIMARY KEY (web);


--
-- Name: unite tr_maj_bureau; Type: TRIGGER; Schema: openresultat; Owner: -
--

CREATE TRIGGER tr_maj_bureau BEFORE INSERT OR UPDATE ON unite FOR EACH ROW EXECUTE FUNCTION pr_maj_bureau();


--
-- Name: animation animation_election_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY animation
    ADD CONSTRAINT animation_election_fkey FOREIGN KEY (election) REFERENCES election(election);


--
-- Name: animation animation_logo_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY animation
    ADD CONSTRAINT animation_logo_fkey FOREIGN KEY (logo) REFERENCES om_logo(om_logo);


--
-- Name: animation animation_perimetre_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY animation
    ADD CONSTRAINT animation_perimetre_fkey FOREIGN KEY (perimetre_aff) REFERENCES unite(unite);


--
-- Name: animation animation_type_aff_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY animation
    ADD CONSTRAINT animation_type_aff_fkey FOREIGN KEY (type_aff) REFERENCES type_unite(type_unite);


--
-- Name: unite bureau_type_bureau_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY unite
    ADD CONSTRAINT bureau_type_bureau_fkey FOREIGN KEY (type_unite) REFERENCES type_unite(type_unite);


--
-- Name: delegation delegation_acteur_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY delegation
    ADD CONSTRAINT delegation_acteur_fkey FOREIGN KEY (acteur) REFERENCES acteur(acteur);


--
-- Name: delegation delegation_election_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY delegation
    ADD CONSTRAINT delegation_election_fkey FOREIGN KEY (election) REFERENCES election(election);


--
-- Name: delegation delegation_unite_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY delegation
    ADD CONSTRAINT delegation_unite_fkey FOREIGN KEY (unite) REFERENCES unite(unite);


--
-- Name: election_unite election_bureau_bureau_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY election_unite
    ADD CONSTRAINT election_bureau_bureau_fkey FOREIGN KEY (unite) REFERENCES unite(unite);


--
-- Name: election_unite election_bureau_election_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY election_unite
    ADD CONSTRAINT election_bureau_election_fkey FOREIGN KEY (election) REFERENCES election(election);


--
-- Name: election_candidat election_candidat_candidat_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY election_candidat
    ADD CONSTRAINT election_candidat_candidat_fkey FOREIGN KEY (candidat) REFERENCES candidat(candidat);


--
-- Name: election_candidat election_candidat_election_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY election_candidat
    ADD CONSTRAINT election_candidat_election_fkey FOREIGN KEY (election) REFERENCES election(election);


--
-- Name: election_candidat election_candidat_parti_politique_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY election_candidat
    ADD CONSTRAINT election_candidat_parti_politique_fkey FOREIGN KEY (parti_politique) REFERENCES parti_politique(parti_politique);


--
-- Name: election election_heure_fermeture_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY election
    ADD CONSTRAINT election_heure_fermeture_fkey FOREIGN KEY (heure_fermeture) REFERENCES tranche(tranche);


--
-- Name: election election_heure_ouverture_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY election
    ADD CONSTRAINT election_heure_ouverture_fkey FOREIGN KEY (heure_ouverture) REFERENCES tranche(tranche);


--
-- Name: election election_reference_election_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY election
    ADD CONSTRAINT election_reference_election_fkey FOREIGN KEY (election_reference) REFERENCES election(election);


--
-- Name: election_resultat election_resultat_election_bureau_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY election_resultat
    ADD CONSTRAINT election_resultat_election_bureau_fkey FOREIGN KEY (election_unite) REFERENCES election_unite(election_unite);


--
-- Name: election_resultat election_resultat_election_candidat_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY election_resultat
    ADD CONSTRAINT election_resultat_election_candidat_fkey FOREIGN KEY (election_candidat) REFERENCES election_candidat(election_candidat);


--
-- Name: election election_type_election_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY election
    ADD CONSTRAINT election_type_election_fkey FOREIGN KEY (type_election) REFERENCES type_election(type_election);


--
-- Name: election election_unite_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY election
    ADD CONSTRAINT election_unite_fkey FOREIGN KEY (perimetre) REFERENCES unite(unite);


--
-- Name: lien_unite lien_unite_unite_parent_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY lien_unite
    ADD CONSTRAINT lien_unite_unite_parent_fkey FOREIGN KEY (unite_parent) REFERENCES unite(unite);


--
-- Name: participation_unite participation_bureau_election_bureau_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY participation_unite
    ADD CONSTRAINT participation_bureau_election_bureau_fkey FOREIGN KEY (election_unite) REFERENCES election_unite(election_unite);


--
-- Name: participation_unite participation_bureau_participation_election_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY participation_unite
    ADD CONSTRAINT participation_bureau_participation_election_fkey FOREIGN KEY (participation_election) REFERENCES participation_election(participation_election);


--
-- Name: participation_election participation_election_election_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY participation_election
    ADD CONSTRAINT participation_election_election_fkey FOREIGN KEY (election) REFERENCES election(election);


--
-- Name: participation_election participation_election_tranche_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY participation_election
    ADD CONSTRAINT participation_election_tranche_fkey FOREIGN KEY (tranche) REFERENCES tranche(tranche);


--
-- Name: lien_unite perimetre_bureau_bureau_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY lien_unite
    ADD CONSTRAINT perimetre_bureau_bureau_fkey FOREIGN KEY (unite_enfant) REFERENCES unite(unite);


--
-- Name: plan_election plan_election_election_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY plan_election
    ADD CONSTRAINT plan_election_election_fkey FOREIGN KEY (election) REFERENCES election(election);


--
-- Name: plan_election plan_election_plan_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY plan_election
    ADD CONSTRAINT plan_election_plan_fkey FOREIGN KEY (plan) REFERENCES plan(plan);


--
-- Name: plan_unite plan_unite_plan_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY plan_unite
    ADD CONSTRAINT plan_unite_plan_fkey FOREIGN KEY (plan) REFERENCES plan(plan);


--
-- Name: plan_unite plan_unite_unite_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY plan_unite
    ADD CONSTRAINT plan_unite_unite_fkey FOREIGN KEY (unite) REFERENCES unite(unite);


--
-- Name: unite unite_canton_parent_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY unite
    ADD CONSTRAINT unite_canton_parent_fkey FOREIGN KEY (canton) REFERENCES canton(canton);


--
-- Name: unite unite_circonscrition_parent_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY unite
    ADD CONSTRAINT unite_circonscrition_parent_fkey FOREIGN KEY (circonscription) REFERENCES circonscription(circonscription);


--
-- Name: unite unite_commune_parent_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY unite
    ADD CONSTRAINT unite_commune_parent_fkey FOREIGN KEY (commune) REFERENCES commune(commune);


--
-- Name: unite unite_département_parent_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY unite
    ADD CONSTRAINT "unite_département_parent_fkey" FOREIGN KEY (departement) REFERENCES departement(departement);


--
-- Name: unite unite_type_unite_contenu_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY unite
    ADD CONSTRAINT unite_type_unite_contenu_fkey FOREIGN KEY (type_unite_contenu) REFERENCES type_unite(type_unite);


--
-- Name: web web_logo_fkey; Type: FK CONSTRAINT; Schema: openresultat; Owner: -
--

ALTER TABLE ONLY web
    ADD CONSTRAINT web_logo_fkey FOREIGN KEY (logo) REFERENCES om_logo(om_logo);


--
-- PostgreSQL database dump complete
--

