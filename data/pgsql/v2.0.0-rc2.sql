-------------------------------------------------------------------------------
-- Script de mise à jour vers la version v2.0.0-rc2 depuis la version v2.0.0-rc1
--
-- @package openresultat
-- @version SVN : $Id$
-------------------------------------------------------------------------------

--
-- BEGIN /  [#9494] Ergonomie et amélioration de la v2 
--
-- Supprimer les éléments inutiles de la base
--

DROP FUNCTION pr_maj_unite();
DROP VIEW participation_election_votant;
DROP VIEW unite_exprime;
