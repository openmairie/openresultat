-------------------------------------------------------------------------------
-- Script de mise à jour vers la version v2.0.0-rc1 depuis la version v2.0.0-b4
-- (non testé)
--
-- @package openresultat
-- @version SVN : $Id$
-------------------------------------------------------------------------------
-- modification

--
-- BEGIN / [#9400] Passer les bureaux de votes de la v2.0.0 dev en unite de saisie
--

-- Les bureaux de vote sont maintenant des unités de saisies, pouvant avoir des
-- types variés.
-- Les périmètres sont maintenant gérés en faisant des liens entre les unités.
-- Ces changements permettent d'avoir plus de souplesse lors de la saisie des
-- résultats et de pouvoir structurer le périmètre d'une élection

CREATE FUNCTION pr_maj_unite() RETURNS trigger
    LANGUAGE plpgsql
    SET search_path TO 'openresultat', 'public', 'pg_catalog'
    AS $$
DECLARE
BEGIN
   IF (TG_OP='INSERT' AND NEW.coordinates IS NOT NULL) THEN
    NEW.geom=ST_Transform(ST_SetSRID(cast(ST_MakePoint(cast(substr(NEW.coordinates,1,strpos( NEW.coordinates, ',')-1) as float),
      cast(substr(NEW.coordinates,strpos( NEW.coordinates, ',')+1) as float)) as geometry),4326),2154);
   ELSEIF(TG_OP='UPDATE') THEN
        IF (NEW.coordinates is null) THEN
      NEW.geom=NULL;
    ELSEIF (OLD.coordinates <> NEW.coordinates OR OLD.coordinates IS NULL) THEN
      NEW.geom=ST_Transform(ST_SetSRID(cast(ST_MakePoint(cast(substr(NEW.coordinates,1,strpos( NEW.coordinates, ',')-1) as float),
      cast(substr(NEW.coordinates,strpos( NEW.coordinates, ',')+1) as float)) as geometry),4326),2154);
    END IF;
   END IF;
   RETURN NEW;
END;
$$;

-- modification du nom des tables
ALTER TABLE
    bureau RENAME TO unite;

ALTER TABLE
    type_bureau RENAME TO type_unite;

ALTER TABLE
    participation_bureau RENAME TO participation_unite;

ALTER TABLE
    election_bureau RENAME TO election_unite;

ALTER TABLE
    perimetre_bureau RENAME TO lien_unite;

-- modification du nom des sequences
ALTER SEQUENCE bureau_seq RENAME TO unite_seq;

ALTER SEQUENCE election_bureau_seq RENAME TO election_unite_seq;

ALTER SEQUENCE participation_bureau_seq RENAME TO participation_unite_seq;

ALTER SEQUENCE perimetre_bureau_seq RENAME TO lien_unite_seq;

ALTER SEQUENCE type_bureau_seq RENAME TO type_unite_seq;

-- modification des attributs
-- Name: unite; Type: TABLE; Schema: openresultat; Owner: -
ALTER TABLE
    unite RENAME COLUMN bureau TO unite;

ALTER TABLE
    unite RENAME COLUMN type_bureau TO type_unite;

ALTER TABLE
    unite
ADD
    COLUMN om_validite_debut DATE;

ALTER TABLE
    unite
ADD
    COLUMN om_validite_fin DATE;

ALTER TABLE
    unite
ADD
    COLUMN type_unite_contenu INTEGER;

ALTER TABLE
    unite
ADD
    COLUMN perimetre BOOLEAN DEFAULT FALSE;

ALTER TABLE
    unite
ADD
    COLUMN code_canton CHARACTER VARYING(3);

ALTER TABLE
    unite
ADD
    COLUMN code_circonscription CHARACTER VARYING(3);

ALTER TABLE
    unite
ADD
    COLUMN code_commune CHARACTER VARYING(3);

ALTER TABLE
    unite
ADD
    COLUMN code_departement CHARACTER VARYING(3);

ALTER TABLE
    unite
ADD
    COLUMN code_bureau_vote CHARACTER VARYING(3);

ALTER TABLE
    unite
ADD
    CONSTRAINT unite_type_unite_contenu_fkey FOREIGN KEY (type_unite_contenu) REFERENCES type_unite;

-- Name: type_unite; Type: TABLE; Schema: openresultat; Owner: -
ALTER TABLE
    type_unite RENAME COLUMN type_bureau TO type_unite;

ALTER TABLE
    type_unite
ADD
    COLUMN hierarchie INTEGER NOT NULL;

ALTER TABLE
    type_unite
ADD
    COLUMN bureau_vote BOOLEAN DEFAULT TRUE;

-- Name: lien_unite; Type: TABLE; Schema: openresultat; Owner: -
ALTER TABLE
    lien_unite RENAME COLUMN perimetre_bureau TO lien_unite;

ALTER TABLE
    lien_unite RENAME COLUMN bureau TO unite_enfant;

ALTER TABLE
    lien_unite RENAME COLUMN perimetre TO unite_parent;

ALTER TABLE
    lien_unite DROP CONSTRAINT IF EXISTS perimetre_bureau_perimetre_fkey;

ALTER TABLE
    lien_unite
ADD
    CONSTRAINT lien_unite_unite_parent_fkey FOREIGN KEY (unite_parent) REFERENCES unite;

-- Name: participation_unite; Type: TABLE; Schema: openresultat; Owner: -
ALTER TABLE
    participation_unite RENAME COLUMN participation_bureau TO participation_unite;

ALTER TABLE
    participation_unite RENAME COLUMN election_bureau TO election_unite;

-- Name: election_unite; Type: TABLE; Schema: openresultat; Owner: -
ALTER TABLE
    election_unite RENAME COLUMN election_bureau TO election_unite;

ALTER TABLE
    election_unite RENAME COLUMN bureau TO unite;

-- Name: election; Type: TABLE; Schema: openresultat; Owner: -
ALTER TABLE
    election DROP CONSTRAINT IF EXISTS election_perimetre_fkey;

ALTER TABLE
    election RENAME COLUMN creation_bureau TO creation_unite;

ALTER TABLE
    election
ADD
    CONSTRAINT election_unite_fkey FOREIGN KEY (perimetre) REFERENCES unite;

-- Name: election_resultat; Type: TABLE; Schema: openresultat; Owner: -
ALTER TABLE
    election_resultat RENAME COLUMN election_bureau TO election_unite;

-- Name: perimetre; Type: TABLE; Schema: openresultat; Owner: -
DROP TABLE perimetre;



-- MODIFICATION DES VUES
-- supression des vues
DROP VIEW IF EXISTS bureau_exprime;

DROP VIEW IF EXISTS election_cumul_bureau;

-- Name: unite_exprime; Type: VIEW; Schema: openresultat; Owner: -
--
CREATE VIEW unite_exprime AS
SELECT
    election_resultat.election_unite,
    sum(election_resultat.resultat) AS exprime
FROM
    election_resultat
GROUP BY
    election_resultat.election_unite;

-- Name: election_cumul_unite; Type: VIEW; Schema: openresultat; Owner: -
--
CREATE VIEW election_cumul_unite AS
SELECT
    election_unite.election,
    sum(election_unite.inscrit) AS inscrit,
    sum(election_unite.votant) AS votant,
    sum(election_unite.nul) AS nul,
    sum(election_unite.blanc) AS blanc,
    sum(election_unite.exprime) AS exprime
FROM
    election_unite
GROUP BY
    election_unite.election;
--
-- END / [#9400] Passer les bureaux de votes de la v2.0.0 dev en unite de saisie
--


--
-- BEGIN / [#9405] Effectuer les corrections nécessaires pour la 2.0.0-develop
--

-- Correction et amélioration du code en ajoutant notamment des commentaires

-- commentaire de la table unite
COMMENT ON COLUMN unite.unite IS 'Identifiant numérique unique de l''unité';
COMMENT ON COLUMN unite.libelle IS 'Nom de l''unité';
COMMENT ON COLUMN unite.type_unite IS 'Type de l''unité';
COMMENT ON COLUMN unite.ordre IS 'Ordonne les unités dans la saisie et les affichages';
COMMENT ON COLUMN unite.canton_prefecture IS 'Code du canton auquel est rattachée l''unité pour envoi à la préfecture';
COMMENT ON COLUMN unite.circonscription_prefecture IS 'Code de la circonscription auquelle est rattachée l''unité pour envoi à la préfecture';
COMMENT ON COLUMN unite.adresse1 IS 'Adresse de l''unité';
COMMENT ON COLUMN unite.adresse2 IS 'Complément d''adresse';
COMMENT ON COLUMN unite.cp IS 'Code postal';
COMMENT ON COLUMN unite.ville IS 'Nom de la ville de l''unité';
COMMENT ON COLUMN unite.geom IS 'Point représentant l''unité sur la carte (SIG)';
COMMENT ON COLUMN unite.coordinates IS 'Coordonnées de l''unité (SIG)';
COMMENT ON COLUMN unite.om_validite_debut IS 'Date de début de validité';
COMMENT ON COLUMN unite.om_validite_fin IS 'Date d''expiration';
COMMENT ON COLUMN unite.type_unite_contenu IS 'Type d''unité que l''unité contiens';
COMMENT ON COLUMN unite.perimetre IS 'Précise si l''unité est un périmètre ou pas, c''est à dire si elle contiens d''autres unités ou pas';
COMMENT ON COLUMN unite.code_canton IS 'Code du canton d''appartenance de l'' unité';
COMMENT ON COLUMN unite.code_circonscription IS 'Code du canton d''appartenance de l''unité, si elle se comporte comme un bureau de vote';
COMMENT ON COLUMN unite.code_commune IS 'Code de la commune d''appartenance de l''unité, si elle se comporte comme un bureau de vote';
COMMENT ON COLUMN unite.code_departement IS 'Code du département d''apartenance de l''unité, si elle se comporte comme un bureau de vote';
COMMENT ON COLUMN unite.code_bureau_vote IS 'Code de l''unité, si elle se comporte comme un bureau de vote';


-- commentaire de la table election
COMMENT ON COLUMN election.election IS 'Identifiant numérique unique de l''élection';
COMMENT ON COLUMN election.libelle IS 'Nom de l''élection';
COMMENT ON COLUMN election.code IS 'Code associé à l''élection';
COMMENT ON COLUMN election.tour IS 'Tour de l''élection';
COMMENT ON COLUMN election.principal IS 'Défini si il s''agit d''une simulation (non principal) ou d''une élection en cours (principal)';
COMMENT ON COLUMN election.type_election IS 'Type de l''élection (Municipale, Départementale, ...)';
COMMENT ON COLUMN election.date IS 'Date à laquelle se déroule l''élection';
COMMENT ON COLUMN election.perimetre IS 'Périmètre sur lequel se déroule l''élection et qui va déterminer les unités concernées';
COMMENT ON COLUMN election.votant_defaut IS 'Nombre de votant par défaut pour une élection non principale. 100 permet de faire une saisie de la première centaine';
COMMENT ON COLUMN election.creation_unite IS 'Indique si les unités de saisie ont été associées à l''élection';
COMMENT ON COLUMN election.heure_ouverture IS 'Heure à laquelle démarre l''élection. Sert à déterminer les tranches horaires pour la saisie de la participation';
COMMENT ON COLUMN election.heure_fermeture IS 'Heure à laquelle s''arrête l''élection. Sert à déterminer les tranches horaires pour la saisie de la participation';
COMMENT ON COLUMN election.creation_participation IS 'Indique si les tranches horaires ont été associées à l''élection';
COMMENT ON COLUMN election.cloture IS 'Cloture l''élection';
COMMENT ON COLUMN election.verrou IS 'Verrouille le paramètrage de l''élection';
COMMENT ON COLUMN election.envoi_initial IS 'Permet lors de l''envoi à la préfecture d''indiquer si il s''agit d''un envoi initial ou pas';
COMMENT ON COLUMN election.affichage_1 IS 'Determine ce qui est envoyé à l''animation 1 : résultat, participation ou liste';
COMMENT ON COLUMN election.refresh_1 IS 'Détermine le temps de raffraichissement de l''animation 1';
COMMENT ON COLUMN election.affichage_2 IS 'Determine ce qui est envoyé à l''animation 1 : résultat, participation ou liste';
COMMENT ON COLUMN election.refresh_2 IS 'Détermine le temps de raffraichissement de l''animation 2';

-- commentaire de la table type_unite
COMMENT ON COLUMN type_unite.type_unite IS 'Identifiant numérique unique du type d''unité';
COMMENT ON COLUMN type_unite.libelle IS 'Nom du type d''unité';
COMMENT ON COLUMN type_unite.hierarchie IS 'Hierarchie du type permettant de structurer les périmètres, une unité avec un type d''unité de faible hiérarchie ne pourra pas contenir une unité de type ayant une hiérarchie plus importante';
COMMENT ON COLUMN type_unite.bureau_vote IS 'Détermine si un type d''unité à le comportement d''un bureau de vote';

-- commentaire de la table type_election
COMMENT ON COLUMN type_election.type_election IS 'Identifiant numérique unique du type d''élection';
COMMENT ON COLUMN type_election.libelle IS 'Nom du type d''élection';
COMMENT ON COLUMN type_election.code IS 'Champ de codification interne pour identifier le type';
COMMENT ON COLUMN type_election.prefecture IS 'Code donné par la préfecture et qui sert dans le(s) envoi(s) à la préfecture';

-- commentaire de la table lien_unite
COMMENT ON COLUMN lien_unite.lien_unite IS 'Identifiant numérique unique du lien entre les unités';
COMMENT ON COLUMN lien_unite.unite_parent IS 'Identifiant de l''unité qui contiens l''autre unité';
COMMENT ON COLUMN lien_unite.unite_enfant IS 'Identifiant de l''unité qui est contenue';

-- commentaire de la table tranche
COMMENT ON COLUMN tranche.tranche IS 'Identifiant numérique unique de la tranche horaire';
COMMENT ON COLUMN tranche.libelle IS 'Nom de la tranche horaire';
COMMENT ON COLUMN tranche.ordre IS 'Ordre des tranches dans la saisie et les affichages';

-- commentaire de la table candidat
COMMENT ON COLUMN candidat.candidat IS 'Identifiant numérique unique du candidat';
COMMENT ON COLUMN candidat.libelle IS 'Nom du candidat, de la liste ou autre';
COMMENT ON COLUMN candidat.libelle_long IS 'Libellé plus long si nécessaire';

-- commentaire de la table election_unite
COMMENT ON COLUMN election_unite.election_unite IS 'Identifiant numérique unique d''une unité de l''élection';
COMMENT ON COLUMN election_unite.election IS 'Identifiant numérique unique faisant référence à l''élection';
COMMENT ON COLUMN election_unite.unite IS 'Identifiant numérique unique faisant référence à l''unité';
COMMENT ON COLUMN election_unite.inscrit IS 'Nombre d''inscrit';
COMMENT ON COLUMN election_unite.votant IS 'Nombre de votant';
COMMENT ON COLUMN election_unite.blanc IS 'Nombre de vote blanc';
COMMENT ON COLUMN election_unite.nul IS 'Nombre de vote nul';
COMMENT ON COLUMN election_unite.exprime IS 'Nombre de vote exprimé';
COMMENT ON COLUMN election_unite.envoi_aff IS 'Indique si les résultats de l''unité, pour cette élection, ont été envoyé à l''animation';
COMMENT ON COLUMN election_unite.envoi_web IS 'Indique si les résultats de l''unité, pour cette élection, ont été envoyé à la page web';

-- commentaire de la table participation_election
COMMENT ON COLUMN participation_election.participation_election IS 'Identifiant numérique unique d''une tranche horaire de participation à une élection';
COMMENT ON COLUMN participation_election.election IS 'Identifiant numérique unique faisant référence à l''élection';
COMMENT ON COLUMN participation_election.tranche IS 'Identifiant numérique unique faisant référence à la tranche horaire';
COMMENT ON COLUMN participation_election.envoi_aff IS 'Indique si la participation de cette tranche, pour cette élection, a été envoyé à l''animation';
COMMENT ON COLUMN participation_election.envoi_web IS 'Indique si la participation de cette tranche, pour cette élection, a été envoyé à la page web';

-- commentaire de la table election_candidat
COMMENT ON COLUMN election_candidat.election_candidat IS 'Identifiant numérique unique d''un candidat de l''élection';
COMMENT ON COLUMN election_candidat.election IS 'Identifiant numérique unique faisant référence à l''élection';
COMMENT ON COLUMN election_candidat.candidat IS 'Identifiant numérique unique faisant référence au candidat';
COMMENT ON COLUMN election_candidat.ordre IS 'Ordre du candidat dans l''élection';
COMMENT ON COLUMN election_candidat.prefecture IS 'Code préfecture du candidat ou de la liste';
COMMENT ON COLUMN election_candidat.creation_resultat IS 'Indique si les résultats du candidat sont créés ou pas';
COMMENT ON COLUMN election_candidat.age_moyen IS 'Age moyen de la liste municipale';
COMMENT ON COLUMN election_candidat.siege IS 'Nombre de siège obtenu par la liste municipale';
COMMENT ON COLUMN election_candidat.age_moyen_com IS 'Age_moyen de la liste communautaire';
COMMENT ON COLUMN election_candidat.siege_com IS 'Nombre de siège obtenu par la liste communautaire';

-- commentaire de la table election_resultat
COMMENT ON COLUMN election_resultat.election_resultat IS 'Identifiant numérique unique du résultat d''un candidat dans une unité de l''élection';
COMMENT ON COLUMN election_resultat.election_unite IS 'Identifiant numérique unique faisant référence à une unité de l''élection';
COMMENT ON COLUMN election_resultat.election_candidat IS 'Identifiant numérique unique faisant référence à un candidat de l''élection';
COMMENT ON COLUMN election_resultat.resultat IS 'Nombre de voix obtenues par le candidat';

-- commentaire de la table participation_unite
COMMENT ON COLUMN participation_unite.participation_unite IS 'Identifiant numérique unique de la participation pour une unité de l''élection';
COMMENT ON COLUMN participation_unite.election_unite IS 'Identifiant numérique unique faisant référence à une unité de l''élection';
COMMENT ON COLUMN participation_unite.participation_election IS 'Identifiant numérique unique faisant référence à une tranche horaire de participation à l''élection';
COMMENT ON COLUMN participation_unite.votant IS 'Nombre de personnes ayant voté';

-- commentaire de la vue candidat_resultat
COMMENT ON COLUMN candidat_resultat.election_candidat IS 'Identifiant numérique unique faisant référence à un candidat de l''élection';
COMMENT ON COLUMN candidat_resultat.resultat IS 'Total des voix du candidat';

-- commentaire de la vue election_cumul_unite
COMMENT ON COLUMN election_cumul_unite.election IS 'Identifiant numérique unique faisant référence à l''élection';
COMMENT ON COLUMN election_cumul_unite.inscrit IS 'Total des inscrits à l''élection';
COMMENT ON COLUMN election_cumul_unite.votant IS 'Total des votants de l''élection';
COMMENT ON COLUMN election_cumul_unite.nul IS 'Total des votes nuls de l''élection';
COMMENT ON COLUMN election_cumul_unite.blanc IS 'Total des votes blancs de l''élection';
COMMENT ON COLUMN election_cumul_unite.exprime IS 'Total des votes exprimés de l''élection';

-- commentaire de la vue election_exprime
COMMENT ON COLUMN election_exprime.election IS 'Identifiant numérique unique faisant référence à l''élection';
COMMENT ON COLUMN election_exprime.exprime IS 'Total des votes exprimés de l''élection';

-- commentaire de la vue participation_election_votant
COMMENT ON COLUMN participation_election_votant.participation_election IS 'Identifiant numérique unique faisant référence à une tranche horaire de participation à l''élection';
COMMENT ON COLUMN participation_election_votant.votant IS 'Total de la participation enregistrée';

-- commentaire de la vue unite_exprime
COMMENT ON COLUMN unite_exprime.election_unite IS 'Identifiant numérique unique faisant référence à une unité de l''élection';
COMMENT ON COLUMN unite_exprime.exprime IS 'Total des votes exprimés pour une unité au cours de l''élection';
--
-- END / [#9405] Effectuer les corrections nécessaires pour la 2.0.0-develop
--

--
-- BEGIN / [#9401] Ajouter les attributs emargement et procuration
--
-- ajout des attributs emargement et procuration pour la saisie des résultats

ALTER TABLE
    election_unite
ADD
    COLUMN emargement INTEGER;

ALTER TABLE
    election_unite
ADD
    COLUMN procuration INTEGER;

-- commentaires
COMMENT ON COLUMN election_unite.emargement IS 'Nombre de personne ayant émargées';
COMMENT ON COLUMN election_unite.procuration IS 'Nombre de procuration';

--
-- END / [#9401] Ajouter les attributs emargement et procuration
--

--
-- BEGIN /  [#9413] Ajouter les tables cantons, circonscription, commune et département
--
-- ajout des tables cantons, circonscriptions, commune et département. Lien entre ces tables
-- et les différents code de la table unité
--
-- Name: canton; Type: TABLE; Schema: openresultat; Owner: -
--
CREATE TABLE canton (
    canton INTEGER NOT NULL,
    libelle CHARACTER VARYING(30) NOT NULL,
    code CHARACTER VARYING(5) NOT NULL,
    prefecture INTEGER NOT NULL
);

ALTER TABLE
    canton
ADD
    CONSTRAINT canton_pkey PRIMARY KEY (canton);

--
-- Name: circonscription; Type: TABLE; Schema: openresultat; Owner: -
--
CREATE TABLE circonscription (
    circonscription INTEGER NOT NULL,
    libelle CHARACTER VARYING(30) NOT NULL,
    code CHARACTER VARYING(5) NOT NULL,
    prefecture INTEGER NOT NULL
);


ALTER TABLE
    circonscription
ADD
    CONSTRAINT circonscription_pkey PRIMARY KEY (circonscription);

--
-- Name: circonscription_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE circonscription_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: commune; Type: TABLE; Schema: openresultat; Owner: -
--
CREATE TABLE commune (
    commune INTEGER NOT NULL,
    libelle CHARACTER VARYING(30) NOT NULL,
    code CHARACTER VARYING(5) NOT NULL,
    prefecture INTEGER NOT NULL
);

ALTER TABLE
    commune
ADD
    CONSTRAINT commune_pkey PRIMARY KEY (commune);

--
-- Name: commune_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE commune_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: departement; Type: TABLE; Schema: openresultat; Owner: -
--
CREATE TABLE departement (
    departement INTEGER NOT NULL,
    libelle CHARACTER VARYING(30) NOT NULL,
    code CHARACTER VARYING(3) NOT NULL,
    prefecture INTEGER NOT NULL
);

ALTER TABLE
    departement
ADD
    CONSTRAINT departement_pkey PRIMARY KEY (departement);

--
-- Name: departement_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE departement_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- modification des codes de la tables unités
-- Les codes sont remplacés par des références vers les tables correspondantes
-- donc, pour chaque attribut de code, on le renomme et on change son type

-- code_canton
ALTER TABLE
    unite
RENAME COLUMN
    code_canton TO canton;

ALTER TABLE
    unite
ALTER COLUMN
    canton TYPE INTEGER USING canton :: INTEGER;

-- code_circonscription
ALTER TABLE
    unite
RENAME COLUMN
    code_circonscription TO circonscription;

ALTER TABLE
    unite
ALTER COLUMN
    circonscription TYPE INTEGER USING circonscription :: INTEGER;

-- code_commune
ALTER TABLE
    unite
RENAME COLUMN
    code_commune TO commune;

ALTER TABLE
    unite
ALTER COLUMN
    commune TYPE INTEGER USING commune :: INTEGER;

-- code_departement
ALTER TABLE
    unite
RENAME COLUMN
    code_departement TO departement;

ALTER TABLE
    unite
ALTER COLUMN
    departement TYPE INTEGER USING departement :: INTEGER;

-- clés étrangères
ALTER TABLE
    unite
ADD
    CONSTRAINT unite_canton_parent_fkey FOREIGN KEY (canton) REFERENCES canton;

ALTER TABLE
    unite
ADD
    CONSTRAINT unite_circonscrition_parent_fkey FOREIGN KEY (circonscription) REFERENCES circonscription;

ALTER TABLE
    unite
ADD
    CONSTRAINT unite_commune_parent_fkey FOREIGN KEY (commune) REFERENCES commune;

ALTER TABLE
    unite
ADD
    CONSTRAINT unite_département_parent_fkey FOREIGN KEY (departement) REFERENCES departement;

-- commentaires
COMMENT ON COLUMN canton.canton IS 'Identifiant du canton';
COMMENT ON COLUMN canton.libelle IS 'Libellé du canton';
COMMENT ON COLUMN canton.code IS 'Code du canton';
COMMENT ON COLUMN canton.prefecture IS 'Code préfecture du canton';

COMMENT ON COLUMN circonscription.circonscription IS 'Identifiant de la circonscription';
COMMENT ON COLUMN circonscription.libelle IS 'Libellé de la circonscription';
COMMENT ON COLUMN circonscription.code IS 'Code de la circonscription';
COMMENT ON COLUMN circonscription.prefecture IS 'Code préfecture de la circonscription';

COMMENT ON COLUMN commune.commune IS 'Identifiant de la commune';
COMMENT ON COLUMN commune.libelle IS 'Libellé de la commune';
COMMENT ON COLUMN commune.code IS 'Code de la commune';
COMMENT ON COLUMN commune.prefecture IS 'Code préfecture de la commune';

COMMENT ON COLUMN departement.departement IS 'Identifiant du departement';
COMMENT ON COLUMN departement.libelle IS 'Libellé du departement';
COMMENT ON COLUMN departement.code IS 'Code du departement';
COMMENT ON COLUMN departement.prefecture IS 'Code préfecture du departement';

COMMENT ON COLUMN unite.canton IS 'Canton d''appartenance de l'' unité';
COMMENT ON COLUMN unite.circonscription IS 'Circonscription d''appartenance de l''unité, si elle se comporte comme un bureau de vote';
COMMENT ON COLUMN unite.commune IS 'Commune d''appartenance de l''unité, si elle se comporte comme un bureau de vote';
COMMENT ON COLUMN unite.departement IS 'Département d''apartenance de l''unité, si elle se comporte comme un bureau de vote';

--
-- END /  [#9413] Ajouter les tables cantons, circonscription, commune et département
--

--
-- BEGIN /  [#9415] Différenciation des unités de type bureau de vote
--
-- Dans une élection les bureaux de vote ont un caractère particulier par rapport aux autres unités de saisie.
-- Le but de cette évolution est donc de mettre en place le comportement particulier des bureaux de vote.
-- Cela consiste à utiliser uniquement les résultats des unités ayant ce comportement pour faire les animations,
-- les éditions, les exports, le calcul des sièges et les résultats de la page web
-- De plus, seules les unités ayant ce comportement ont besoin des codes cantons, circonscriptions, dept et communes
--
-- Name: canton; Type: TABLE; Schema: openresultat; Owner: -
--

-- modification de la vue permettant de récupérer les résultats des candidats
-- la somme des résultats est maintenant faite uniquement à partir des unités
-- ayant un comportement de bureau de vote

CREATE OR REPLACE VIEW candidat_resultat AS
SELECT
    election_resultat.election_candidat,
    sum(election_resultat.resultat) AS resultat
FROM
    election_resultat
    INNER JOIN election_unite ON election_resultat.election_unite = election_unite.election_unite
    INNER JOIN unite ON election_unite.unite = unite.unite
    INNER JOIN type_unite ON unite.type_unite = type_unite.type_unite
WHERE
    type_unite.bureau_vote = true
GROUP BY
    election_resultat.election_candidat;

-- modification de la vue permettant de récupérer les résultats d'une élection
-- la somme des résultats est maintenant faite uniquement à partir des unités
-- ayant un comportement de bureau de vote

CREATE OR REPLACE VIEW election_cumul_unite AS
SELECT
    election_unite.election,
    sum(election_unite.inscrit) AS inscrit,
    sum(election_unite.votant) AS votant,
    sum(election_unite.nul) AS nul,
    sum(election_unite.blanc) AS blanc,
    sum(election_unite.exprime) AS exprime
FROM
    election_unite
    INNER JOIN unite ON election_unite.unite = unite.unite
    INNER JOIN type_unite ON unite.type_unite = type_unite.type_unite
WHERE
    type_unite.bureau_vote = true
GROUP BY
    election_unite.election;

-- modification de la vue permettant de récupérer la participation totale d'une élection
-- la somme des participations est maintenant faite uniquement à partir des unités
-- ayant un comportement de bureau de vote

CREATE OR REPLACE VIEW participation_election_votant AS
SELECT
    participation_election.participation_election,
    sum(participation_unite.votant) AS votant
FROM
    participation_unite
    INNER JOIN participation_election
        ON participation_election.participation_election
            = participation_unite.participation_election
    INNER JOIN election_unite ON participation_unite.election_unite = election_unite.election_unite
    INNER JOIN unite ON election_unite.unite = unite.unite
    INNER JOIN type_unite ON unite.type_unite = type_unite.type_unite
WHERE
    type_unite.bureau_vote = 't'
GROUP BY
    participation_election.participation_election;

-- augmentation de la taille des chaines de caractères des affichages
-- car le nom de certains template de l'affichage sont trop grand
ALTER TABLE election
    ALTER affichage_1 TYPE CHARACTER VARYING(50) USING affichage_1 :: CHARACTER VARYING(50);

ALTER TABLE election
    ALTER affichage_2 TYPE CHARACTER VARYING(50) USING affichage_2 :: CHARACTER VARYING(50);
--
-- END /  [#9415] Différenciation des unités de type bureau de vote
--


--
-- BEGIN /  [#9418] Gestion des périmètres
--
-- La gestion des périmètres consiste à effectuer automatiquement le
-- calcul des résultats de toutes les unités étant des périmètres. Il
-- s'agit également de les distinguer, des autres unités, dans les affichages.
--

--
-- Name: election; Type: TABLE; Schema: openresultat; Owner: -
--
-- Attribut permettant de paramétrer le périmètre à partir duquel l'affichage
-- sera réalisé et le type d'unité à afficher

-- Pour l'affichage 1
-- Attribut de paramétrage du périmètre

ALTER TABLE
    election
ADD
    COLUMN perimetre_aff1 INTEGER;

ALTER TABLE
    election
ADD
    CONSTRAINT election_unite_aff1_fkey FOREIGN KEY (perimetre_aff1) REFERENCES unite;

-- Attribut de paramétrage du type

ALTER TABLE
    election
ADD
    COLUMN type_aff1 INTEGER;

ALTER TABLE
    election
ADD
    CONSTRAINT election_type_unite_aff1_fkey FOREIGN KEY (type_aff1) REFERENCES type_unite;

-- Pour l'affichage 2
-- Attribut de paramétrage du périmètre

ALTER TABLE
    election
ADD
    COLUMN perimetre_aff2 INTEGER;

ALTER TABLE
    election
ADD
    CONSTRAINT election_unite_aff2_fkey FOREIGN KEY (perimetre_aff2) REFERENCES unite;

-- Attribut de paramétrage du type

ALTER TABLE
    election
ADD
    COLUMN type_aff2 INTEGER;

ALTER TABLE
    election
ADD
    CONSTRAINT election_type_unite_aff2_fkey FOREIGN KEY (type_aff2) REFERENCES type_unite;
-- commentaires
COMMENT ON COLUMN election.type_aff1 IS 'Paramétrage du type d''unité à afficher pour le 1er ecran d''affichage';
COMMENT ON COLUMN election.perimetre_aff1 IS 'Paramétrage du périmètre de référence pour le 1er ecran d''affichage';
COMMENT ON COLUMN election.type_aff2 IS 'Paramétrage du type d''unité à afficher pour le 2eme ecran d''affichage';
COMMENT ON COLUMN election.perimetre_aff2 IS 'Paramétrage du périmètre de référence pour le 2eme ecran d''affichage';

--
-- END /  [#9418] Gestion des périmètres
--
--
-- BEGIN /  [#9430] Gestion des centaines
--
-- La saisie par centaine permet de simuler les résultats d'une élection
-- à partir des résultats de la 1ère, 2éme ou n ième centaine enregistrée.
--

-- Attribut permettant de savoir si c'est une centaine ou pas
ALTER TABLE
    election
ADD
    COLUMN is_centaine BOOLEAN DEFAULT FALSE;

-- Attribut permettant d'obtenir l'élection de référence pour une centaine
ALTER TABLE
    election
ADD
    COLUMN election_reference INTEGER;

ALTER TABLE
    election
ADD
    CONSTRAINT election_reference_election_fkey FOREIGN KEY (election_reference) REFERENCES election;

-- commentaires
COMMENT ON COLUMN election.is_centaine IS 'Paramètre permettant de savoir si il s''agit d''une simulation ou d''une élection';
COMMENT ON COLUMN election.election_reference IS 'Paramètre permettant de savoir quelle élection est concerné par la saisie par centaine';

--
-- END /  [#9430] Gestion des centaines
--

--
-- BEGIN /  [#9433] Personnalisation de l'animation
--
-- Permettre la personnalisation des animations. Ajouter une table
-- dédiée à la configuration des animations
--

-- Suppression des attributs de la table élection servant à paramétrer les animations
ALTER TABLE
    election
DROP
    COLUMN affichage_1;

ALTER TABLE
    election
DROP
    COLUMN refresh_1;

ALTER TABLE
    election
DROP
    COLUMN perimetre_aff1;

ALTER TABLE
    election
DROP
    COLUMN type_aff1;

ALTER TABLE
    election
DROP
    COLUMN affichage_2;

ALTER TABLE
    election
DROP
    COLUMN refresh_2;

ALTER TABLE
    election
DROP
    COLUMN perimetre_aff2;

ALTER TABLE
    election
DROP
    COLUMN type_aff2;

-- Creation de la table de paramétrage des animations
CREATE TABLE animation (
    animation INTEGER NOT NULL,
    election INTEGER,
    libelle CHARACTER VARYING(60) NOT NULL,
    refresh INTEGER NOT NULL,
    modele INTEGER,
    is_modele BOOLEAN DEFAULT FALSE,
    actif BOOLEAN DEFAULT FALSE,
    titre CHARACTER VARYING(30),
    sous_titre CHARACTER VARYING(100),
    couleur_titre CHARACTER VARYING(13),
    couleur_bandeau CHARACTER VARYING(13),
    logo INTEGER,
    perimetre_aff INTEGER,
    type_aff INTEGER,
    affichage text,
    election_comparaison INTEGER,
    is_config BOOLEAN DEFAULT FALSE
);

-- Création de la séquence
CREATE SEQUENCE animation_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Ajout des contraintes
-- Clé primaire
ALTER TABLE
    animation
ADD
    CONSTRAINT animation_pkey PRIMARY KEY (animation);

-- Clés étrangères
ALTER TABLE
    animation
ADD
    CONSTRAINT animation_election_fkey FOREIGN KEY (election) REFERENCES election;

ALTER TABLE
    animation
ADD
    CONSTRAINT animation_logo_fkey FOREIGN KEY (logo) REFERENCES om_logo;

ALTER TABLE
    animation
ADD
    CONSTRAINT animation_perimetre_fkey FOREIGN KEY (perimetre_aff) REFERENCES unite;

ALTER TABLE
    animation
ADD
    CONSTRAINT animation_type_aff_fkey FOREIGN KEY (type_aff) REFERENCES type_unite;

ALTER TABLE
    animation
ADD
    CONSTRAINT animation_modele_fkey FOREIGN KEY (modele) REFERENCES animation;

ALTER TABLE
    animation
ADD
    CONSTRAINT animation_election_comparaison_fkey FOREIGN KEY (election_comparaison) REFERENCES election;

-- commentaires des colonnes de la table
COMMENT ON COLUMN animation.animation IS 'Identifiant de l''animation';
COMMENT ON COLUMN animation.election IS 'Identifiant de l''élection pour laquelle l''affichage est paramétré';
COMMENT ON COLUMN animation.libelle IS 'Libelle de l''animation';
COMMENT ON COLUMN animation.refresh IS 'Définit l''intervalle de temps entre chaque raffraichissement de l''animation';
COMMENT ON COLUMN animation.titre IS 'Titre de la page de l''affichage';
COMMENT ON COLUMN animation.sous_titre IS 'Sous-titre de la page de l''affichage';
COMMENT ON COLUMN animation.couleur_titre IS 'Définit la couleur du titre';
COMMENT ON COLUMN animation.couleur_bandeau IS 'Définit la couleur du bandeau de titre';
COMMENT ON COLUMN animation.logo IS 'Logo à afficher';
COMMENT ON COLUMN animation.perimetre_aff IS 'Périmètre choisit pour l''affichage';
COMMENT ON COLUMN animation.type_aff IS 'Type des unités à afficher';
COMMENT ON COLUMN animation.modele IS 'Modèle de mise en page de l''animation';
COMMENT ON COLUMN animation.is_modele IS 'Permet d''enregistrer la configuration de l''animation comme modèle';
COMMENT ON COLUMN animation.affichage IS 'Paramétrage de l''animation, choix des blocs à afficher, des diagrammes, etc.';
COMMENT ON COLUMN animation.is_config IS 'Différencie les animations pré-configurée de celle créées par les utilisateurs';
COMMENT ON COLUMN animation.election_comparaison IS 'Election servant de comparaison pour la participation';
COMMENT ON COLUMN animation.actif IS 'Permet de définir si les animations sont actives/utilisables ou pas';

-- Attribut permettant de récupérer une photo pour un candidat

ALTER TABLE
    election_candidat
ADD
    COLUMN couleur CHARACTER VARYING(13);

-- Pour l'affichage c'est le numéro d'ordre du candidat qui a été
-- privilégié. Pour être sur que ce numéro ne soit attribué qu'à
-- un seul candidat de l'élection, une contrainte d'unicité du couple
-- "election, ordre" de la table "election_candidat" est ajouté

ALTER TABLE
    election_candidat
ADD
    CONSTRAINT ordre_unique UNIQUE (ordre, election);

ALTER TABLE
    election_candidat
ADD
    COLUMN photo CHARACTER VARYING(100);

-- commentaires du nouvel attribut
COMMENT ON COLUMN election_candidat.photo IS 'Photo du candidat';
COMMENT ON COLUMN election_candidat.couleur IS 'Couleur associée au candidat dans l''animation';

--
-- END /  [#9433] Personnalisation de l'animation
--

--
-- BEGIN /  [#9402] Gestion et personnalisation des plans
--
-- Permettre d'importer des plans et de placer les différentes unités dessus.
-- Les plans doivent également être personnalisable, c'est à dire que la forme
-- du pointeur des unités et sa couleur doivent être choisis par l'utilisateur.
--

--
-- Name: plan; Type: TABLE; Schema: openresultat; Owner: -
--
CREATE TABLE plan (
    plan INTEGER NOT NULL,
    libelle CHARACTER VARYING(50) NOT NULL,
    image_plan CHARACTER VARYING(100) NOT NULL,
    img_unite_arrivee CHARACTER VARYING(100) NOT NULL,
    img_unite_non_arrivee CHARACTER VARYING(100) NOT NULL,
    par_defaut BOOLEAN
);

COMMENT ON COLUMN plan.plan IS 'Identifiant du plan';
COMMENT ON COLUMN plan.libelle IS 'Nom du plan';
COMMENT ON COLUMN plan.image_plan IS 'Image du plan';
COMMENT ON COLUMN plan.img_unite_arrivee IS 'Image par défaut des unités arrivées';
COMMENT ON COLUMN plan.img_unite_non_arrivee IS 'Image par défaut des unités non arrivées';
COMMENT ON COLUMN plan.par_defaut IS 'Permet de savoir si le plan doit être ajouté par défaut à l''élecrtion';

CREATE SEQUENCE plan_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE
    plan
ADD
    CONSTRAINT plan_pkey PRIMARY KEY (plan);

--
-- Name: plan_unite; Type: TABLE; Schema: openresultat; Owner: -
--
CREATE TABLE plan_unite (
    plan_unite INTEGER NOT NULL,
    plan INTEGER NOT NULL,
    unite INTEGER NOT NULL,
    img_unite_arrivee CHARACTER VARYING(100),
    img_unite_non_arrivee CHARACTER VARYING(100),
    texte_image CHARACTER VARYING(30),
    couleur_texte CHARACTER VARYING(13),
    position_x INTEGER,
    position_y INTEGER
);

COMMENT ON COLUMN plan_unite.plan_unite IS 'Identifiant du plan';
COMMENT ON COLUMN plan_unite.plan IS 'Nom du plan';
COMMENT ON COLUMN plan_unite.unite IS 'Image du plan';
COMMENT ON COLUMN plan_unite.img_unite_arrivee IS 'Image affichée si l''unité est arrivée';
COMMENT ON COLUMN plan_unite.img_unite_non_arrivee IS 'Image affichée si l''unité n''est pas arrivée';
COMMENT ON COLUMN plan_unite.texte_image IS 'Texte présent sur l''image';
COMMENT ON COLUMN plan_unite.couleur_texte IS 'Couleur du texte';
COMMENT ON COLUMN plan_unite.position_x IS 'Coordonnée en x, en pixel, de l''image sur le plan';
COMMENT ON COLUMN plan_unite.position_y IS 'Coordonnée en y, en pixel, de l''image sur le plan';

CREATE SEQUENCE plan_unite_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE
    plan_unite
ADD
    CONSTRAINT plan_unite_pkey PRIMARY KEY (plan_unite);

ALTER TABLE
    plan_unite
ADD
    CONSTRAINT plan_unite_plan_fkey FOREIGN KEY (plan) REFERENCES plan;

ALTER TABLE
    plan_unite
ADD
    CONSTRAINT plan_unite_unite_fkey FOREIGN KEY (unite) REFERENCES unite;

ALTER TABLE
    plan_unite
ADD
    CONSTRAINT plan_unite_unique UNIQUE(plan, unite);

--
-- Name: plan_election; Type: TABLE; Schema: openresultat; Owner: -
--
CREATE TABLE plan_election (
    plan_election INTEGER NOT NULL,
    plan INTEGER NOT NULL,
    election INTEGER NOT NULL
);

COMMENT ON COLUMN plan_election.plan_election IS 'Identifiant du lien entre le plan et l''election';
COMMENT ON COLUMN plan_election.plan IS 'Identifiant du plan';
COMMENT ON COLUMN plan_election.election IS 'Identifiant de l''election';

CREATE SEQUENCE plan_election_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE
    plan_election
ADD
    CONSTRAINT plan_election_pkey PRIMARY KEY (plan_election);

ALTER TABLE
    plan_election
ADD
    CONSTRAINT plan_election_plan_fkey FOREIGN KEY (plan) REFERENCES plan;

ALTER TABLE
    plan_election
ADD
    CONSTRAINT plan_election_election_fkey FOREIGN KEY (election) REFERENCES election;

--
-- Name: web; Type: TABLE; Schema: openresultat; Owner: -
--
CREATE TABLE web (
    web INTEGER NOT NULL,
    libelle CHARACTER VARYING(30) NOT NULL,
    logo INTEGER,
    entete CHARACTER VARYING(100),
    url_collectivite CHARACTER VARYING(100),
    libelle_url CHARACTER VARYING(30),
    feuille_style text
);

COMMENT ON COLUMN web.web IS 'Identifiant de l''affichage web';
COMMENT ON COLUMN web.libelle IS 'Nom du modèle';
COMMENT ON COLUMN web.logo IS 'Logo a afficher sur la page';
COMMENT ON COLUMN web.entete IS 'Titre de l''entête de l''affichage web';
COMMENT ON COLUMN web.url_collectivite IS 'url vers le site de la collectivité';
COMMENT ON COLUMN web.libelle_url IS 'texte du lien / url';
COMMENT ON COLUMN web.feuille_style IS 'Paramétrage de la feuille de style';

CREATE SEQUENCE web_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE
    web
ADD
    CONSTRAINT web_pkey PRIMARY KEY (web);

ALTER TABLE
    web
ADD
    CONSTRAINT web_logo_fkey FOREIGN KEY (logo) REFERENCES om_logo;
--
-- Name: election; Type: TABLE; Schema: openresultat; Owner: -
--

ALTER TABLE
    election
ADD
    COLUMN web INTEGER NOT NULL;

ALTER TABLE
    election
ADD
    CONSTRAINT election_web_fkey FOREIGN KEY (web) REFERENCES web;

--
-- END /  [#9402] Gestion et personnalisation des plans
--

--
-- BEGIN /  [#9466] Workflow 
--
-- Mettre un place un worflow de l'élection et améliorer l'ergonomie de
-- l'application.
--

--
-- Name: election; Type: TABLE; Schema: openresultat; Owner: -
--
ALTER TABLE
    election
ADD
    COLUMN workflow CHARACTER VARYING(15);

ALTER TABLE
    election
ADD
    CONSTRAINT etape_workflow CHECK (workflow IN ('Paramétrage', 'Simulation', 'Saisie', 'Finalisation', 'Archivage'));

ALTER TABLE
    election
DROP
    COLUMN cloture;

ALTER TABLE
    election
DROP
    COLUMN verrou;

ALTER TABLE
    election
DROP
    COLUMN creation_unite;

ALTER TABLE
    election
DROP
    COLUMN creation_participation;

ALTER TABLE
    election
DROP
    COLUMN principal;

ALTER TABLE
    election
ADD
   COLUMN publication_auto BOOLEAN;

ALTER TABLE
    election
ADD
   COLUMN publication_erreur BOOLEAN;

ALTER TABLE
    election
ADD
   COLUMN calcul_auto_exprime BOOLEAN;

ALTER TABLE
    election
ADD
   COLUMN garder_resultat_simulation BOOLEAN;

COMMENT ON COLUMN election.workflow IS
    'Workflow de l''élection, les étapes sont : Paramétrage, Simulation, Saisie, Finalisation et Archivage';
COMMENT ON COLUMN election.publication_auto IS
    'Si cette option est activée, les résultats sont automatiquement publiés suite à leur saisie/modification';
COMMENT ON COLUMN election.publication_erreur IS
    'Si cette option n''est pas activé, seul les résultats corrects sont publiés';
COMMENT ON COLUMN election.calcul_auto_exprime IS
    'Défini si le nombre de vote exprimé est saisie par l''utilisateur ou automatiquement calculé';
COMMENT ON COLUMN election.garder_resultat_simulation IS
    'Défini si les résultats saisis pendant la simulation doivent être conservé par la suite';
--
-- Name: election_candidat; Type: TABLE; Schema: openresultat; Owner: -
--
ALTER TABLE
    election_candidat
DROP
    COLUMN creation_resultat;

-- Name: plan_election; Type: TABLE; Schema: openresultat; Owner: -
--
-- Contrainte d'unicité sur les plans permettant d'éviter qu'un même plan
-- soit ajouté plusieurs fois à l'élection, ce qui pose des problèmes lors
-- de l'affichage
ALTER TABLE
    plan_election
ADD
   CONSTRAINT plan_unique UNIQUE (plan, election);

--
-- Name: election_unite; Type: TABLE; Schema: openresultat; Owner: -
--
-- Ajout d'un attribut permettant de savoir l'état de la saisie des résultats

ALTER TABLE
    election_unite
ADD
   COLUMN saisie CHARACTER VARYING(10);

COMMENT ON COLUMN election_unite.saisie IS 'Etat de la saisie (en attente, en defaut, correcte)';

--
-- END /  [#9466] Workflow 
--

--
-- BEGIN /  [#9495] Délégation de saisie 
-- Mettre en place la délégation de saisie pour permettre aux différents acteurs
-- de ne saisir que les résultats de leur bureau.
--

--
-- Name: acteur; Type: TABLE; Schema: openresultat; Owner: -
--
CREATE TABLE acteur (
    acteur INTEGER NOT NULL,
    nom CHARACTER VARYING(30),
    login CHARACTER VARYING(30)
);

ALTER TABLE
    acteur
ADD
    CONSTRAINT acteur_pkey PRIMARY KEY (acteur);

CREATE SEQUENCE acteur_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

COMMENT ON COLUMN acteur.acteur IS 'Identifiant de l''acteur';
COMMENT ON COLUMN acteur.nom IS 'Nom de l''acteur';

--
-- Name: delegation; Type: TABLE; Schema: openresultat; Owner: -
--
CREATE TABLE delegation (
    delegation INTEGER NOT NULL,
    election INTEGER NOT NULL,
    unite INTEGER NOT NULL,
    acteur INTEGER NOT NULL
);

ALTER TABLE
    delegation
ADD
    CONSTRAINT delegation_pkey PRIMARY KEY (delegation);

ALTER TABLE
    delegation
ADD
    CONSTRAINT delegation_acteur_fkey FOREIGN KEY (acteur) REFERENCES acteur;

ALTER TABLE
    delegation
ADD
    CONSTRAINT delegation_unite_fkey FOREIGN KEY (unite) REFERENCES unite;

ALTER TABLE
    delegation
ADD
    CONSTRAINT delegation_election_fkey FOREIGN KEY (election) REFERENCES election;

CREATE SEQUENCE delegation_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

COMMENT ON COLUMN delegation.delegation IS 'Identifiant de la delegation';
COMMENT ON COLUMN delegation.acteur IS 'Identifiant de l''acteur concerné';
COMMENT ON COLUMN delegation.unite IS 'Identifiant de l''unite concernée';
COMMENT ON COLUMN delegation.election IS 'Identifiant de l''élection concernée';

--
-- Name: election_unite; Type: TABLE; Schema: openresultat; Owner: -
--
ALTER TABLE
    election_unite
ADD
    COLUMN validation CHARACTER VARYING(30);

COMMENT ON COLUMN election_unite.validation IS 'Statut de validation de la saisie : preparation, en attente de validation ou validee';

--
-- Name: participation_unite; Type: TABLE; Schema: openresultat; Owner: -
--
ALTER TABLE
    participation_unite
ADD
    COLUMN saisie BOOLEAN;

ALTER TABLE
    participation_unite
ADD
    COLUMN envoi_aff BOOLEAN;

ALTER TABLE
    participation_unite
ADD
    COLUMN envoi_web BOOLEAN;

COMMENT ON COLUMN participation_unite.saisie IS 'Indique si la saisie a été réalisée ou pas';
COMMENT ON COLUMN participation_unite.envoi_aff IS 'Indique si la participation a été envoyée à l''affichage';
COMMENT ON COLUMN participation_unite.envoi_web IS 'Indique si la participation a été envoyée à la page web';

--
-- Name: participation_election; Type: TABLE; Schema: openresultat; Owner: -
--
ALTER TABLE
    participation_election
DROP
    COLUMN envoi_aff;

ALTER TABLE
    participation_election
DROP
    COLUMN envoi_web;

--
-- Name: election; Type: TABLE; Schema: openresultat; Owner: -
--
ALTER TABLE
    election
ADD
    COLUMN delegation_saisie BOOLEAN;

ALTER TABLE
    election
ADD
    COLUMN delegation_participation BOOLEAN;

ALTER TABLE
    election
ADD
    COLUMN validation_avant_publication BOOLEAN;

COMMENT ON COLUMN election.delegation_saisie IS 'Option permettant d''activer la délégation de saisie';
COMMENT ON COLUMN election.delegation_participation IS 'Option permettant d''activer la délégation de saisie de la validation';
COMMENT ON COLUMN election.validation_avant_publication IS 'Option permettant de rendre la validation obligatoire avant de publier les résultats';

ALTER TABLE
    election_unite
ADD
    COLUMN date_derniere_modif float8;

ALTER TABLE
    participation_election
ADD
    COLUMN date_derniere_modif float8;

ALTER TABLE
    participation_unite
ADD
    COLUMN date_derniere_modif float8;


CREATE VIEW resultat_participation_unite AS
SELECT
    election_unite.election_unite,
    every(participation_unite.saisie) AS saisie,
    every(participation_unite.envoi_aff) AS envoi_aff,
    every(participation_unite.envoi_web) AS envoi_web,
    sum(participation_unite.votant) AS votant
FROM
    election_unite INNER JOIN participation_unite
        ON election_unite.election_unite = participation_unite.election_unite
GROUP BY
    election_unite.election_unite;


CREATE VIEW resultat_participation_tranche AS
SELECT
    participation_election.participation_election,
    every(participation_unite.saisie) AS saisie,
    every(participation_unite.envoi_aff) AS envoi_aff,
    every(participation_unite.envoi_web) AS envoi_web,
    sum(participation_unite.votant) AS votant
FROM
    participation_election INNER JOIN participation_unite
        ON participation_election.participation_election = participation_unite.participation_election
    INNER JOIN election_unite
        ON participation_unite.election_unite = election_unite.election_unite
    INNER JOIN unite
        ON election_unite.unite = unite.unite
    INNER JOIN type_unite
        ON unite.type_unite = type_unite.type_unite
WHERE
    type_unite.bureau_vote = true
GROUP BY
    participation_election.participation_election;
--
-- END /  [#9495] Délégation de saisie 
--

--
-- BEGIN /  [#9494] Ergonomie et amélioration de la v2 
--
-- Améliorer l'ergonomie de l'application et faire les corrections, améliorations
-- remontés par les utilisateurs
--

--
-- Name: parti_politique; Type: TABLE; Schema: openresultat; Owner: -
--
CREATE TABLE parti_politique (
    parti_politique INTEGER NOT NULL,
    libelle CHARACTER VARYING(50) NOT NULL,
    couleur CHARACTER VARYING(8)
);

ALTER TABLE
    parti_politique
ADD
    CONSTRAINT parti_politique_pkey PRIMARY KEY (parti_politique);

COMMENT ON COLUMN parti_politique.parti_politique IS 'Identifiant du parti';
COMMENT ON COLUMN parti_politique.libelle IS 'Nom du parti';
COMMENT ON COLUMN parti_politique.couleur IS 'Couleur associée au parti';

--
-- Name: parti_politique_seq; Type: SEQUENCE; Schema: openresultat; Owner: -
--

CREATE SEQUENCE parti_politique_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: election_candidat; Type: TABLE; Schema: openresultat; Owner: -
--
ALTER TABLE
    election_candidat
ADD
    COLUMN parti_politique INTEGER;

ALTER TABLE
    election_candidat
ADD
    CONSTRAINT election_candidat_parti_politique_fkey FOREIGN KEY (parti_politique) REFERENCES parti_politique;

COMMENT ON COLUMN election_candidat.parti_politique IS 'Parti du candidat';

--
-- Name: election; Type: TABLE; Schema: openresultat; Owner: -
--

ALTER TABLE election
    ALTER COLUMN libelle TYPE CHARACTER VARYING(100);

--
-- Name: animation; Type: TABLE; Schema: openresultat; Owner: -
--

ALTER TABLE animation
    ALTER COLUMN libelle TYPE CHARACTER VARYING(100),
    ALTER COLUMN titre TYPE CHARACTER VARYING(100);

ALTER TABLE
    animation
ADD
    COLUMN defaut BOOLEAN;

COMMENT ON COLUMN animation.defaut IS 'Defini si une animation doit être associée par défaut aux élections';

--
-- END /  [#9494] Ergonomie et amélioration de la v2 
--

--
-- BEGIN /  [#9463] Import des unités de saisie 
--
-- Mettre en place les imports des unités de saisie à partir d'un fichier csv issu
-- d'ELIRE.

-- Passage de la taille des champs permettant de récupérer les codes préfecture des
-- canton, circonscription, département et commune à 5 caractères

--
-- Name: unite; Type: TABLE; Schema: openresultat; Owner: -
--
ALTER TABLE
    unite
ALTER
    COLUMN canton_prefecture TYPE INTEGER USING canton_prefecture :: INTEGER;

ALTER TABLE
    unite
ALTER
    COLUMN circonscription_prefecture TYPE INTEGER USING circonscription_prefecture :: INTEGER;

--
-- Name: canton; Type: TABLE; Schema: openresultat; Owner: -
--
ALTER TABLE
    canton
ALTER
    COLUMN code SET DATA TYPE CHARACTER VARYING(10);

ALTER TABLE
    canton
ALTER
    COLUMN prefecture TYPE INTEGER USING prefecture :: INTEGER;

--
-- Name: circonscription; Type: TABLE; Schema: openresultat; Owner: -
--
ALTER TABLE
    circonscription
ALTER
    COLUMN code SET DATA TYPE CHARACTER VARYING(10);

ALTER TABLE
    circonscription
ALTER
    COLUMN prefecture TYPE INTEGER USING prefecture :: INTEGER;

--
-- Name: departement; Type: TABLE; Schema: openresultat; Owner: -
--
ALTER TABLE
    departement
ALTER
    COLUMN code SET DATA TYPE CHARACTER VARYING(10);

ALTER TABLE
    departement
ALTER
    COLUMN prefecture TYPE INTEGER USING prefecture :: INTEGER;

--
-- Name: commune; Type: TABLE; Schema: openresultat; Owner: -
--
ALTER TABLE
    commune
ALTER
    COLUMN code SET DATA TYPE CHARACTER VARYING(10);

ALTER TABLE
    commune
ALTER
    COLUMN prefecture TYPE INTEGER USING prefecture :: INTEGER;

--
-- END /  [#9463] Import des unités de saisie 
--

--
-- BEGIN /  [#9464] Personnalisation du portail web
--
-- Rendre le portail web personnalisable
--

--
-- Name: election; Type: TABLE; Schema: openresultat; Owner: -
--
ALTER TABLE
    election
DROP
    COLUMN web;

--
-- Name: web; Type: TABLE; Schema: openresultat; Owner: -
--

ALTER TABLE
    web
ADD
    COLUMN jscript_stats TEXT;

ALTER TABLE
    web
ADD
    COLUMN display_simulation BOOLEAN NOT NULL DEFAULT false;

ALTER TABLE
    web
ADD
    COLUMN actif BOOLEAN NOT NULL DEFAULT false;

COMMENT ON COLUMN web.display_simulation IS 'Option permettant de définir si les centaines doivent être affichée ou pas dans le listing du portail web';
COMMENT ON COLUMN web.jscript_stats IS 'JavaScript à ajouter à l''affichage web';
COMMENT ON COLUMN web.actif IS 'Indique quel modèle web est utilisé';
--
-- END /  [#9464] Personnalisation du portail web
--

--
-- BEGIN /  [#9402] Gestion et personnalisation des plans
--

--
-- Name: plan; Type: TABLE; Schema: openresultat; Owner: -
--
ALTER TABLE
    plan
ADD
    COLUMN largeur_icone INTEGER NOT NULL;

ALTER TABLE
    plan
ADD
    COLUMN hauteur_icone INTEGER NOT NULL;

COMMENT ON COLUMN plan.hauteur_icone IS 'Hauteur de l''icone sur le plan';
--
-- Name: plan_unite; Type: TABLE; Schema: openresultat; Owner: -
--

ALTER TABLE
    plan_unite
ADD
    COLUMN largeur_icone INTEGER;

ALTER TABLE
    plan_unite
ADD
    COLUMN hauteur_icone INTEGER;

COMMENT ON COLUMN plan_unite.largeur_icone IS 'Largeur de l''icone sur le plan';
COMMENT ON COLUMN plan_unite.hauteur_icone IS 'Hauteur de l''icone sur le plan';
--
-- END /  [#9402] Gestion et personnalisation des plans
--

--
-- BEGIN /  [#9433] Personnalisation de l'animation
--
-- Permettre la personnalisation des animations. Ajouter une table
-- dédiée à la configuration des animations
--

--
-- Name: candidat; Type: TABLE; Schema: openresultat; Owner: -
--
ALTER TABLE candidat
    RENAME COLUMN libelle_long TO libelle_liste;

ALTER TABLE candidat
    ALTER COLUMN libelle_liste TYPE CHARACTER VARYING(100),
    ALTER COLUMN libelle TYPE CHARACTER VARYING(100);

-- commentaires
COMMENT ON COLUMN candidat.libelle_liste IS 'Nom de la liste auquelle appartient le candidat';

--
-- Name: animation; Type: TABLE; Schema: openresultat; Owner: -
--
ALTER TABLE animation
    ADD COLUMN feuille_style text,
    ADD COLUMN script text;

ALTER TABLE animation
    DROP COLUMN election_comparaison,
    DROP COLUMN modele,
    DROP COLUMN defaut;

-- commentaires
COMMENT ON COLUMN animation.feuille_style IS 'Feuille de style de l''animation';
COMMENT ON COLUMN animation.script IS 'Script a ajouter à l''animation';

--
-- Name: unite; Type: TABLE; Schema: openresultat; Owner: -
--

ALTER TABLE unite
    ALTER COLUMN libelle TYPE CHARACTER VARYING(100);

--
-- END /  [#9433] Personnalisation de l'animation
--

--
-- BEGIN /  [#9494] Ergonomie et amélioration de la v2 
--
-- Améliorer l'ergonomie de l'application et faire les corrections, améliorations
-- remontés par les utilisateurs
--

ALTER TABLE unite
    ALTER COLUMN ordre TYPE CHARACTER VARYING(10) USING ordre :: CHARACTER VARYING(10),
    ALTER COLUMN code_bureau_vote TYPE CHARACTER VARYING(10) USING code_bureau_vote :: CHARACTER VARYING(10);

--
-- END /  [#9494] Ergonomie et amélioration de la v2 
--

--
-- BEGIN /  [#9463] Import des unités de saisie 
--
-- Mettre en place les imports des unités de saisie à partir d'un fichier csv issu
-- d'ELIRE ou d'openElec.

-- Passage de la taille des champs permettant de récupérer les codes préfecture des
-- canton, circonscription, département et commune à 5 caractères

--
-- Name: unite; Type: TABLE; Schema: openresultat; Owner: -
--
--
-- Name: canton; Type: TABLE; Schema: openresultat; Owner: -
--
ALTER TABLE
    canton
ALTER
    COLUMN prefecture SET DATA TYPE CHARACTER VARYING(10);

--
-- Name: circonscription; Type: TABLE; Schema: openresultat; Owner: -
--
ALTER TABLE
    circonscription
ALTER
    COLUMN prefecture SET DATA TYPE CHARACTER VARYING(10);

--
-- Name: departement; Type: TABLE; Schema: openresultat; Owner: -
--
ALTER TABLE
    departement
ALTER
    COLUMN prefecture SET DATA TYPE CHARACTER VARYING(10);

--
-- Name: commune; Type: TABLE; Schema: openresultat; Owner: -
--
ALTER TABLE
    commune
ALTER
    COLUMN prefecture SET DATA TYPE CHARACTER VARYING(10);

--
-- Name: unite; Type: TABLE; Schema: openresultat; Owner: -
--
ALTER TABLE unite
    ALTER COLUMN circonscription_prefecture TYPE CHARACTER VARYING(10) USING ordre :: CHARACTER VARYING(10),
    ALTER COLUMN canton_prefecture TYPE CHARACTER VARYING(10) USING code_bureau_vote :: CHARACTER VARYING(10);

ALTER TABLE unite
    ADD COLUMN id_reu INTEGER;
-- commentaires
COMMENT ON COLUMN unite.id_reu IS 'Identifiant du bureau sur le REU';


--
-- END /  [#9463] Import des unités de saisie 
--

--
-- BEGIN /  [#9402] Gestion et personnalisation des plans
--

--
-- Name: plan; Type: TABLE; Schema: openresultat; Owner: -
--
ALTER TABLE
    plan
DROP
    COLUMN hauteur_icone;

ALTER TABLE
    plan
ALTER COLUMN
    largeur_icone
DROP NOT NULL;
--
-- Name: plan_unite; Type: TABLE; Schema: openresultat; Owner: -
--
ALTER TABLE
    plan_unite
DROP COLUMN hauteur_icone,
DROP COLUMN texte_image,
DROP COLUMN couleur_texte;
--
-- END /  [#9402] Gestion et personnalisation des plans
--

--
-- BEGIN /  [#9494] Ergonomie et amélioration de la v2 
--
-- Améliorer l'ergonomie de l'application et faire les corrections, améliorations
-- remontés par les utilisateurs
--

ALTER TABLE election
    ADD COLUMN sieges INTEGER,
    ADD COLUMN sieges_com INTEGER;

ALTER TABLE unite
    RENAME COLUMN code_bureau_vote TO code_unite;

ALTER TABLE unite
    DROP COLUMN canton_prefecture,
    DROP COLUMN circonscription_prefecture;

--
-- END /  [#9494] Ergonomie et amélioration de la v2 
--

--
-- BEGIN /  [#9503] Gestion des élections métropolitaine 
--
-- Améliorer l'ergonomie de l'application et faire les corrections, améliorations
-- remontés par les utilisateurs
--

ALTER TABLE election
    ADD COLUMN sieges_mep INTEGER NOT NULL DEFAULT 0;

ALTER TABLE election_candidat
    ADD COLUMN age_moyen_mep INTEGER NOT NULL DEFAULT 0,
    ADD COLUMN siege_mep INTEGER NOT NULL DEFAULT 0;

--
-- END /  [#9503] Gestion des élections métropolitaine 
--